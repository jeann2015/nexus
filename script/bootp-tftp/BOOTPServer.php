<?php

include("Logger.php");
include("Util.php");

include("BOOTPPackets/UDP.php");

class BOOTPServer {

    public $socket;
    public $packet; //binary 
    public $listenPort;
    public $listenIP;
    public $remotePort;
    public $remoteIP;
    public $serverIP;
    public $broadcastIP;
    public $xpacket; //hexadecimal 

    public $locale      = "es";
    //public $interface   = "eth0";
    public $interface   = "wlan0";

    public $iniFile     = "/home/rodolfo/Escritorio/EjemploMP1XX.ini";
    public $cmpFile     = "/home/rodolfo/MP118_SIP_F6_60A_274.cmp -fb";

    public $availableIPs = array("192.168.10.131");

    public function __construct($options = null){
        
        if(!is_null($options)){
            foreach ($options as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    private function initListening()
    {
        $netInfo = Util::getNetInfo();

        //$this->availableIPs = $netInfo['ips'];
        $this->serverIP     = $netInfo['ip'];
        $this->broadcastIP  = $netInfo['bcast'];

        $this->listenIP     = '0.0.0.0';
        $this->listenPort   = 67;

        Logger::info("ServerIP: " . $this->serverIP);
        Logger::info("BroadcastIP: " . $this->broadcastIP);

    }

    private function initSocketing()
    {
        /** Creamos un socket UDP **/ 
        if(!($this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP )))
        {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            Logger::err("Could not create the socket : [".$errorcode."] " . $errormsg); 
            die();
        }
        
        Logger::info("Socket created");
         
        // Bind the source address
        Logger::info("Bind " . $this->listenIP . ":" . $this->listenPort);
        if( !socket_bind($this->socket, $this->listenIP , $this->listenPort) )
        {
            socket_close($this->socket);
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            Logger::err("Could not bind socket : [".$errorcode."] " . $errormsg); 
            die();
        }
         
        Logger::info("Socket bind OK");
        Logger::info("Now listening on " . $this->listenIP . " port " . $this->listenPort . " ... ");
        //socket_close($this->socket);
    }
	
    public function startListening()
    {
        Logger::info("=================================================================");
        Logger::info("============= STARTING NEXUS BOOTP SERVER v1.0 ==================");
        Logger::info("=================================================================");

        $this->initListening();
        $this->initSocketing();

        //Do some communication, this loop can handle multiple clients
        while(1)
        {        
            //Receive some data
            socket_recvfrom($this->socket, $this->packet, 512, 0, $this->remoteIP, $this->remotePort);

            Logger::info("Remote client " . $this->remoteIP .":". $this->remotePort . " sent some data");

            $this->xpacket = bin2hex($this->packet);
            Logger::debug("Remote client sent this hexadecimal data: " . $this->xpacket);

            $udp        = new UDP($this->packet);
            $udp->Op    = "02"; // Bootp Reply hex
            $udp->YIAddr= Util::ip2hex($this->availableIPs[0]);
            $udp->SIAddr= Util::ip2hex($this->serverIP);

            Logger::info("Remote client Mac Address Hex: " . $udp->CHAddr);
            Logger::info("Remote client Mac Address String: " . Util::hex2mac($udp->CHAddr));

            // Validamos si el mac address pertenece a un audio code
            if(!$this->isAudioCode(Util::hex2mac($udp->CHAddr))){
                Logger::warn("Client request BOOTP is not  an AudioCode ");
                continue;
            }

            Logger::info("Giving IP Address to client: " . $udp->YIAddr);

            $fileHex    = Util::str2hex($this->iniFile);
            $udp->File  = substr_replace($udp->File,$fileHex,0,strlen($fileHex));

            $this->sendBOOTPPacket($udp);

        }
    }

    public function sendBOOTPPacket($udp)
    {
 
        $values         = (array)$udp;
        $this->xpacket  = implode($values);

        $this->packet   = pack('H*',$this->xpacket);
        //Send back the data to the client
        Logger::info("Sending BOOTP Data to " . $this->remoteIP .":". $this->remotePort);
        socket_set_option($this->socket, SOL_SOCKET, SO_BROADCAST, 1);

        $bytesSent = socket_sendto($this->socket, $this->packet , strlen($this->packet) , 0 , $this->broadcastIP , $this->remotePort);
        Logger::info("Sent " . $bytesSent . " bytes.");
    }

    public function isAudioCode($mac){
        // https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob_plain;f=manuf
        // 00:05:E7    NetrakeA               # Netrake an AudioCodes Company
        // 00:17:19    Audiocod               # AudioCodes USA, Inc
        // 00:90:8F    AudioCod               # AUDIO CODES LTD.

        $mac = strtoupper($mac);
        $audioCodes = array("00:05:E7","00:17:19","00:90:8F");
        $macParts = explode(":", $mac);
        $manufacter = $macParts[0] . ":" . $macParts[1] . ":" . $macParts[2];

        return in_array($manufacter, $audioCodes);

    }
}