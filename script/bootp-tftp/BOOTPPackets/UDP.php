<?php

class UDP {

    public $Op;
    public $HType;
    public $HLen;
    public $Hops;
    public $XID;
    public $Secs;
    public $Flags;
    public $CIAddr;
    public $YIAddr;
    public $SIAddr;
    public $GIAddr;
    public $CHAddr;
    public $SName;
    public $File;
    public $Vend;

    public function __construct($packet = null){

        $fields = array();
        $fields['Op']       = array(1, 'H*');
        $fields['HType']    = array(1, 'H*');
        $fields['HLen']     = array(1, 'H*');
        $fields['Hops']     = array(1, 'H*');
        $fields['XID']      = array(4, 'H*');
        $fields['Secs']     = array(2, 'H*');
        $fields['Flags']    = array(2, 'H*');
        $fields['CIAddr']   = array(4, 'H*');
        $fields['YIAddr']   = array(4, 'H*');
        $fields['SIAddr']   = array(4, 'H*');
        $fields['GIAddr']   = array(4, 'H*');
        $fields['CHAddr']   = array(16, 'H*');
        $fields['SName']    = array(64, 'H*');
        $fields['File']     = array(128, 'H*');
        $fields['Vend']     = array(64, 'H*');

        if(!is_null($packet)){
            $values = array();
            $start = 0;
            foreach($fields as $type => $setting) {
                list($len, $format) = $setting;
                $unpacked = unpack($format, substr($packet, $start, $len));
                $this->$type = $unpacked[1];
                $start += $len;
            }
        }
    }
}