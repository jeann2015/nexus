<?php

class Util {

	static public function ip2hex($ip)
	{
	    $ipArray = explode('.', $ip);
	    $ipHex = "";
	    foreach ($ipArray as $dec) {
	        $ipHex .= self::zeropad(dechex($dec));
	    }
	    return $ipHex;
	}

	static public function zeropad($num, $lim = 2)
	{
	   return (strlen($num) >= $lim) ? $num : self::zeropad("0" . $num);
	}

	static public function str2hex($string)
	{
	    $hex = '';
	    for ($i=0; $i<strlen($string); $i++){
	        $ord = ord($string[$i]);
	        $hexCode = dechex($ord);
	        $hex .= substr('0'.$hexCode, -2);
	    }
	    return strToUpper($hex);
	}

	static public function hex2str($hex)
	{
	    $string='';
	    for ($i=0; $i < strlen($hex)-1; $i+=2){
	        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
	    }
	    return $string;
	}

	static public function hex2mac($hex)
	{
		$string = "";
		$part = 1;
	    for($i = 0; $i < 12; $i = $i + 2){

	    	$string .= substr($hex, $i, 2); 
	    	if($part != 6 ){
	    		$string .= ":";
	    	}

	    	$part++;
	    }
	    
	    return $string;
	}

	static public function cidr2broadcast($network, $cidr)
	{
	    $broadcast = long2ip(ip2long($network) + pow(2, (32 - $cidr)) - 1);

		return $broadcast;
	}

	static public function getNetInfo($options = null)
	{
		$locale 		= substr(locale_get_default(),0,2);
		$interface 		= "eth0";
		$os 			= PHP_OS;

		$inet = array(
            'en'            =>  "inet addr:",
            'es'            =>  "Direc. inet:",
        );

        $bcast = array(
            'en'            =>  "Bcast:",
            'es'            =>  "Difus.:",
        );

		if(is_array($options)){
			$locale 	= array_key_exists('locale', $options) 		? $options['locale'] 	: $locale;
			$interface 	= array_key_exists('interface', $options) 	? $options['interface'] : $interface;
			$os 		= array_key_exists('os', $options) 			? $options['os'] 		: $os; 
		}

        $cmdIP              = "ifconfig ".$interface." | grep '".$inet[$locale]."' | cut -d: -f2 | awk '{ print $1}'";
        $cmdMask	        = "ifconfig ".$interface." | grep '".$inet[$locale]."' | cut -d: -f4 | awk '{ print $1}'";
		   
		$ip_addr 	= exec ($cmdIP);
		$subnet_mask = exec ($cmdMask); 

		$ip = ip2long($ip_addr); 
		$nm = ip2long($subnet_mask); 
		$nw = ($ip & $nm); 
		$bc = $nw | (~$nm); 

		$availablesIPs = array();
		$start = ip2long(long2ip($nw + 1));
		$end = ip2long(long2ip($bc - 1));
		$range = range($start, $end);
		$availablesIPs = array_map('long2ip', $range);

		return array(
			"ip"    		=> long2ip($ip), //IP Address
			"mask"			=> long2ip($nm), //Subnet Mask
			"net"			=> long2ip($nw), //Network Address
			"bcast" 		=> long2ip($bc), //Broadcast Address
			"hosts" 		=> ($bc - $nw - 1), //Number of Hosts
			"range" 		=> long2ip($nw + 1) . " -> " . long2ip($bc - 1), //Host Range
			"range-start" 	=> long2ip($nw + 1), // Range starts at
			"range-end" 	=> long2ip($bc - 1), // Range ends at
			"ips"			=> $availablesIPs, //Available IP count
		);
	}
}