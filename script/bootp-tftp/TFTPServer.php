<?php

include("Logger.php");
include("Util.php");

include("TFTPPackets/RRQ.php");
include("TFTPPackets/WRQ.php");
include("TFTPPackets/DATA.php");
include("TFTPPackets/ACK.php");
include("TFTPPackets/ERROR.php");

class TFTPServer {

    public $socket;
    public $packet; //binary 
    public $listenPort;
    public $listenIP;
    public $remotePort;
    public $remoteIP;
    public $xpacket; //hexadecimal 

    public $rrq;
    public $wrq;
    public $ack;
    public $error;
    public $data;

    public function __construct(){
        
    }

    private function initListening()
    {
        /** es || en */
        $locale             = "es";
        $interface          = "eth0";
        //$interface          = "wlan0";

        $inet = array(
            'en'            =>  "inet addr:",
            'es'            =>  "Direc. inet:",
        );

        $bcast = array(
            'en'            =>  "Bcast:",
            'es'            =>  "Difus.:",
        );

        $cmdIP              = "ifconfig ".$interface." | grep '".$inet[$locale]."' | cut -d: -f2 | awk '{ print $1}'";
        $cmdBroadcast       = "ifconfig ".$interface." | grep '".$inet[$locale]."' | cut -d: -f3 | awk '{ print $1}'";

        $serverIP           = exec ($cmdIP);
        $broadcastIP        = exec ($cmdBroadcast);
         
        $this->listenIP     = $serverIP;
        $this->listenPort   = 69;
    }

    private function initSocketing()
    {
        /** Creamos un socket UDP **/ 
        if(!($this->socket = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP )))
        {
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            Logger::err("Could not create the socket : [".$errorcode."] " . $errormsg); 
            die();
        }
        
        Logger::info("Socket created");
         
        // Bind the source address
        if( !socket_bind($this->socket, $this->listenIP , $this->listenPort) )
        {
            socket_close($this->socket);
            $errorcode = socket_last_error();
            $errormsg = socket_strerror($errorcode);
            Logger::err("Could not bind socket : [".$errorcode."] " . $errormsg); 
            die();
        }
         
        Logger::info("Socket bind OK");
        Logger::info("Now listening on " . $this->listenIP . " port " . $this->listenPort . " ... ");
        //socket_close($this->socket);
    }

    private function getTFTPPacket()
    {
        /*
        TFTP is define in RFC 1350
        
              opcode  operation
                1     Read request (RRQ)
                2     Write request (WRQ)
                3     Data (DATA)
                4     Acknowledgment (ACK)
                5     Error (ERROR)    
        */

        $unpacked          = unpack("H*", substr($this->packet, 0, 2)); // desde el byte 0 al 2 obtenemos el opcode
        $opcodeHex         = $unpacked[1];
        $opcodeDec         = hexdec($opcodeHex);

        // Dependiendo de los primeros 2 bytes del paquete binario sabemos que request es.
        // Returnamos el request.
        switch ($opcodeDec) {
            case 1:
                $this->rrq = new RRQ($this->packet);
                return $this->rrq;
                break;
            case 2:
                $this->wrq = new WRQ($this->packet);
                return $this->wrq;
                break;
            case 3:
                $this->data = new DATA($this->packet);
                return $this->data;
                break;
            case 4:
                $this->ack = new ACK($this->packet);
                return $this->ack;
                break;
            case 5:
                $this->error = new ERROR($this->packet);
                return $this->error;
                break;
            default:
                return null;
                break;
        }

    }
	
    public function startListening()
    {
        Logger::info("=================================================================");
        Logger::info("============== STARTING NEXUS TFTP SERVER v1.0 ==================");
        Logger::info("=================================================================");

        $this->initListening();
        $this->initSocketing();

        //Do some communication, this loop can handle multiple clients
        while(1)
        {        
            //Receive some data
            socket_recvfrom($this->socket, $this->packet, 512, 0, $this->remoteIP, $this->remotePort);

            Logger::info("Remote client " . $this->remoteIP .":". $this->remotePort . " sent some data");

            $this->xpacket = bin2hex($this->packet);
            Logger::debug("Remote client sent this hexadecimal data: " . $this->xpacket);

            // Como no sabemos qué paquete llegó, obtenemos el request adecuado
            Logger::info("Finding TFTP Packet");
            $TFTPPacket = $this->getTFTPPacket();
            Logger::info(get_class($TFTPPacket) . " found!");

            // Si es un ack
            if(isset($this->ack) && isset($this->rrq)){

                if($TFTPPacket->opcode == 4){
                    $this->rrq->blockData = (int)$TFTPPacket->block + 1;
                    Logger::info("ACK Found. Increasing block to " . $this->rrq->blockData);
                    $this->packet = $this->rrq->process();
                } else {
                    $this->packet = $TFTPPacket->process();
                }
                
                $this->sendTFTPPacket();
            }  else {
                Logger::info("Proessing TFTP Packet " . get_class($TFTPPacket));
                $this->packet = $TFTPPacket->process();
                $this->sendTFTPPacket();
            }
        }
    }

    public function sendTFTPPacket(){

        // length off pocket
        $len = strlen($this->packet);
        Logger::info("Sending TFTP Data to " . $this->remoteIP .":". $this->remotePort);
        $bytesSent = socket_sendto($this->socket, $this->packet, $len, 0, $this->remoteIP, $this->remotePort);

        if(isset($this->rrq)){
            Logger::info("Sent: " . $bytesSent . " bytes of " . $this->rrq->fileSize);
        }

        if($bytesSent != 516){
            unset($this->packet); 
            unset($this->xpacket); //hexadecimal 

            unset($this->rrq);
            unset($this->wrq);
            unset($this->ack);
            unset($this->error);
            unset($this->data);

            //socket_close($this->socket);
        }
    }
}