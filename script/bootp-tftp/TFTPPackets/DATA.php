<?php

    /*
    	TFTP is define in RFC 1350
    	================================
        opcode    	operation
        ======		====================
            1     	Read request (RRQ)
            2     	Write request (WRQ)
            3     	Data (DATA)
            4     	Acknowledgment (ACK)
            5     	Error (ERROR)    
    */

class DATA {
	
    public $opcode = 3;
    public $block;
    public $data;

    public $packet;

    public function __construct(){

    }

    public function generatePacket(){

        // prepare packet for send data from file 
        //5 =  ERROR, 1 = Not defined, string mensaje, 1 bytes of zero
        $this->packet = pack('n',$this->opcode) . pack('n',$this->block) . $this->data;
        return $this->packet;
    }
}