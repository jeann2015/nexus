<?php

    /*
    	TFTP is define in RFC 1350
    	================================
        opcode    	operation
        ======		====================
            1     	Read request (RRQ)
            2     	Write request (WRQ)
            3     	Data (DATA)
            4     	Acknowledgment (ACK)
            5     	Error (ERROR)    
    */

class ACK {
	
    public $opcode;
    public $block;

    public $packet;

    public function __construct($packet){

        $this->packet      = $packet;

        $unpacked          = unpack("H*", substr($packet, 0, 2)); // desde el byte 0 al 2 obtenemos el opcode
        $opcodeHex         = $unpacked[1];
        $this->opcode      = hexdec($opcodeHex);

        $unpacked          = unpack("H*", substr($packet, 2, 2)); // desde el byte 2 al 2 obtenemos el block
        $blockHex          = $unpacked[1];
        $this->block       = hexdec($blockHex);
    }

    public function process()
    {
        return $this->packet;
    }
}