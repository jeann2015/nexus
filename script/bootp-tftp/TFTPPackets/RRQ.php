<?php

    /*
        TFTP is define in RFC 1350
        ================================
        opcode      operation
        ======      ====================
            1       Read request (RRQ)
            2       Write request (WRQ)
            3       Data (DATA)
            4       Acknowledgment (ACK)
            5       Error (ERROR)    
    */

class RRQ {

    public $opcode;
    public $filename;
    public $mode;

    public $blockData      = 1;
    public $maxDataLength  = 512;
    public $offsetData     = 0;
    public $fileSize;
    
    public function __construct($packet){

        $hex = bin2hex($packet);
        // For Request TFTP
        $opHexIndex        = 0;
        $opHexLen          = 4;
        $opBinIndex        = $opHexIndex / 2;
        $opBinLen          = 2;

        $fileHexIndex      = 4;
        $fileHexLen        = strpos($hex,'00',$fileHexIndex) - $fileHexIndex;
        $fileHexLen        = $fileHexLen % 2 != 0 ? $fileHexLen + 1 : $fileHexLen;
        $fileBinIndex      = $fileHexIndex / 2;
        $fileBinLen        = $fileHexLen / 2;

        $modeHexIndex      = $fileHexIndex + $fileHexLen + 2;
        $modeHexLen        = strpos($hex,'00',$modeHexIndex) - $modeHexIndex;
        $modeHexLen        = $modeHexLen % 2 != 0 ? $modeHexLen + 1 : $modeHexLen;
        $modeBinIndex      = $modeHexIndex / 2;
        $modeBinLen        = $modeHexLen / 2;

        // RFC1350
        $fields = array();
        $fields['opcode']       = array($opBinIndex,$opBinLen, 'H*');
        $fields['filename']     = array($fileBinIndex,$fileBinLen,'H*');
        $fields['mode']         = array($modeBinIndex,$modeBinLen, 'H*');

        $values = array();
        foreach($fields as $type=>$setting) {
            list($start, $len, $format) = $setting;
            $unpacked = unpack($format, substr($packet, $start, $len));
            $values[$type] = $unpacked[1];
        }

        $this->opcode   = Util::hex2str($values['opcode']);
        $this->filename = Util::hex2str($values['filename']);
        $this->mode     = Util::hex2str($values['mode']);
    }

    public function process()
    {
        try {

            if (!isset($this->to_transmit))
            {
                $filename           = $this->filename;
                if (file_exists($filename))
                { 
                    Logger::info("Fetching file " . $filename);
                    $fh                 = fopen($filename, 'r');
                    $this->to_transmit  = fread($fh, filesize($filename));
                    $this->fileSize = filesize($filename);
                    fclose($fh);

                } else {

                    $error = new ERROR();
                    $error->errormessage = $filename . " File does not exist!";
                    $error->errorcode    = 1;
                    $error->opcode       = 5;

                    $packet = $error->generatePacket();

                    Logger::err( $error->errormessage );
                    return $packet;
                }
            }
                 
            $data = new DATA();
            $data->block = $this->blockData;
            $data->data = substr($this->to_transmit,$this->offsetData,$this->maxDataLength);

            $packet = $data->generatePacket();

            $this->offsetData = $this->offsetData + $this->maxDataLength;
            return $packet;

        } catch(Exception $e){

            $error = new ERROR();
            $error->errormessage = $e->getMessage();
            $error->errorcode    = 0;

            $packet = $error->generatePacket();
            Logger::err( $error->errormessage );
            return $packet;
        }
    }
}