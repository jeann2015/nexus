<?php

    /*
    	TFTP is define in RFC 1350
    	================================
        opcode    	operation
        ======		====================
            1     	Read request (RRQ)
            2     	Write request (WRQ)
            3     	Data (DATA)
            4     	Acknowledgment (ACK)
            5     	Error (ERROR)    
    */

    /*
       Error Codes
       ==================================================
       Value     Meaning
       =====     ========================================
       0         Not defined, see error message (if any).
       1         File not found.
       2         Access violation.
       3         Disk full or allocation exceeded.
       4         Illegal TFTP operation.
       5         Unknown transfer ID.
       6         File already exists.
       7         No such user.
    */

class ERROR {
	
    public $opcode = 5;
    public $errorcode;
    public $errormessage;
    public $packet;

    public function __construct(){

    }

    public function generatePacket(){

        // prepare packet for send data from file 
        //5 =  ERROR, 1 = Not defined, string mensaje, 1 bytes of zero
        $this->packet = pack('n',$this->opcode) . pack('n',$this->errorcode) . pack('H*',Util::str2hex($this->errormessage)) . pack('n',0);
        return $this->packet;
    }
}