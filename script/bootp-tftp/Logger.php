<?php

/**
* 
*/
class Logger
{

	static public $isInfo  = true;
	static public $isWarn  = true;
	static public $isErr   = true;
	static public $isDebug = false;

	static public function info($m)
	{
		if(self::$isInfo){
			$date = date("Y-m-d H:i:s");
			print $date . " [info] " . $m . "\n";
		}
	}

	static public function warn($m)
	{
		if(self::$isWarn){
			$date = date("Y-m-d H:i:s");
			print $date . " [warn] " . $m . "\n";
		}
	}

	static public function err($m)
	{
		if(self::$isErr){
			$date = date("Y-m-d H:i:s");
			print $date . " [err] " . $m . "\n";
		}
	}

	static public function debug($m)
	{
		if(self::$isDebug){
			$date = date("Y-m-d H:i:s");
			print $date . " [debug] " . $m . "\n";
		}
	}
}