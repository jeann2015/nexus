<?php 
ini_set('display_errors', 1);
date_default_timezone_set('UTC');

//make sure we don't use the already registered framework
set_include_path(implode(PATH_SEPARATOR, array(
    __DIR__ . '/library',
    '.:/usr/local/zend/share/pear',
)));

$zf2Path = __DIR__ . '/../vendor/ZendFramework-2.2.7/library';

include $zf2Path . '/Zend/Loader/AutoloaderFactory.php';
Zend\Loader\AutoloaderFactory::factory(array(
    'Zend\Loader\StandardAutoloader' => array(
        'autoregister_zf' => true,
        'prefixes' => array(
            'Zend_' => __DIR__ . '/library/Zend',
        ),
        'namespaces' => array(
            'Provisioning'  => __DIR__ . '/../module/Provisioning/src/Provisioning',
            'Preference'    => __DIR__ . '/../module/Preference/src/Preference',
            'Generic'       => __DIR__ . '/../module/Generic/src/Generic',
        ),
        'Zend\Loader\ClassMapAutoloader' => array(
        ),
    )
));

use Provisioning\Model\Message;
use Provisioning\Model\Queue;
use Provisioning\Model\Xml;
use Provisioning\Model\Order;
use Preference\Model\Preference;

use Provisioning\Dao\MessageDao;
use Provisioning\Dao\QueueDao;
use Provisioning\Dao\XmlDao;
use Provisioning\Dao\OrderDao;
use Preference\Dao\PreferenceDao;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Adapter\Adapter as dbAdapter;
use Zend\Log\Writer\Stream as Writter;
use Zend\Log\Logger;

$global = include(__DIR__ . '/../config/autoload/global.php');
$local = include(__DIR__ . '/../config/autoload/local.php');

$config = array_merge_recursive($global, $local);

$options = array(
    'username' => $config['db']['username'],
    'password' => $config['db']['password'],
    'type' => 'pdo_mysql',
    'port' => 3306,
);

list($protocol, $dbInfo) = explode(':', $config['db']['dsn']);

foreach (explode(';', $dbInfo) as $params) {
    list($paramName, $paramValue) = explode('=', $params);
    if ($paramName == 'dbname') {
        $options['dbname'] = $paramValue;
    } elseif ($paramName == 'host') {
        $options['host'] = $paramValue;
    }
}

$dbAdapter = new dbAdapter($config['db']);


$messageResultSetPrototype = new ResultSet();
$messageResultSetPrototype->setArrayObjectPrototype(new Message());
$messageTableGateway = new TableGateway('message', $dbAdapter, null, $messageResultSetPrototype);
$messageDao = new MessageDao($messageTableGateway);

$queueResultSetPrototype = new ResultSet();
$queueResultSetPrototype->setArrayObjectPrototype(new Queue());
$queueTableGateway = new TableGateway('queue', $dbAdapter, null, $queueResultSetPrototype);
$queueDao = new QueueDao($queueTableGateway);

$xmlResultSetPrototype = new ResultSet();
$xmlResultSetPrototype->setArrayObjectPrototype(new Xml());
$xmlTableGateway = new TableGateway('xml', $dbAdapter, null, $xmlResultSetPrototype);
$xmlDao = new XmlDao($xmlTableGateway);

$orderResultSetPrototype = new ResultSet();
$orderResultSetPrototype->setArrayObjectPrototype(new Order());
$orderTableGateway = new TableGateway('order', $dbAdapter, null, $orderResultSetPrototype);
$orderDao = new OrderDao($orderTableGateway);

$preferenceResultSetPrototype = new ResultSet();
$preferenceResultSetPrototype->setArrayObjectPrototype(new Preference());
$preferenceTableGateway = new TableGateway('preference', $dbAdapter, null, $preferenceResultSetPrototype);
$preferenceDao = new PreferenceDao($preferenceTableGateway);


    
    // Create a database queue.
$queue = new Zend_Queue('Db', array(
    'driverOptions' => $options,
    'name' => 'to-broadsoft',
));

if ($queue->count() > 0) {

    $userPreference  = $preferenceDao->getByName("provisioning_cwp_user");
    $user = $userPreference->value;

    $passPreference  = $preferenceDao->getByName("provisioning_cwp_password");
    $pass = $passPreference->value;

    $broadsoftPreference  = $preferenceDao->getByName("broadsoft_cwp");
    $broadsoft = $broadsoftPreference->value;

    $messages = $queue->receive(100);
    foreach ($messages as $message) {
        $xml = unserialize(base64_decode($message->body));
        $requestFile    = " ../../tmp/" . $xml['file'];
        $responseFile   = " ../../tmp/" . $xml['file'] . ".response.xml";

        $console = "cd ocisoapclient; sh startociclient.sh " .
            $user . " " .
            $pass . " " .
            "-i " . $requestFile . " ".
            "-u " . "http://" . $broadsoft . "/webservice/services/ProvisioningService;";
        
        //print $console . "\n";
        print "Enviando " . $xml['name'] . " . . .\n";
        $output = shell_exec($console);
        //print_r($output);


        $responseContent = file_get_contents($responseFile);
        $responseXML = @new SimpleXMLElement($responseContent);
        $result = (string)$responseXML->command['type'];
        print $result . "\n";
        if(strtolower($result) == "error"){
            $summary = (string)$responseXML->command->summary;
            print $summary . "\n\n";
        }
        print "\n";

        $queue->deleteMessage($message);
        unlink($requestFile);
        unlink($responseFile);

        $xml['status'] = $result;

        $xmlObject = new Xml();
        $xmlObject->exchangeArray($xml);
        $xmlDao->save($xmlObject);

        $orderId = $xml['orderId'];
        $next    = (int)$xml['priority']+1;

        $xmlNext = $xmlDao->getNext($orderId,$next);

        if($xmlNext) {
            $queue              = $queueDao->getQueueByName("to-broadsoft");
            $message            = new Message();
            $message->queue_id  = $queue->queue_id;
            $message->xmlId     = $xmlNext->id;

            $message->body      = base64_encode(serialize((array)$xmlNext));
            $message->md5       = md5(base64_encode(serialize( (array)$xmlNext )));
            $message->created   = time();
            $messageDao->saveMessage($message);
        } else {
            $order              = $orderDao->get($orderId);
            $order->setStatusId(2); //Aprovisionada
            $orderDao->save($order);
        }
    }
}

