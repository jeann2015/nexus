<?php 
    ini_set('display_errors', 1);
    header('Content-type: text/plain; charset=utf-8');
    date_default_timezone_set('UTC');

    //make sure we don't use the already registered framework
    set_include_path(implode(PATH_SEPARATOR, array(
        __DIR__ . '/library',
        '.:/usr/local/zend/share/pear',
    )));

    $zf2Path = __DIR__ . '/../../vendor/ZendFramework-2.2.7/library';

    include $zf2Path . '/Zend/Loader/AutoloaderFactory.php';
    Zend\Loader\AutoloaderFactory::factory(array(
        'Zend\Loader\StandardAutoloader' => array(
            'autoregister_zf' => true,
            'prefixes' => array(
                'Zend_' => __DIR__ . '/library/Zend',
            ),
            'namespaces' => array(
                'Provisioning'  => __DIR__ . '/../../module/Provisioning/src/Provisioning',
                'Preference'    => __DIR__ . '/../../module/Preference/src/Preference',
                'Generic'       => __DIR__ . '/../../module/Generic/src/Generic',
            ),
            'Zend\Loader\ClassMapAutoloader' => array(
            ),
        )
    ));

    use Provisioning\Model\Xml;
    use Provisioning\Model\Order;
    use Preference\Model\Preference;

    use Provisioning\Dao\XmlDao;
    use Provisioning\Dao\OrderDao;
    use Preference\Dao\PreferenceDao;

    use Zend\Db\ResultSet\ResultSet;
    use Zend\Db\TableGateway\TableGateway;
    use Zend\Db\Adapter\Adapter as dbAdapter;
    use Zend\Log\Writer\Stream as Writter;
    use Zend\Log\Logger;

    $global = include(__DIR__ . '/../../config/autoload/global.php');
    $local = include(__DIR__ . '/../../config/autoload/local.php');

    $config = array_merge_recursive($global, $local);

    $options = array(
        'username' => $config['db']['username'],
        'password' => $config['db']['password'],
        'type' => 'pdo_mysql',
        'port' => 3306,
    );

    list($protocol, $dbInfo) = explode(':', $config['db']['dsn']);

    foreach (explode(';', $dbInfo) as $params) {
        list($paramName, $paramValue) = explode('=', $params);
        if ($paramName == 'dbname') {
            $options['dbname'] = $paramValue;
        } elseif ($paramName == 'host') {
            $options['host'] = $paramValue;
        }
    }

    $dbAdapter = new dbAdapter($config['db']);


    $xmlResultSetPrototype = new ResultSet();
    $xmlResultSetPrototype->setArrayObjectPrototype(new Xml());
    $xmlTableGateway = new TableGateway('xml', $dbAdapter, null, $xmlResultSetPrototype);
    $xmlDao = new XmlDao($xmlTableGateway);

    $orderResultSetPrototype = new ResultSet();
    $orderResultSetPrototype->setArrayObjectPrototype(new Order());
    $orderTableGateway = new TableGateway('order', $dbAdapter, null, $orderResultSetPrototype);
    $orderDao = new OrderDao($orderTableGateway);

    $preferenceResultSetPrototype = new ResultSet();
    $preferenceResultSetPrototype->setArrayObjectPrototype(new Preference());
    $preferenceTableGateway = new TableGateway('preference', $dbAdapter, null, $preferenceResultSetPrototype);
    $preferenceDao = new PreferenceDao($preferenceTableGateway);


    $userPreference         = $preferenceDao->getByName("provisioning_cwp_user");
    $user                   = $userPreference->value;

    $passPreference         = $preferenceDao->getByName("provisioning_cwp_password");
    $pass                   = $passPreference->value;

    $broadsoftPreference    = $preferenceDao->getByName("broadsoft_cwp");
    $broadsoft              = $broadsoftPreference->value;

    $orderId    = (int)$_GET['o'];
    //$priority   = (int)$_GET['p'];

    $xml        = $xmlDao->getNext($orderId);

    $order = $orderDao->get($orderId);

    if($xml === null){
            print json_encode(array(
                'name'          => '',
                'result'        => 'last',
                'summary'       => 'No se encontró otro xml',
                //'nextpriority' => $priority + 1,
                'orderid'      => $orderId,
            ));

            $order->setStatusId(2);
            $orderDao->save($order);
            exit;
    }

    $requestFile    = "../../tmp/" . $xml->file;
    $responseFile   = "../../tmp/" . $xml->file . ".response.xml";

    $console = "cd ../../script/ocisoapclient; sh startociclient.sh " .
        $user . " " .
        $pass . " " .
        "-i " . $requestFile . " ".
        "-u " . "http://" . $broadsoft . "/webservice/services/ProvisioningService;";

    $output = shell_exec($console);
    //$output = "";
    if(strpos(strtolower($output),'timed out') !== false){
            print json_encode(array(
                'name'          => $xml->name,
                'result'        => 'error',
                'summary'       => 'El servidor de Broadsoft envió TIMED OUT.',
                //'nextpriority' => $priority + 1,
                'orderid'      => $orderId,
            ));
            $order->setStatusId(4);
            $orderDao->save($order);
            exit;
    }

    //sleep(2);

    if(!file_exists($responseFile)){
            print json_encode(array(
                'name'          => $xml->name,
                'result'        => 'error',
                'summary'       => 'No se generó archivo xml de respuesta.',
                //'nextpriority' => $priority + 1,
                'orderid'      => $orderId,
            ));
            $order->setStatusId(4);
            $orderDao->save($order);
            exit;
    }

    $responseContent = file_get_contents($responseFile);
    //$responseXML = new simplexml_load_string($responseContent);
    if(strpos(strtolower($responseContent),'errorresponse') !== false){
        $result = "error";
    } else {
        $result = "success";
    }
    //$result = (string)$responseXML->command['type'];
    //print $result . "\n";

    //unlink($requestFile);
    //unlink($responseFile);
    
    $order->setStatusId(2);
    $summary = "";
    if(strtolower($result) == "error"){
        $start  = "<summary>";
        $end    = "</summary>";
        $summary  = substr($responseContent, 
            strpos($responseContent, $start) + strlen($start), 
            strpos($responseContent, $end)-(strpos($responseContent, $start) + strlen($start))
            );
        $order->setStatusId(4);
    } 

    $xml->status = $result;
    $xmlDao->save($xml);

    $orderDao->save($order);

    print json_encode(array(
        'name'          => $xml->name,
        'result'        => $result,
        'summary'       => $summary,
        //'nextpriority' => $priority + 1,
        'orderid'      => $orderId,
    ));

    

