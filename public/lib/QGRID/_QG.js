var _QG = {

    items          : Array(),
    pages          : Array(),

    source  	   : '',
    status 		   : 'success',

    itemsPerPage    : 5,
    currentPage     : 1,
    totalItems      : 0,
    totalPages      : 0,
    curretPerPage   : 0,
    rangePage       : 5,

    currentMinPage  : 1,
    currentMaxPage  : 5,
    currentMedPage  : 3,

    orderby         : 'created',
    sort            : { default : 'desc', alt : 'down'},

    optionsPerPage  : [5,10,20,50,100],

    templateDir     : "templates",
    layoutFile      : 'layout.html',
    pagesFile       : 'pages.html',
    gridFile        : 'grid.html',
    filterFile      : 'filter.html',

    loaderImage     : '',

    layoutTemplate  : null,
    pagesTemplate   : null,
    gridTemplate    : null,
    filterTemplate  : null,

    isPerPageEnabled    : false,
    isFilterEnabled     : false,
    isOrderEnabled      : false,
    isPaginationEnabled : false,

    hasOptionsColumn    : false,

    cols        : Array(),
    columns     : Array(),

    inputs      : Array(),

    filters     : {},
    dataToSend  : {},

    icons       : {
      1 :    "text-warning fa fa-warning fa-fw",
      2 :    "text-primary fa fa-calendar fa-fw",
      3 :    "             fa fa-cog fa-spin fa-fw fa-lg",
      4 :    "             fa fa-cog fa-fw fa-lg",
      5 :    "text-success fa fa-check fa-fw",
      6 :    "text-error   fa fa-exclamation-circle fa-fw",
      7 :    "             fa fa-times fa-fw",
      8 :    "text-warning fa fa-check fa-fw",
      9 :    "text-warning fa fa-clock-o fa-fw"
    },

    init : function(settings){
        $.each(settings, function (key, value) {
            _QG[key] = value;
        });
    }
};

_QGEngine = {
    
    start : function(){

        _QGEngine.loadTemplates();
        _QGEngine.fetch();
    },

    fetch : function(){

        $("div._QG_REFRESH").show();
        $("div._QG_LOADER").show();

        var inputLabels = Array();
        _QG.inputs.forEach(function(input){
            $.each(input, function(index, val) {
                if(index=='label') inputLabels.push(val);
            });
        });

        var optionLabels = Array();
        _QG.inputs.forEach(function(input){
            if(input.type == 'select'){
                if(input.hasOwnProperty("options")){
                    $.each(input.options, function(index, option) {
                        if(typeof option == 'object') optionLabels.push(option.label);
                    });
                }
            }
        });

        _QG.dataToSend = {
            limit   : _QG.isPaginationEnabled ? _QG.itemsPerPage : null,
            offset  : ( _QG.currentPage * _QG.itemsPerPage ) - _QG.itemsPerPage,
            order   : _QG.orderby,
            sort    : _QG.sort.default,
            filters : _QG.filters
        };

    	var request = $.ajax(
    	{
            url 	: _QG.source,
            type 	: "POST",
            data 	: _QG.dataToSend
		});

		request.done(_QGEngine.process);
    },

    process : function(data){

        $("div._QG_REFRESH").hide();
        $("div._QG_LOADER").hide();

    	if(data.status == 'success'){

            //_QGEngine.translate(data);

    		_QG.items 		= data.payload.items;
    		_QG.totalItems  = data.payload.total;

    		var total 		= parseInt(_QG.totalItems / _QG.itemsPerPage);
    		var residuo		= _QG.totalItems % _QG.itemsPerPage;
    		_QG.totalPages  = residuo != 0 ? total + 1 : total;

            if(_QG.items[0] === false){
                _QG.items = Array();
            } 
            
            _QG.currentPerPage = _QG.items.length;
   

    		_QG.pages 		= Array();
            mitad = parseInt(_QG.currentMaxPage / 2);
            resid = _QG.currentMaxPage % 2;

            _QG.currentMedPage = resid == 0 ? mitad : mitad + 1;

            if(_QG.currentMaxPage > _QG.totalPages){
                _QG.currentMaxPage =  _QG.totalPages;
                _QG.currentMinPage =  (_QG.currentMaxPage - _QG.rangePage) + 1;
            } 
            
            if(_QG.currentMinPage <= 0){
                _QG.currentMinPage =  1;
                _QG.currentMaxPage =  (_QG.currentMinPage + _QG.rangePage) - 1;
            }

            if(_QG.totalPages < _QG.currentMaxPage){
                _QG.currentMaxPage = _QG.totalPages;
            }

    		for (var i = _QG.currentMinPage; i <= _QG.currentMaxPage; i++) {
    			_QG.pages.push(i);
    		};

            if(_QG.isPaginationEnabled){_QGEngine.renderPages();}
            _QGEngine.renderGrid();
            if(_QG.isFilterEnabled){_QGEngine.renderFilter(); }
            //localStorage.setItem('_QG', JSON.stringify(_QG));
    	}

    },

    renderGrid : function(){
        
        var result = _QG.gridTemplate.process(_QG);
        $("div#_QG_GRID").html(result);
        $("select#_QG_PERPAGE").change(_QGEngine.perpage);

        $("th a").click(_QGEngine.order);

        _QG.items.forEach(function(item){
            $("i#_QG_FA_"+item.id).addClass(_QG.icons[item.status_id]);
        });

        $("div._QG_REFRESH").css('width',parseInt($("div#_QG").css('width')));
        $("div._QG_LOADER").css('width',parseInt($("div#_QG").css('width')));

        $("div._QG_REFRESH").css('height',parseInt($("div#_QG").css('height')));
        $("div._QG_LOADER").css('height',parseInt($("div#_QG").css('height')));
        $(document).trigger("_QG_GRID_DONE");
    },

    renderPages : function(){

        var result = _QG.pagesTemplate.process(_QG);
    	$("div#_QG_PAGINATOR").html(result);

    	//EVENTS
    	$("#_QG_FIRST").click(_QGEngine.first);
    	$("#_QG_PREVIOUS").click(_QGEngine.previous);
    	$("#_QG_NEXT").click(_QGEngine.next);
    	$("#_QG_LAST").click(_QGEngine.last);

    	_QG.pages.forEach(function(element, index){
    		$("#_QG_PAGE_" + element).click(_QGEngine.page);
    	});

    },

    renderFilter : function(){

        // Este for es para validar los calendar que tenemos
        // Si es calendar preguntamos si se quiere otro calendar para manejar rangos -> use2dates
        // Se crea otro igualito y lo insertamos en el array inmediatamente despues de su original
        // Si el index es impar eso quiere decir que el otro calendar adicional quedará en la linea de abajo,
        // asi que tomamos el siguiente input y reacomodamos de tal manera que dos calendar queden uno a lado del otro.
        for(var index = 0; index <= _QG.inputs.length ; index++){
            var input = _QG.inputs[index];
            if(typeof input != 'undefined' && input.type == 'calendar' && input.hasOwnProperty("use2dates")){
                if(input.use2dates){
                    delete input.use2dates;
                    var newobj = {};
                    $.extend( newobj , input, {} );
                    input.id     = input.id + "QGDATE1";
                    input.label  = input.label + " 1";
                    newobj.id    = newobj.id + "QGDATE2";
                    newobj.label = newobj.label + " 2";

                    var nextToMe = 1;
                    if(index % 2 != 0){
                        var next = _QG.inputs.splice(index + 1, 1);
                        var nextObject = next[0];
                        if(typeof nextObject != 'undefined'){
                            _QG.inputs.splice(index,0,next[0]);
                            nextToMe = 2;
                        }
                    } 
                    
                    _QG.inputs.splice(index + nextToMe,0,newobj);
 
                }
            }
        }

        _QG.inputs.forEach(function(input, index){
            if(input.type == 'select' && !input.hasOwnProperty("options") && input.hasOwnProperty("source")){
                //Si el select no tiene opciones, lo llenamos con lo que eśtá en la bd
                $.ajax({
                      async: false,
                      url: input.source,
                      success: function (data) {
                        input.options = data.payload;
                        input.options.forEach(function(element, index){
                            element.label = element.name;
                            element.value = element.id;
                        });
                      },
                      dataType: "json"
                });
            }
        });

        var result = _QG.filterTemplate.process(_QG);
        $("div#_QG_MODALS").html(result);
        $("button#_QG_BTN_FILTER").click(_QGEngine.submit);
        //$("button#_QG_BTN_RESET").click(_QGEngine.reset);
        //$("form#_QG_FORM_FILTER input[type=checkbox]").prettyCheckable();

        _QG.inputs.forEach(function(input, index){

            if(input.type == 'calendar' && input.hasOwnProperty("datetimepicker")){
                
                if(input.datetimepicker.hasOwnProperty("pickTime") && input.datetimepicker.pickTime == true){
                    $('input#' + input.id).attr('data-format','dd-MM-yyyy hh:mm');
                } else {
                    $('input#' + input.id).attr('data-format','dd-MM-yyyy');
                }

                $('#datetimepicker_' + input.id).datetimepicker({
                    pickTime    : input.datetimepicker.pickTime,
                    pickDate    : input.datetimepicker.pickDate,
                    pickSeconds : false,
                    pick12HourFormat: true,
                    endDate     : new Date(),
                    icons: {
                        time    : "fa fa-clock-o",
                        date    : "fa fa-calendar",
                        up      : "fa fa-arrow-up",
                        down    : "fa fa-arrow-down"
                    }
                });
            }

        });
    },

    // ACTIONS

    first : function(){
    	if(_QG.currentPage != 1){
            _QG.currentPage = 1;
            _QG.currentMinPage = 1;
            _QG.currentMaxPage = _QG.rangePage;
            _QGEngine.fetch();
        }
    },

    previous : function(){
        if(_QG.currentPage != 1){
    	   _QG.currentPage = _QG.currentPage - 1;

            if(_QG.currentPage < _QG.currentMinPage) {
                _QG.currentMinPage = _QG.currentMinPage - 1;
                _QG.currentMaxPage = _QG.currentMaxPage - 1;
            }

    	   _QGEngine.fetch();
        }
    },

    next : function(){
        if(_QG.currentPage != _QG.totalPages){
    	   _QG.currentPage = _QG.currentPage + 1;

            if(_QG.currentPage > _QG.currentMedPage) {
                _QG.currentMinPage = _QG.currentMinPage + 1;
                _QG.currentMaxPage = _QG.currentMaxPage + 1;
            }

    	   _QGEngine.fetch();
        }
    },

    last : function(){
        if(_QG.currentPage != _QG.totalPages){
    	   _QG.currentPage = _QG.totalPages;
           _QG.currentMinPage = (_QG.totalPages - _QG.rangePage) + 1;
           _QG.currentMaxPage = _QG.totalPages;
    	   _QGEngine.fetch();
        }
    },

    page : function(){
    	_QG.currentPage = parseInt($(this).text());

        if(_QG.currentPage > _QG.currentMedPage) {
            _QG.currentMinPage = _QG.currentMinPage + 1;
            _QG.currentMaxPage = _QG.currentMaxPage + 1;
        }


    	_QGEngine.fetch();
    },

    perpage : function(){
        _QG.itemsPerPage = parseInt($(this).val());
        _QG.currentPage = 1;
        _QGEngine.fetch();
    },

    order : function(){
        if(this.id != _QG.orderby){
            _QG.orderby = this.id;
        } else {
            if(_QG.sort.default == 'asc')  { 
                _QG.sort.default    = 'desc'; 
                _QG.sort.alt        = 'down';
            }
            else if(_QG.sort.default == 'desc') { 
                _QG.sort.default    = 'asc';
                _QG.sort.alt        = 'up';
            }
        }

        _QGEngine.fetch();
    },

    submit : function(){
        var form = $("form#_QG_FORM_FILTER");
        _QG.filters = form.serializeObject();
        $.each(_QG.filters, function (key, value) {
            if(value == "" && typeof key == 'string' ){
                delete _QG.filters[key];
            }
        });

        _QGEngine.fetch();
        $("div#_QG_MODAL_FILTER").modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    },

    reset : function(){
        _QG.filters = Array();
        _QGEngine.renderFilter();
    },

    loadTemplates : function(){

        $.ajax({
              async: false,
              url: _QG.templateDir + "/" + _QG.layoutFile,
              success: function (html) {
                $("body").append(html);
                _QG.layoutTemplate  = TrimPath.parseDOMTemplate("_QG_TEMPLATE_LAYOUT");
                var result = _QG.layoutTemplate.process(_QG);
                $("div#_QG").html(result);
                $("div._QG_REFRESH").css('height',100);
                $("div._QG_LOADER").css('height',100);
              },
              dataType: "html"
        });

        if(_QG.isPaginationEnabled){

            $.ajax({
                  async: false,
                  url: _QG.templateDir + "/" + _QG.pagesFile,
                  success: function (html) {
                    $("div#_QG_TEMPLATES").append(html);
                    _QG.pagesTemplate  = TrimPath.parseDOMTemplate("_QG_TEMPLATE_PAGES");
                  },
                  dataType: "html"
            });
        }

        $.ajax({
              async: false,
              url: _QG.templateDir + "/" + _QG.gridFile,
              success: function (html) {
                $("div#_QG_TEMPLATES").append(html);
                _QG.gridTemplate  = TrimPath.parseDOMTemplate("_QG_TEMPLATE_GRID");
              },
              dataType: "html"
        });

        if(_QG.isFilterEnabled){

            $.ajax({
                  async: false,
                  url: _QG.templateDir + "/" + _QG.filterFile,
                  success: function (html) {
                    $("div#_QG_TEMPLATES").append(html);
                    _QG.filterTemplate  = TrimPath.parseDOMTemplate("_QG_TEMPLATE_FILTER");
                  },
                  dataType: "html"
            });

        }
    }

};



jQuery(document).ready(function($) {
    localStorage.clear();
    /*if (localStorage.getItem("_QG") !== null) {
        // Retrieve the object from storage
        var retrievedObject = localStorage.getItem('_QG');
        _QG = JSON.parse(retrievedObject);
    }*/
});

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};