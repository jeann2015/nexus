<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace DeviceManager;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use DeviceManager\Model\IniTemplate;
use DeviceManager\Dao\IniTemplateDao;

use DeviceManager\Model\DeviceType;
use DeviceManager\Dao\DeviceTypeDao;

use DeviceManager\Model\Firmware;
use DeviceManager\Dao\FirmwareDao;

use DeviceManager\Model\Inivariable;
use DeviceManager\Dao\InivariableDao;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'DeviceManager\Dao\IniTemplateDao' => function($sm) {
                    $tableGateway = $sm->get('IniTemplateTableGateway');
                    $table = new IniTemplateDao($tableGateway);
                    $table->setServiceLocator($sm);
                    return $table;
                },
                'IniTemplateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new IniTemplate());
                    return new TableGateway('nx_initemplate', $dbAdapter, null, $resultSetPrototype);
                },

                'DeviceManager\Dao\DeviceTypeDao' => function($sm) {
                    $tableGateway = $sm->get('DeviceTypeTableGateway');
                    $table = new DeviceTypeDao($tableGateway);
                    $table->setServiceLocator($sm);
                    return $table;
                },
                'DeviceTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DeviceType());
                    return new TableGateway('bs_device_type', $dbAdapter, null, $resultSetPrototype);
                },

                'DeviceManager\Dao\FirmwareDao' => function($sm) {
                    $tableGateway = $sm->get('FirmwareTableGateway');
                    $table = new FirmwareDao($tableGateway);
                    $table->setServiceLocator($sm);
                    return $table;
                },
                'FirmwareTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Firmware());
                    return new TableGateway('nx_firmware', $dbAdapter, null, $resultSetPrototype);
                },

                'DeviceManager\Dao\InivariableDao' => function($sm) {
                    $tableGateway = $sm->get('InivariableTableGateway');
                    $table = new InivariableDao($tableGateway);
                    $table->setServiceLocator($sm);
                    return $table;
                },
                'InivariableTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Inivariable());
                    return new TableGateway('nx_inivariable', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}
