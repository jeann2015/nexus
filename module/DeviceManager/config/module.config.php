<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'ini' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/ini[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'IniTemplateController',
                        'action'     => 'list',
                    ),
                ),
            ),
            'devicemanager' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/devicemanager[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'DeviceManagerController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'devicetype' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/devicetype[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'DeviceTypeController',
                        'action'     => 'list',
                    ),
                ),
            ),

	       'firmware' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/firmware[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'FirmwareController',
                        'action'     => 'list',
                    ),
                ),
            ),

           'inivariable' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/inivariable[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'InivariableController',
                        'action'     => 'list',
                    ),
                ),
            ),

        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
    	    'IniTemplateController'    => 'DeviceManager\Controller\IniTemplateController',
            'DeviceManagerController'  => 'DeviceManager\Controller\DeviceManagerController',
    	    'DeviceTypeController'     => 'DeviceManager\Controller\DeviceTypeController',
    	    'FirmwareController'       => 'DeviceManager\Controller\FirmwareController',
            'InivariableController'    => 'DeviceManager\Controller\InivariableController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        /*'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/authentication/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),*/
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
