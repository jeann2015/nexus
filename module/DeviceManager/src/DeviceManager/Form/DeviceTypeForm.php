<?php

namespace DeviceManager\Form;

use Generic\Form\GenericForm;

class DeviceTypeForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id',
            ),
        ));
                
        $this->add(array(
            'name' => 'type',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'type',
            ),
        ));
        
        $this->add(array(
            'name' => 'ports',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'ports',
            ),
        ));

        $this->add(array(
	    'type' => 'Zend\Form\Element\Select',
            'name' => 'initemplate_id',
            'attributes' => array(
                'type'  => 'select',
                'id'    => 'initemplate_id',
		        'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'firmware_id',
            'attributes' => array(
                'type'  => 'select',
                'id'    => 'firmware_id',
                'class' => 'form-control',
                'required' => false,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'active',
            'attributes' => array(
                'type'  => 'select',
                'id'    => 'active',
                'class' => 'form-control',
            ),
            'options' => array(
                'value_options' => array('1' => "Si",'0' => "No"),
                'empty_option'  => '',
            )
        ));
    }
}
