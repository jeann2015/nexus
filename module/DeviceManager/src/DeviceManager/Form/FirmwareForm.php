<?php

namespace DeviceManager\Form;

use Generic\Form\GenericForm;

class FirmwareForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'upload-input',
            'attributes' => array(
                'type'  => 'file',
                'id'    => 'upload-input',
            ),
        ));
    }
}
