<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DeviceManager\Dao;

/**
 * Description of RoleDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;

use Generic\Dao\GenericDao;

class DeviceTypeDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway)
    {
        parent::__construct($tableGateway);
    }
    
    /***
     * La idea es esta:
     * SELECT distinct 
                nexus.role.*, 
                count(distinct nexus.user.id) as usuarios
        FROM 
                nexus.role
                LEFT JOIN nexus.user on nexus.role.id = nexus.user.role_id
        GROUP BY
                nexus.role.id; 
     *
     *****/
    public function fetchAll()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_initemplate', 'bs_device_type.initemplate_id = nx_initemplate.id',array('ini'=>'name'),'LEFT');
	    $select->join('nx_firmware', 'bs_device_type.firmware_id = nx_firmware.id',array('firmware'=>'name'),'LEFT');
        //$select->group('nx_role.id');
        
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function getByType($type)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );

        $where = new Where();
        $where->equalTo('type', $type);
        $select->where($where);
        
        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }

        return $row;
    }

    public function fetchFilter(
        $limit = null, $offset = 0, $order = 'created', $sort = 'desc',$filters = null)
    {
        $table = "bs_device_type";
        $result = array();

        $query = "SELECT COUNT(*) AS total FROM $table ";

        if(!is_null($filters)){
            $query .= "WHERE ";
            $and = "";
            foreach ($filters as $key => $value) {

                if (strpos($key,'QGDATE2') !== false) {
                    continue;
                }
                
                if (strpos($key,'QGDATE1') !== false) {
                    list($field,$number) = explode("QGDATE", $key);
                    $value1 = $value;
                    $value2 = $filters[$field . 'QGDATE2'];
                    $query .= $and . " $table.".$field." between '" . $value1 . "' and '" . $value2 . "' ";
                } else {
                    $query .= $and . " $table.".$key." like '%" . $value . "%'";
                }
                
                $and = " AND ";
            }
        }

        $query .= ";";
        //print $query;

        $statement = $this->tableGateway->adapter->query($query);
        $resultSet = $statement->execute();
        $object = $resultSet->current();
        $result["total"] = $object['total'];



        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_initemplate', 'bs_device_type.initemplate_id = nx_initemplate.id',array('ini'=>'name'),'LEFT');
        $select->join('nx_firmware', 'bs_device_type.firmware_id = nx_firmware.id',array('firmware'=>'name'),'LEFT');
        $select->order($order . " " . $sort);

        if(!is_null($limit)  && isset($limit) && $limit != ""){
            $select->limit((int)$limit);
            $select->offset((int)$offset);
        }

        if(!is_null($filters)){
            
            $where = new Where() ;
            foreach ($filters as $key => $value) {

                if (strpos($key,'QGDATE2') !== false) {
                    continue;
                }
                
                if (strpos($key,'QGDATE1') !== false) {
                    list($field,$number) = explode("QGDATE", $key);
                    $value1 = $value;
                    $value2 = $filters[$field . 'QGDATE2'];
                    $where->between("$table.".$field, $value1, $value2);
                } else {
                    $where->like("$table.".$key, '%'.$value.'%');
                }
            }
            
            $select->where($where);
        }

        //print $select->getSqlString();

        $resultSet = $this->tableGateway->selectWith($select);
        $resultSet->buffer();

        $result["resultSet"] = $resultSet;
        return $result;
    }
    
}
