<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DeviceManager\Model;

use Generic\Model\Generic;
/**
 * Description of Role
 *
 * @author rodolfo
 */
class IniTemplate extends Generic {
    
    public $name;
    public $description;
    public $template;

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getTemplate() {
        return $this->template;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setTemplate($template) {
        $this->template = $template;
    }
    
}
