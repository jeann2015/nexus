<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DeviceManager\Model;

use Generic\Model\Generic;
/**
 * Description of Role
 *
 * @author rodolfo
 */
class Firmware extends Generic {
    
    public $name;
    public $path;
    public $created;

    public function getName() {
        return $this->name;
    }

    public function getPath() {
        return $this->path;
    }

    public function getCreated() {
        return $this->created;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPath($path) {
        $this->path = $path;
    }

    public function setCreated($created) {
        $this->created = $created;
    }   
}
