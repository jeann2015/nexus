<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace DeviceManager\Model;

use Generic\Model\Generic;
/**
 * Description of Role
 *
 * @author rodolfo
 */
class DeviceType extends Generic {
    
    public $type;
    public $ports;
    public $initemplate_id;
    public $firmware_id;
    public $active;

    public function getType() {
        return $this->type;
    }

    public function getPorts() {
        return $this->ports;
    }

    public function getInitemplateId() {
        return $this->initemplate_id;
    }

    public function getFirmwareId() {
        return $this->firmware_id;
    }

    public function getActive() {
        return $this->active;
    }

    public function getInbroadsoft() {
        return $this->inbroadsoft;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function setPorts($ports) {
        $this->ports = $ports;
    }

    public function setInitemplateId($initemplate_id) {
        $this->initemplate_id = $initemplate_id;
    }
   
    public function setActive($active) {
        $this->active = $active;
    }

    public function setInbroadsoft($inbroadsoft) {
        $this->inbroadsoft = $inbroadsoft;
    } 
}
