<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace DeviceManager\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
 
use Generic\Controller\GenericController;

class DeviceManagerController extends GenericController
{ 
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        return new ViewModel();
    }
    
}
