<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace DeviceManager\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
 
use DeviceManager\Model\IniTemplate;
use DeviceManager\Form\IniTemplateForm;
use Generic\Controller\GenericController;

class IniTemplateController extends GenericController
{ 
    public $initemplateDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        $initemplates = $this->getDao()->fetchAll();
        return new ViewModel(array('initemplates' => $initemplates));
    }
      
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $role = new Role();
            $form = new RoleForm();
            $form->setInputFilter($role->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $role->exchangeArray($form->getData());
                  
                $this->getDao()->save($role);

                return $this->redirect()->toRoute('role');
            }

        }

        return new ViewModel();
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $initemplate = $this->getDao()->get($id);
        
        $form = new IniTemplateForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $initemplate = new IniTemplate();
            $form->setInputFilter($initemplate->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $initemplate->exchangeArray($form->getData());
                  
                $this->getDao()->save($initemplate);

                return $this->redirect()->toRoute('ini');
            }

        } else {
            $form->setData($initemplate->getArrayCopy());
        }

        return new ViewModel(array(
            'form' 		=> $form,
            'initemplate'	=> $initemplate,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function accessAction()
    {
        $id = $this->params()->fromRoute('id');
        $config = $this->getServiceLocator()->get('config');
        $routes = array_keys($config['router']['routes']);
        
        $exclude = array('root','application','auth','home');
        foreach ($exclude as $ex)
        {
            if(($key = array_search($ex, $routes)) !== false) {
                unset($routes[$key]);
            }
        }
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $post = $request->getPost();
            var_dump($post);

        }
        
        return new ViewModel(array(
            'routes'    =>  $routes,
            'id'        =>  $id,
        ));
    }
    
    public function getDao()
    {
        if (!$this->initemplateDao) {
            $sm = $this->getServiceLocator();
            $this->initemplateDao = $sm->get('DeviceManager\Dao\IniTemplateDao');
        }
        
        return $this->initemplateDao;
    }

    public function fetchinisAction(){
        $model = new JsonModel();

        $inis = $this->getDao()->fetchAll();
        $model->setVariable("status","success");
        $iArray = array();
        foreach ($inis as $i) {
            unset($i->template);
            array_push($iArray, $i);
        }
        $model->setVariable("payload",$iArray);

        return $model;
    }
}
