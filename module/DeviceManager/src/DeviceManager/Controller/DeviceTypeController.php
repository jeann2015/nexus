<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace DeviceManager\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
 
use DeviceManager\Model\DeviceType;
use DeviceManager\Form\DeviceTypeForm;
use Generic\Controller\GenericController;

use DeviceManager\Dao\IniTemplateDao;
use DeviceManager\Dao\FirmwareDao;

use Provisioning\Service\OCISoapService;
use Provisioning\Service\XmlService;

class DeviceTypeController extends GenericController
{ 
    public $dao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        return new ViewModel();
    }
      
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $deviceType = new DeviceType();
            $form = new DeviceTypeForm();
            $form->setInputFilter($deviceType->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $deviceType->exchangeArray($form->getData());
                  
                $this->getDao()->save($deviceType);

                return $this->redirect()->toRoute('devicetype');
            }

        }

        return new ViewModel();
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $devicetype = $this->getDao()->get($id);

        $iniTemplateTableGateway = $this->getServiceLocator()->get('IniTemplateTableGateway');
        $iniTemplateDao = new IniTemplateDao($iniTemplateTableGateway);
        $iniTemplateFetch = $iniTemplateDao->fetchAll();
        $iniTemplateArray = array(""=>"");
        
        foreach ($iniTemplateFetch as $iniTemplate) {
            $iniTemplateArray[$iniTemplate->id] = $iniTemplate->name;
        }
        
        $form = new DeviceTypeForm();
        $form->get('initemplate_id')->setValueOptions($iniTemplateArray);
        $form->get('initemplate_id')->setValue($devicetype->initemplateId);


        $firmwareTableGateway = $this->getServiceLocator()->get('FirmwareTableGateway');
        $firmwareDao = new FirmwareDao($firmwareTableGateway);
        $firmwareFetch = $firmwareDao->fetchAll();
        $firmwareArray = array(""=>"");

        foreach ($firmwareFetch as $firmware) {
            $firmwareArray[$firmware->id] = $firmware->name;
        }

        $form->get('firmware_id')->setValueOptions($firmwareArray);
        $form->get('firmware_id')->setValue($devicetype->firmwareId);

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $devicetype = new DeviceType();

            $inputFilter  = $devicetype->getInputFilter();

            $inputFilter->add(array(
                'name'        => 'firmware_id',
                'required'    => false,
                'allow_empty' => true,
            ));

            $inputFilter->add(array(
                'name'        => 'initemplate_id',
                'required'    => false,
                'allow_empty' => true,
            ));

            $form->setInputFilter($inputFilter);

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $devicetype->exchangeArray($form->getData());

		        $devicetype->firmware_id = $devicetype->firmware_id == "" ? null : $devicetype->firmware_id;
		        $devicetype->initemplate_id = $devicetype->initemplate_id == "" ? null : $devicetype->initemplate_id;
                  
                $this->getDao()->save($devicetype);

                return $this->redirect()->toRoute('devicetype');
            } else {
                $err = $form->getMessages();
                print_r ($err);
            }

        } else {
            $form->setData($devicetype->getArrayCopy());
        }

        return new ViewModel(array(
            'form'          => $form,
            'devicetype'    => $devicetype,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function accessAction()
    {
        $id = $this->params()->fromRoute('id');
        $config = $this->getServiceLocator()->get('config');
        $routes = array_keys($config['router']['routes']);
        
        $exclude = array('root','application','auth','home');
        foreach ($exclude as $ex)
        {
            if(($key = array_search($ex, $routes)) !== false) {
                unset($routes[$key]);
            }
        }
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $post = $request->getPost();
            var_dump($post);

        }
        
        return new ViewModel(array(
            'routes'    =>  $routes,
            'id'        =>  $id,
        ));
    }
    
    public function getDao($daoName = "DeviceTypeDao",$namespace = "DeviceManager")
    {
        $dao = $namespace . "\Dao\\" . $daoName;

        if (!is_a($this->dao,$dao) ) {
            $is = 1;
            $sm = $this->getServiceLocator();
            $this->dao = $sm->get($dao);
        }

        return $this->dao;
    }

    public function syncAction() 
    {
        $model = new JsonModel();
        
        try {
            $xmltemplate = $this->getDao("XmlTemplateDao","Provisioning")->getByCategoryLikeCommand('list','SystemAccessDeviceTypeGetList');

            if($xmltemplate) { 
                $xml = $xmltemplate->getXml();

                $variableDao = $this->getDao("VariableDao","Provisioning");
                $variables = $variableDao->fetchByXmlTemplateId($xmltemplate->getId());

                //$array = $request->getPost();
                $array = array();

                $xmlService = new XmlService();
                $xmlGenerated = $xmlService->mergeArray($xml,$variables,$array);
                
                $OCISoapService = new OCISoapService($this->getServiceLocator());
                $response = $OCISoapService->send($xmlGenerated);
                $xmlresponse = $xmlService->getCommnadResponseFromXml($response);

                if($xmlresponse->result == "success"){

                    $devices = $this->getDao()->fetchAll();
                    foreach ($devices as $device) {
                        $device->setInbroadsoft(0);
                        unset($device->ini);
                        unset($device->firmware);
                        $this->getDao()->save($device);
                    }

                    foreach ((array)$xmlresponse->command->deviceType as $deviceType) {
                        $modelo = $this->getDao()->getByType($deviceType);

                        if(isset($modelo)){
                            $modelo->setInbroadsoft(1);
                        } else {
                            $modelo = new DeviceType();
                            $modelo->setType($deviceType);
                            $modelo->setInbroadsoft(1);
                        }
                        
                        $this->getDao()->save($modelo);
                    }

                    $model->setVariable('status', "success");
                    $model->setVariable('payload', $summary);
                } else {
                    $summary = $xmlresponse->summary;
                    $model->setVariable('status', "error");
                    $model->setVariable('payload', $summary);
                }
            } else {
                $summary = "[Error] No existe comando xml de consulta.";
                $model->setVariable('status', "error");
                $model->setVariable('payload', $summary);
            }

        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }

    public function filterAction(){

        $model = new JsonModel();

        $request = $this->getRequest();
        if($request->isPost()){
            $post   = (array)$request->getPost();
            $limit  = isset( $post['limit'] ) ?  $post['limit'] : null;
            $offset = $post['offset'];
            $order  = $post['order'];
            $sort   = $post['sort'];

            $filters = array_key_exists('filters',$post) ? $post['filters'] : null;

            if($filters)
            foreach ($filters as $key => $value) {
                if (strpos($key,'QGDATE') !== false) {
                    if(stripos($value,":") !== false){
                        $filters[$key] = $this->convertToUTC($value);
                    } else {
                        $filters[$key] = $this->convertToUTC($value,array('date'=>true));
                    }
                    //print $filters[$key] . "\n";
                }
            }

            $result = $this->getDao()->fetchFilter($limit,$offset,$order,$sort,$filters);
        } 

        $items = array();
        foreach ($result['resultSet'] as $item) {
            $item->options = "";
            foreach ($item as $key => $value) {

                //Aqui se hace alguna transformación que los valores del grid requieran
                //$value      = $this->translator($value);
                //$item->$key = $value;

                if($this->isDate($value)){
                    if(stripos($value,":") === false){
                        $item->$key = $this->convertFromUTC($value,array('date'=>true));
                    } else {
                        $item->$key = $this->convertFromUTC($value);
                    }  
                }

                if($key == "options"){
                    $url = $this->url()->fromRoute('devicetype',array('action'=>'edit','id'=>$item->id));
                    $item->$key = "<a href='".$url."'><i class='fa fa-edit'></i> Editar</a>";
                }

                if($key == "inbroadsoft"){
                    $item->$key = $value == 1 ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-close text-danger'></i>";
                }

                if($key == "active"){
                    $item->$key = $value == 1 ? "<i class='fa fa-check text-success'></i>" : "";
                }
            }

            array_push($items, $item);
        }

        $model->setVariable("status","success");
        $model->setVariable("payload",array(
            "total"     =>  $result['total'],
            "items"     =>  $items,
        ));

        return $model;
    }
}
