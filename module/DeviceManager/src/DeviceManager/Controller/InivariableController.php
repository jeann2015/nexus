<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace DeviceManager\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
 
use DeviceManager\Model\Inivariable;
use DeviceManager\Form\InivariableForm;
use Generic\Controller\GenericController;

class InivariableController extends GenericController
{ 
    public $inivariableDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        $inivariables = $this->getDao()->fetchAll();
        return new ViewModel(array('inivariables' => $inivariables));
    }
      
    public function addAction()
    {
        $form = new InivariableForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $inivariable = new Inivariable();
            
            $form->setInputFilter($inivariable->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $inivariable->exchangeArray($form->getData());
                  
                $this->getDao()->save($inivariable);

                return $this->redirect()->toRoute('inivariable');
            }

        }

        return new ViewModel(array(
            'form'            => $form,
        ));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $inivariable = $this->getDao()->get($id);
        
        $form = new InivariableForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $inivariable = new Inivariable();
            $form->setInputFilter($inivariable->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $inivariable->exchangeArray($form->getData());
                  
                $this->getDao()->save($inivariable);

                return $this->redirect()->toRoute('inivariable');
            }

        } else {
            $form->setData($inivariable->getArrayCopy());
        }

        return new ViewModel(array(
            'form' 		      => $form,
            'inivariable'	  => $inivariable,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    
    public function getDao()
    {
        if (!$this->inivariableDao) {
            $sm = $this->getServiceLocator();
            $this->inivariableDao = $sm->get('DeviceManager\Dao\InivariableDao');
        }
        
        return $this->inivariableDao;
    }

}
