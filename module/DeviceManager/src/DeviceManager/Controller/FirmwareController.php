<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace DeviceManager\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Zend\Validator\File\Size;
use Zend\Validator\File\Extension;
use Zend\File\Transfer\Adapter\Http;
 
use DeviceManager\Model\Firmware;
use DeviceManager\Form\FirmwareForm;
use Generic\Controller\GenericController;

use \Exception;

class FirmwareController extends GenericController
{ 
    public $firmwareDao;
    public $inivariableDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $firmwares = $this->getDao()->fetchAll();
        return new ViewModel(array('firmwares' => $firmwares,'form'=>new FirmwareForm()));
    }
      
    public function uploadAction()
    {
	    $request = $this->getRequest(); 
        $form = new FirmwareForm();

        if ($request->isPost()) {
            $nonFile = $request->getPost()->toArray();
            $File    = $this->params()->fromFiles('upload-input');
            $data = array_merge(
                 $nonFile,
                 array('name'=> $File['name'])
             );

            //$form->setData($data);
	        //$size = new Size(array('max'=> 100 * (1024) * (1024) )); // MB * (1024) * (1024)
            $extension = new Extension('cmp');
            $adapter = new Http();
            $adapter->setValidators(array($extension), $File['name']);

            $firmware_path = $this->getInivariableDao()->getByName("firmware_path")->value;
            $firmware_path = "/".trim($firmware_path,"/"). "/";
	
    	    if ($adapter->isValid()){
    	    	$adapter->setDestination($firmware_path);

                if($adapter->receive($File['name'])){
        			$firmware = new Firmware();
        			$firmware->setName($File['name']);
        			$firmware->setPath($firmware_path . $File['name']);
        			$firmware->setCreated(date('Y-m-d H:i:s'));
        			$this->getDao()->save($firmware);
        			return new JsonModel(array('result'=>'success'));
    	    	}
    	    } else {
    		  $dataError = $adapter->getMessages();
    		  return new JsonModel(array('result'=>'failed','payload'=>$dataError));
    	    }
            
        }

        return new JsonModel(array('result'=>'success'));
    }

    public function downloadAction()
    {
        $id = $this->params()->fromRoute('id');
        $firmware = $this->getDao()->get($id);

        $fileContents = file_get_contents($firmware->path);

        $response = $this->getResponse();
        $response->setContent($fileContents);

        $headers = $response->getHeaders();
        $headers->clearHeaders()
            ->addHeaderLine('Content-Type', 'application/octet-stream')
            ->addHeaderLine('Content-Disposition', 'attachment; filename="' . $firmware->name . '"')
            ->addHeaderLine('Content-Length', strlen($fileContents));


        return $this->response;
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $firmware = $this->getDao()->get($id);
            $result = $this->getDao()->fetchDeviceTypeWithFirmwareId($id);

            if($result->count() == 0){
                unlink($firmware->path);
                $this->getDao()->delete($id);
                $model->setVariable('status', "success");
                $model->setVariable('payload', $id);
            } else {
                $model->setVariable('status', 'error');
                $model->setVariable('payload', "Hay dispositivos que aun utilizan este firmware");
            }
            
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao()
    {
        if (!$this->firmwareDao) {
            $sm = $this->getServiceLocator();
            $this->firmwareDao = $sm->get('DeviceManager\Dao\FirmwareDao');
        }
        
        return $this->firmwareDao;
    }

    public function getInivariableDao()
    {
        if (!$this->inivariableDao) {
            $sm = $this->getServiceLocator();
            $this->inivariableDao = $sm->get('DeviceManager\Dao\InivariableDao');
        }
        
        return $this->inivariableDao;
    }

    public function fetchfirmwaresAction()
    {
        $model = new JsonModel();

        $firmwares = $this->getDao()->fetchAll();
        $model->setVariable("status","success");
        $fArray = array();
        foreach ($firmwares as $f) {
            array_push($fArray, $f);
        }
        $model->setVariable("payload",$fArray);

        return $model;
    }
}
