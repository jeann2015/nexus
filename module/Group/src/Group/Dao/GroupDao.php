<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Group\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Generic\Dao\GenericDao;

class GroupDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }
    
    /***
     * La idea es esta:
     * SELECT distinct 
                nexus.group.*, 
                count(distinct nexus.user.id) as usuarios
        FROM 
                nexus.group
                LEFT JOIN nexus.user on nexus.group.id = nexus.user.group_id
        GROUP BY
                nexus.group.id; 
     *
     **/
    public function fetchWithUserCount($paginated = false)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_user', 'nx_user.group_id = nx_group.id',array('usuarios'=>new Expression("count(distinct nx_user.id)")),'LEFT');
        $select->group('nx_group.id');
        
        if ($paginated) {
            $dbTableGatewayAdapter = new DbSelect($select,$sql);
            //$dbTableGatewayAdapter = new DbTableGateway($this->tableGateway);
            $paginator = new Paginator($dbTableGatewayAdapter);
            return $paginator;
        }
        
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }
    
}
