<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Group\Model;

use Group\InputFilter\GroupInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Group extends GroupInputFilter {
    
    public $name;
    public $email;

    public function getName() {
        return $this->name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    
}
