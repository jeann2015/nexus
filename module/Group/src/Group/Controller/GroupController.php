<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Group\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Group\Model\Group;
use Group\Form\GroupForm;
use Generic\Controller\GenericController;

class GroupController extends GenericController
{ 
    public $groupDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;

        // fetchWithUserCount(true) return paginator
        // fetchWithUserCount() return resultset
        $paginator = $this->getDao()->fetchWithUserCount(true);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        $paginator->setPageRange(7);

        return new ViewModel(array('paginator' => $paginator));
    }
      
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $group = new Group();
            $form = new GroupForm();
            $form->setInputFilter($group->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $group->exchangeArray($form->getData());
                  
                $this->getDao()->save($group);

                return $this->redirect()->toRoute('group',array('action'=>'list'));
            }

        }

        return new ViewModel();
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $group = $this->getDao()->get($id);
        
        $form = new GroupForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $group = new Group();
            $form->setInputFilter($group->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $group->exchangeArray($form->getData());
                  
                $this->getDao()->save($group);

                return $this->redirect()->toRoute('group',array('action'=>'list'));
            }

        } else {
            $form->setData($group->getArrayCopy());
        }

        return new ViewModel(array(
            'form' => $form,
            'group'=> $group,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao()
    {
        if (!$this->groupDao) {
            $sm = $this->getServiceLocator();
            $this->groupDao = $sm->get('Group\Dao\GroupDao');
        }
        
        return $this->groupDao;
    }

}
