<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Role\Dao;

/**
 * Description of RoleDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

use Generic\Dao\GenericDao;

class RoleDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway)
    {
        parent::__construct($tableGateway);
    }
    
    /***
     * La idea es esta:
     * SELECT distinct 
                nexus.role.*, 
                count(distinct nexus.user.id) as usuarios
        FROM 
                nexus.role
                LEFT JOIN nexus.user on nexus.role.id = nexus.user.role_id
        GROUP BY
                nexus.role.id; 
     *
     *****/
    public function fetchWithUserCount()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_user', 'nx_user.role_id = nx_role.id',array('usuarios'=>new Expression("count(distinct nx_user.id)")),'LEFT');
        $select->group('nx_role.id');
        
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }
    
}
