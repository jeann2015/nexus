<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Role\Model;

use Generic\Model\Generic;
/**
 * Description of Role
 *
 * @author rodolfo
 */
class Role extends Generic {
    
    public $name;
    public $description;

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }
    
}
