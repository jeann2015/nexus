<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Role\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
 
use Role\Model\Role;
use Role\Form\RoleForm;
use Generic\Controller\GenericController;

class RoleController extends GenericController
{ 
    public $roleDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
       
        $roles = $this->getDao()->fetchWithUserCount();
        return new ViewModel(array('roles' => $roles));
    }
      
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $role = new Role();
            $form = new RoleForm();
            $form->setInputFilter($role->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $role->exchangeArray($form->getData());
                  
                $this->getDao()->save($role);

                return $this->redirect()->toRoute('role');
            }

        }

        return new ViewModel();
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $role = $this->getDao()->get($id);
        
        $form = new RoleForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $role = new Role();
            $form->setInputFilter($role->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $role->exchangeArray($form->getData());
                  
                $this->getDao()->save($role);

                return $this->redirect()->toRoute('role');
            }

        } else {
            $form->setData($role->getArrayCopy());
        }

        return new ViewModel(array(
            'form' => $form,
            'role'=> $role,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function accessAction()
    {
        $id = $this->params()->fromRoute('id');
        $config = $this->getServiceLocator()->get('config');
        $routes = array_keys($config['router']['routes']);
        
        $exclude = array('root','application','auth','home');
        foreach ($exclude as $ex)
        {
            if(($key = array_search($ex, $routes)) !== false) {
                unset($routes[$key]);
            }
        }
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $post = $request->getPost();
            var_dump($post);

        }
        
        return new ViewModel(array(
            'routes'    =>  $routes,
            'id'        =>  $id,
        ));
    }
    
    public function getDao()
    {
        if (!$this->roleDao) {
            $sm = $this->getServiceLocator();
            $this->roleDao = $sm->get('Role\Dao\RoleDao');
        }
        
        return $this->roleDao;
    }

}
