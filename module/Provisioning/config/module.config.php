<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'domain' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/domain[/:action][/page/:page][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'DomainController',
                        'action'     => 'index',
                    ),
                ),
            ),

             
            'product' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/product[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'ProductController',
                        'action'     => 'index',
                    ),
                ),
            ),
            
            /*'group' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/group[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',                       
                    ),
                    'defaults' => array(
                        'controller' => 'GroupController',
                        'action'     => 'index',
                        'id'        => '[0-9]*', 
                    ),
                ),
            ),*/
            
            'services' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/services[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',                       
                    ),
                    'defaults' => array(
                        'controller' => 'ServicesController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'servicepack' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/servicepack[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',                       
                    ),
                    'defaults' => array(
                        'controller' => 'ServicepackController',
                        'action'     => 'index',
                    ),
                ),
            ),

            'typec' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/typec[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',                       
                    ),
                    'defaults' => array(
                        'controller' => 'TypecController',
                        'action'     => 'index',
                    ),
                ),
            ),

            'grouptc' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/grouptc[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',                       
                    ),
                    'defaults' => array(
                        'controller' => 'GrouptcController',
                        'action'     => 'index',
                    ),
                ),
            ),


            'structure' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/structure[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',                       
                    ),
                    'defaults' => array(
                        'controller' => 'StructureController',
                        'action'     => 'index',
                    ),
                ),
            ),

            'productservices' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/productservices[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',                       
                    ),
                    'defaults' => array(
                        'controller' => 'ProductServicesController',
                        'action'     => 'index',
                        'id'        => '[0-9]*', 
                    ),
                ),
            ),
            
            'case' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/case[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'CaseController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'device' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/device[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'DeviceController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'xmltemplate' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/xmltemplate[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'XmlTemplateController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'order' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/order[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'OrderController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'resource' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/resource[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'ResourceController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'configuration' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/configuration[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'ConfigurationController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'platform' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/platform[/:action][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'PlatformController',
                        'action'     => 'index',
                    ),
                ),
            ),
            'provider' => array(
                'type' => 'segment',
                'options' => array(
                    'route'    => '/provider[/:action][/page/:page][/:id]',
                    'constraints' => array(
                        'action'    => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'        => '[0-9]*',
                    ),
                    'defaults' => array(
                        'controller' => 'ProviderController',
                        'action'     => 'index',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'ProductController'         => 'Provisioning\Controller\ProductController',
            'CaseController'            => 'Provisioning\Controller\CaseController',
            'DeviceController'          => 'Provisioning\Controller\DeviceController',
            'XmlTemplateController'     => 'Provisioning\Controller\XmlTemplateController',
            'OrderController'           => 'Provisioning\Controller\OrderController',
            'ResourceController'        => 'Provisioning\Controller\ResourceController',
            'ConfigurationController'   => 'Provisioning\Controller\ConfigurationController',
            'DomainController'          => 'Provisioning\Controller\DomainController',
            'PlatformController'        => 'Provisioning\Controller\PlatformController',
            'ProviderController'        => 'Provisioning\Controller\ProviderController',
            'ServicesController'        => 'Provisioning\Controller\ServicesController',
            'ProductServicesController' => 'Provisioning\Controller\ProductServicesController',
            //'GroupController'           => 'Provisioning\Controller\GroupController',
            'StructureController'       => 'Provisioning\Controller\StructureController',
            'GrouptcController'         => 'Provisioning\Controller\GrouptcController',
            'TypecController'           => 'Provisioning\Controller\TypecController',

            'ServicepackController'     => 'Provisioning\Controller\ServicepackController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        /*'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/authentication/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),*/
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map' => array(
            'paginator-order'           => __DIR__ . '/../view/layout/slideOrderPaginator.phtml',
            'paginator-domain'          => __DIR__ . '/../view/layout/slideDomainPaginator.phtml',
            'paginator-provider'        => __DIR__ . '/../view/layout/slideProviderPaginator.phtml',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
