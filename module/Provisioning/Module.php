<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Provisioning\Model\Product;
use Provisioning\Model\ProductServices;
use Provisioning\Model\nxCase;
use Provisioning\Model\XmlTemplate;
use Provisioning\Model\Variable;
use Provisioning\Model\Order;
use Provisioning\Model\Customer;
use Provisioning\Model\Group;
use Provisioning\Model\Device;
use Provisioning\Model\User;
use Provisioning\Model\CaseXmlTemplate;
use Provisioning\Model\Xml;
use Provisioning\Model\Message;
use Provisioning\Model\Queue;
use Provisioning\Model\NumberStock;
use Provisioning\Model\Domain;
use Provisioning\Model\DomainAdd;
use Provisioning\Model\ProviderAdd;
use Provisioning\Model\Platform;
use Provisioning\Model\Services;
use Provisioning\Model\Provider;
use Provisioning\Model\ProviderDomain;
use Provisioning\Model\Dn;
use Provisioning\Model\GroupServiceProvider;
use Provisioning\Model\UserServiceAssign;
use Provisioning\Model\ServiceProviderEmpresaAdd;
use Provisioning\Model\UserModifyAdd;
use Provisioning\Model\AdditionLine;
use Provisioning\Model\ChangeNumber;
use Provisioning\Model\DeleteLine;
use Provisioning\Model\CreationGroup;
use Provisioning\Model\DeviceType;
use Provisioning\Model\DeleteGroup;
use Provisioning\Model\Structure;
use Provisioning\Model\Grouptc;
use Provisioning\Model\Typec;
use Provisioning\Model\Servicepack;
use Provisioning\Model\ServicepackService;


use Provisioning\Dao\AdditionLineDao;
use Provisioning\Dao\ProductDao;
use Provisioning\Dao\ProductServicesDao;
use Provisioning\Dao\CaseDao;
use Provisioning\Dao\XmlTemplateDao;
use Provisioning\Dao\VariableDao;
use Provisioning\Dao\OrderDao;
use Provisioning\Dao\CustomerDao;
use Provisioning\Dao\GroupDao;
use Provisioning\Dao\DeviceDao;
use Provisioning\Dao\UserDao;
use Provisioning\Dao\CaseXmlTemplateDao;
use Provisioning\Dao\XmlDao;
use Provisioning\Dao\MessageDao;
use Provisioning\Dao\QueueDao;
use Provisioning\Dao\NumberStockDao;
use Provisioning\Dao\DomainDao;
use Provisioning\Dao\DomainAddDao;
use Provisioning\Dao\ProviderAddDao;
use Provisioning\Dao\PlatformDao;
use Provisioning\Dao\ProviderDao;
use Provisioning\Dao\ServicesDao;
use Provisioning\Dao\ProviderDomainDao;
use Provisioning\Dao\DnDao;
use Provisioning\Dao\GroupServiceProviderDao;
use Provisioning\Dao\UserServiceAssignDao;
use Provisioning\Dao\ServiceProviderEmpresaAddDao;
use Provisioning\Dao\UserModifyAddDao;
use Provisioning\Dao\ChangeNumberDao;
use Provisioning\Dao\DeleteLineDao;
use Provisioning\Dao\CreationGroupDao;
use Provisioning\Dao\DeviceTypeDao;
use Provisioning\Dao\DeleteGroupDao;
use Provisioning\Dao\StructureDao;
use Provisioning\Dao\GrouptcDao;
use Provisioning\Dao\TypecDao;
use Provisioning\Dao\ServicepackDao;
use Provisioning\Dao\ServicepackServiceDao;


use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Qv'          => __DIR__ . '/../../vendor/Qv',
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array(

                'Provisioning\Dao\TypecDao' => function($sm) {
                    $tableGateway = $sm->get('TypecTableGateway');
                    $table = new TypecDao($tableGateway);
                    return $table;
                },
                'TypecTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Typec());
                    return new TableGateway('nx_type_client', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\GrouptcDao' => function($sm) {
                    $tableGateway = $sm->get('GrouptcTableGateway');
                    $table = new GrouptcDao($tableGateway);
                    return $table;
                },
                'GrouptcTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Grouptc());
                    return new TableGateway('nx_grouptc', $dbAdapter, null, $resultSetPrototype);
                },

                'Provisioning\Dao\StructureDao' => function($sm) {
                    $tableGateway = $sm->get('StructureTableGateway');
                    $table = new StructureDao($tableGateway);
                    return $table;
                },
                'StructureTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Structure());
                    return new TableGateway('nx_structure', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\DeleteGroupDao' => function($sm) {
                    $tableGateway = $sm->get('DeleteGroupTableGateway');
                    $table = new DeleteGroupDao($tableGateway);
                    return $table;
                },
                'DeleteGroupTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DeleteGroup());
                    return new TableGateway('nx_deletegroup', $dbAdapter, null, $resultSetPrototype);
                },

                'Provisioning\Dao\DeviceTypeDao' => function($sm) {
                    $tableGateway = $sm->get('DeviceTypeTableGateway');
                    $table = new DeviceTypeDao($tableGateway);
                    return $table;
                },
                'DeviceTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DeviceType());
                    return new TableGateway('bs_device_type', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\CreationGroupDao' => function($sm) {
                    $tableGateway = $sm->get('CreationGroupTableGateway');
                    $table = new CreationGroupDao($tableGateway);
                    return $table;
                },
                'CreationGroupTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CreationGroup());
                    return new TableGateway('nx_creationgroup', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\DeleteLineDao' => function($sm) {
                    $tableGateway = $sm->get('DeleteLineTableGateway');
                    $table = new DeleteLineDao($tableGateway);
                    return $table;
                },
                'DeleteLineTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DeleteLine());
                    return new TableGateway('nx_deleteline', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\ChangeNumberDao' => function($sm) {
                    $tableGateway = $sm->get('ChangeNumberTableGateway');
                    $table = new ChangeNumberDao($tableGateway);
                    return $table;
                },
                'ChangeNumberTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ChangeNumber());
                    return new TableGateway('nx_changenumber', $dbAdapter, null, $resultSetPrototype);
                },

                'Provisioning\Dao\AdditionLineDao' => function($sm) {
                    $tableGateway = $sm->get('AdditionLineTableGateway');
                    $table = new AdditionLineDao($tableGateway);
                    return $table;
                },
                'AdditionLineTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AdditionLine());
                    return new TableGateway('nx_additionline', $dbAdapter, null, $resultSetPrototype);
                },

                'Provisioning\Dao\GroupDao' => function($sm) {
                    $tableGateway = $sm->get('GroupTableGateway');
                    $table = new GroupDao($tableGateway);
                    return $table;
                },
                'GroupTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Group());
                    return new TableGateway('bs_group', $dbAdapter, null, $resultSetPrototype);
                },



                'Provisioning\Dao\UserModifyAddDao' => function($sm) {
                    $tableGateway = $sm->get('UserModifyAddTableGateway');
                    $table = new UserModifyAddDao($tableGateway);
                    return $table;
                },
                'UserModifyAddTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserModifyAdd());
                    return new TableGateway('nx_user_modify', $dbAdapter, null, $resultSetPrototype);
                },



                'Provisioning\Dao\ServiceProviderEmpresaAddDao' => function($sm) {
                    $tableGateway = $sm->get('ServiceProviderEmpresaAddTableGateway');
                    $table = new ServiceProviderEmpresaAddDao($tableGateway);
                    return $table;
                },
                'ServiceProviderEmpresaAddTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ServiceProviderEmpresaAdd());
                    return new TableGateway('nx_services_provider_order', $dbAdapter, null, $resultSetPrototype);
                },



                'Provisioning\Dao\ProviderAddDao' => function($sm) {
                    $tableGateway = $sm->get('ProviderAddTableGateway');
                    $table = new ProviderAddDao($tableGateway);
                    return $table;
                },
                'ProviderAddTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProviderAdd());
                    return new TableGateway('nx_provider_service', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\DomainAddDao' => function($sm) {
                    $tableGateway = $sm->get('DomainAddTableGateway');
                    $table = new DomainAddDao($tableGateway);
                    return $table;
                },
                'DomainAddTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DomainAdd());
                    return new TableGateway('nx_domain_order', $dbAdapter, null, $resultSetPrototype);
                },




                'Provisioning\Dao\UserServiceAssignDao' => function($sm) {
                    $tableGateway = $sm->get('UserServiceAssignTableGateway');
                    $table = new UserServiceAssignDao($tableGateway);
                    return $table;
                },
                'UserServiceAssignTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new UserServiceAssign());
                    return new TableGateway('nx_userserviceassign', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\GroupServiceProviderDao' => function($sm) {
                    $tableGateway = $sm->get('GroupServiceProviderTableGateway');
                    $table = new GroupServiceProviderDao($tableGateway);
                    return $table;
                },
                'GroupServiceProviderTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new GroupServiceProvider());
                    return new TableGateway('nx_groupserviceprovider', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\ProductServicesDao' => function($sm) {
                    $tableGateway = $sm->get('ProductServicesTableGateway');
                    $table = new ProductServicesDao($tableGateway);
                    return $table;
                },
                'ProductServicesTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProductServices());
                    return new TableGateway('nx_product_services', $dbAdapter, null, $resultSetPrototype);
                },


                'Provisioning\Dao\ProductDao' => function($sm) {
                    $tableGateway = $sm->get('ProductTableGateway');
                    $table = new ProductDao($tableGateway);
                    return $table;
                },
                'ProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Product());
                    return new TableGateway('nx_product', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\ServicesDao' => function($sm) {
                    $tableGateway = $sm->get('ServicesTableGateway');
                    $table = new ServicesDao($tableGateway);
                    return $table;
                },
                'ServicesTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Services());
                    return new TableGateway('bs_service', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\CaseDao' => function($sm) {
                    $tableGateway = $sm->get('CaseTableGateway');
                    $table = new CaseDao($tableGateway);
                    return $table;
                },
                'CaseTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new nxCase());
                    return new TableGateway('nx_case', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\XmlTemplateDao' => function($sm) {
                    $tableGateway = $sm->get('XmlTemplateTableGateway');
                    $table = new XmlTemplateDao($tableGateway);
                    return $table;
                },
                'XmlTemplateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new XmlTemplate());
                    return new TableGateway('nx_xmltemplate', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\VariableDao' => function($sm) {
                    $tableGateway = $sm->get('VariableTableGateway');
                    $table = new VariableDao($tableGateway);
                    return $table;
                },
                'VariableTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Variable());
                    return new TableGateway('nx_variable', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\OrderDao' => function($sm) {
                    $tableGateway = $sm->get('OrderTableGateway');
                    $table = new OrderDao($tableGateway);
                    return $table;
                },
                'OrderTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Order());
                    return new TableGateway('nx_order', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\CustomerDao' => function($sm) {
                    $tableGateway = $sm->get('CustomerTableGateway');
                    $table = new CustomerDao($tableGateway);
                    return $table;
                },
                'CustomerTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Customer());
                    return new TableGateway('nx_customer', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\GroupDao' => function($sm) {
                    $tableGateway = $sm->get('BGroupTableGateway');
                    $table = new GroupDao($tableGateway);
                    return $table;
                },
                'BGroupTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Group());
                    return new TableGateway('bs_group', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\UserDao' => function($sm) {
                    $tableGateway = $sm->get('BUserTableGateway');
                    $table = new UserDao($tableGateway);
                    return $table;
                },
                'BUserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('bs_user', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\CaseXmlTemplateDao' => function($sm) {
                    $tableGateway = $sm->get('CaseXmlTemplateTableGateway');
                    $table = new CaseXmlTemplateDao($tableGateway);
                    return $table;
                },
                'CaseXmlTemplateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CaseXmlTemplate());
                    return new TableGateway('nx_case_xmltemplate', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\XmlDao' => function($sm) {
                    $tableGateway = $sm->get('XmlTableGateway');
                    $table = new XmlDao($tableGateway);
                    return $table;
                },
                'XmlTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Xml());
                    return new TableGateway('nx_xml', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\DeviceDao' => function($sm) {
                    $tableGateway = $sm->get('DeviceTableGateway');
                    $table = new DeviceDao($tableGateway);
                    return $table;
                },
                'DeviceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Device());
                    return new TableGateway('bs_device', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\NumberStockDao' => function($sm) {
                    $tableGateway = $sm->get('NumberStockTableGateway');
                    $table = new NumberStockDao($tableGateway);
                    return $table;
                },
                'NumberStockTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new NumberStock());
                    return new TableGateway('bs_number_stock', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\DomainDao' => function($sm) {
                    $tableGateway = $sm->get('DomainTableGateway');
                    $table = new DomainDao($tableGateway);
                    return $table;
                },
                'DomainTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Domain());
                    return new TableGateway('bs_domain', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\PlatformDao' => function($sm) {
                    $tableGateway = $sm->get('PlatformTableGateway');
                    $table = new PlatformDao($tableGateway);
                    return $table;
                },
                'PlatformTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Platform());
                    return new TableGateway('nx_platform', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\ProviderDao' => function($sm) {
                    $tableGateway = $sm->get('ProviderTableGateway');
                    $table = new ProviderDao($tableGateway);
                    return $table;
                },
                'ProviderTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Provider());
                    return new TableGateway('bs_service_provider', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\ProviderDomainDao' => function($sm) {
                    $tableGateway = $sm->get('ProviderDomainTableGateway');
                    $table = new ProviderDomainDao($tableGateway);
                    return $table;
                },
                'ProviderDomainTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ProviderDomain());
                    return new TableGateway('bs_service_provider_domain', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\DnDao' => function($sm) {
                    $tableGateway = $sm->get('DnTableGateway');
                    $table = new DnDao($tableGateway);
                    return $table;
                },
                'DnTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Dn());
                    return new TableGateway('bs_dn', $dbAdapter, null, $resultSetPrototype);
                },

                'Provisioning\Dao\ServicepackDao' => function($sm) {
                    $tableGateway = $sm->get('ServicepackTableGateway');
                    $table = new ServicepackDao($tableGateway);
                    return $table;
                },
                'ServicepackTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Servicepack());
                    return new TableGateway('bs_servicepack', $dbAdapter, null, $resultSetPrototype);
                },

                'Provisioning\Dao\ServicepackServiceDao' => function($sm) {
                    $tableGateway = $sm->get('ServicepackServiceTableGateway');
                    $table = new ServicepackServiceDao($tableGateway);
                    return $table;
                },
                'ServicepackServiceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ServicepackService());
                    return new TableGateway('bs_servicepack_service', $dbAdapter, null, $resultSetPrototype);
                },


                /* 
                ** Bloque para manejo de colas
                */
                'Provisioning\Dao\MessageDao' => function($sm) {
                    $tableGateway = $sm->get('MessageTableGateway');
                    $table = new MessageDao($tableGateway);
                    return $table;
                },
                'MessageTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Message());
                    return new TableGateway('nx_message', $dbAdapter, null, $resultSetPrototype);
                },
                'Provisioning\Dao\QueueDao' => function($sm) {
                    $tableGateway = $sm->get('QueueTableGateway');
                    $table = new QueueDao($tableGateway);
                    return $table;
                },
                'QueueTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Queue());
                    return new TableGateway('nx_queue', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }
}
