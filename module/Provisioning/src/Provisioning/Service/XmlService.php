<?php
/**
 * @package Provisioning
 * @author Rodolfo Saenz
 * @version
 **/
 
namespace Provisioning\Service;

use Provisioning\Model\XmlResponse;

use Provisioning\Model\Product;
use Provisioning\Model\nxCase;
use Provisioning\Model\XmlTemplate;
use Provisioning\Model\Variable;
use Provisioning\Model\Order;
use Provisioning\Model\Customer;
use Provisioning\Model\Group;
use Provisioning\Model\Device;
use Provisioning\Model\User;
use Provisioning\Model\CaseXmlTemplate;
use Provisioning\Model\Xml;
use Provisioning\Model\Message;
use Provisioning\Model\Queue;
use Provisioning\Model\NumberStock;
use Provisioning\Model\Domain;
use Provisioning\Model\Platform;
use Provisioning\Model\Provider;
use Provisioning\Model\ProviderDomain;
use Provisioning\Model\Dn;


class XmlService
{

    private $xml;
    private $variables;

    private $serviceDao;
    private $serviceLocator;

    public function __construct($serviceLocator){
        $this->serviceLocator = $serviceLocator;
    }

    public function mergeArray($xml = null, $variables = null, $array = null) 
    {
       
            $variablesArray = array();
            foreach ($variables as $variable) {
                $key        = $variable->name;
                $object     = $variable->mappingObject      ?   $variable->mappingObject    : $variable->mapping_object;
                $attribute  = $variable->mappingAttribute   ?   $variable->mappingAttribute : $variable->mapping_attribute;
                $treatment  = $variable->treatment;

                $value                  = $array[$attribute];
                $variablesArray[$key]   = $value;
            }

            $xmlGenerated = $this->merge($xml, $variablesArray);
            return $xmlGenerated;

    }

    public function mergeOrder($xml = null, $variables = null, $instance = null) 
    {

            $classpathArray = explode("\\", get_class($instance));
            $instancename = trim($classpathArray[count($classpathArray)-1]);

            $variablesArray = array();
            foreach ($variables as $variable) {

                $key        = $variable->name;
                $object     = $variable->mappingObject      ?   $variable->mappingObject    : $variable->mapping_object;
                $attribute  = $variable->mappingAttribute   ?   $variable->mappingAttribute : $variable->mapping_attribute;
                $treatment  = $variable->treatment;

                $relationId = strtolower($object) . "Id";
                $value = "";

                if($object == "Preference"){
                    $objectDao  = $this->getDao("PreferenceDao","Preference");
                    $instance   = $objectDao->getByName($attribute);
                    $value      = isset($instance->value)?$instance->value:"";
                } elseif( $object == $instancename){
                    $value  = $instance->$attribute;
                } elseif(property_exists($instance,$relationId)) {
                    $daoName    = $object . "Dao";
                    $dao        = $this->getDao($daoName);
                    $relationInstance = $dao->get($instance->$relationId);
                    $value = $relationInstance->$attribute;
                } else {

                    $relation_level1            = $instance;
                    
                    /** Obtenemos los atributos algoId de la clase niver 1 **/
                    $class_vars_level1          = get_class_vars(get_class($relation_level1));
                    $relationAttributes_level1  = array();
                    foreach ($class_vars_level1 as $attributeName => $value) {
                        if(substr($attributeName, -2) === "Id"){
                            array_push($relationAttributes_level1, $attributeName);
                        }
                    }

                    /** Recorremos cada atributo algoId de la clase nivel 1, instanciamos
                        y buscamos la relacion en la clase nivel 2
                    **/
                    foreach ($relationAttributes_level1 as $attributeId_level1) {

                        $attrExploded           = explode("Id",$attributeId_level1);
                        $relationName_level2    = trim($attrExploded[0]);
                        $relationObject_level2  = "Provisioning\Model\\" . ucfirst($relationName_level2);
                        eval("\$relation_level2 = new " . $relationObject_level2 . "();");
                        //$relation = new $relationObject();
                        if(property_exists($relation_level2,$relationId)) {
                            /** Si la relacion existe, buscamos el valor en el siguiente nivel, osea el 3 **/

                            $daoName_level2             = ucfirst($relationName_level2) . "Dao";
                            $dao_level2                 = $this->getDao($daoName_level2);
                            $relationInstance_level2    = $dao_level2->get($relation_level1->$attributeId_level1);

                            /** Obtenemos el valor de la clase nivel 3 (object) **/
                            $daoName_level3             = $object . "Dao";
                            $dao_level3                 = $this->getDao($daoName_level3);
                            $relationInstance_level3    = $dao_level3->get($relationInstance_level2->$relationId);
                            $value                      = $relationInstance_level3->$attribute;

                        } else {
       
                            /** Obtenemos los atributos algoId de la clase niver 2 **/
                            $class_vars_level2          = get_class_vars(get_class($relation_level2));
                            $relationAttributes_level2  = array();
                            foreach ($class_vars_level2 as $attributeName => $value) {
                                if(substr($attributeName, -2) === "Id"){
                                    array_push($relationAttributes_level2, $attributeName);
                                }
                            }

                            /** Recorremos cada atributo algoId de la clase nivel 2, instanciamos
                                y buscamos la relacion en la clase nivel 3
                            **/
                            foreach ($relationAttributes_level2 as $attributeId_level2) {

                                $attrExploded           = explode("Id",$attributeId_level2);
                                $relationName_level3    = trim($attrExploded[0]);
                                $relationObject_level3  = "Provisioning\Model\\" . ucfirst($relationName_level3);
                                eval("\$relation_level3 = new " . $relationObject_level3 . "();");
                                //$relation = new $relationObject();
                                if(property_exists($relation_level3,$relationId)) {

                                    /** Si la relacion existe, buscamos el valor en el siguiente nivel, osea el 3 **/
                                    $daoName_level2             = ucfirst($relationName_level2) . "Dao";
                                    $dao_level2                 = $this->getDao($daoName_level2);
                                    $relationInstance_level2    = $dao_level2->get($relation_level1->$attributeId_level1);
                                    
                                    /** Si la relacion existe, buscamos el valor en el siguiente nivel, osea el 3 **/
                                    $daoName_level3             = ucfirst($relationName_level3) . "Dao";
                                    $dao_level3                 = $this->getDao($daoName_level3);
                                    $relationInstance_level3    = $dao_level3->get($relationInstance_level2->$attributeId_level2);

                                    /** Obtenemos el valor de la clase nivel 4 (object) **/
                                    $daoName_level4             = $object . "Dao";
                                    $dao_level4                 = $this->getDao($daoName_level4);
                                    $relationInstance_level4    = $dao_level4->get($relationInstance_level3->$relationId);
                                    $value                      = $relationInstance_level4->$attribute;
                                    break;
                                } 
                            }
                        }

                        if(!empty($value)) break;
                    }
                }

                
                switch ($treatment) {
                    case "concat":
                        $array = explode(" ", $value);
                        $value = implode("_", $array);
                        break;
                    case "initials":
                        $array = explode(" ", $value);
                        $initials = "";
                        foreach ($array as $v) {
                            $initials .= strtoupper($v[0]);
                        }
                        $value = $initials;
                        break;
                    default:
                        break;
                }
                
                $variablesArray[$key] = $value;
            }
            
            $xmlGenerated = $this->merge($xml, $variablesArray);
            
            return $xmlGenerated;

    }

    
    
    public function merge($body, $vars)
    {
        if (count($vars) == 0) {
            return $body;
        }

        $patterns = array();
        $replacements = array();
        foreach ($vars as $key => $value) {
            $patterns[] = '/' . $key . '/i';
            $replacements[] = $this->cleanUp($value);
        }

        if (null === ($body = preg_replace($patterns, $replacements, $body))) {
            throw new \Exception('The message could not be merged');
        }

        return $body;
    }
    
    private function cleanUp($var)
    {
        $workingCopy = $var;

        //Additional clean ups should be implemented here
        //utf8 encoding
        //replacing HTML entities via html_entity_decode/encode as
        //the message should go with tildes inside it

        $workingCopy = trim($var);

        return $workingCopy;
    }

    public function getResponseFromXml($xml){

            $xmlresponse = new XmlResponse();

            if(strpos(strtolower($xml),'errorresponse') !== false){
                $xmlresponse->setResult("error");
            } else {
                $xmlresponse->setResult("success");
            }


            $summary = "";
            if(strtolower($xmlresponse->getResult()) == "error"){
                $start  = "<summary>";
                $end    = "</summary>";
                $summary  = substr($xml, 
                    strpos($xml, $start) + strlen($start), 
                    strpos($xml, $end)-(strpos($xml, $start) + strlen($start))
                    );
                $xmlresponse->setSummary("Broadsoft: " . $summary);
            }

            return $xmlresponse;
    }

    public function getCommnadResponseFromXml($xml){

            $xmlresponse = new XmlResponse();

            if(strpos(strtolower($xml),'errorresponse') !== false){
                $xmlresponse->setResult("error");
            } else {
                $xmlresponse->setResult("success");
            }

            $command = substr($xml,
                strpos($xml,'<command'),
                strpos($xml,'</command>') - strpos($xml,'<command') + strlen('</command>')
            );


            /*$commandplain = substr_replace($command,">",
                strpos($command,'<command') + strlen('<command'),
                strpos($command,'Response">') + strlen('Response">')-1);*/
            //$commandplain = preg_replace('/<command\(\S\|\s\)\*>/','<command>',$command);            

            $simplexml = simplexml_load_string($command);
            if(strtolower($xmlresponse->getResult()) != "error"){
                $xmlresponse->setCommand($simplexml);
            }

            $summary = "";
            if(strtolower($xmlresponse->getResult()) == "error"){
                $start  = "<summary>";
                $end    = "</summary>";
                $summary  = substr($xml, 
                    strpos($xml, $start) + strlen($start), 
                    strpos($xml, $end)-(strpos($xml, $start) + strlen($start))
                    );
                $xmlresponse->setSummary("Broadsoft: " . $summary);
            }

            return $xmlresponse;
    }

    public function getDao($dao = "OrderDao",$namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->serviceDao;
    }

    public function getServiceLocator(){
        return $this->serviceLocator;
    }
  
}