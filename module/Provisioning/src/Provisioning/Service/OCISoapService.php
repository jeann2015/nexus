<?php
/**
 * @package Provisioning
 * @author Rodolfo Saenz
 * @version
 **/
 
namespace Provisioning\Service;

use Zend\Soap\Client;

class OCISoapService
{

    private $client; 
    private $sessionId;
    private $nonce;
    private $userId;
    private $password;
    private $wsdl;		
    private $error = 0;
    private $xml;
    private $dao;
    private $serviceLocator;
    private $currentUser;

    public function __construct($serviceLocator){

        $this->serviceLocator   = $serviceLocator;
        $this->currentUser      = (object)$this->serviceLocator->get('AuthService')->getIdentity();
       	$this->sessionId 	    = time() . uniqid();
        $this->preparePlatform();
    }

    private function preparePlatform(){
        $platformDao        = $this->getDao("PlatformDao","Provisioning");
        $platform           = $platformDao->get($this->currentUser->platform_id);

        $ip                 = $platform->ip;
        $wsdl               = $platform->wsdl       == "" ? "/"     : $platform->wsdl;
        $protocol           = $platform->protocol   == "" ? "http"  : $platform->protocol;
        $port               = $platform->port       == "" ? "80"    : $platform->port;
        $this->userId       = $platform->username;
        $this->password     = base64_decode($platform->password);

        $this->wsdl         = $protocol . "://" . $ip . ":" . $port . $wsdl;
        $this->client       = new Client($this->wsdl);
    }

    public function send($xml){
    	$this->xml = $xml;

    	$this->authenticate();
    	$this->login();
    	$this->populateSessionId();
		$response = $this->client->processOCIMessage(array('in0' =>  $this->xml ));    	
		$this->logout();
    	return $response->processOCIMessageReturn;
    }

    private function authenticate(){

        $xmltemplateFetch = $this->getDao("XmlTemplateDao")->fetchByCategory('authenticate');
        $xmltemplate = $xmltemplateFetch->current();
        $xml = $xmltemplate->xml;
        $vars = array("%SESSIONID%" => $this->sessionId, "%USERID%" => $this->userId, );
        $xml = $this->merge($xml,$vars);

		$response = $this->client->processOCIMessage(array('in0' =>  $xml ));

		if (preg_match('|<nonce>(\d+)|', $response->processOCIMessageReturn, $n)) {
		    $this->nonce = $n[1];
            $this->password = md5($this->nonce. ":" .sha1($this->password));
		}

		return $this;
    }

    private function login(){

        $xmltemplateFetch = $this->getDao("XmlTemplateDao")->fetchByCategory('login');
        $xmltemplate = $xmltemplateFetch->current();
        $xml = $xmltemplate->xml;
        $vars = array("%SESSIONID%" => $this->sessionId, "%USERID%" => $this->userId, "%PASSWORD%" => $this->password,);
        $xml = $this->merge($xml,$vars);

		$response = $this->client->processOCIMessage(array('in0' =>  $xml ));

		return $this;
    }

    private function logout(){

        $xmltemplateFetch = $this->getDao("XmlTemplateDao")->fetchByCategory('logout');
        $xmltemplate = $xmltemplateFetch->current();
        $xml = $xmltemplate->xml;
        $vars = array("%SESSIONID%" => $this->sessionId, "%USERID%" => $this->userId,);
        $xml = $this->merge($xml,$vars);

		$response = $this->client->processOCIMessage(array('in0' =>  $xml ));

		return $this;
    }

    private function populateSessionId(){
    	$toSearch = '<sessionId xmlns=""></sessionId>';
    	$toReplace = '<sessionId xmlns="">'.$this->sessionId.'</sessionId>';
    	$this->xml = str_replace($toSearch,$toReplace,$this->xml);
    	return $this;
    }

    public function getDao($dao = "XmlTemplateDao", $namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->dao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->dao;
    }

    public function getServiceLocator(){
        return $this->serviceLocator;
    }

    private function merge($body, $vars)
    {
        if (count($vars) == 0) {
            return $body;
        }

        $patterns = array();
        $replacements = array();
        foreach ($vars as $key => $value) {
            $patterns[] = '/' . $key . '/i';
            $replacements[] = $this->cleanUp($value);
        }

        if (null === ($body = preg_replace($patterns, $replacements, $body))) {
            throw new \Exception('The message could not be merged');
        }

        return $body;
    }
    
    private function cleanUp($var)
    {
        $workingCopy = $var;

        //Additional clean ups should be implemented here
        //utf8 encoding
        //replacing HTML entities via html_entity_decode/encode as
        //the message should go with tildes inside it

        $workingCopy = trim($var);

        return $workingCopy;
    }
  
}