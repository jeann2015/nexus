<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class UserAddalForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');     
        
        $this->add(array(
            'name' => 'userid',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'userid',
                'required' => false,
                 'autofocus' =>true,
                'class' => 'form-control',
                'placeholder'=> 'IDUser',
            ),
        ));
        
        $this->add(array(
            'name' => 'firstName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'firstName',
                'required' => false,
                'class' => 'form-control',
                'placeholder'=> 'Nombre',
            ),
        ));

        $this->add(array(
            'name' => 'lastname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'lastname',
                'required' => false,
                'autofocus' =>true,
                'class' => 'form-control',
                'placeholder'=> 'Apellido',
            ),
        ));

         $this->add(array(
            'name' => 'callingLineIdLastName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'callingLineIdLastName',
                'required' => false,
                'autofocus' =>true,
                'class' => 'form-control',
                'placeholder'=> 'callingLineIdLastName',
            ),
        ));

         $this->add(array(
            'name' => 'callingLineIdFirstName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'callingLineIdFirstName',
                'required' => false,
                'autofocus' =>true,
                'class' => 'form-control',
                'placeholder'=> 'callingLineIdFirstName',
            ),
        ));

          $this->add(array(
            'name' => 'language',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'language',
                'required' => false,
                'autofocus' =>true,
                'value' =>'Spanish',
                'class' => 'form-control',
                'placeholder'=> 'Lenguaje',
            ),
        ));

          $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'timezone',
            'attributes' => array(
                'id'    => 'timezone',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '',
                'value_options'=> \DateTimeZone::listIdentifiers(),
                'default_option'=> 'America/Panama',
            ),
        ));
        
       

        

        

       
    }
}
