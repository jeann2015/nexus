<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class PlatformForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
        
        /**
         * From here is customer information
         */
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name'
            ),
        ));

        $this->add(array(
            'name' => 'ip',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'ip'
            ),
        ));
        
        
        $this->add(array(
            'name' => 'protocol',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'protocol'
            ),
        ));
        
        $this->add(array(
            'name' => 'port',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'port'
            ),
        ));
        
        $this->add(array(
            'name' => 'wsdl',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'wsdl',
            ),
        ));

        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'username',
            ),
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'password',
            ),
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name'  => 'active',
            'attributes' => array(
                'id'    => 'active',
                'class' => 'form-control',
            ),
            'options' => array(
                 'checked_value'    => '1',
                 'unchecked_value'  => '0'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'class',
            'attributes' => array(
                'id'    => 'class',
                'class' => 'form-control',
            ),
            'options' => array(
                 'value_options'    => array(
                    'default'   => 'Default',
                    'info'      => 'Info',
                    'primary'   => 'Primary',
                    'success'   => 'Success',
                    'warning'   => 'Warning',
                    'danger'    => 'Danger',
                 ),
            ),
        ));
    }
}
