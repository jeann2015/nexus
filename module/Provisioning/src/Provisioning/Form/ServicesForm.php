<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class ServicesForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name',
                'class' => 'form-control',
            ),
        ));

       

           $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'category',
            'attributes' => array(
                'id'    => 'category',
                'class' => 'form-control',
                
            ),
            'options' => array(
                    
                     'value_options' => array(
                             '1'          => 'Usuarios',
                             '2'       => 'Grupos'
                     ),
             )
        ));
        

        
    }
}
