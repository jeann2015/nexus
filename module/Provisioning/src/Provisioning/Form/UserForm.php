<?php

namespace Provisioning\Form;

use Qv\Language;
use Generic\Form\GenericForm;

class UserForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
        
        /**
         * From here is customer information
         */
        $this->add(array(
            'name' => 'firstname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'firstname'
            ),
        ));

        $this->add(array(
            'name' => 'lastname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'lastname'
            ),
        ));
        
        
        $this->add(array(
            'name' => 'userid',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'userid'
            ),
        ));
        
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'password',
                'id'    => 'password'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'language',
            'attributes' => array(
                'id'    => 'language',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '',
                //'value_options'=> Language::getListNames(),
                'default_option'=> 'Spanish',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'timezone',
            'attributes' => array(
                'id'    => 'timezone',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '',
                'value_options'=> \DateTimeZone::listIdentifiers(),
                'default_option'=> 'America/Panama',
            ),
        ));

        $this->add(array(
            'name' => 'flag',
            'attributes' => array(
                'type'  => 'checkbox',
                'id'    => 'flag'
            ),
        ));
    }
}

