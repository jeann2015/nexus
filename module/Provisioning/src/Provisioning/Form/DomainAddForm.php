<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class DomainAddForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        /*$this->add(array(
            'name' => 'id_domain_order',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id_domain_order'
                
            ),
        ));*/
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'id_domain',
            'attributes' => array(
                'id'    => 'id_domain',
                'required' => true,
                'class' => 'form-control',
            ),
            
        ));
        

        

        
    }
}
