<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;
use Zend\Form\Element\Radio;

class XmlTemplateForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name'
            ),
        ));
        
        $this->add(array(
            'name' => 'command',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'command'
            ),
        ));
        
        $this->add(array(
            'name' => 'xml',
            'attributes' => array(
                'type'  => 'textarea',
                'id'    => 'xml'
            ),
        ));

        $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'category',
             'options' => array(
                    'label' => 'Tipo de XML',
                    'label_attributes' => array(
                        'class'  => 'form-control'
                     ),
                     'value_options' => array(
                             'add'          => 'Creación (add)',
                             'delete'       => 'Eliminación (delete)',
                             'get'          => 'Consulta (get)',
                             'list'         => 'Consulta Listado (list)',
                             'modify'       => 'Modificación (modify)',
                             'authenticate' => 'Autenticación (authenticate)',
                             'login'        => 'Log In (login)',
                             'logout'       => 'Log Out (logout)',
                     ),
             )
        ));

        

        $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'addxmltemplateId',
             'id'   => 'addxmltemplateId',
             'options' => array(
                'label_attributes' => array(
                    'class'  => 'form-control'
                 ),
             ),
             'attributes' => array(
                'id'        =>  'addxmltemplateId',
                'required'  =>  false,
            ),
        ));
    }
}
