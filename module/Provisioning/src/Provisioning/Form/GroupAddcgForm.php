<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class GroupAddcgForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

       

      $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'groupid',
            'attributes' => array(
                'id'    => 'groupid',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                '' => 'Seleccione',
            ),
        ));
        
        $this->add(array(
            'name' => 'id_domain',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id_domain',
                
    
            ),
        ));

        $this->add(array(
            'name' => 'domain',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'domain',
                'required' => false,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Dominio',
    
            ),
        ));
        
        $this->add(array(
            'name' => 'userlimit',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'userlimit',
                'required' => false,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Límite de Usuario',
            ),
        ));
        
        $this->add(array(
            'name' => 'groupname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'groupname',
                'required' => false,
                'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre de Grupo',
            ),
        ));
        
       

        $this->add(array(
            'name' => 'callingLineIdName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'callingLineIdName',
                'required' => true,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Calling LineId Name',
            ),
        ));


        $this->add(array(
            'name' => 'timezone',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'timezone',
                'required' => true,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Time Zone',
            ),
        ));

         $this->add(array(
            'name' => 'contactname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactname',
                'required' => true,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre de Contacto',
            ),
        ));

          $this->add(array(
            'name' => 'contactnumber',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactnumber',
                'required' => true,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Número de Contacto',
            ),
        ));

        

        

        

       
    }
}
