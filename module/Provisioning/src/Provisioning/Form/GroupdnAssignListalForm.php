<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class GroupdnAssignListalForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');     
        
        $this->add(array(
            'name' => 'contactName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactName',
                'required' => false,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre de contacto',
            ),
        ));

        $this->add(array(
            'name' => 'grupo',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'grupo',
                'required' => false,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Grupo',
            ),
        ));
        
        $this->add(array(
            'name' => 'number',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'number',
                'required' => false,
                'autofocus' =>true,
                'class' => 'form-control',
                'placeholder'=> 'Número',
            ),
        ));
        
       

        

        

       
    }
}
