<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class UserModifycgForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

       
        $this->add(array(
            'name' => 'numero',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'numero',
                'required' => false,                 
                'class' => 'form-control',
                'autofocus' =>true,
                'placeholder'=> 'Número de Usuario',
    
            ),
        ));

         $this->add(array(
            'name' => 'deviceLevel',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'deviceLevel',
                'required' => false,                 
                'class' => 'form-control',
                
                'value' =>'Grupo',
                'placeholder'=> 'Nivel del Dispositivo',
    
            ),
        ));

        $this->add(array(
            'name' => 'deviceName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'deviceName',
                'required' => false,                 
                'class' => 'form-control',
                
                'value' =>'ATA_EXT_BLA',
                'placeholder'=> 'Nombre del Dispositivo',
    
            ),
        ));

        $this->add(array(
            'name' => 'lineport',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'lineport',
                'required' => false,                 
                'class' => 'form-control',
                
                'value' =>'',
                'placeholder'=> 'Linea de Puerto',
    
            ),
        ));
    }
}
