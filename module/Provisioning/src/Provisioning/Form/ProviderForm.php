<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class ProviderForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name',
                'required' => true,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'domain',
            'attributes' => array(
                'id'    => 'domain',
                'class' => 'form-control',
                'required' => true,
            ),
        ));
    }
}
