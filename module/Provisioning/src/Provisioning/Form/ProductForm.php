<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class ProductForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'providerId',
            'attributes' => array(
                'id'    => 'providerId',
                'class' => 'form-control',
                 'required' => true,
                
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'domainId',
            'attributes' => array(
                'id'    => 'domainId',
                'class' => 'form-control',
                 'required' => true,
            ),
        ));
        
       
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'services',
            'attributes' => array(
                'id'    => 'services',
                'class' => 'form-control',
                'multiple' => 'multiple',
                 'required' => true,
            ),
        ));     
    }
}
