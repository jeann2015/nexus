<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class GrouptcForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
       
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'description',
                'required' => true,
                'class' => 'form-control',
            ),
        ));

         $this->add(array(
            'name' => 'group',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'group',
                'required' => true,
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'providerId',
            'attributes' => array(
                'id'    => 'providerId',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '',
            ),
        ));

    }
}
