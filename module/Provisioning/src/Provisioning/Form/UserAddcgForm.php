<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class UserAddcgForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

       
        $this->add(array(
            'name' => 'userid',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'userid',
                'required' => false,                 
                'class' => 'form-control',
                'placeholder'=> 'Usuario',
    
            ),
        ));

     
        
         $this->add(array(
            'name' => 'firstname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'firstname',
                'required' => false,
                 
                'class' => 'form-control',
                'placeholder'=> 'Nombre',
    
            ),
        ));
        
        $this->add(array(
            'name' => 'lastname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'lastname',
                'required' => false,
                 
                 
                'class' => 'form-control',
                'placeholder'=> 'Apellido',
            ),
        ));

        $this->add(array(
            'name' => 'callingLineidfirstname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'callingLineidfirstname',
                'required' => false,
                 
                 
                'class' => 'form-control',
                'placeholder'=> 'Nombre de identificación de Línea de Llamada',
            ),
        ));

          $this->add(array(
            'name' => 'callinglineidlastname',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'callinglineidlastname',
                'required' => false,
                 
                 
                'class' => 'form-control',
                'placeholder'=> 'Apellido de identificación de Línea de Llamada',
            ),
        ));

        $this->add(array(
            'name' => 'lenguague',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'lenguague',
                'required' => false,
                 
                 'value' => 'Spahish',
                'class' => 'form-control',
                'placeholder'=> 'Lenguaje',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'timezone',
            'attributes' => array(
                'id'    => 'timezone',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                'selected'=> 'America/Panama',
                'value_options'=> \DateTimeZone::listIdentifiers(),
                
            ),
        ));
        
       
        

        

        

       
    }
}
