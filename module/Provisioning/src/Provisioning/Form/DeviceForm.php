<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class DeviceForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name',
                'required' => true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre del dispositivo',
            ),
        ));
        
        $this->add(array(
            'name' => 'type',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'type'
            ),
        ));
        
        $this->add(array(
            'name' => 'protocol',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'protocol'
            ),
        ));

        $this->add(array(
            'name' => 'flag',
            'attributes' => array(
                'type'  => 'checkbox',
                'id'    => 'flag'
            ),
        ));
    
    }
}
