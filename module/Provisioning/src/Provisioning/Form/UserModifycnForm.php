<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class UserModifycnForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

       

      $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'userId',
            'attributes' => array(
                'id'    => 'userId',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                '' => 'Seleccione',
            ),
        ));
        
        $this->add(array(
            'name' => 'id_group',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id_group',
    
            ),
        ));
        
        $this->add(array(
            'name' => 'devicename',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'devicename',
                'required' => false,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre de Dispositivo',
            ),
        ));
        
        $this->add(array(
            'name' => 'devicelevel',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'devicelevel',
                'required' => false,
                'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Device Level',
            ),
        ));
        
       

        $this->add(array(
            'name' => 'lineport',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'lineport',
                'required' => true,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Limite de usuarios',
            ),
        ));

        

        

        

       
    }
}
