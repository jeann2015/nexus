<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class ServiceProviderOrderAddForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'number',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'number',
                'required' => true,
                'class' => 'form-control',

            ),
        ));
        
        

       
        
    }
}
