<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class CustomerNumberForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'numberStock',
            'attributes' => array(
                'id'    => 'numberStock',
                'class' => 'form-control',
                'multiple' => 'multiple',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'number',
            'attributes' => array(
                'id'    => 'number',
                'required' => true,
                'class' => 'form-control',
                'multiple' => 'multiple',
            ),
        ));
    }
}
