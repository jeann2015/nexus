<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class GroupDnAssignForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'availableDnRange',
            'attributes' => array(
                'id'    => 'availableDnRange',
                'required' => false,
                'multiple' => 'multiple',
                'size' => 15,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'availableDn',
            'attributes' => array(
                'id'    => 'availableDn',
                'required' => false,
                'multiple' => 'multiple',
                'size' => 15,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'groupDn',
            'attributes' => array(
                'id'    => 'groupDn',
                'required' => true,
                'multiple' => 'multiple',
                'size' => 15,
            ),
        ));
    }
}
