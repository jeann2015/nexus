<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class UserModifyalForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');     
        
        $this->add(array(
            'name' => 'userid',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'userid',
                'required' => false,
                 'readonly' =>true,
                 'class' => 'form-control',
                'placeholder'=> 'IDUser',
            ),
        ));
        
        $this->add(array(
            'name' => 'deviceLevel',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'deviceLevel',
                'required' => false,
                'readonly' =>true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre',
            ),
        ));

        $this->add(array(
            'name' => 'deviceName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'deviceName',
                'required' => false,
                'readonly' =>true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre de Dispositivo',
            ),
        ));

         $this->add(array(
            'name' => 'linePort',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'linePort',
                'required' => false,
                'autofocus' =>true,
                'class' => 'form-control',
                'placeholder'=> 'Linea Puerto',
            ),
        ));

          $this->add(array(
            'name' => 'number',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'number',
                'required' => false,
                
                'class' => 'form-control',
                'placeholder'=> 'Numero',
            ),
        ));

         
        
       

        

        

       
    }
}
