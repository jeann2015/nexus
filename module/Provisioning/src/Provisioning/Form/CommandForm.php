<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class CommandForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name'
            ),
        ));
        
        $this->add(array(
            'name' => 'command',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'command'
            ),
        ));
        
        $this->add(array(
            'name' => 'xml',
            'attributes' => array(
                'type'  => 'textarea',
                'id'    => 'xml'
            ),
        ));
    }
}
