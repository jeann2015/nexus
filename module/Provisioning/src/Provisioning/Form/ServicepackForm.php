<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class ServicepackForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name',
                'class' => 'form-control',
            ),
        ));

       

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'category',
            'attributes' => array(
                'id'    => 'category',
                'class' => 'form-control',
                
            ),
            'options' => array(  
                'empty_option'  => array(''=>''),   
                'value_options' => array(
                     '1'       => 'Usuarios',
                     '2'       => 'Grupos',
                ),
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'servicesAvailable',
            'attributes' => array(
                'id'        => 'servicesAvailable',
                'class'     => 'form-control',
                'multiple'  => 'multiple',
                'size'      => 20,
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'servicesAssigned',
            'attributes' => array(
                'id'        => 'servicesAssigned',
                'class'     => 'form-control',
                'multiple'  => 'multiple',
                'size'      => 20,
            ),
        ));    

        
    }
}
