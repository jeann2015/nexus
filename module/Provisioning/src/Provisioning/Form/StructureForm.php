<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class StructureForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name',
                'required' => true,
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'description',
                'required' => true,
                'class' => 'form-control',
            ),
        ));

         $this->add(array(
             'type' => 'Zend\Form\Element\Select',
             'name' => 'structure',
             'class'  => 'form-control',
             'attributes' => array(
                'id'    => 'structure',
                'required' => true,
                'class' => 'form-control',
            ),

             'options' => array(
                    'label' => 'Nivel',
                    
                     'value_options' => array(
                             'Nivel 1'     => 'Nivel 1',
                             'Nivel 2'     => 'Nivel 2',
                             'Nivel 3'     => 'Nivel 3'
                     ),
             )
        ));

    }
}
