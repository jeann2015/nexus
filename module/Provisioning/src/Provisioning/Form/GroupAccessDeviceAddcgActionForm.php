<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class GroupAccessDeviceAddcgActionForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

       

      $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'devicetype',
            'attributes' => array(
                'id'    => 'devicetype',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                '' => 'Seleccione',
            ),
        ));
        
         $this->add(array(
            'name' => 'devicename',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'devicename',
                'required' => false,
                 
                'class' => 'form-control',
                'placeholder'=> 'Nombre de Dispositivo',
    
            ),
        ));
        
        $this->add(array(
            'name' => 'protocol',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'protocol',
                'required' => false,
                 'readonly' => true,
                 'value' => 'SIP 2.0',
                'class' => 'form-control',
                'placeholder'=> 'Protocolo',
            ),
        ));
        
       
        

        

        

       
    }
}
