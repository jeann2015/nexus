<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class UserDnDeviceAssignForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

         $this->add(array(
            'name' => 'id_order',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id_order'
            ),
        ));
       
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'dnId',
            'attributes' => array(
                'id'    => 'dnId',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => '',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'deviceId',
            'attributes' => array(
                'id'    => 'deviceId',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => '',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'id',
            'attributes' => array(
                'id'    => 'id',
                'required' => true,
            ),
            'options' => array(
                'empty_option' => '',
            ),
        ));
    }
}
