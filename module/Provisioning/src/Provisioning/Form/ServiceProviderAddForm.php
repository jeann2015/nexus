<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class ServiceProviderAddForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id_order_domain',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id_order_domain'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'id_service_provider',
            'attributes' => array(
                'id'    => 'id_service_provider',
                'required' => true,
                'class' => 'form-control',
            ),
            
        ));
        

       
        
    }
}
