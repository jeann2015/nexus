<?php

namespace Provisioning\Form;

use Qv\Language;
use Generic\Form\GenericForm;

class GroupSeriesCompletionModifyInstancealForm extends GenericForm
{
   public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'nuevonombre',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'nuevonombre',
                'required' => true,
                'class' => 'form-control',
                'placeholder'=> 'Nuevo Nombre',
            ),
        ));


        $this->add(array(
            'name' => 'usera',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'usera',
                'required' => true,
                'class' => 'form-control',
                'placeholder'=> 'UserID',
            ),
        ));

        $this->add(array(
            'name' => 'userb',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'userb',
                'required' => true,
                'class' => 'form-control',
                'placeholder'=> 'UserID',
            ),
        ));


    }
}

