<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class GroupForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
        

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name',
                'required' => true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre del grupo o cliente',
            ),
        ));
        
        
        $this->add(array(
            'name' => 'contactName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactName',
                'required' => false,
                'class' => 'form-control',
                'placeholder'=> 'Nombre de contacto',
            ),
        ));
        
        $this->add(array(
            'name' => 'contactNumber',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactNumber',
                'required' => false,
                'class' => 'form-control',
                'placeholder'=> 'Número de contacto',
            ),
        ));
        
        $this->add(array(
            'name' => 'contactEmail',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactEmail',
                'required' => false,
                'class' => 'form-control',
                'placeholder'=> 'Correo de contacto',
            ),
        ));

        $this->add(array(
            'name' => 'userlimit',
            'attributes' => array(
                'type'  => 'number',
                'id'    => 'userlimit',
                'required' => true,
                'class' => 'form-control',
                'placeholder'=> 'Limite de usuarios',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'timezone',
            'attributes' => array(
                'id'    => 'timezone',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '',
                'value_options'=> \DateTimeZone::listIdentifiers(),
                'default_option'=> 'America/Panama',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'productId',
            'attributes' => array(
                'id'    => 'productId',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '',
            ),
        ));

        $this->add(array(
            'name' => 'cisid',
            'attributes' => array(
                'type'       => 'text',
                'id'         => 'cisid',
                'required'   => true,
                'class'      => 'form-control',
                'placeholder'=> 'ID del cliente en CIS',
            ),
        ));
    }
}
