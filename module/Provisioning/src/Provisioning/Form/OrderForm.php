<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class OrderForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'caseId',
            'attributes' => array(
                'id'    => 'caseId',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                'empty_option' => '',
            ),
        ));
        
        $this->add(array(
            'name'  => 'platformId',
            'attributes' => array(
                'id'    => 'platformId',
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'cisid',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'cisid',
                'required' => true,
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'description',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'description',
                'class' => 'form-control',
            ),
        ));
    }
}
