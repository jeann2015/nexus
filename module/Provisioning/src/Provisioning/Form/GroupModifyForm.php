<?php

namespace Provisioning\Form;

use Generic\Form\GenericForm;

class GroupModifyForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
        


      $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'groupId',
            'attributes' => array(
                'id'    => 'groupId',
                'required' => true,
                'class' => 'form-control',
            ),
            'options' => array(
                '' => 'Seleccione',
            ),
        ));
        
        
        $this->add(array(
            'name' => 'contactName',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactName',
                'required' => false,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Nombre de contacto',
            ),
        ));
        
        $this->add(array(
            'name' => 'contactNumber',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'contactNumber',
                'required' => false,
                'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Número de contacto',
            ),
        ));
        
        $this->add(array(
            'name' => 'contactEmail',
            'attributes' => array(
                'type'  => 'text',
                 'readonly' => true,
                'id'    => 'contactEmail',
                'required' => false,
                
                'class' => 'form-control',
                'placeholder'=> 'Correo de contacto',
            ),
        ));

        $this->add(array(
            'name' => 'userlimit',
            'attributes' => array(
                'type'  => 'number',
                'id'    => 'userlimit',
                'required' => true,
                 
                'class' => 'form-control',
                'placeholder'=> 'Limite de usuarios',
            ),
        ));

         $this->add(array(
            'name' => 'timezone',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'timezone',
                'required' => false,
                'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'TimeZone',
            ),
        ));

        $this->add(array(
            'name' => 'productId',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'productId',
                'required' => false,
                 'readonly' => true,
                'class' => 'form-control',
                'placeholder'=> 'Id Producto',
            ),
        ));

        

        

       
    }
}
