<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of CaseXmlTemplateDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

use Generic\Dao\GenericDao;

class CaseXmlTemplateDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }
    
    public function fetchByCaseId($id){
        
        $resultSet = $this->tableGateway->select(function(Select $select) use($id){
            $select->where(array('case_id' => $id));
            $select->order('priority ASC');
         });
        
        return $resultSet;
    }
    
    public function getByCaseIdAndXmlTemplateId($caseId,$xmltemplateId){
        
        $rowset = $this->tableGateway->select(array(
            'case_id'           => $caseId,
            'xmltemplate_id'    => $xmltemplateId,
        ));
        
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        
        return $this->mapperToObject($row);
    }

    public function getByCaseIdAndPriority($caseId,$priority){
        
        $rowset = $this->tableGateway->select(array(
            'case_id'       => $caseId,
            'priority'      => $priority,
        ));
        
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        
        return $this->mapperToObject($row);
    }
    
    public function delete($id)
    {
        $this->tableGateway->delete(array('case_id' => $id));
    }
}
