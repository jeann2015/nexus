<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of services
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class UserServiceAssignDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
       // $this->tableGateway = $tableGateway;
    }
	
	public function fetchAll()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        //$select->columns(array('id.bs_service', 'name'));

        $select->order("id");

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

     public function fetchByServicesId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->columns(array('id', 'name'));
        $select->join('nx_product_services', 'nx_product_services.id_services');
        $where = new Where();
        $where->equalTo("id_product",$id);
        $select->where($where);
        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }
    
}
