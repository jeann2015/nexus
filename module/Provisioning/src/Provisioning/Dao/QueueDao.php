<?php

// module/EntryPoint/src/EntryPoint/Model/AlbumTable.php:

namespace Provisioning\Dao;

use Zend\Db\TableGateway\TableGateway;

class QueueDao
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getQueue($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('queue_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getQueueByName($name)
    {
        $rowset = $this->tableGateway->select(array('queue_name' => $name));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveQueue(Queue $queue)
    {
        $data = array(
            'queue_name' => $queue->queue_name,
            'timeout' => $queue->timeout,
        );

        $id = (int) $queue->queue_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getQueue($id)) {
                $this->tableGateway->update($data, array('queue_id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteQueue($id)
    {
        $this->tableGateway->delete(array('queue_id' => $id));
    }
}
