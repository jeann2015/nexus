<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;

use Generic\Dao\GenericDao;

class VariableDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }
    
    public function fetchByXmlTemplateId($xmltemplateId)
    {
        $resultSet = $this->tableGateway->select(array(
            'xmltemplate_id'   =>  $xmltemplateId,
        ));
        
        return $resultSet;
    }
    
    public function getByXmlTemplateIdAndName($xmltemplateId,$name)
    {
        $rowset = $this->tableGateway->select(array(
            'xmltemplate_id'    =>  $xmltemplateId,
            'name'               =>  $name,
        ));
        
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        
        return $this->mapperToObject($row);
    }
    
    public function deleteByXmlTemplateId($xmltemplateId)
    {
        $this->tableGateway->delete(array(
            'xmltemplate_id' =>  $xmltemplateId,
        ));

    }
    
}
