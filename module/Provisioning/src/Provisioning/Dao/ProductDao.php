<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class ProductDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    
    public function saveProduct($product)
    {
        $this->tableGateway->insert($product);
        return $this->tableGateway->getLastInsertValue();
    }
     public function updateProduct($product_set,$product_where)
    {
        $this->tableGateway->update($product_set,$product_where);
        //return $this->tableGateway->getLastInsertValue();
    }

    public function fetchByPlatformId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join("bs_service_provider","bs_service_provider.id = nx_product.provider_id",array("provider"=>"name"));
        $select->join("bs_domain","bs_domain.id = nx_product.domain_id",array("domain"=>"name"));
        
        $where = new Where();
        $where->equalTo("bs_service_provider.platform_id",$id);

        $select->where($where);
        $select->order("name asc");

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }
    
}
