<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of ProviderDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class ProviderDomainDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    } 

    public function fetchByProviderId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("provider_id",$id);

        $select->where($where);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    } 

    public function fetchByDomainId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("domain_id",$id);

        $select->where($where);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }
    
}
