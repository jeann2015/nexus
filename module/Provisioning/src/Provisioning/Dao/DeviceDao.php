<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class DeviceDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    public function fetchByGroupId($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('group_id' => $id));
        return $rowset;
	}  

    public function fetchUnassignedByGroupId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join("bs_user","bs_user.device_id = bs_device.id",array(),"left");
        
        $where = new Where();
        $where->equalTo("bs_device.group_id",$id);
        $where->isNull("bs_user.device_id");

        $select->where($where);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }  
}
