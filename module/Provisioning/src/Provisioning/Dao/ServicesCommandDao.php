<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

use Generic\Dao\GenericDao;

class ServicesCommandDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }
    
    public function fetchByServiceId($serviceId){
        
        $resultSet = $this->tableGateway->select(function(Select $select) use($serviceId){
            $select->where(array('case_id' => $serviceId));
            $select->order('priority ASC');
         });
        
        return $resultSet;
    }
    
    public function getByServiceIdAndCommandId($serviceId,$commandId){
        
        $rowset = $this->tableGateway->select(array(
            'case_id' => $serviceId,
            'xmltemplate_id' => $commandId,
        ));
        
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        
        return $this->mapperToObject($row);
    }

    public function getByServiceIdAndPriority($serviceId,$priority){
        
        $rowset = $this->tableGateway->select(array(
            'case_id' => $serviceId,
            'priority'   => $priority,
        ));
        
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        
        return $this->mapperToObject($row);
    }
    
    public function delete($id)
    {
        $this->tableGateway->delete(array('case_id' => $id));
    }
}
