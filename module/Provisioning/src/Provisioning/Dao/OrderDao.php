<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Generic\Dao\GenericDao;

class OrderDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }
    
    public function fetchAll($paginated = false, $filter = false)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('bs_group', 'bs_group.id = nx_order.group_id',array("group"=>"name"));
        $select->join('nx_case', 'nx_case.id = nx_order.case_id',array("case"=>"name"));
        $select->join('nx_order_status', 'nx_order_status.id = nx_order.status_id',array("status"=>"name"));
        //$select->group('role.id');
        
        if($filter) {
            $where = new Where();
            foreach ($filter as $left => $right) {
                $where->equalTo("nx_order.".$left, $right);
            }
            $select->where($where);
        }

        $select->order("nx_order.id DESC");
        
        if ($paginated) {
            $dbTableGatewayAdapter = new DbSelect($select,$sql);
            //$dbTableGatewayAdapter = new DbTableGateway($this->tableGateway);
            $paginator = new Paginator($dbTableGatewayAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function getOrder($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_case', 'nx_case.id = nx_order.case_id',array("case"=>"name"));
        $select->join('nx_order_status', 'nx_order_status.id = nx_order.status_id',array("status"=>"name"));
        $select->join('nx_user', 'nx_user.id = nx_order.user_id',array("user"=>"name"));
        $select->join('nx_platform', 'nx_platform.id = nx_order.platform_id',array("platform"=>"name"));
        $select->join('bs_group', 'bs_group.id = nx_order.group_id',array("group"=>"name"));

        $where = new Where();
        $where->equalTo("nx_order.id", $id);
        $select->where($where);
        $select->limit(1);

        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row);
    }

    public function fetchByIntervention($intervention)
    {
        $resultSet = $this->tableGateway->select(array('intervention' => $intervention));
        return $resultSet;
    }

    public function fetchFilter(
        $limit = null, $offset = 0, $order = 'created', $sort = 'desc',$filters = null)
    {
        $table = "nx_order";
        $result = array();

        $query = "SELECT COUNT(*) AS total FROM $table ";
        $query .= "INNER JOIN nx_case ON nx_case.id = nx_order.case_id ";
        $query .= "INNER JOIN nx_order_status ON nx_order_status.id = nx_order.status_id ";
        $query .= "INNER JOIN nx_user ON nx_user.id = nx_order.user_id ";
        $query .= "INNER JOIN nx_platform ON nx_platform.id = nx_order.platform_id ";
        $query .= "INNER JOIN bs_group ON bs_group.id = nx_order.group_id ";

        if(!is_null($filters)){
            $query .= "WHERE ";
            $and = "";
            foreach ($filters as $key => $value) {

                if (strpos($key,'QGDATE2') !== false) {
                    continue;
                }
                
                if (strpos($key,'QGDATE1') !== false) {
                    list($field,$number) = explode("QGDATE", $key);
                    $value1 = $value;
                    $value2 = $filters[$field . 'QGDATE2'];
                    $query .= $and . " $table.".$field." between '" . $value1 . "' and '" . $value2 . "' ";
                } else {
                    if($key == 'case'){
                        $query .= $and . " nx_case.name like '%" . $value . "%'";
                    } elseif($key == 'group'){
                        $query .= $and . " bs_group.name like '%" . $value . "%'";
                    } else { 
                        $query .= $and . " $table.".$key." like '%" . $value . "%'"; 
                    }
                    
                }
                
                $and = " AND ";
            }
        }

        $query .= ";";
        //print $query;

        $statement = $this->tableGateway->adapter->query($query);
        $resultSet = $statement->execute();
        $object = $resultSet->current();
        $result["total"] = $object['total'];



        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_case', 'nx_case.id = nx_order.case_id',array("case"=>"name"));
        $select->join('nx_order_status', 'nx_order_status.id = nx_order.status_id',array("status"=>"name"));
        $select->join('nx_user', 'nx_user.id = nx_order.user_id',array("user"=>"name"));
        $select->join('nx_platform', 'nx_platform.id = nx_order.platform_id',array("platform"=>"name"));
        $select->join('bs_group', 'bs_group.id = nx_order.group_id',array("group"=>"name"));
        $select->order($order . " " . $sort);

        if(!is_null($limit)  && isset($limit) && $limit != ""){
            $select->limit((int)$limit);
            $select->offset((int)$offset);
        }

        if(!is_null($filters)){
            
            $where = new Where() ;
            foreach ($filters as $key => $value) {

                if (strpos($key,'QGDATE2') !== false) {
                    continue;
                }
                
                if (strpos($key,'QGDATE1') !== false) {
                    list($field,$number) = explode("QGDATE", $key);
                    $value1 = $value;
                    $value2 = $filters[$field . 'QGDATE2'];
                    $where->between("$table.".$field, $value1, $value2);
                } else {
                    if($key == 'case'){
                        $where->like("nx_case.name", '%'.$value.'%');
                    } elseif($key == 'group'){
                        $where->like("bs_group.name", '%'.$value.'%');
                    } else { 
                        $where->like("$table.".$key, '%'.$value.'%'); 
                    }
                }
            }
            
            $select->where($where);
        }

        //print $select->getSqlString();

        $resultSet = $this->tableGateway->selectWith($select);
        $resultSet->buffer();

        $result["resultSet"] = $resultSet;
        return $result;
    }
    
}
