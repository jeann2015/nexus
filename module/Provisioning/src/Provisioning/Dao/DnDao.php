<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of DomainDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use Generic\Dao\GenericDao;


class DnDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    public function fetchByGroupId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("group_id",$id);

        $select->where($where);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }

    public function fetchUnassignedByGroupId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join("bs_user","bs_user.dn_id = bs_dn.id",array(),"left");
        
        $where = new Where();
        $where->equalTo("bs_dn.group_id",$id);
        $where->isNull("bs_user.dn_id");

        $select->where($where);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }

    public function deleteByGroupId($id){
        $this->tableGateway->delete(array('group_id' => $id));
    }

    public function getByNumber($number)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("number",$number);

        $select->where($where);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet->current();
    }
}
