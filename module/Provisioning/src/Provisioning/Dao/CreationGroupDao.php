<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of DomainDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use Generic\Dao\GenericDao;

use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class CreationGroupDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    public function fetchByPlatformIdPaginated($id,$filter = false)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("platform_id",$id);

        if($filter) {
            foreach ($filter as $left => $right) {
                $where->equalTo("bs_domain.".$left, $right);
            }
            $select->where($where);
        }

        $select->order("name asc");
        
        $dbTableGatewayAdapter = new DbSelect($select,$sql);
        
        $paginator = new Paginator($dbTableGatewayAdapter);
        return $paginator;
    }

    public function fetchByUserId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("id",$id);

        $select->where($where);
        $select->order("id asc");
        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }

    public function fetchByOrderId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("id_order",$id);

        $select->where($where);
        $select->order("id asc");
        $select->limit(1);
        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }

     public function fetchByOrderIdUserID($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("id_order",$id);
        $where->notEqualTo("id_user",'');

        $select->where($where);
        $select->order("id asc");
        $select->limit(1);
        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }

    public function fetchAllA()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $where = new Where();
        $where->isnotnull("id_user");
        $select->where($where);
        $select->order("id asc");
        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    } 

    
}
