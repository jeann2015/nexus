<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of DomainDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use Generic\Dao\GenericDao;

use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class UserModifyAddDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    public function fetchByPlatformIdPaginated($id,$filter = false)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("platform_id",$id);

        if($filter) {
            foreach ($filter as $left => $right) {
                $where->equalTo("bs_domain.".$left, $right);
            }
            $select->where($where);
        }

        $select->order("name asc");
        
        $dbTableGatewayAdapter = new DbSelect($select,$sql);
        //$dbTableGatewayAdapter = new DbTableGateway($this->tableGateway);
        $paginator = new Paginator($dbTableGatewayAdapter);
        return $paginator;
    }

    public function fetchByOrderId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("id_order",$id);

        $select->where($where);
        
        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row);
    } 

    public function set_id_service_provider($id,$id_services_provider)
    {
        $data = array( 'id_service_provider' => $id_services_provider );
        $where = array( 'id_order' => $id );
        $this->tableGateway->update($data,$where);
    }

    public function getCountSyncStatus($syncStatus)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->columns(array(
            'cantidad'  =>  new Expression("count(*)"),
        ));

        $where = new Where();
        $where->equalTo("sync_status",$syncStatus);

        $select->where($where);
        $select->limit(1);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet->current();
    }
}
