<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of PlatformDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class PlatformDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
    } 

    public function fetchAll()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );

        $select->order("active DESC");

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }
    
}
