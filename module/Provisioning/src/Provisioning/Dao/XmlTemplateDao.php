<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class XmlTemplateDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    public function fetchByCategory($category)
    {
        $resultset = $this->tableGateway->select(array('category' => $category));
        return $resultset;
    } 
    
   


    public function getByCategoryLikeCommand($category,$command)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );

        $where = new Where();
        $where->equalTo("category", $category);
        $where->like("command", "%".$command."%");
        $select->where($where);
        $select->limit(1);

        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        
        if (!$row) {
            return null;
        }
 
        return $this->mapperToObject($row);

    }    
}
