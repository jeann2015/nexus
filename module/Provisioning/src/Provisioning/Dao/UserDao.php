<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class UserDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    public function getLastUserLetterByGroupId($groupId)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );

        $where = new Where();
        $where->equalTo("group_id", $groupId);
        $select->where($where);
        $select->order("letter desc");
        $select->limit(1);

        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row)->letter;
	}

    public function fetchByGroupId($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('group_id' => $id));
        return $rowset;
    } 

    

    public function fetchUnassignedByGroupId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("group_id",$id);
        $where->isNull("dn_id");
        $where->isNull("device_id");

        $select->where($where);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    } 
    
}
