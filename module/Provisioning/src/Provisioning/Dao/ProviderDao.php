<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of ProviderDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use Generic\Dao\GenericDao;

use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class ProviderDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }


    public function fetchByPlatformIdPaginated($id,$filter = false)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("platform_id",$id);

        if($filter) {
            foreach ($filter as $left => $right) {
                $where->equalTo("bs_provider.".$left, $right);
            }
            $select->where($where);
        }

        $select->order("name asc");
        
        $dbTableGatewayAdapter = new DbSelect($select,$sql);
        //$dbTableGatewayAdapter = new DbTableGateway($this->tableGateway);
        $paginator = new Paginator($dbTableGatewayAdapter);
        return $paginator;
    }

    public function fetchByPlatformId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("platform_id",$id);

        $select->where($where);
        $select->order("name asc");

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }

    public function fetchBy_Id($id)
    {
       $id = (int) $id;
       error_log($id);
       $sql = new Sql( $this->tableGateway->adapter ) ;
       $select = $sql->select() ;
       $select->from( $this->tableGateway->getTable() );
        
       $where = new Where();
       $where->equalTo("id",$id);
       $select->where($where);
       $resultSet = $this->tableGateway->selectWith($select);
       return $resultSet;
    }  

    public function fetchById($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("id",$id);

        $select->where($where);
        //$select->order("name asc");
        
        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row);

        //$resultSet = $this->tableGateway->selectWith($select);
        
        //return $resultSet;
    } 


    public function setAllSyncStatus($syncStatus)
    {
        $data = array('sync_status' => $syncStatus, );
        $this->tableGateway->update($data);
    }

    public function getCountSyncStatus($syncStatus)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->columns(array(
            'cantidad'  =>  new Expression("count(*)"),
        ));

        $where = new Where();
        $where->equalTo("sync_status",$syncStatus);

        $select->where($where);
        $select->limit(1);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet->current();
    }

    public function fetchNoEnterprise()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("isenterprise",'false');
        $where->equalTo("inbroadsoft",'1');

        $select->where($where);
        $select->order("name asc");

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }
    
    public function fetchFilter(
        $limit = null, $offset = 0, $order = 'created', $sort = 'desc',$filters = null)
    {
        $table = "bs_service_provider";
        $result = array();

        $query = "SELECT COUNT(*) AS total FROM $table ";

        if(!is_null($filters)){
            $query .= "WHERE ";
            $and = "";
            foreach ($filters as $key => $value) {

                if (strpos($key,'QGDATE2') !== false) {
                    continue;
                }
                
                if (strpos($key,'QGDATE1') !== false) {
                    list($field,$number) = explode("QGDATE", $key);
                    $value1 = $value;
                    $value2 = $filters[$field . 'QGDATE2'];
                    $query .= $and . " $table.".$field." between '" . $value1 . "' and '" . $value2 . "' ";
                } else {
                    $query .= $and . " $table.".$key." like '%" . $value . "%'";
                }
                
                $and = " AND ";
            }
        }

        $query .= ";";
        //print $query;

        $statement = $this->tableGateway->adapter->query($query);
        $resultSet = $statement->execute();
        $object = $resultSet->current();
        $result["total"] = $object['total'];



        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->order($order . " " . $sort);

        if(!is_null($limit)  && isset($limit) && $limit != ""){
            $select->limit((int)$limit);
            $select->offset((int)$offset);
        }

        if(!is_null($filters)){
            
            $where = new Where() ;
            foreach ($filters as $key => $value) {

                if (strpos($key,'QGDATE2') !== false) {
                    continue;
                }
                
                if (strpos($key,'QGDATE1') !== false) {
                    list($field,$number) = explode("QGDATE", $key);
                    $value1 = $value;
                    $value2 = $filters[$field . 'QGDATE2'];
                    $where->between("$table.".$field, $value1, $value2);
                } else {
                    $where->like("$table.".$key, '%'.$value.'%');
                }
            }
            
            $select->where($where);
        }

        //print $select->getSqlString();

        $resultSet = $this->tableGateway->selectWith($select);
        $resultSet->buffer();

        $result["resultSet"] = $resultSet;
        return $result;
    }
}
