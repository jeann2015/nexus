<?php

namespace Provisioning\Dao;

use Zend\Db\TableGateway\TableGateway;
use Provisioning\Model\Message;

use Generic\Dao\GenericDao;

class MessageDao //extends GenericDao
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getMessage($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('message_id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
            //throw new \Exception("Could not find row $id");
        }
        return $row;
    }
    
    public function getMessagesByQueueId($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('queue_id' => $id));
        return $rowset;
    }

    public function saveMessage(Message $message)
    {
        $data = array(
            'queue_id'  => $message->queue_id,
            'handle'    => isset($message->handle) ? $message->handle : null,
            'body'      => $message->body,
            'md5'       => $message->md5,
            'timeout'   => isset($message->timeout) ? $message->timeout : null,
            'created'   => $message->created,
            'xml_id'    => $message->xmlId,
        );

        $id = (int) $message->message_id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } else {
            if ($this->getMessage($id)) {
                $this->tableGateway->update($data, array('message_id' => $id));
            } else {
                throw new \Exception('Form id does not exist');
            }
        }
    }

    public function deleteMessage($id)
    {
        $this->tableGateway->delete(array('message_id' => $id));
    }
    
    public function deleteMessagesByQueueId($id)
    {
        $this->tableGateway->delete(array('queue_id' => $id));
    }
    
    public function deleteMessageByOrderId($id)
    {
        $this->tableGateway->delete(array('order_id' => $id));
    }
}
