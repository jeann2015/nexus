<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class GroupDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    public function getLastGroupLetterByCustomerId($customerId)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );

        $where = new Where();
        $where->equalTo("customer_id", $customerId);
        $select->where($where);
        $select->order("letter desc");
        $select->limit(1);

        $rowset = $this->tableGateway->selectWith($select);
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row)->letter;
	}

    public function fetchByCustomerId($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('customer_id' => $id));
        return $rowset;
    } 

    public function fetchById($id)
    {
       $id = (int) $id;
      
       $sql = new Sql( $this->tableGateway->adapter ) ;
       $select = $sql->select() ;
       $select->from( $this->tableGateway->getTable() );
        
       $where = new Where();
       $where->equalTo("id",$id);
       $select->where($where);
       $resultSet = $this->tableGateway->selectWith($select);
       return $resultSet;
    }  
    
}
