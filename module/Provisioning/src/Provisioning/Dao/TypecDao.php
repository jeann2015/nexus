<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of services
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class TypecDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
       // $this->tableGateway = $tableGateway;
    }
	
	public function fetchAll()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        //$select->columns(array('id.bs_service', 'name'));

        $select->order("id");

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function fetchAllS()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_structure', 'nx_structure.id = nx_type_client.structure',array("structure_name"=>"name"));

        $select->order("id");

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

     public function fetchById($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->columns(array('id', 'name','id_structure'));
        
        $where = new Where();
        $where->equalTo("id",$id);
        $select->where($where);
        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }
    
}
