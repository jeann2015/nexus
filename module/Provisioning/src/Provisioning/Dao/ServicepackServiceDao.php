<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of services
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class ServicepackServiceDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
       // $this->tableGateway = $tableGateway;
    }
	
	public function fetchByServicepackId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        //$select->columns(array('id.bs_service', 'name'));

        $where = new Where();
        $where->equalTo("servicepack_id",$id);

        $select->where($where);
        $select->order("service_id");

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

    public function deleteByServicepackId($id)
    {
        $this->tableGateway->delete(array('servicepack_id' => $id));
    }
}
