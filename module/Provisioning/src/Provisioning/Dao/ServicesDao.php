<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of services
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Generic\Dao\GenericDao;

class ServicesDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
       // $this->tableGateway = $tableGateway;
    }
	
	public function fetchAll()
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        //$select->columns(array('id.bs_service', 'name'));

        $select->order("name asc");

        $resultSet = $this->tableGateway->selectWith($select);
        return $resultSet;
    }

     public function fetchByServicesId($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->columns(array('id', 'name'));
        $select->join('nx_product_services', 'nx_product_services.id_services');
        $where = new Where();
        $where->equalTo("id_product",$id);
        $select->where($where);
        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet;
    }

    public function fetchFilter(
        $limit = null, $offset = 0, $order = 'created', $sort = 'desc',$filters = null)
    {
        $table = "bs_service";
        $result = array();

        $query = "SELECT COUNT(*) AS total FROM $table ";

        if(!is_null($filters)){
            $query .= "WHERE ";
            $and = "";
            foreach ($filters as $key => $value) {

                if (strpos($key,'QGDATE2') !== false) {
                    continue;
                }
                
                if (strpos($key,'QGDATE1') !== false) {
                    list($field,$number) = explode("QGDATE", $key);
                    $value1 = $value;
                    $value2 = $filters[$field . 'QGDATE2'];
                    $query .= $and . " $table.".$field." between '" . $value1 . "' and '" . $value2 . "' ";
                } else {
                    $query .= $and . " $table.".$key." like '%" . $value . "%'";
                }
                
                $and = " AND ";
            }
        }

        $query .= ";";
        //print $query;

        $statement = $this->tableGateway->adapter->query($query);
        $resultSet = $statement->execute();
        $object = $resultSet->current();
        $result["total"] = $object['total'];



        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->order($order . " " . $sort);

        if(!is_null($limit)  && isset($limit) && $limit != ""){
            $select->limit((int)$limit);
            $select->offset((int)$offset);
        }

        if(!is_null($filters)){
            
            $where = new Where() ;
            foreach ($filters as $key => $value) {

                if (strpos($key,'QGDATE2') !== false) {
                    continue;
                }
                
                if (strpos($key,'QGDATE1') !== false) {
                    list($field,$number) = explode("QGDATE", $key);
                    $value1 = $value;
                    $value2 = $filters[$field . 'QGDATE2'];
                    $where->between("$table.".$field, $value1, $value2);
                } else {
                    $where->like("$table.".$key, '%'.$value.'%');
                }
            }
            
            $select->where($where);
        }

        //print $select->getSqlString();

        $resultSet = $this->tableGateway->selectWith($select);
        $resultSet->buffer();

        $result["resultSet"] = $resultSet;
        return $result;
    } 
}
