<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Dao;

/**
 * Description of GroupDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

use Generic\Dao\GenericDao;

class XmlDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }

    public function getNext($orderId)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("order_id",$orderId);
        $where->equalTo("status",'pendiente');

        $select->where($where);
        $select->order("priority asc");
        $select->limit(1);

        $resultSet = $this->tableGateway->selectWith($select);
        $row = $resultSet->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row);
    }

    public function fetchByOrderId($id)
    {
        $resultSet = $this->tableGateway->select(array('order_id' => $id));
        
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_xml_status', 'nx_xml_status.id = nx_xml.status_id',array("status"=>"name"));

        $where = new Where();
        $where->equalTo("order_id", $id);
        $select->where($where);
        $select->order("priority asc, id asc");

        $resultSet = $this->tableGateway->selectWith($select);

        return $resultSet;
    }

    public function deleteByObjectId($objectId)
    {
        $where = new Where();
        $where->equalTo("object_id", $objectId);

        $this->tableGateway->delete($where);
    }

    public function deleteByOrderIdLikeCommand($orderId,$command)
    {
        $where = new Where();
        $where->equalTo("order_id", $orderId);
        $where->like("name", $command . "Request");

        $this->tableGateway->delete($where);
    }

    public function deleteByOrderIdLikeCommandLikeContent($orderId,$command,$content)
    {
        $where = new Where();
        $where->equalTo("order_id", $orderId);
        $where->like("name", $command . "Request");
        $where->like("body", $content);

        $this->tableGateway->delete($where);
    }
    
}
