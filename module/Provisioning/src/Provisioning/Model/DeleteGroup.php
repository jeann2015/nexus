<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\DeleteGroupInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class DeleteGroup extends DeleteGroupInputFilter {
    
    
    public $id_order;
    public $id_group;
    public $id_service_provider;
    public $number_empresa;
    public $createdate;
    public $command;
    
     public function getcommand() {
        return $this->command;
    }

    public function setcommand($command) {
        $this->command = $command;
    }
    
    public function getcreatedate() {
        return $this->createdate;
    }

    public function setcreatedate($createdate) {
        $this->createdate = $createdate;
    }

    public function getnumber_empresa() {
        return $this->number_empresa;
    }

    public function setnumber_empresa($number_empresa) {
        $this->number_empresa = $number_empresa;
    }

    public function getid_order() {
        return $this->id_order;
    }

    public function setid_order($id_order) {
        $this->id_order = $id_order;
    }

    public function getid_group() {
        return $this->id_group;
    }

    public function setid_group($id_group) {
        $this->id_group = $id_group;
    }

    public function getid_service_provider() {
        return $this->id_service_provider;
    }

    public function setid_service_provider($id_service_provider) {
        $this->id_service_provider = $id_service_provider;
    }

    
    
    
   
}
