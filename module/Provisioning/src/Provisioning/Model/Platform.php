<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\PlatformInputFilter;
/**
 * Description of Platform
 *
 * @author rodolfo
 */
class Platform extends PlatformInputFilter {
    
   public $name;
   public $ip;
   public $protocol;
   public $port;
   public $wsdl;
   public $username;
   public $password;
   public $active;
   public $class;

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the value of name.
     *
     * @param mixed $name the name 
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of ip.
     *
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }
    
    /**
     * Sets the value of ip.
     *
     * @param mixed $ip the ip 
     *
     * @return self
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Gets the value of protocol.
     *
     * @return mixed
     */
    public function getProtocol()
    {
        return $this->protocol;
    }
    
    /**
     * Sets the value of protocol.
     *
     * @param mixed $protocol the protocol 
     *
     * @return self
     */
    public function setProtocol($protocol)
    {
        $this->protocol = $protocol;

        return $this;
    }

    /**
     * Gets the value of port.
     *
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }
    
    /**
     * Sets the value of port.
     *
     * @param mixed $port the port 
     *
     * @return self
     */
    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    /**
     * Gets the value of wsdl.
     *
     * @return mixed
     */
    public function getWsdl()
    {
        return $this->wsdl;
    }
    
    /**
     * Sets the value of wsdl.
     *
     * @param mixed $wsdl the wsdl 
     *
     * @return self
     */
    public function setWsdl($wsdl)
    {
        $this->wsdl = $wsdl;

        return $this;
    }

    /**
     * Gets the value of username.
     *
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }
    
    /**
     * Sets the value of username.
     *
     * @param mixed $username the username 
     *
     * @return self
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Gets the value of password.
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * Sets the value of password.
     *
     * @param mixed $password the password 
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets the value of active.
     *
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Sets the value of active.
     *
     * @param mixed $active the active 
     *
     * @return self
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Gets the value of class.
     *
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }
    
    /**
     * Sets the value of class.
     *
     * @param mixed $class the class 
     *
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }
}
