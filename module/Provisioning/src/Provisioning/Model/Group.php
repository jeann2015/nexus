<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\GroupInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Group extends GroupInputFilter {
    
    public $name;
    public $contactName;
    public $contactNumber;
    public $contactEmail;
    public $letter;
    public $userlimit;
    public $timezone;
    public $cisid;
    public $productId;
    public $provisioned;
    public $syncStatus;
    public $providerId;

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of contactName.
     *
     * @return mixed
     */
    public function getContactName()
    {
        return $this->contactName;
    }

    /**
     * Sets the value of contactName.
     *
     * @param mixed $contactName the contact name
     *
     * @return self
     */
    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    /**
     * Gets the value of contactNumber.
     *
     * @return mixed
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Sets the value of contactNumber.
     *
     * @param mixed $contactNumber the contact number
     *
     * @return self
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Gets the value of contactEmail.
     *
     * @return mixed
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    /**
     * Sets the value of contactEmail.
     *
     * @param mixed $contactEmail the contact email
     *
     * @return self
     */
    public function setContactEmail($contactEmail)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Gets the value of letter.
     *
     * @return mixed
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * Sets the value of letter.
     *
     * @param mixed $letter the letter
     *
     * @return self
     */
    public function setLetter($letter)
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * Gets the value of userlimit.
     *
     * @return mixed
     */
    public function getUserlimit()
    {
        return $this->userlimit;
    }

    /**
     * Sets the value of userlimit.
     *
     * @param mixed $userlimit the userlimit
     *
     * @return self
     */
    public function setUserlimit($userlimit)
    {
        $this->userlimit = $userlimit;

        return $this;
    }

    /**
     * Gets the value of timezone.
     *
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Sets the value of timezone.
     *
     * @param mixed $timezone the timezone
     *
     * @return self
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Gets the value of cisid.
     *
     * @return mixed
     */
    public function getCisid()
    {
        return $this->cisid;
    }

    /**
     * Sets the value of cisid.
     *
     * @param mixed $cisid the cisid
     *
     * @return self
     */
    public function setCisid($cisid)
    {
        $this->cisid = $cisid;

        return $this;
    }

    /**
     * Gets the value of productId.
     *
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Sets the value of productId.
     *
     * @param mixed $productId the product id
     *
     * @return self
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Gets the value of provisioned.
     *
     * @return mixed
     */
    public function getProvisioned()
    {
        return $this->provisioned;
    }

    /**
     * Sets the value of provisioned.
     *
     * @param mixed $provisioned the provisioned
     *
     * @return self
     */
    public function setProvisioned($provisioned)
    {
        $this->provisioned = $provisioned;

        return $this;
    }

    /**
     * Gets the value of syncStatus.
     *
     * @return mixed
     */
    public function getSyncStatus()
    {
        return $this->syncStatus;
    }

    /**
     * Sets the value of syncStatus.
     *
     * @param mixed $syncStatus the sync status
     *
     * @return self
     */
    public function setSyncStatus($syncStatus)
    {
        $this->syncStatus = $syncStatus;

        return $this;
    }

    /**
     * Gets the value of providerId.
     *
     * @return mixed
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Sets the value of providerId.
     *
     * @param mixed $providerId the provider id
     *
     * @return self
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }
}
