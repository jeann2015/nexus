<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ServicesInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Services extends ServicesInputFilter {
    
    public $name;
    public $category;
    

    public function getName() {
        return $this->name;
    }

    

    public function setName($name) {
        $this->name = $name;
    }

    public function getCategory() {
        return $this->category;
    }

    

    public function setCategory($category) {
        $this->category = $category;
    }
	
  

}
