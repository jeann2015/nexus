<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\UserServiceAssignInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class UserServiceAssign extends UserServiceAssignInputFilter {
    
    public $user_id;
    public $command;
    public $order_id;
    
    
    /**
     * Gets the value of Group ID.
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Sets the value of Group ID.
     *
     * @param mixed $group_id the group_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

   

    /**
     * Gets the value of getOrderId.
     *
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Sets the value of setOrderId.
     *
     * @param mixed $order_id the order_id
     *
     * @return self
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;

        return $this;
    }

     /**
     * Gets the value of command.
     *
     * @return mixed
     */
    public function getcommand()
    {
        return $this->command;
    }

    /**
     * Sets the value of command.
     *
     * @param mixed $command the command
     *
     * @return self
     */
    public function setcommand($command)
    {
        $this->command = $command;

        return $this;
    }

    
}
