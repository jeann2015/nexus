<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ServicesInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class ServicesCommand extends ServicesInputFilter {
    
    public $caseId;
    public $xmltemplateId;
    public $priority;

    /**
     * Gets the value of caseId.
     *
     * @return mixed
     */
    public function getCaseId()
    {
        return $this->caseId;
    }
    
    /**
     * Sets the value of caseId.
     *
     * @param mixed $caseId the case id 
     *
     * @return self
     */
    public function setCaseId($caseId)
    {
        $this->caseId = $caseId;

        return $this;
    }

    /**
     * Gets the value of xmltemplateId.
     *
     * @return mixed
     */
    public function getXmltemplateId()
    {
        return $this->xmltemplateId;
    }
    
    /**
     * Sets the value of xmltemplateId.
     *
     * @param mixed $xmltemplateId the xmltemplate id 
     *
     * @return self
     */
    public function setXmltemplateId($xmltemplateId)
    {
        $this->xmltemplateId = $xmltemplateId;

        return $this;
    }

    /**
     * Gets the value of priority.
     *
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }
    
    /**
     * Sets the value of priority.
     *
     * @param mixed $priority the priority 
     *
     * @return self
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }
}
