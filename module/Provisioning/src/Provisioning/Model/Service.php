<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ServicesInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Service extends ServicesInputFilter {
    
    public $name;
    public $description;

    public function getName() {
        return $this->name;
    }

    

    public function setName($name) {
        $this->name = $name;
    }


}
