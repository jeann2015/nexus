<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\UserInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class User extends UserInputFilter {
    
    public $firstname;
    public $lastname;
    public $userid;
    public $timezone;
    public $language;
    public $password;
    public $groupId;
    public $dnId;
    public $deviceId;

    /**
     * Gets the value of firstname.
     *
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Sets the value of firstname.
     *
     * @param mixed $firstname the firstname
     *
     * @return self
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Gets the value of lastname.
     *
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Sets the value of lastname.
     *
     * @param mixed $lastname the lastname
     *
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Gets the value of userid.
     *
     * @return mixed
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Sets the value of userid.
     *
     * @param mixed $userid the userid
     *
     * @return self
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Gets the value of timezone.
     *
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Sets the value of timezone.
     *
     * @param mixed $timezone the timezone
     *
     * @return self
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Gets the value of language.
     *
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Sets the value of language.
     *
     * @param mixed $language the language
     *
     * @return self
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Gets the value of password.
     *
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the value of password.
     *
     * @param mixed $password the password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Gets the value of groupId.
     *
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Sets the value of groupId.
     *
     * @param mixed $groupId the group id
     *
     * @return self
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Gets the value of dnId.
     *
     * @return mixed
     */
    public function getDnId()
    {
        return $this->dnId;
    }

    /**
     * Sets the value of dnId.
     *
     * @param mixed $dnId the dn id
     *
     * @return self
     */
    public function setDnId($dnId)
    {
        $this->dnId = $dnId;

        return $this;
    }

    /**
     * Gets the value of deviceId.
     *
     * @return mixed
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Sets the value of deviceId.
     *
     * @param mixed $deviceId the device id
     *
     * @return self
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }
}
