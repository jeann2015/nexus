<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\GroupServiceProviderInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class GroupServiceProvider extends GroupServiceProviderInputFilter {
    
    public $group_id;
    public $serviceprovider_id;
    public $order_id;
    public $command;
    public $user_id;
    public $name;
    public $clave;
    
    /**
     * Gets the value of Group ID.
     *
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Sets the value of Group ID.
     *
     * @param mixed $group_id the group_id
     *
     * @return self
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;

        return $this;
    }

    /**
     * Gets the value of serviceprovider_Id.
     *
     * @return mixed
     */
    public function getServiceproviderId()
    {
        return $this->serviceprovider_id;
    }

    /**
     * Sets the value of serviceprovider_Id.
     *
     * @param mixed $serviceprovider_Id the serviceprovider_Id
     *
     * @return self
     */
    public function setServiceproviderId($serviceprovider_id)
    {
        $this->serviceprovider_id = $serviceprovider_id;

        return $this;
    }

    /**
     * Gets the value of getOrderId.
     *
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Sets the value of setOrderId.
     *
     * @param mixed $order_id the order_id
     *
     * @return self
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;

        return $this;
    }

     /**
     * Gets the value of command.
     *
     * @return mixed
     */
    public function getcommand()
    {
        return $this->command;
    }

    /**
     * Sets the value of command.
     *
     * @param mixed $command the command
     *
     * @return self
     */
    public function setcommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Gets the value of Group ID.
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Sets the value of Group ID.
     *
     * @param mixed $group_id the group_id
     *
     * @return self
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Gets the value of Group ID.
     *
     * @return mixed
     */
    public function getname()
    {
        return $this->name;
    }

    /**
     * Sets the value of Group ID.
     *
     * @param mixed $group_id the group_id
     *
     * @return self
     */
    public function setname($name)
    {
        $this->name = $name;

        return $this;
    }


     /**
     * Gets the value of Group ID.
     *
     * @return mixed
     */
    public function getclave()
    {
        return $this->clave;
    }

    /**
     * Sets the value of Group ID.
     *
     * @param mixed $group_id the group_id
     *
     * @return self
     */
    public function setclave($clave)
    {
        $this->clave = $clave;

        return $this;
    }

    
}
