<?php

namespace Provisioning\Model;

class Queue
{

    public $queue_id;
    public $queue_name;
    public $timeout;

    public function exchangeArray($data)
    {
        foreach (get_object_vars($this) as $key => $value) {
            $this->$key = isset($data[$key]) ? $data[$key] : null;
        }
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
