<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\CaseInputFilter;
/**
 * Description of nxCase
 *
 * @author rodolfo
 */
class nxCase extends CaseInputFilter {
    
    public $name;
    public $description;
    

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

  

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

   
}
