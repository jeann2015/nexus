<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\NumberStockInputFilter;
/**
 * Description of Number
 *
 * @author rodolfo
 */
class NumberStock extends NumberStockInputFilter {
    
    public $number;
    public $created;
    public $available;

    /**
     * Gets the value of number.
     *
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }
    
    /**
     * Sets the value of number.
     *
     * @param mixed $number the number 
     *
     * @return self
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Gets the value of created.
     *
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }
    
    /**
     * Sets the value of created.
     *
     * @param mixed $created the created 
     *
     * @return self
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Gets the value of available.
     *
     * @return mixed
     */
    public function getAvailable()
    {
        return $this->available;
    }
    
    /**
     * Sets the value of available.
     *
     * @param mixed $available the available 
     *
     * @return self
     */
    public function setAvailable($available)
    {
        $this->available = $available;

        return $this;
    }
}
