<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\CustomerInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Customer extends CustomerInputFilter {
    
    public $name;
    public $domain;
    public $cisId;
    public $platform;
    
    public function getName() {
        return $this->name;
    }

    public function getDomain() {
        return $this->domain;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDomain($domain) {
        $this->domain = $domain;
    }

    public function getCisId() {
        return $this->cisId;
    }

    public function setCisId($cisId) {
        $this->cisId = $cisId;
    }

    public function getPlatform() {
        return $this->platform;
    }

    public function setPlatform($platform) {
        $this->platform = $platform;
    }
   
}
