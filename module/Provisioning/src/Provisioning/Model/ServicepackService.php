<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ServicepackServiceInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class ServicepackService extends ServicepackServiceInputFilter {
    
    public $servicepackId;
    public $serviceId;

    /**
     * Gets the value of servicepackId.
     *
     * @return mixed
     */
    public function getServicepackId()
    {
        return $this->servicepackId;
    }

    /**
     * Sets the value of servicepackId.
     *
     * @param mixed $servicepackId the servicepack id
     *
     * @return self
     */
    public function setServicepackId($servicepackId)
    {
        $this->servicepackId = $servicepackId;

        return $this;
    }

    /**
     * Gets the value of serviceId.
     *
     * @return mixed
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Sets the value of serviceId.
     *
     * @param mixed $serviceId the service id
     *
     * @return self
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;

        return $this;
    }
}
