<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\TypecInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Typec extends TypecInputFilter {
    
    public $name;
    public $description;
    public $structure;
    public $datecreated;

    

    public function getName() {
        return $this->name;
    }

    

    public function setName($name) {
        $this->name = $name;
    }

    public function getdescription() {
        return $this->description;
    }    

    public function setdescription($description) {
        $this->description = $description;
    }


    public function getstructure() {
        return $this->structure;
    }

    

    public function setstructure($structure) {
        $this->structure = $structure;
    }

    public function getdatecreated() {
        return $this->datecreated;
    }

    

    public function setdatecreated($datecreated) {
        $this->datecreated = $datecreated;
    }
	
  

}
