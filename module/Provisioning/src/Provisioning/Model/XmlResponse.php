<?php

namespace Provisioning\Model;

class XmlResponse
{
    public $result;
    public $summary;
    public $command;

    /**
     * Gets the value of result.
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }
    
    /**
     * Sets the value of result.
     *
     * @param mixed $result the result 
     *
     * @return self
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Gets the value of summary.
     *
     * @return mixed
     */
    public function getSummary()
    {
        return $this->summary;
    }
    
    /**
     * Sets the value of summary.
     *
     * @param mixed $summary the summary 
     *
     * @return self
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Gets the value of command.
     *
     * @return mixed
     */
    public function getCommand()
    {
        return $this->command;
    }
    
    /**
     * Sets the value of command.
     *
     * @param mixed $command the command 
     *
     * @return self
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }
}
