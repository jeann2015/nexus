<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\VariableInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Variable extends VariableInputFilter {
    
    public $xmltemplateId;
    public $name;
    public $mappingObject;
    public $mappingAttribute;
    public $treatment;
    

    /**
     * Gets the value of xmltemplateId.
     *
     * @return mixed
     */
    public function getXmltemplateId()
    {
        return $this->xmltemplateId;
    }
    
    /**
     * Sets the value of xmltemplateId.
     *
     * @param mixed $xmltemplateId the xmltemplate id 
     *
     * @return self
     */
    public function setXmltemplateId($xmltemplateId)
    {
        $this->xmltemplateId = $xmltemplateId;

        return $this;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the value of name.
     *
     * @param mixed $name the name 
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of mappingObject.
     *
     * @return mixed
     */
    public function getMappingObject()
    {
        return $this->mappingObject;
    }
    
    /**
     * Sets the value of mappingObject.
     *
     * @param mixed $mappingObject the mapping object 
     *
     * @return self
     */
    public function setMappingObject($mappingObject)
    {
        $this->mappingObject = $mappingObject;

        return $this;
    }

    /**
     * Gets the value of mappingAttribute.
     *
     * @return mixed
     */
    public function getMappingAttribute()
    {
        return $this->mappingAttribute;
    }
    
    /**
     * Sets the value of mappingAttribute.
     *
     * @param mixed $mappingAttribute the mapping attribute 
     *
     * @return self
     */
    public function setMappingAttribute($mappingAttribute)
    {
        $this->mappingAttribute = $mappingAttribute;

        return $this;
    }

    /**
     * Gets the value of treatment.
     *
     * @return mixed
     */
    public function getTreatment()
    {
        return $this->treatment;
    }
    
    /**
     * Sets the value of treatment.
     *
     * @param mixed $treatment the treatment 
     *
     * @return self
     */
    public function setTreatment($treatment)
    {
        $this->treatment = $treatment;

        return $this;
    }
    
    
}
