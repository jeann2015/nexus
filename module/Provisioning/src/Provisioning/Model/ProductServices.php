<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ProductServicesInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class ProductServices extends ProductServicesInputFilter {
    
    public $id_product;
    public $id_services;
    public $provider_id;   

    /**
     * Gets the value of id_product.
     *
     * @return mixed
     */
    public function getid_Product()
    {
        return $this->id_product;
    }

    /**
     * Sets the value of id_product.
     *
     * @param mixed $id_product the id_product
     *
     * @return self
     */
    public function setid_Product($id_product)
    {
        $this->id_product = $id_product;

        return $this;
    }

    /**
     * Gets the value of id_services.
     *
     * @return mixed
     */
    public function getid_Services()
    {
        return $this->id_services;
    }

    /**
     * Sets the value of id_services.
     *
     * @param mixed $provider_id the id_services
     *
     * @return self
     */
    public function setid_Services($id_services)
    {
        $this->id_services = $id_services;

        return $this;
    }

    /**
     * Gets the value of provider_id.
     *
     * @return mixed
     */
    public function getProvider_id()
    {
        return $this->provider_id;
    }

    /**
     * Sets the value of provider_id.
     *
     * @param mixed $provider_id the provider_id
     *
     * @return self
     */
    public function setProvider_id($provider_id)
    {
        $this->provider_id = $provider_id;

        return $this;
    }

    
}
