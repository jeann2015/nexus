<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ProductInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Product extends ProductInputFilter {
    
    public $name;
    public $providerId;
    public $domainId;   

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of providerId.
     *
     * @return mixed
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Sets the value of providerId.
     *
     * @param mixed $providerId the provider id
     *
     * @return self
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * Gets the value of domainId.
     *
     * @return mixed
     */
    public function getDomainId()
    {
        return $this->domainId;
    }

    /**
     * Sets the value of domainId.
     *
     * @param mixed $domainId the domain id
     *
     * @return self
     */
    public function setDomainId($domainId)
    {
        $this->domainId = $domainId;

        return $this;
    }
}
