<?php

namespace Provisioning\Model;

class Message
{

    public $message_id;
    public $queue_id;
    public $handle;
    public $body;
    public $md5;
    public $timeout;
    public $created;
    public $xmlId;

    public function exchangeArray($data)
    {
        foreach (get_object_vars($this) as $key => $value) {
            $this->$key = isset($data[$key]) ? $data[$key] : null;
        }
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
