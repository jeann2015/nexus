<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\CommandInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Command extends CommandInputFilter {
    
    public $name;
    public $command;
    public $xml;

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
   
    public function getXml() {
        return $this->xml;
    }

    public function setXml($xml) {
        $this->xml = $xml;
    }
    
    public function getCommand() {
        return $this->command;
    }

    public function setCommand($command) {
        $this->command = $command;
    }

}
