<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\CreationGroupInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class CreationGroup extends CreationGroupInputFilter {
    
    
    public $id_order;
    public $id_user;
    public $id_group;
    public $id_service_provider;
    public $domain;
    public $userlimit;
    public $groupname;
    public $callinglineidname;
    public $timezone;
    public $contactname;
    public $contactnumber;
    public $type;
    public $createdate;
    public $command;
    public $protocol;
    public $devicename;
    public $devicetype;
    public $transportprotocol;
    public $lastname;
    public $firstname;
    public $callinglineidlastname;
    public $callinglineidfirstname;
    public $password;
    public $languague;
    public $number_empresa;
    public $number_grupo;
    public $number_usuario;
    public $devicelevel;
    public $lineport;
    public $servicename;
    public $username;
    public $newpassword;
    public $number_activar;
    public $name;
    
    public function getcontactnumber() {
        return $this->contactnumber;
    }

    public function setcontactnumber($contactnumber) {
        $this->contactnumber = $contactnumber;
    }

    public function getcallinglineidname() {
        return $this->callinglineidname;
    }

    public function setcallinglineidname($callinglineidname) {
        $this->callinglineidname = $callinglineidname;
    }


    public function getname() {
        return $this->name;
    }

    public function setname($name) {
        $this->name = $name;
    }
    
    public function getnumber_activar() {
        return $this->number_activar;
    }

    public function setnumber_activar($number_activar) {
        $this->number_activar = $number_activar;
    }

    public function getnewpassword() {
        return $this->newpassword;
    }

    public function setnewpassword($newpassword) {
        $this->newpassword = $newpassword;
    }

    public function getusername() {
        return $this->username;
    }

    public function setusername($username) {
        $this->username = $username;
    }

    public function getservicename() {
        return $this->servicename;
    }

    public function setservicename($servicename) {
        $this->servicename = $servicename;
    }

    public function getlineport() {
        return $this->lineport;
    }

    public function setlineport($lineport) {
        $this->lineport = $lineport;
    }

    public function getdevicelevel() {
        return $this->devicelevel;
    }

    public function setdevicelevel($devicelevel) {
        $this->devicelevel = $devicelevel;
    }

    public function getnumber_usuario() {
        return $this->number_usuario;
    }

    public function setnumber_usuario($number_usuario) {
        $this->number_usuario = $number_usuario;
    }

    public function getnumber_grupo() {
        return $this->number_grupo;
    }

    public function setnumber_grupo($number_grupo) {
        $this->number_grupo = $number_grupo;
    }

    public function getnumber_empresa() {
        return $this->number_empresa;
    }

    public function setnumber_empresa($number_empresa) {
        $this->number_empresa = $number_empresa;
    }

    public function getlanguague() {
        return $this->languague;
    }

    public function setlanguague($languague) {
        $this->languague = $languague;
    }

    public function getpassword() {
        return $this->password;
    }

    public function setpassword($password) {
        $this->password = $password;
    }

    public function getcallinglineidfirstname() {
        return $this->callinglineidfirstname;
    }

    public function setcallinglineidfirstname($callinglineidfirstname) {
        $this->callinglineidfirstname = $callinglineidfirstname;
    }

    public function getcallinglineidlastname() {
        return $this->callinglineidlastname;
    }

    public function setcallinglineidlastname($callinglineidlastname) {
        $this->callinglineidlastname = $callinglineidlastname;
    }

    public function getfirstname() {
        return $this->firstname;
    }

    public function setfirstname($firstname) {
        $this->firstname = $firstname;
    }

    public function getlastname() {
        return $this->lastname;
    }

    public function setlastname($lastname) {
        $this->lastname = $lastname;
    }

    public function gettransportprotocol() {
        return $this->transportprotocol;
    }

    public function settransportprotocol($transportprotocol) {
        $this->transportprotocol = $transportprotocol;
    }

    public function getdevicetype() {
        return $this->devicetype;
    }

    public function setdevicetype($devicetype) {
        $this->devicetype = $devicetype;
    }

    public function getdevicename() {
        return $this->devicename;
    }

    public function setdevicename($devicename) {
        $this->devicename = $devicename;
    }

    public function getprotocol() {
        return $this->protocol;
    }

    public function setprotocol($protocol) {
        $this->protocol = $protocol;
    }

    public function getcommand() {
        return $this->command;
    }

    public function setcommand($command) {
        $this->command = $command;
    }


    public function getcreatedate() {
        return $this->createdate;
    }

    public function setcreatedate($createdate) {
        $this->createdate = $createdate;
    }

    public function getcontactname() {
        return $this->contactname;
    }

    public function setcontactname($contactname) {
        $this->contactname = $contactname;
    }

    public function gettimezone() {
        return $this->timezone;
    }

    public function settimezone($timezone) {
        $this->timezone = $timezone;
    }

    public function getgroupname() {
        return $this->groupname;
    }

    public function setgroupname($groupname) {
        $this->groupname = $groupname;
    }

    public function getuserlimit() {
        return $this->userlimit;
    }

    public function setuserlimit($userlimit) {
        $this->userlimit = $userlimit;
    }

    public function getdomain() {
        return $this->domain;
    }

    public function setdomain($domain) {
        $this->domain = $domain;
    }

    public function getid_service_provider() {
        return $this->id_service_provider;
    }

    public function setid_service_provider($id_service_provider) {
        $this->id_service_provider = $id_service_provider;
    }


    public function getid_group() {
        return $this->id_group;
    }

    public function setid_group($id_group) {
        $this->id_group = $id_group;
    }


    public function getid_user() {
        return $this->id_user;
    }

    public function setid_user($id_user) {
        $this->id_user = $id_user;
    }


    public function getid_order() {
        return $this->id_order;
    }

    public function setid_order($id_order) {
        $this->id_order = $id_order;
    }


    public function gettype() {
        return $this->type;
    }

    public function settype($type) {
        $this->type = $type;
    }
    
    
   
}
