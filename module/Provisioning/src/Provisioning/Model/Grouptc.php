<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\GrouptcInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Grouptc extends GrouptcInputFilter {
    
    public $group;
    public $description;
    public $datecreated;
    public $providerId;

    /**
     * Gets the value of group.
     *
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Sets the value of group.
     *
     * @param mixed $group the group
     *
     * @return self
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param mixed $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets the value of datecreated.
     *
     * @return mixed
     */
    public function getDatecreated()
    {
        return $this->datecreated;
    }

    /**
     * Sets the value of datecreated.
     *
     * @param mixed $datecreated the datecreated
     *
     * @return self
     */
    public function setDatecreated($datecreated)
    {
        $this->datecreated = $datecreated;

        return $this;
    }

    /**
     * Gets the value of providerId.
     *
     * @return mixed
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Sets the value of providerId.
     *
     * @param mixed $providerId the provider id
     *
     * @return self
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }
}
