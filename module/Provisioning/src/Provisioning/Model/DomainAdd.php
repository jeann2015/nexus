<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\DomainAddInputFilter;
/**
 * Description of Domain
 *
 * @author rodolfo
 */
class DomainAdd extends DomainAddInputFilter {
    
    public $name;
    public $id_order;
    public $id_domain;

    

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getid_order()
    {
        return $this->id_order;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setid_order($id_order)
    {
        $this->id_order = $id_order;

        return $this;
    }

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getid_domain()
    {
        return $this->id_domain;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setid_domain($id_domain)
    {
        $this->id_domain = $id_domain;

        return $this;
    }


    
}
