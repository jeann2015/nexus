<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ProviderInputFilter;
/**
 * Description of Provider
 *
 * @author rodolfo
 */
class Provider extends ProviderInputFilter {
    
    public $name;
    public $domain;
    public $platformId;
    public $inbroadsoft;
    public $isenterprise;

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of domain.
     *
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Sets the value of domain.
     *
     * @param mixed $domain the domain
     *
     * @return self
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setPlatformId($platformId)
    {
        $this->platformId = $platformId;

        return $this;
    }

    /**
     * Gets the value of inbroadsoft.
     *
     * @return mixed
     */
    public function getInbroadsoft()
    {
        return $this->inbroadsoft;
    }

    /**
     * Sets the value of inbroadsoft.
     *
     * @param mixed $inbroadsoft the inbroadsoft
     *
     * @return self
     */
    public function setInbroadsoft($inbroadsoft)
    {
        $this->inbroadsoft = $inbroadsoft;

        return $this;
    }

    /**
     * Gets the value of isenterprise.
     *
     * @return mixed
     */
    public function getIsenterprise()
    {
        return $this->isenterprise;
    }

    /**
     * Sets the value of isenterprise.
     *
     * @param mixed $isenterprise the isenterprise
     *
     * @return self
     */
    public function setIsenterprise($isenterprise)
    {
        $this->isenterprise = $isenterprise;

        return $this;
    }
}
