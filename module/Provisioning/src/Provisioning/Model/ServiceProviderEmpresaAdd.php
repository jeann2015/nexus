<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ServiceProviderEmpresaAddInputFilter;
/**
 * Description of Domain
 *
 * @author rodolfo
 */
class ServiceProviderEmpresaAdd extends ServiceProviderEmpresaAddInputFilter {
    
    public $id_order;
    public $id_service_provider;
    public $name;
    public $number;

    

  

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getid_order()
    {
        return $this->id_order;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setid_order($id_order)
    {
        $this->id_order = $id_order;

        return $this;
    }

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function get_service_provider()
    {
        return $this->id_service_provider;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function set_service_provider($id_service_provider)
    {
        $this->id_service_provider = $id_service_provider;

        return $this;
    }

      /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

      /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }


    
}
