<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\AdditionLineInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class AdditionLine extends AdditionLineInputFilter {
    
    public $type;
    public $id_order;
    public $id_group;
    public $id_service_provider;
    public $command;
    public $id_user;
    public $name;
    public $createdate;
    public $contactname;
    public $contactnumber;
    public $timezone;
    public $limituser;
    public $lastname;
    public $firtsname;
    public $callinglastname;
    public $callingfirstname;
    public $clave;
    public $number;
    public $devicelevel;
    public $devicename;
    public $lineport;
    public $groupname;
    public $groupnamenew;
    public $userid1;
    public $userid2;
    public $id_product;
    
    public function getid_product() {
        return $this->id_product;
    }

    public function setid_product($id_product) {
        $this->id_product = $id_product;
    }
    
    public function getuserid1() {
        return $this->userid1;
    }

    public function setuserid1($userid1) {
        $this->userid1 = $userid1;
    }

    public function getuserid2() {
        return $this->userid2;
    }

    public function setuserid2($userid2) {
        $this->userid2 = $userid2;
    }

    public function getgroupnamenew() {
        return $this->groupnamenew;
    }

    public function setgroupnamenew($groupnamenew) {
        $this->groupnamenew = $groupnamenew;
    }

    public function getgroupname() {
        return $this->groupname;
    }

    public function setgroupname($groupname) {
        $this->groupname = $groupname;
    }


    public function getlineport() {
        return $this->lineport;
    }

    public function setlineport($lineport) {
        $this->lineport = $lineport;
    }

    public function getdevicename() {
        return $this->devicename;
    }

    public function setdevicename($devicename) {
        $this->devicename = $devicename;
    }


    public function getdevicelevel() {
        return $this->devicelevel;
    }

    public function setdevicelevel($devicelevel) {
        $this->devicelevel = $devicelevel;
    }


    public function getnumber() {
        return $this->number;
    }

    public function setnumber($number) {
        $this->number = $number;
    }


    public function getclave() {
        return $this->clave;
    }

    public function setclave($clave) {
        $this->clave = $clave;
    }

    public function getcallingfirstname() {
        return $this->callingfirstname;
    }

    public function setcallingfirstname($callingfirstname) {
        $this->callingfirstname = $callingfirstname;
    }


    public function getcallinglastname() {
        return $this->callinglastname;
    }

    public function setcallinglastname($callinglastname) {
        $this->callinglastname = $callinglastname;
    }


    public function getfirstname() {
        return $this->firtsname;
    }

    public function setfirstname($firtsname) {
        $this->firtsname = $firtsname;
    }


    public function getlastname() {
        return $this->lastname;
    }

    public function setlastname($lastname) {
        $this->lastname = $lastname;
    }

    public function gettype() {
        return $this->type;
    }

    public function settype($type) {
        $this->type = $type;
    }

    public function getid_order() {
        return $this->id_order;
    }

    public function setid_order($id_order) {
        $this->id_order = $id_order;
    }

    public function getid_group() {
        return $this->id_group;
    }

    public function setid_group($id_group) {
        $this->id_group = $id_group;
    }  


    public function getid_service_provider() {
        return $this->id_service_provider;
    }

    public function setid_service_provider($id_service_provider) {
        $this->id_service_provider = $id_service_provider;
    } 

    public function getcommand() {
        return $this->command;
    }

    public function setcommand($command) {
        $this->command = $command;
    }  

    public function getid_user() {
        return $this->id_user;
    }

    public function setid_user($id_user) {
        $this->id_user = $id_user;
    }   

    public function getname() {
        return $this->name;
    }

    public function setname($name) {
        $this->name = $name;
    }  

    public function getcreatedate() {
        return $this->createdate;
    }

    public function setcreatedate($createdate) {
        $this->createdate = $createdate;
    }   

    public function getcontactname() {
        return $this->contactname;
    }

    public function setcontactname($contactname) {
        $this->contactname = $contactname;
    } 

    public function getcontactnumber() {
        return $this->contactnumber;
    }

    public function setcontactnumber($contactnumber) {
        $this->contactnumber = $contactnumber;
    }  

    public function gettimezone() {
        return $this->timezone;
    }

    public function settimezone($timezone) {
        $this->timezone = $timezone;
    }

    public function getlimituser() {
        return $this->limituser;
    }

    public function setlimituser($limituser) {
        $this->limituser = $limituser;
    }   
   
}
