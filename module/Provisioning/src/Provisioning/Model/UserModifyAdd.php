<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\UserModifyAddInputFilter;
/**
 * Description of Domain
 *
 * @author rodolfo
 */
class UserModifyAdd extends UserModifyAddInputFilter {
    
    public $number;
    public $id_order;
    public $id_user;
    public $lineport;
    public $devicegroup;
    public $devicename;

    

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getid_order()
    {
        return $this->id_order;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setid_order($id_order)
    {
        $this->id_order = $id_order;

        return $this;
    }

     /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getid_user()
    {
        return $this->id_user;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setid_user($id_user)
    {
        $this->id_user = $id_user;

        return $this;
    }

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getlineport()
    {
        return $this->lineport;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setlineport($lineport)
    {
        $this->lineport = $lineport;

        return $this;
    }

     /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getdevicegroup()
    {
        return $this->devicegroup;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setdevicegroup($devicegroup)
    {
        $this->devicegroup = $devicegroup;

        return $this;
    }

     /**
     * Gets the value of platformId.
     *
     * @return mixed
     */

    public function getdevicename()
    {
        return $this->devicename;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */

    public function setdevicename($devicename)
    {
        $this->devicename = $devicename;

        return $this;
    }


    
}
