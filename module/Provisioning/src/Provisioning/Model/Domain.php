<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\DomainInputFilter;
/**
 * Description of Domain
 *
 * @author rodolfo
 */
class Domain extends DomainInputFilter {
    
    public $name;
    public $platformId;
    public $inbroadsoft;
    public $default;

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param mixed $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setPlatformId($platformId)
    {
        $this->platformId = $platformId;

        return $this;
    }

    /**
     * Gets the value of inbroadsoft.
     *
     * @return mixed
     */
    public function getInbroadsoft()
    {
        return $this->inbroadsoft;
    }

    /**
     * Sets the value of inbroadsoft.
     *
     * @param mixed $inbroadsoft the inbroadsoft
     *
     * @return self
     */
    public function setInbroadsoft($inbroadsoft)
    {
        $this->inbroadsoft = $inbroadsoft;

        return $this;
    }

    /**
     * Gets the value of default.
     *
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Sets the value of default.
     *
     * @param mixed $default the default
     *
     * @return self
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }
}
