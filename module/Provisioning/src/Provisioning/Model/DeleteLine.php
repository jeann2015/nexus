<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\DeleteLineInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class DeleteLine extends DeleteLineInputFilter {
    
    public $type;
    public $id_order;
    public $id_group;
    public $id_service_provider;
    public $command;
    public $id_user;
    public $name;
    public $createdate;
    public $contactname;
    public $contactnumber;
    public $timezone;
    public $userlimit;
    public $groupname;
    public $callinglineidname;
    public $number_grupo;
    public $number_empresa;
    public $domain;
    
    public function getcontactnumber() {
        return $this->contactnumber;
    }

    public function setcontactnumber($contactnumber) {
        $this->contactnumber = $contactnumber;
    }

    public function getdomain() {
        return $this->domain;
    }

    public function setdomain($domain) {
        $this->domain = $domain;
    }

    public function getnumber_empresa() {
        return $this->number_empresa;
    }

    public function setnumber_empresa($number_empresa) {
        $this->number_empresa = $number_empresa;
    }

    public function getgroupname() {
        return $this->groupname;
    }

    public function setgroupname($groupname) {
        $this->groupname = $groupname;
    }

    public function getcontactname() {
        return $this->contactname;
    }

    public function setcontactname($contactname) {
        $this->contactname = $contactname;
    }


   


    public function setcallinglineidname() {
        return $this->callinglineidname;
    }

    public function setsetcallinglineidname($callinglineidname) {
        $this->callinglineidname = $callinglineidname;
    }


    
    public function gettype() {
        return $this->type;
    }

    public function settype($type) {
        $this->type = $type;
    }

    public function getid_order() {
        return $this->id_order;
    }

    public function setid_order($id_order) {
        $this->id_order = $id_order;
    }

    public function getid_group() {
        return $this->id_group;
    }

    public function setid_group($id_group) {
        $this->id_group = $id_group;
    }  


    public function getid_service_provider() {
        return $this->id_service_provider;
    }

    public function setid_service_provider($id_service_provider) {
        $this->id_service_provider = $id_service_provider;
    } 

    public function getcommand() {
        return $this->command;
    }

    public function setcommand($command) {
        $this->command = $command;
    }  

    public function getid_user() {
        return $this->id_user;
    }

    public function setid_user($id_user) {
        $this->id_user = $id_user;
    }   

    public function getname() {
        return $this->name;
    }

    public function setname($name) {
        $this->name = $name;
    }  

    public function getcreatedate() {
        return $this->createdate;
    }

    public function setcreatedate($createdate) {
        $this->createdate = $createdate;
    }   

    

    public function getnumber_grupo() {
        return $this->number_grupo;
    }

    public function setnumber_grupo($number_grupo) {
        $this->number_grupo = $number_grupo;
    }  

    public function gettimezone() {
        return $this->timezone;
    }

    public function settimezone($timezone) {
        $this->timezone = $timezone;
    }

    public function getuserlimit() {
        return $this->userlimit;
    }

    public function setuserlimit($userlimit) {
        $this->userlimit = $userlimit;
    }   
   
}
