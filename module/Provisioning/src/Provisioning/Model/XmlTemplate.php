<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\XmlTemplateInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class XmlTemplate extends XmlTemplateInputFilter {
    
    public $name;
    public $command;
    public $xml;
    public $category;
    public $addxmltemplateId;
    

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the value of name.
     *
     * @param mixed $name the name 
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of command.
     *
     * @return mixed
     */
    public function getCommand()
    {
        return $this->command;
    }
    
    /**
     * Sets the value of command.
     *
     * @param mixed $command the command 
     *
     * @return self
     */
    public function setCommand($command)
    {
        $this->command = $command;

        return $this;
    }

    /**
     * Gets the value of xml.
     *
     * @return mixed
     */
    public function getXml()
    {
        return $this->xml;
    }
    
    /**
     * Sets the value of xml.
     *
     * @param mixed $xml the xml 
     *
     * @return self
     */
    public function setXml($xml)
    {
        $this->xml = $xml;

        return $this;
    }

    /**
     * Gets the value of category.
     *
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }
    
    /**
     * Sets the value of category.
     *
     * @param mixed $category the category 
     *
     * @return self
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Gets the value of addxmltemplateId.
     *
     * @return mixed
     */
    public function getAddxmltemplateId()
    {
        return $this->addxmltemplateId;
    }
    
    /**
     * Sets the value of addxmltemplateId.
     *
     * @param mixed $addxmltemplateId the addxmltemplate id 
     *
     * @return self
     */
    public function setAddxmltemplateId($addxmltemplateId)
    {
        $this->addxmltemplateId = $addxmltemplateId;

        return $this;
    }

    
    
}
