<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\Model;

use Provisioning\InputFilter\ChangeNumberInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class ChangeNumber extends ChangeNumberInputFilter {
    
    public $type;
    public $id_order;
    public $id_group;
    public $id_service_provider;
    public $command;
    public $id_user;
    public $name;
    public $createdate;
    public $devicelevel;
    public $devicename;
    public $lineport;
    public $number_grupo;
    public $number_empresa;
    public $number_agregar;
    public $number_asignar;
    public $number_usuario;
    public $number_activar;
    

    public function getnumber_activar() {
        return $this->number_activar;
    }

    public function setnumber_activar($number_activar) {
        $this->number_activar = $number_activar;
    } 

    public function getnumber_usuario() {
        return $this->number_usuario;
    }

    public function setnumber_usuario($number_usuario) {
        $this->number_usuario = $number_usuario;
    } 

    public function getnumber_asignar() {
        return $this->number_asignar;
    }

    public function setnumber_asignar($number_asignar) {
        $this->number_asignar = $number_asignar;
    } 


    public function getnumber_agregar() {
        return $this->number_agregar;
    }

    public function setnumber_agregar($number_agregar) {
        $this->number_agregar = $number_agregar;
    } 

    public function getnumber_empresa() {
        return $this->number_empresa;
    }

    public function setnumber_empresa($number_empresa) {
        $this->number_empresa = $number_empresa;
    }  

    public function getnumber_grupo() {
        return $this->number_grupo;
    }

    public function setnumber_grupo($number_grupo) {
        $this->number_grupo = $number_grupo;
    }    


    public function getlineport() {
        return $this->lineport;
    }

    public function setlineport($lineport) {
        $this->lineport = $lineport;
    }

    public function getdevicename() {
        return $this->devicename;
    }

    public function setdevicename($devicename) {
        $this->devicename = $devicename;
    }


    public function getdevicelevel() {
        return $this->devicelevel;
    }

    public function setdevicelevel($devicelevel) {
        $this->devicelevel = $devicelevel;
    }


    public function getnumber() {
        return $this->number;
    }

    public function setnumber($number) {
        $this->number = $number;
    }


    public function gettype() {
        return $this->type;
    }

    public function settype($type) {
        $this->type = $type;
    }

    public function getid_order() {
        return $this->id_order;
    }

    public function setid_order($id_order) {
        $this->id_order = $id_order;
    }

    public function getid_group() {
        return $this->id_group;
    }

    public function setid_group($id_group) {
        $this->id_group = $id_group;
    }  


    public function getid_service_provider() {
        return $this->id_service_provider;
    }

    public function setid_service_provider($id_service_provider) {
        $this->id_service_provider = $id_service_provider;
    } 

    public function getcommand() {
        return $this->command;
    }

    public function setcommand($command) {
        $this->command = $command;
    }  

    public function getid_user() {
        return $this->id_user;
    }

    public function setid_user($id_user) {
        $this->id_user = $id_user;
    }   

    public function getname() {
        return $this->name;
    }

    public function setname($name) {
        $this->name = $name;
    }  

    public function getcreatedate() {
        return $this->createdate;
    }

    public function setcreatedate($createdate) {
        $this->createdate = $createdate;
    }   

   
    
   
}
