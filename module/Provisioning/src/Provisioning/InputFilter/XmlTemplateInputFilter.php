<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Provisioning\InputFilter;

use Zend\InputFilter\InputFilter;
use Generic\Model\Generic;

/**
 * Description of XmlTemplateInputFilter
 *
 * @author rodolfo
 */

class XmlTemplateInputFilter extends Generic
{
    

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }

	    $this->inputFilter->add(array(
	        'name'        => 'addxmltemplateId',
	        'required'    => false,
	        'allow_empty' => true,
	    ));

        return $this->inputFilter;
    }
}