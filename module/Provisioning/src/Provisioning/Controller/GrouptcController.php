<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Provisioning\Model\Grouptc;
use Provisioning\Form\GrouptcForm;
use Generic\Controller\GenericController;

use Provisioning\Service\OCISoapService;
use Provisioning\Service\XmlService;

class GrouptcController extends GenericController
{ 
    //public $serviceDao;
    
    public function indexAction()
    {
    	
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    public function listAction()
    {
    	
		
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $structure = $this->getDao()->fetchAll();

        return new ViewModel(array('grouptc' => $structure));
    }
	
	
      
    public function addAction()
    {
        $grouptc = new Grouptc();
        $form = new GrouptcForm();
        $summary = "";

        /**
         * Llenamos el select con los service providers no enterprise y in broadsoft
         */
        $providerSelect = $form->get('providerId');       
        $providerFetch = $this->getDao("ProviderDao")->fetchNoEnterprise();
        $providerArray = array();
        foreach ($providerFetch as $provider) {
            $providerArray[$provider->id] = $provider->name;
        }
        $providerSelect->setAttribute('options',$providerArray);
    	
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand('add','GroupAddRequest');
            if($xmltemplate) { 
                $xml = $xmltemplate->getXml();

                $post = $request->getPost();
                $provider = $this->getDao("ProviderDao")->get($post['providerId']);

                $variables = array(
                    '%SERVICE_PROVIDER%'    =>  $provider->name,
                    '%GROUP_ID%'            =>  $post['group'],
                    '%DOMAIN%'              =>  $provider->domain,
                    '%USERLIMIT%'           =>  '999',
                    '%GROUP_NAME%'          =>  $post['group'],
                    '%CALLING_LINE_ID_NAME%'=>  $post['group'],
                    '%TIMEZONE%'            =>  'America/Panama',
                    '%CONTACT_NAME%'        =>  'CWP',      // Para un group que no es empresa hay que ver que poner aqui
                    '%CONTACT_NUMBER%'      =>  '800-0000', // Para un group que no es empresa hay que ver que poner aqui
                    '%CONTACT_EMAIL%'       =>  'soporte@cwpanama.com',// Para un group que no es empresa hay que ver que poner aqui
                );

                $xmlService = new XmlService();
                $xmlGenerated = $xmlService->merge($xml,$variables);
                $this->log($xmlGenerated);

                $OCISoapService = new OCISoapService($this->getServiceLocator());
                $response = $OCISoapService->send($xmlGenerated);
                $this->log($response);
                $xmlresponse = $xmlService->getResponseFromXml($response);

                $form->setInputFilter($grouptc->getInputFilter());
                $form->setData($request->getPost());

                if($xmlresponse->result == "success"){

                    if ($form->isValid()) {
                        $grouptc->exchangeArray($form->getData());
                        $grouptc->setDatecreated(date('Y-m-d H:i:s'));
                        $this->getDao()->save($grouptc);
                        return $this->redirect()->toRoute('grouptc',array('action'=>'list'));
                    }
                } else {
                    $summary = $xmlresponse->summary;
                }
            } else {
                $summary = "[Error] No existe comando xml de creación.";
            }
        }

        return new ViewModel(array(
            'form'      => $form,
            'grouptc'   => $grouptc,
            'summary'   => $summary,
        ));
    }
    
    public function editAction()
    {
        $form = new GrouptcForm();

        $id = $this->params()->fromRoute('id');
        $grouptc = $this->getDao()->get($id);

        /**
         * Llenamos el select con los service providers no enterprise y in broadsoft
         */
        $providerSelect = $form->get('providerId');       
        $providerFetch = $this->getDao("ProviderDao")->fetchNoEnterprise();
        $providerArray = array();
        foreach ($providerFetch as $provider) {
            if($grouptc->providerId == $provider->id){
                $providerArray[$provider->id] = $provider->name;
            }
        }
        
        $providerSelect->setAttribute('options',$providerArray);
        
       
        $request = $this->getRequest();
        if ($request->isPost()) {   
          
            $form->setInputFilter($grouptc->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $grouptc->exchangeArray($form->getData());
                  
                $this->getDao()->save($grouptc);
                return $this->redirect()->toRoute('grouptc',array('action'=>'list'));
            }

        } else {
            $form->setData($grouptc->getArrayCopy());
        }
        
        //$commands = $this->getDao("CommandDao")->fetchAll();
		$commands = $this->getDao()->fetchAll();
        $commandsArray = array();
        foreach ($commands as $command) {
            $commandsArray[$command->id] = $command->name;
        }

        $form->get("group")->setAttribute("readonly","readonly");
        $form->get("providerId")->setAttribute("readonly","readonly");

        return new ViewModel(array(
            'form'      => $form,
            'grouptc'   => $grouptc,
        ));
    }
    
    public function commandAction()
    {
        $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                
                $thePost = json_decode($request->getContent());
                $scdao = $this->getDao("ServiceCommandDao");
                $scdao->delete($id); //Elimina todo
                foreach ($thePost as $item) {
                    //$sc = $scdao->getByServiceIdAndCommandId($item->serviceId,$item->commandId);
                    //if($sc) {
                        
                    //}
                    $sc = new ServiceCommand();
                    $sc->setPriority($item->priority);
                    $sc->setXmltemplateId($item->xmltemplateId);
                    $sc->setCaseId($item->caseId);
                    $scdao->save($sc);
                    unset($sc);
                }
                
                $return = array("result" => "success","payload"=>$thePost);
                
                
            } catch (\Exception $e){
                
                $return = array("result" => "error","payload"=>$e->getMessage());
            }
            
            return new JsonModel($return);

        } else {
            $id = $this->params()->fromRoute('id');
            $service = $this->getDao()->get($id);

            $form = new ServiceForm();
            $form->setData($service->getArrayCopy());
        
        
            $commands = $this->getDao("CommandDao")->fetchAll();
            $commandsArray = array();
            foreach ($commands as $command) {
                $commandsArray[$command->id] = $command->name;
            }
            
            $servicecommands = $this->getDao("ServiceCommandDao")->fetchByServiceId($id);
            $servicecommandsArray = array();
            foreach ($servicecommands as $servicecommand) {
                $servicecommandsArray[$servicecommand->xmltemplate_id] = $servicecommand->priority;
            }
            
            return new ViewModel(array(
                'service'           => $service,
                'commands'          => $commandsArray,
                'servicecommands'   => $servicecommandsArray,
            ));
        }
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id         = $this->params()->fromRoute('id');
            $group      = $this->getDao()->get($id);
            $provider   = $this->getDao("ProviderDao")->get($group->providerId);
            
            $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand('delete','GroupDeleteRequest');
            if($xmltemplate) { 
                $xml = $xmltemplate->getXml();

                $variables = array(
                    '%SERVICE_PROVIDER%'    =>  $provider->name,
                    '%GROUP_ID%'            =>  $group->group,
                );

                $xmlService = new XmlService();
                $xmlGenerated = $xmlService->merge($xml,$variables);
                $this->log($xmlGenerated);

                $OCISoapService = new OCISoapService($this->getServiceLocator());
                $response = $OCISoapService->send($xmlGenerated);
                $this->log($response);
                $xmlresponse = $xmlService->getResponseFromXml($response);

                if($xmlresponse->result == "success"){
                    $this->getDao()->delete($id);
                    $model->setVariable('status', "success");
                    $model->setVariable('payload', $id);
                } else {
                    $summary = $xmlresponse->summary;
                    $model->setVariable('status', 'error');
                    $model->setVariable('payload', $summary);
                }
            } else {
                $summary = "[Error] No existe comando xml de eliminación.";
                $model->setVariable('status', 'error');
                $model->setVariable('payload', $summary);
            }


        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "GrouptcDao")
    {
        //if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->structureDao = $sm->get('Provisioning\Dao\\'.$dao);
        //}
        
        return $this->structureDao;
    }

}
