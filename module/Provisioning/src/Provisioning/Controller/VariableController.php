<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Generic\Controller\GenericController;

class VariableController extends GenericController
{ 
    public $serviceDao;
    
    public function getDao()
    {
        if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get('Provisioning\Dao\VariableDao');
        }
        
        return $this->serviceDao;
    }

}
