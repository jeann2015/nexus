<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Provisioning\Model\Typec;
use Provisioning\Form\TypecForm;
use Generic\Controller\GenericController;

class TypecController extends GenericController
{ 
    //public $serviceDao;
    
    public function indexAction()
    {
    	
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    public function listAction()
    {
    	
		
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $typec = $this->getDao()->fetchAllS();

        return new ViewModel(array('typec' => $typec));
    }
	
	
      
    public function addAction()
    {
        $typec = new Typec();
        $form = new TypecForm();
    	
        $structureidSelect = $form->get('structure');
        $structureFetch = $this->getDao("StructureDao")->fetchAll();
        $structureArray = array();

        foreach ($structureFetch as $structure) {
            $structureArray[$structure->id]=$structure->name;                
        }

         $structureidSelect->setAttribute('options',$structureArray);


        $request = $this->getRequest();
        if ($request->isPost()) {
            
            
            
            $form->setInputFilter($typec->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
               

                $name = $form->get("name")->getValue();
                $description = $form->get("description")->getValue();
                $nivel = $form->get("structure")->getValue();


                $fechaHora = date("Y-m-d H:i:s");                
                $typec->setname($name);
                $typec->setdescription($description);
                $typec->setdatecreated($fechaHora);
                $typec->setstructure($nivel);


                $this->getDao()->save($typec);
                return $this->redirect()->toRoute('typec',array('action'=>'list'));
            }

        }

        return new ViewModel(array(
            'form'      => $form,
            'typec'   => $typec,
        ));
    }
    
    public function editAction()
    {

        $form = new TypecForm();
        $id = $this->params()->fromRoute('id');
        

        $structureidSelect = $form->get('structure');
        $structureFetch = $this->getDao("StructureDao")->fetchAll();
        $structureArray = array();

        foreach ($structureFetch as $structure) {
        $structureArray[$structure->id]=$structure->name;                
        }     

        $structureidSelect->setAttribute('options',$structureArray);
        
        $typec  = $this->getDao()->get($id);

        $request = $this->getRequest();
        if ($request->isPost()) {   
           
            $form->setInputFilter($typec->getInputFilter());
           
            $form->setData($request->getPost());
            if ($form->isValid()) {
                
              
                $fechaHora = date("Y-m-d H:i:s");   
                $name = $form->get("name")->getValue();
                $description = $form->get("description")->getValue();
                $id_structure = $form->get("structure")->getValue();
                $typec->setName($name);
                $typec->setdescription($description);
                $typec->setstructure($id_structure);
                $typec->setdatecreated($fechaHora);

               

                $this->getDao()->save($typec);
                
                

                return $this->redirect()->toRoute('typec',array('action'=>'list'));
            }

        } else {
            
            $form->setData($typec->getArrayCopy());
        }
        
        return new ViewModel(array(
            'form'      => $form,
            'typec'   => $typec,
        ));
    }
    
    public function commandAction()
    {
        $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                
                $thePost = json_decode($request->getContent());
                $scdao = $this->getDao("ServiceCommandDao");
                $scdao->delete($id); //Elimina todo
                foreach ($thePost as $item) {
                    //$sc = $scdao->getByServiceIdAndCommandId($item->serviceId,$item->commandId);
                    //if($sc) {
                        
                    //}
                    $sc = new ServiceCommand();
                    $sc->setPriority($item->priority);
                    $sc->setXmltemplateId($item->xmltemplateId);
                    $sc->setCaseId($item->caseId);
                    $scdao->save($sc);
                    unset($sc);
                }
                
                $return = array("result" => "success","payload"=>$thePost);
                
                
            } catch (\Exception $e){
                
                $return = array("result" => "error","payload"=>$e->getMessage());
            }
            
            return new JsonModel($return);

        } else {
            $id = $this->params()->fromRoute('id');
            $service = $this->getDao()->get($id);

            $form = new ServiceForm();
            $form->setData($service->getArrayCopy());
        
        
            $commands = $this->getDao("CommandDao")->fetchAll();
            $commandsArray = array();
            foreach ($commands as $command) {
                $commandsArray[$command->id] = $command->name;
            }
            
            $servicecommands = $this->getDao("ServiceCommandDao")->fetchByServiceId($id);
            $servicecommandsArray = array();
            foreach ($servicecommands as $servicecommand) {
                $servicecommandsArray[$servicecommand->xmltemplate_id] = $servicecommand->priority;
            }
            
            return new ViewModel(array(
                'service'           => $service,
                'commands'          => $commandsArray,
                'servicecommands'   => $servicecommandsArray,
            ));
        }
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "TypecDao")
    {
        //if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->typecDao = $sm->get('Provisioning\Dao\\'.$dao);
        //}
        
        return $this->typecDao;
    }

}
