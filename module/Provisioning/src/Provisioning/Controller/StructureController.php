<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Provisioning\Model\Structure;
use Provisioning\Form\StructureForm;
use Generic\Controller\GenericController;

class StructureController extends GenericController
{ 
    //public $serviceDao;
    
    public function indexAction()
    {
    	
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    public function listAction()
    {
    	
		
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $structure = $this->getDao()->fetchAll();

        return new ViewModel(array('structure' => $structure));
    }
	
	
      
    public function addAction()
    {
        $structure = new Structure();
        $form = new StructureForm();
    	
        $request = $this->getRequest();
        if ($request->isPost()) {
            

            $name = $form->get("name")->getValue();
            $description = $form->get("description")->getValue();
            $nivel = $form->get("structure")->getValue();
            $fechaHora = date("Y-m-d H:i:s");                
            $structure->setname($name);
            $structure->setdescription($description);
            $structure->setdatecreated($fechaHora);
            $structure->setstructure($nivel);
            

            
            $form->setInputFilter($structure->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $structure->exchangeArray($form->getData());
                $this->getDao()->save($structure);

                return $this->redirect()->toRoute('structure',array('action'=>'list'));
            }

        }

        return new ViewModel(array(
            'form'      => $form,
            'structure'   => $structure,
        ));
    }
    
    public function editAction()
    {
        $form = new StructureForm();

        $id = $this->params()->fromRoute('id');
        $structure = $this->getDao()->get($id);
        
       
       
        $request = $this->getRequest();
        if ($request->isPost()) {   

            //$structure = new Structure();         
           
            $form->setInputFilter($structure->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $structure->exchangeArray($form->getData());
                  
                $this->getDao()->save($structure);

                return $this->redirect()->toRoute('structure',array('action'=>'list'));
            }

        } else {
            $form->setData($structure->getArrayCopy());
        }
        
        

        return new ViewModel(array(
            'form'      => $form,
            'structure'   => $structure,
        ));
    }
    
    public function commandAction()
    {
        $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                
                $thePost = json_decode($request->getContent());
                $scdao = $this->getDao("ServiceCommandDao");
                $scdao->delete($id); //Elimina todo
                foreach ($thePost as $item) {
                    //$sc = $scdao->getByServiceIdAndCommandId($item->serviceId,$item->commandId);
                    //if($sc) {
                        
                    //}
                    $sc = new ServiceCommand();
                    $sc->setPriority($item->priority);
                    $sc->setXmltemplateId($item->xmltemplateId);
                    $sc->setCaseId($item->caseId);
                    $scdao->save($sc);
                    unset($sc);
                }
                
                $return = array("result" => "success","payload"=>$thePost);
                
                
            } catch (\Exception $e){
                
                $return = array("result" => "error","payload"=>$e->getMessage());
            }
            
            return new JsonModel($return);

        } else {
            $id = $this->params()->fromRoute('id');
            $service = $this->getDao()->get($id);

            $form = new ServiceForm();
            $form->setData($service->getArrayCopy());
        
        
            $commands = $this->getDao("CommandDao")->fetchAll();
            $commandsArray = array();
            foreach ($commands as $command) {
                $commandsArray[$command->id] = $command->name;
            }
            
            $servicecommands = $this->getDao("ServiceCommandDao")->fetchByServiceId($id);
            $servicecommandsArray = array();
            foreach ($servicecommands as $servicecommand) {
                $servicecommandsArray[$servicecommand->xmltemplate_id] = $servicecommand->priority;
            }
            
            return new ViewModel(array(
                'service'           => $service,
                'commands'          => $commandsArray,
                'servicecommands'   => $servicecommandsArray,
            ));
        }
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "StructureDao")
    {
        //if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->structureDao = $sm->get('Provisioning\Dao\\'.$dao);
        //}
        
        return $this->structureDao;
    }

}
