<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

//use Provisioning\Model\Device;
//use Provisioning\Form\DeviceForm;
use Generic\Controller\GenericController;

class DeviceController extends GenericController
{ 
    public $serviceDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        //$services = $this->getDao()->fetchAll();
        return new ViewModel();
        //return new ViewModel(array('services' => $services));
    }
      
    /*public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $service = new Device();
            $form = new DeviceForm();
            $form->setInputFilter($service->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $service->exchangeArray($form->getData());
                  
                $this->getDao()->save($service);

                return $this->redirect()->toRoute('service');
            }

        }

        return new ViewModel();
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $service = $this->getDao()->get($id);
        
        $form = new DeviceForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $service = new Device();
            $form->setInputFilter($service->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $service->exchangeArray($form->getData());
                  
                $this->getDao()->save($service);

                return $this->redirect()->toRoute('service');
            }

        } else {
            $form->setData($service->getArrayCopy());
        }

        return new ViewModel(array(
            'form'      => $form,
            'service'   => $service,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao()
    {
        if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get('Provisioning\Dao\DeviceDao');
        }
        
        return $this->serviceDao;
    }*/

}
