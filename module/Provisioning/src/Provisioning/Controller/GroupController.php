<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Provisioning\Model\Group;
use Provisioning\Form\GroupForm;
use Generic\Controller\GenericController;

class GroupController extends GenericController
{ 
    public $group;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()->get('AuthService')->hasIdentity())
        {
            return $this->redirect()->toRoute('auth');
        }

      
    }

    public function fetchbyAction()
    { 
         $id = $this->params()->fromRoute('id');
         //error_log($id);
        $servicesFetch = $this->getDao('GroupDao')->fetchById($id);
        $services = array();
        foreach ($servicesFetch as $servicios) {
            error_log($services);
            $services[] = $servicios;
        }
        print_r($services);
        return new JsonModel($services);

        //return new JsonModel([{'1':'1'}]);
    }

   

}
