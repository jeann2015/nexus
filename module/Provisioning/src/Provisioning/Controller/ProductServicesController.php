<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Provisioning\Model\ProductServices;
use Provisioning\Form\ProductForm;
use Generic\Controller\GenericController;

class ProductServicesController extends GenericController
{ 
    public $serviceDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()->get('AuthService')->hasIdentity())
        {
            return $this->redirect()->toRoute('auth');
        }

      
    }

    public function fetchbyServicesAction()
    { 
         $id = $this->params()->fromRoute('id');

        $servicesFetch = $this->getDao()->fetchByServicesId($id);
        $services = array();
        foreach ($servicesFetch as $servicios) {
            $services[] = $servicios;
        }
        //print_r($services);
        return new JsonModel($services);

        //return new JsonModel([{'1':'1'}]);
    }

    public function getDao($dao = "ProductServicesDao",$namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->serviceDao;
    }

}
