<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Generic\Controller\GenericController;

use Provisioning\Model\Provider;
use Provisioning\Model\ProviderDomain;

use Provisioning\Form\ProviderForm;

use Provisioning\Service\OCISoapService;
use Provisioning\Service\XmlService;


class ProviderController extends GenericController
{ 
    public $dao;
  
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
         
        return new ViewModel();
    }
      
    public function addAction()
    {
        $request = $this->getRequest();
        $form = new ProviderForm();
        $summary = "";

        /**
         * Llenamos el select con los dominios
         */
        $domainSelect = $form->get('domain'); 
        $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();   
        $domainFetch = $this->getDao("DomainDao")->fetchByPlatformId($currentUser->platform_id);
        $domainArray = array(""=>"");
        $defaultDomain = "";
        foreach ($domainFetch as $domain) {
            $domainArray[$domain->name] = $domain->name;
            if($domain->default == '1') $defaultDomain = $domain->name;
        }
        $domainSelect->setAttribute('options',$domainArray);
        $domainSelect->setValue($defaultDomain);


        if ($request->isPost()) {

            $provider = new Provider();
            
            $form->setInputFilter($provider->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {

                $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand('add','ServiceProviderAddRequest');
                if($xmltemplate) { 
                    $xml = $xmltemplate->getXml();

                    $variableDao = $this->getDao("VariableDao");
                    $variables = $variableDao->fetchByXmlTemplateId($xmltemplate->getId());

                    $array = $request->getPost();
                    $array['isenterprise'] = 'false';

                    $xmlService = new XmlService();
                    $xmlGenerated = $xmlService->mergeArray($xml,$variables,$array);
                    $this->log($xmlGenerated);

                    $OCISoapService = new OCISoapService($this->getServiceLocator());
                    $response = $OCISoapService->send($xmlGenerated);
                    $this->log($response);
                    $xmlresponse = $xmlService->getResponseFromXml($response);

                    if($xmlresponse->result == "success"){
                        $provider->exchangeArray($form->getData());
                        $currentUser              = (object)$this->getServiceLocator()->get('AuthService')->getIdentity(); 
                        $provider->platformId     = $currentUser->platform_id;
                        $provider->isenterprise   = 'false';
                        $provider->inbroadsoft    = 1;
                        $provider->id = $this->getDao()->save($provider);
                        $this->syncProviderDomains($provider);
                        return $this->redirect()->toRoute('provider',array('action'=>'list'));
                    } else {
                        $summary = $xmlresponse->summary;
                    }
                } else {
                    $summary = "[Error] No existe comando xml de creación.";
                }
            }

        }

        return new ViewModel(array(
            'form'      => $form,
            'summary'   => $summary,
        ));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $provider = $this->getDao()->get($id);
        
        $form = new ProviderForm();
        /**
         * Llenamos el select con los dominios
         */
        $domainSelect = $form->get('domain'); 
        $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();   
        $domainFetch = $this->getDao("DomainDao")->fetchByPlatformId($currentUser->platform_id);
        $domainArray = array(""=>"");
        foreach ($domainFetch as $domain) {
            $domainArray[$domain->name] = $domain->name;
        }
        $domainSelect->setAttribute('options',$domainArray);

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $provider = new Provider();
            $form->setInputFilter($provider->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $provider->exchangeArray($form->getData());
                $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity(); 
                $provider->platformId = $currentUser->platform_id;
                $this->getDao()->save($provider);

                return $this->redirect()->toRoute('provider',array('action'=>'list'));
            }

        } else {
            $form->setData($provider->getArrayCopy());
        }

        return new ViewModel(array(
            'form'      => $form,
            'provider'   => $provider,
        ));
    }

    public function viewAction()
    {
        $id = $this->params()->fromRoute('id');
        $provider = $this->getDao()->get($id);

        $domains = $this->getDao("DomainDao")->fetchByProviderId($id);

        return new ViewModel(array(
            'provider'  => $provider,
            'domains'   => $domains,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        $id = $this->params()->fromRoute('id');
        $provider = $this->getDao()->get($id);
        $array  = array("name"=>$provider->getName());
        
        try {
            $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand('delete','ServiceProvider');
            if($xmltemplate) { 
                $xml = $xmltemplate->getXml();

                $variableDao = $this->getDao("VariableDao");
                $variables = $variableDao->fetchByXmlTemplateId($xmltemplate->getId());

                //$array = $request->getPost();

                $xmlService = new XmlService();
                $xmlGenerated = $xmlService->mergeArray($xml,$variables,$array);
                
                $OCISoapService = new OCISoapService($this->getServiceLocator());
                $response = $OCISoapService->send($xmlGenerated);
                $xmlresponse = $xmlService->getResponseFromXml($response);

                if($xmlresponse->result == "success"){
                    $this->getDao()->delete($id);
                    $model->setVariable('status', "success");
                    $model->setVariable('payload', $id);
                } else {
                    $summary = $xmlresponse->summary;
                    
                    if (strpos(strtolower($summary),'not found') !== false) {
                        $summary .= "<br><br><a id='forcedelete'>
                        Eliminar De Todas Formas De Nexus</a>"; 
                    }
                    $model->setVariable('status', "error");
                    $model->setVariable('payload', $summary);
                }
            } else {
                $summary = "[Error] No existe comando xml de eliminación.";
                $model->setVariable('status', "error");
                $model->setVariable('payload', $summary);
            }

        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }

    public function forcedeleteAction() 
    {
        $model = new JsonModel();
        $id = $this->params()->fromRoute('id');
        
        try {
            $this->getDao()->delete($id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }

    public function syncAction() 
    {
        $model = new JsonModel();
        
        try {
            $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand('list','ServiceProviderGet');
            if($xmltemplate) { 
                $xml = $xmltemplate->getXml();

                $variableDao = $this->getDao("VariableDao");
                $variables = $variableDao->fetchByXmlTemplateId($xmltemplate->getId());

                //$array = $request->getPost();

                $xmlService = new XmlService();
                $xmlGenerated = $xmlService->mergeArray($xml,$variables,$array);
                
                $OCISoapService = new OCISoapService($this->getServiceLocator());
                $response = $OCISoapService->send($xmlGenerated);
                $xmlresponse = $xmlService->getCommnadResponseFromXml($response);

                if($xmlresponse->result == "success"){

                    $providers = $this->getDao()->fetchAll();
                    foreach ($providers as $provider) {
                        $provider->setInbroadsoft(0);
                        $provider->setPlatformId($provider->platform_id);
                        $this->getDao()->save($provider);
                    }

                    $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();
                    
                    $countTotal     = 0;
                    $countNews      = 0;
                    $countExists    = 0;
                    $countErrors    = 0;
                    foreach ($xmlresponse->command->serviceProviderTable->row as $xmlrow) {

                        // $xmlrow->col[0] -> Service Provider Id
                        // $xmlrow->col[1] -> Service Provider Name
                        // $xmlrow->col[2] -> Is Enterprise

                        $name           = $xmlrow->col[0];
                        $isenterprise   = $xmlrow->col[2];

                        $modelo = $this->getDao()->getByName($name);

                        if($modelo){
                            $countExists++;
                            $modelo->setInbroadsoft(1);
                            $modelo->setPlatformId($currentUser->platform_id);
                            $modelo->setIsenterprise($isenterprise);
                        } else {
                            $modelo = new Provider();
                            $modelo->setName($name);
                            $modelo->setInbroadsoft(1);
                            $modelo->setPlatformId($currentUser->platform_id);
                            $modelo->setIsenterprise($isenterprise);
                        }

                        $this->getDao()->save($modelo);

                        $countTotal++;
                    }

                    $model->setVariable('status', "success");
                    $model->setVariable('payload', $countTotal);
                } else {
                    $summary = $xmlresponse->summary;
                    $model->setVariable('status', "error");
                    $model->setVariable('payload', $summary);
                }
            } else {
                $summary = "[Error] No existe comando xml de consulta.";
                $model->setVariable('status', "error");
                $model->setVariable('payload', $summary);
            }

        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }

    public function syncproviderAction(){

        $model = new JsonModel();

        try {
            $id = $this->params()->fromRoute('id');
            $provider = $this->getDao()->get($id);

            $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand('get','ServiceProvider');
            if($xmltemplate) { 
                $xml = $xmltemplate->getXml();

                $variableDao = $this->getDao("VariableDao");
                $variables = $variableDao->fetchByXmlTemplateId($xmltemplate->getId());

                $array  = array("name"=>$provider->name);

                $xmlService = new XmlService();
                $xmlGenerated = $xmlService->mergeArray($xml,$variables,$array);

                $OCISoapService = new OCISoapService($this->getServiceLocator());
                $response = $OCISoapService->send($xmlGenerated);

                $xmlresponse = $xmlService->getCommnadResponseFromXml($response);

                if($xmlresponse->result == "success"){

                    $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();
                    
                    //$provider = new Provider();
                    $provider->setName($provider->name);
                    $provider->setDomain((string)$xmlresponse->command->defaultDomain);
                    $provider->setPlatformId($currentUser->platform_id);
                    $provider->setIsenterprise($provider->isenterprise);
                    $provider->setId($provider->id);;

                    $this->getDao()->save($provider);

                    $this->syncProviderDomains($provider);
                    
                    $model->setVariable('status', "success");
                    $model->setVariable('payload', $countTotal);
   
                } else {
                    $summary = $xmlresponse->summary;
                    $model->setVariable('status', "error");
                    $model->setVariable('payload', $summary);
                }
            } else {
                $summary = "[Error] No existe comando xml de consulta.";
                $model->setVariable('status', "error");
                $model->setVariable('payload', $summary);
            }

        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }

    private function syncProviderDomains(Provider $provider){

        try {
            $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand('list','ServiceProviderDomain');
            if($xmltemplate) { 
                $xml = $xmltemplate->getXml();

                $variableDao = $this->getDao("VariableDao");
                $variables = $variableDao->fetchByXmlTemplateId($xmltemplate->getId());

                $array  = array(
                    "name"  =>  $provider->getName() 
                );

                $xmlService = new XmlService();
                $xmlGenerated = $xmlService->mergeArray($xml,$variables,$array);

                $OCISoapService = new OCISoapService($this->getServiceLocator());
                $response = $OCISoapService->send($xmlGenerated);

                $xmlresponse = $xmlService->getCommnadResponseFromXml($response);

                if($xmlresponse->result == "success"){

                    $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();

                    if(is_array($xmlresponse->command->domain)){

                        foreach ($xmlresponse->command->domain as $domainName) {
                            
                            $domain = $this->getDao("DomainDao")->getByName($domainName);
                            $domainId = null;

                            if($domain){
                                $domainId = $domain->id;
                            } else {
                                $domain = new Domain();
                                $domain->setName($domainName);
                                $domain->setPlatformId($currentUser->platform_id);
                                $domainId = $this->getDao("DomainDao")->save($domain);
                            }

                            $providerDomain = new ProviderDomain();
                            $providerDomain->setProviderId($provider->getId());
                            $providerDomain->setDomainId($domainId);
                            $this->getDao("ProviderDomainDao")->save($providerDomain);

                        }
                    } else {

                            $domainName = $xmlresponse->command->domain;
                            $domain = $this->getDao("DomainDao")->getByName($domainName);
                            $domainId = null;

                            if($domain){
                                $domainId = $domain->id;
                            } else {
                                $domain = new Domain();
                                $domain->setName($domainName);
                                $domain->setPlatformId($currentUser->platform_id);
                                $domainId = $this->getDao("DomainDao")->save($domain);
                            }

                            $providerDomain = new ProviderDomain();
                            $providerDomain->setProviderId($provider->getId());
                            $providerDomain->setDomainId($domainId);

                            $providerDomainDao = $this->getDao("ProviderDomainDao");
                            $providerDomainDao->save($providerDomain);

                    }

                    return true;
   
                } else {
                    return false;
                }
            } else {
                return false;
            }

        } catch (\Exception $ex) {
            return false;
        }
    }
    
    public function getDao($dao = "ProviderDao",$namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->dao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->dao;
    }

    private function provisionAction() {
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $name           = $request->getPost("name");
            $xmltemplateId  = $request->getPost("xmltemplateId");

            $xmltemplateDao = $this->getDao("XmlTemplateDao");
            $xmltemplate    = $xmltemplateDao->get($scModel->commandId);
            $xml            = $xmltemplate->getXml();

        }

        $relationship = array(
            "Device" => array(
                "relationField" => "groupId",
                "daoFunction"   => "getBy",
                "parentClass"   => "Group", 
            ), 
            "Group" => array(
                "relationField" => "customerId",
                "daoFunction"   => "fetchByCustomerId",
            ),
        );

        $classpath = get_class($class);
        $classpathArray = explode("\\", $classpath);
        $classname = trim($classpathArray[count($classpathArray)-1]);

        $orderDao       = $this->getDao("OrderDao");
        $xmlDao         = $this->getDao("XmlDao");
        $scDao          = $this->getDao("ServiceXmlTemplateDao");
        $commandDao     = $this->getDao("XmlTemplateDao");
        //$deviceDao     = $this->getDao("DeviceDao");

        $order          = $orderDao->get($orderId);
        $scModel        = $scDao->getByServiceIdAndPriority($order->getServiceId(),$priority);

        //foreach ($scModels as $scModel) {
            
            $command = $commandDao->get($scModel->commandId);
            //print_r($command);
            $xml = $command->getXml();
            
            $variableDao = $this->getDao("VariableDao");
            $variables = $variableDao->fetchByCommandId($command->id);
            $variablesArray = array();
            foreach ($variables as $variable) {
                $key        = $variable->name;
                $object     = $variable->mappingObject      ?   $variable->mappingObject    : $variable->mapping_object;
                $attribute  = $variable->mappingAttribute   ?   $variable->mappingAttribute : $variable->mapping_attribute;
                $treatment  = $variable->treatment;
                $daoName    = $object . "Dao";
                $objectDao  = $this->getDao($daoName,$object=="Preference"?$object:"Provisioning");
                $objectAttr = lcfirst($object) . "Id";

                if($object == "Preference"){
                    $instance   = $objectDao->getByName($attribute);
                    $value      = isset($instance->value)?$instance->value:"";
                } elseif($classname == $object) {
                    $instance   = $class;
                    $value      = isset($instance->$attribute)?$instance->$attribute:"";
                } elseif(property_exists($order, $objectAttr)) {
                    $instance   = $objectDao->get($order->$objectAttr);
                    $value      = isset($instance->$attribute)?$instance->$attribute:"";
                } else {
                    $instance   = $objectDao->get($class->$objectAttr);
                    $value      = isset($instance->$attribute)?$instance->$attribute:"";
                }
                
                switch ($treatment) {
                    case "concat":
                        $array = explode(" ", $value);
                        $value = implode("_", $array);
                        break;
                    case "initials":
                        $array = explode(" ", $value);
                        $initials = "";
                        foreach ($array as $v) {
                            $initials .= strtoupper($v[0]);
                        }
                        $value = $initials;
                        break;
                    default:
                        break;
                }
                
                $variablesArray[$key] = $value;
            }

            $xmlGenerated = $this->merge($xml, $variablesArray);
            /*
             * Nombre del archivo:
             * orderid + commandid + timestamp
             */
            $filename = $order->id . $command->id . time();
            file_put_contents("tmp/" . $filename,
                    $xmlGenerated, LOCK_EX);

            $xml = new Xml();
            $xml->setName($command->name);
            $xml->setOrderId($order->id);
            $xml->setStatus("pendiente");
            $xml->setPriority($scModel->priority);
            $xml->setFile($filename);

            $xml->id = $xmlDao->save($xml);

    }

    public function filterAction(){

        $model = new JsonModel();

        $request = $this->getRequest();
        if($request->isPost()){
            $post   = (array)$request->getPost();
            $limit  = isset( $post['limit'] ) ?  $post['limit'] : null;
            $offset = $post['offset'];
            $order  = $post['order'];
            $sort   = $post['sort'];

            $filters = array_key_exists('filters',$post) ? $post['filters'] : null;

            if($filters)
            foreach ($filters as $key => $value) {
                if (strpos($key,'QGDATE') !== false) {
                    if(stripos($value,":") !== false){
                        $filters[$key] = $this->convertToUTC($value);
                    } else {
                        $filters[$key] = $this->convertToUTC($value,array('date'=>true));
                    }
                    //print $filters[$key] . "\n";
                }
            }

            $result = $this->getDao()->fetchFilter($limit,$offset,$order,$sort,$filters);
        } 

        $items = array();
        foreach ($result['resultSet'] as $item) {
            $item->options = "";
            foreach ($item as $key => $value) {

                //Aqui se hace alguna transformación que los valores del grid requieran
                //$value      = $this->translator($value);
                //$item->$key = $value;

                if($this->isDate($value)){
                    if(stripos($value,":") === false){
                        $item->$key = $this->convertFromUTC($value,array('date'=>true));
                    } else {
                        $item->$key = $this->convertFromUTC($value);
                    }  
                }

                if($key == "options"){
                    $url = $this->url()->fromRoute('provider',array('action'=>'view','id'=>$item->id));
                    $view = "<a class='btn btn-link btn-xs text-warning' href='$url'><i class='fa fa-search'></i> Detalles</a> ";
                    $item->$key = $view . "<a class='btn btn-link btn-xs text-danger' data-toggle='modal' data-target='#myModal' data-id='".$item->id."'><i class='fa fa-trash-o'></i> Eliminar</a>";
                }

                if($key == "inbroadsoft"){
                    $item->$key = $value == 1 ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-close text-danger'></i>";
                }

                if($key == "active" || $key == "default"){
                    $item->$key = $value == 1 ? "<i class='fa fa-check text-success'></i>" : "";
                }
            }

            array_push($items, $item);
        }

        $model->setVariable("status","success");
        $model->setVariable("payload",array(
            "total"     =>  $result['total'],
            "items"     =>  $items,
        ));

        return $model;
    }
}
