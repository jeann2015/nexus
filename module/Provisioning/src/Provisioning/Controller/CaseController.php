<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Provisioning\Model\nxCase;
use Provisioning\Model\CaseXmlTemplate;

use Provisioning\Form\CaseForm;
use Generic\Controller\GenericController;

class CaseController extends GenericController
{ 
    public $caseDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $cases = $this->getDao()->fetchAll();

        return new ViewModel(array('cases' => $cases));
    }
      
    public function addAction()
    {
        $form = new CaseForm();
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $case = new nxCase();
            $form = new CaseForm();
            $form->setInputFilter($case->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $case->exchangeArray($form->getData());
                  
                $id = $this->getDao()->save($case);

                return $this->redirect()->toRoute('case',array('action'=>'xmltemplate','id'=>$id));
            }

        }

        return new ViewModel(array(
            'form'  =>  $form,
            'error' =>  $error,
            'case' =>  $case,
            //'step'  =>  $step,
        ));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $case = $this->getDao()->get($id);
        
        $form = new CaseForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $case = new nxCase();
            $form->setInputFilter($case->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $case->exchangeArray($form->getData());
                  
                $this->getDao()->save($case);

                return $this->redirect()->toRoute('case',array('action'=>'xmltemplate','id'=>$id));
            }

        } else {
            $form->setData($case->getArrayCopy());
        }
        
        $xmltemplates = $this->getDao("XmlTemplateDao")->fetchAll();
        $xmltemplatesArray = array();
        foreach ($xmltemplates as $xmltemplate) {
            $xmltemplatesArray[$xmltemplate->id] = $xmltemplate->name;
        }

        return new ViewModel(array(
            'form'      => $form,
            'case'      => $case,
        ));
    }
    
    public function xmltemplateAction()
    {
        $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                
                $thePost = json_decode($request->getContent());
                $scdao = $this->getDao("CaseXmlTemplateDao");
                $scdao->delete($id); //Elimina todo
                foreach ($thePost as $item) {
                    //$sc = $scdao->getByCaseIdAndXmlTemplateId($item->caseId,$item->xmltemplateId);
                    //if($sc) {
                        
                    //}
                    $sc = new CaseXmlTemplate();
                    $sc->setPriority($item->priority);
                    $sc->setXmltemplateId($item->xmltemplateId);
                    $sc->setCaseId($item->caseId);
                    $scdao->save($sc);
                    unset($sc);
                }
                
                $return = array("result" => "success","payload"=>$thePost);
                
                
            } catch (\Exception $e){
                
                $return = array("result" => "error","payload"=>$e->getMessage());
            }
            
            return new JsonModel($return);

        } else {
            $id = $this->params()->fromRoute('id');
            $case = $this->getDao()->get($id);

            $form = new CaseForm();
            $form->setData($case->getArrayCopy());
        
        
            $xmltemplates = $this->getDao("XmlTemplateDao")->fetchAll();
            $xmltemplatesArray = array();
            foreach ($xmltemplates as $xmltemplate) {
                $xmltemplatesArray[$xmltemplate->id] = $xmltemplate->name;
            }
            
            $casexmltemplates = $this->getDao("CaseXmlTemplateDao")->fetchByCaseId($id);
            $casexmltemplatesArray = array();
            foreach ($casexmltemplates as $casexmltemplate) {
                $casexmltemplatesArray[$casexmltemplate->xmltemplate_id] = $casexmltemplate->priority;
            }
            
            return new ViewModel(array(
                'case'           => $case,
                'xmltemplates'          => $xmltemplatesArray,
                'casexmltemplates'   => $casexmltemplatesArray,
            ));
        }
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "CaseDao")
    {
        //if (!$this->caseDao) {
            $sm = $this->getServiceLocator();
            $this->caseDao = $sm->get('Provisioning\Dao\\'.$dao);
        //}
        
        return $this->caseDao;
    }

}
