<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Generic\Controller\GenericController;

use Provisioning\Model\Platform;
use Provisioning\Form\PlatformForm;


class PlatformController extends GenericController
{ 
    public $dao;
  
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $platforms = $this->getDao()->fetchAll();

        return new ViewModel(array('platforms' => $platforms));
    }
      
    public function addAction()
    {
        $request = $this->getRequest();
        $form = new PlatformForm();

        if ($request->isPost()) {

            $platform = new Platform();
            
            $form->setInputFilter($platform->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $platform->exchangeArray($form->getData());
                $platform->password = base64_encode ( $platform->password );
                $this->getDao()->save($platform);
                return $this->redirect()->toRoute('platform',array('action'=>'list'));

            }

        }

        return new ViewModel(array(
            'form'      => $form,
        ));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $platform = $this->getDao()->get($id);
        $platform->password = base64_decode ( $platform->password );
        
        $form = new PlatformForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $platform = new Platform();
            $form->setInputFilter($platform->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $platform->exchangeArray($form->getData());
                $platform->password = base64_encode ( $platform->password );  
                $this->getDao()->save($platform);

                return $this->redirect()->toRoute('platform',array('action'=>'list'));
            }

        } else {
            $form->setData($platform->getArrayCopy());
        }

        return new ViewModel(array(
            'form'      => $form,
            'platform'  => $platform,
        ));
    }

    public function viewAction()
    {
        $id = $this->params()->fromRoute('id');
        $platform = $this->getDao()->get($id);

        return new ViewModel(array(
            'platform'  => $platform,
        ));
    }
    
    
    public function getDao($dao = "PlatformDao",$namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->dao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->dao;
    }

    public function checkAction()
    {
        $model = new JsonModel();
        try {
            $request = $this->getRequest();
            if ($request->isPost()) {
                $url = $request->getPost("ip");
                $context = stream_context_create(array('http' => array('method' => 'HEAD','timeout' => 5)));
                $response = file_get_contents("http://".$url, false, $context);
                $exists = ($response !== false);
                $model->setVariable("payload",array("up"=>$exists,"ip"=>$url));
                $model->setVariable("status","success");
            }
        } catch(\Exception $e){
                $model->setVariable("payload",$e->getMessage());
                $model->setVariable("status","error");
        }

        return $model;
    }

}
