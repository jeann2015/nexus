<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Provisioning\Model\Servicepack;
use Provisioning\Model\ServicepackService;
use Provisioning\Form\ServicepackForm;
use Generic\Controller\GenericController;

class ServicepackController extends GenericController
{ 
    public $dao;

    public function indexAction()
    {
    	
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    public function listAction()
    {
    	
		
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
	
	 public function listDataAction()
    {
        $services = $this->getDao()->fetchAll();
		$ser =array();
		foreach ($services as $domain) {
            $ser[] = $domain;
        }
		
        return new JsonModel($ser);
    }
      
    public function addAction()
    {
    	$form = new ServicepackForm();

        /**
         * Llenamos el select multiple con los services disponibles
         */
        $availableSelect = $form->get('servicesAvailable');       
        $availableFetch = $this->getDao("ServicesDao")->fetchAll();

        $availableGroupServicesArray = array();
        $availableUserServicesArray  = array();

        foreach ($availableFetch as $available) {

            if($available->category == 1){
                $availableUserServicesArray[$available->id] = $available->name;
            }
            if($available->category == 2){
                $availableGroupServicesArray[$available->id] = $available->name;
            }
        }

        //$availableSelect->setAttribute('options',$availableGroupServicesArray);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $servicepack = new Servicepack();
            $servicepack->setName($request->getPost("name"));
            $servicepack->setCategory($request->getPost("category"));
            $servicepackId = $this->getDao()->save($servicepack);

            $services = $request->getPost("servicesAssigned");

            foreach ($services as $serviceId) {
                $servicepackService = new ServicepackService();
                $servicepackService->setServicepackId($servicepackId);
                $servicepackService->setServiceId($serviceId);
                $this->getDao("ServicepackServiceDao")->save($servicepackService);
            }

            return $this->redirect()->toRoute('servicepack',array('action'=>'list'));

        }

        return new ViewModel(array(
            'form'          => $form,
            'userServices'  => $availableUserServicesArray,
            'groupServices' => $availableGroupServicesArray,
        ));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $servicepack = $this->getDao()->get($id);
        $servicepackServices = $this->getDao("ServicepackServiceDao")->fetchByServicepackId($id);
        
        $form = new ServicepackForm();

        /**
         * Llenamos el select multiple con los services disponibles
         */
        $availableSelect = $form->get('servicesAvailable');       
        $availableFetch = $this->getDao("ServicesDao")->fetchAll();

        $availableGroupServicesArray = array();
        $availableUserServicesArray  = array();

        foreach ($availableFetch as $available) {

            if($available->category == 1){
                $availableUserServicesArray[$available->id] = $available->name;
            }
            if($available->category == 2){
                $availableGroupServicesArray[$available->id] = $available->name;
            }
        }

        $servicepackServicesArray  = array();

        foreach ($servicepackServices as $servicepackService) {

            if(array_key_exists($servicepackService->service_id,$availableUserServicesArray)){
                $servicepackServicesArray[$servicepackService->service_id] = $availableUserServicesArray[$servicepackService->service_id];
                unset($availableUserServicesArray[$servicepackService->service_id]);
            }
            if(array_key_exists($servicepackService->service_id,$availableGroupServicesArray)){
                $servicepackServicesArray[$servicepackService->service_id] = $availableGroupServicesArray[$servicepackService->service_id];
                unset($availableGroupServicesArray[$servicepackService->service_id]);
            }
        }

        $assignedSelect = $form->get("servicesAssigned");
        $assignedSelect->setAttribute('options',$servicepackServicesArray);

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            //$servicepack = new Servicepack();
            $servicepack->setName($request->getPost("name"));
            $servicepack->setCategory($request->getPost("category"));
            $servicepackId = $this->getDao()->save($servicepack);

            $this->getDao("ServicepackServiceDao")->deleteByServicepackId($id);
            $services = $request->getPost("servicesAssigned");

            foreach ($services as $serviceId) {
                $servicepackService = new ServicepackService();
                $servicepackService->setServicepackId($servicepackId);
                $servicepackService->setServiceId($serviceId);
                $this->getDao("ServicepackServiceDao")->save($servicepackService);
            }

            return $this->redirect()->toRoute('servicepack',array('action'=>'list'));

        } else {
            $form->setData($servicepack->getArrayCopy());
        }
        

        return new ViewModel(array(
            'form'          => $form,
            'servicepack'   => $servicepack,
            'userServices'  => $availableUserServicesArray,
            'groupServices' => $availableGroupServicesArray,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "ServicepackDao")
    {
        //if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->dao = $sm->get('Provisioning\Dao\\'.$dao);
        //}
        
        return $this->dao;
    }

    public function filterAction(){

        $model = new JsonModel();

        $request = $this->getRequest();
        if($request->isPost()){
            $post   = (array)$request->getPost();
            $limit  = isset( $post['limit'] ) ?  $post['limit'] : null;
            $offset = $post['offset'];
            $order  = $post['order'];
            $sort   = $post['sort'];

            $filters = array_key_exists('filters',$post) ? $post['filters'] : null;

            if($filters)
            foreach ($filters as $key => $value) {
                if (strpos($key,'QGDATE') !== false) {
                    if(stripos($value,":") !== false){
                        $filters[$key] = $this->convertToUTC($value);
                    } else {
                        $filters[$key] = $this->convertToUTC($value,array('date'=>true));
                    }
                    //print $filters[$key] . "\n";
                }
            }

            $result = $this->getDao()->fetchFilter($limit,$offset,$order,$sort,$filters);
        } 

        $items = array();
        foreach ($result['resultSet'] as $item) {
            $item->options = "";
            foreach ($item as $key => $value) {

                //Aqui se hace alguna transformación que los valores del grid requieran
                //$value      = $this->translator($value);
                //$item->$key = $value;

                if($this->isDate($value)){
                    if(stripos($value,":") === false){
                        $item->$key = $this->convertFromUTC($value,array('date'=>true));
                    } else {
                        $item->$key = $this->convertFromUTC($value);
                    }  
                }

                if($key == "options"){
                    $url = $this->url()->fromRoute('servicepack',array('action'=>'edit','id'=>$item->id));
                    $edit = "<a class='btn btn-link btn-xs text-warning' href='$url'><i class='fa fa-edit'></i> Editar</a> ";
                    $item->$key = $edit . "<a class='btn btn-link btn-xs text-danger' data-toggle='modal' data-target='#myModal' data-id='".$item->id."'><i class='fa fa-trash-o'></i> Eliminar</a>";
                }

                if($key == "inbroadsoft"){
                    $item->$key = $value == 1 ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-close text-danger'></i>";
                }

                if($key == "active" || $key == "default"){
                    $item->$key = $value == 1 ? "<i class='fa fa-check text-success'></i>" : "";
                }

                if($key == "category"){
                    $item->$key = $value == 1 ? "Usuario" : "Grupo";
                }
            }

            array_push($items, $item);
        }

        $model->setVariable("status","success");
        $model->setVariable("payload",array(
            "total"     =>  $result['total'],
            "items"     =>  $items,
        ));

        return $model;
    }

}
