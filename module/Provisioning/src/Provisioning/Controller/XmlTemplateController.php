<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Provisioning\Model\XmlTemplate;
use Provisioning\Model\Variable;

use Provisioning\Model\Customer;
use Provisioning\Model\Group;
use Provisioning\Model\Order;
use Provisioning\Model\Product;
use Provisioning\Model\Device;
use Provisioning\Model\User;
use Provisioning\Model\Domain;
use Provisioning\Model\DomainAdd;
use Provisioning\Model\ServiceProviderEmpresaAdd;
use Provisioning\Model\UserModifyAdd;
use Provisioning\Model\ProviderAdd;
use Provisioning\Model\Provider;
use Provisioning\Model\Dn;
use Provisioning\Model\Services;
use Provisioning\Model\GroupServiceProvider;
use Provisioning\Model\AdditionLine;
use Provisioning\Model\ChangeNumber;
use Provisioning\Model\DeleteLine;
use Provisioning\Model\CreationGroup;
use Provisioning\Model\DeleteGroup;






use Provisioning\Form\XmlTemplateForm;

use Preference\Dao\PreferenceDao;

use Generic\Controller\GenericController;

class XmlTemplateController extends GenericController
{ 
    public $serviceDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }


        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $xmltemplates = $this->getDao()->fetchAll();

        return new ViewModel(array('xmltemplates' => $xmltemplates));
    }
      
    public function addAction()
    {
        $form                   = new XmlTemplateForm();
        $addxmltemplatesSelect  = $form->get("addxmltemplateId");
        $addxmltemplates        = $this->getDao()->fetchByCategory('add');

        $addxmltemplatesArray   = array(null => "");
        foreach ($addxmltemplates as $addxmltemplate) {
            $addxmltemplatesArray[$addxmltemplate->id] = $addxmltemplate->name . " - " . $addxmltemplate->command;
        }
        $addxmltemplatesSelect->setAttribute('options',$addxmltemplatesArray);

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $xmltemplate = new XmlTemplate();
            $xmltemplate->unsetInputFilter();
            $form->setInputFilter($xmltemplate->getInputFilter());

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $xmltemplate->exchangeArray($form->getData());
                  
                $id = $this->getDao()->save($xmltemplate);

                $xml = $xmltemplate->xml;
                $xml = str_replace("\n", '', $xml);
                $xml = str_replace("\t", '', $xml);

                if(preg_match_all('/%[A-Z_-]*%/mi', $xml, $variablesInXml) === 0)
                {
                    $this->getDao("VariableDao")->deleteByXmlTemplateId($id);
                    return $this->redirect()->toRoute('xmltemplate',array('action'=>'list'));
                } else {
                    if(!is_array($variablesInXml)) { $variablesInXml[0] = $variablesInXml; } 
                    foreach ($variablesInXml[0] as $variableInXml){
                        
                        $variable = $this->getDao("VariableDao")->getByXmlTemplateIdAndName($id,$variableInXml);
                        if($variable === null){
                            $variable = new Variable();
                            $variable->setXmlTemplateId($id);
                            $variable->setName($variableInXml);
                            $this->getDao("VariableDao")->save($variable);
                        }

                    }
                    return $this->redirect()->toRoute('xmltemplate',array("action"=>"variable","id"=>$id));
                }
            } else {
                                        var_dump($form->getMessages());
            }

        }

//exit;
        return new ViewModel(array(
            'form'      => $form,
        ));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $xmltemplate = $this->getDao()->get($id);

        $form                   = new XmlTemplateForm();
        $addxmltemplatesSelect  = $form->get("addxmltemplateId");
        $addxmltemplates        = $this->getDao()->fetchByCategory('add');

        $addxmltemplatesArray   = array(0 => "");
        foreach ($addxmltemplates as $addxmltemplate) {
            $addxmltemplatesArray[$addxmltemplate->id] = $addxmltemplate->name . " - " . $addxmltemplate->command;
        }
        $addxmltemplatesSelect->setAttribute('options',$addxmltemplatesArray);
        
        

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $xmltemplate = new XmlTemplate();
            $form->setInputFilter($xmltemplate->getInputFilter());

            $form->setData($request->getPost());

            if ($form->isValid()) {

                $xmltemplate->exchangeArray($form->getData());
                  
                $id = $this->getDao()->save($xmltemplate);
                
                $xml = $xmltemplate->xml;
                $xml = str_replace("\n", '', $xml);
                $xml = str_replace("\t", '', $xml);

                if(preg_match_all('/%[A-Z_-]*%/mi', $xml, $variablesInXml) === 0)
                {
                    $this->getDao("VariableDao")->deleteByXmlTemplateId($id);
                    return $this->redirect()->toRoute('xmltemplate',array('action'=>'list'));
                } else {
                    if(!is_array($variablesInXml)) { $variablesInXml[0] = $variablesInXml; } 
                    foreach ($variablesInXml[0] as $variableInXml){
                        
                        $variable = $this->getDao("VariableDao")->getByXmlTemplateIdAndName($id,$variableInXml);
                        if($variable === null){
                            $variable = new Variable();
                            $variable->setXmltemplateId($id);
                            $variable->setName($variableInXml);
                            $this->getDao("VariableDao")->save($variable);
                        }
                    }

                    return $this->redirect()->toRoute('xmltemplate',array("action"=>"variable","id"=>$id));
                }
            }

        } else {
            $form->setData($xmltemplate->getArrayCopy());
        }

        return new ViewModel(array(
            'form'          => $form,
            'xmltemplate'   => $xmltemplate,
        ));
    }
    

    public function fetchbyTypeProcesingAction()
    { 
         $id = $this->params()->fromRoute('id');

        $typeProcesingFetch = $this->getDao()->getTypeProcesing($id);
        $typeProcesing = array();
        foreach ($typeProcesingFetch as $typeProcesing) {
            $typeProce[] = $typeProcesing;
        }
        
      return new JsonModel($typeProce);

    }


    public function variableAction()
    {
        
        $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        if ($request->isPost()) {
            foreach($request->getPost() as $key => $value){
                $pos = strpos($key, "id");
                if($pos === 0){
                    $variable = new Variable();
                    $variable->setId($value);
                    $variable->setXmltemplateId($id);
                    $variable->setName($request->getPost("name".$value));
                    $variable->setMappingObject($request->getPost("mappingObject".$value));
                    $variable->setMappingAttribute($request->getPost("mappingAttribute".$value));
                    $variable->setTreatment($request->getPost("treatment".$value));
                    $this->getDao("VariableDao")->save($variable);
                } else {
                    $pos = strpos($key, "delete");
                    if($pos === 0){
                        $this->getDao("VariableDao")->delete($value);
                    }
                }
            }
            return $this->redirect()->toRoute('xmltemplate',array('action'=>'list'));
        }
        
        
        $xmltemplate = $this->getDao()->get($id);
        $variables = $this->getDao("VariableDao")->fetchByXmlTemplateId($id);
        
        
        $xml = $xmltemplate->xml;
        $xml = str_replace("\n", '', $xml);
        $xml = str_replace("\t", '', $xml);

        preg_match_all('/%[A-Z_-]*%/mi', $xml, $variablesInXml);
        if(!is_array($variablesInXml)) { $variablesInXml[0] = $variablesInXml; } 
        
        $preferenceFetch = $this->getDao("PreferenceDao","Preference")->fetchByCategory("provisioning");
        $preferences = array();
        foreach ($preferenceFetch as $p) {
            array_push($preferences, $p->name);
        }
        
        return new ViewModel(array(
            'id'                =>  $id,
            'variables'         =>  $variables,
            'variablesInXml'    =>  $variablesInXml[0],
            'objects'           =>  array(
                                        "Order"     =>"Orden",
                                        "Customer"  =>"Cliente",
                                        "Group"     =>"Grupo",
                                        "Product"   =>"Producto",
                                        "Preference"=>"Preferencias",
                                        "Device"    =>"Dispositivo",
                                        "User"      =>"Usuario",
                                        "Domain"    =>"Dominio",
                                        "Provider"  =>"Service Provider",
                                        "Dn"        =>"Número Dn",
					                    "Services"  =>"Servicios",
                                        "DomainAdd"  =>"Dominios por Ordenes",
                                        "ProviderAdd"  =>"Service Provider por Ordenes",
                                        "ServiceProviderEmpresaAdd"  =>"Service Provider por Ordenes Empresa",
                                        "UserModifyAdd"  =>"Modificacion de Usuario",
                                        "GroupServiceProvider" => "Agregar Servicios de Grupo y Usuarios a la Empresa",
                                        "AdditionLine" => "Adicionde Linea",
                                        "ChangeNumber" => "Cambio de Numero",
                                        "DeleteLine" => "Eliminacion de Linea",
                                        "CreationGroup" => "Creacion de Grupo",
                                        "DeleteGroup" => "Eliminacion de Grupo",
                                    ),
            'attributes'        =>  array( 
                "Customer"  => array_keys( get_object_vars ( new Customer() ) ),
                "Group"     => array_keys( get_object_vars ( new Group() ) ),
                "Order"     => array_keys( get_object_vars ( new Order()    ) ),
                "Product"   => array_keys( get_object_vars ( new Product()  ) ),
                "Device"    => array_keys( get_object_vars ( new Device()   ) ),
                "User"      => array_keys( get_object_vars ( new User()   ) ),
                "Domain"    => array_keys( get_object_vars ( new Domain()   ) ),
                "Provider"  => array_keys( get_object_vars ( new Provider()   ) ),
                "Dn"        => array_keys( get_object_vars ( new Dn()   ) ),
		        "Services"  => array_keys( get_object_vars ( new Services()   ) ),
                "DomainAdd"  => array_keys( get_object_vars ( new DomainAdd()   ) ),
                "ProviderAdd"  => array_keys( get_object_vars ( new ProviderAdd()   ) ),
                "ServiceProviderEmpresaAdd"  => array_keys( get_object_vars ( new ServiceProviderEmpresaAdd()   ) ),
                "UserModifyAdd"  => array_keys( get_object_vars ( new UserModifyAdd()   ) ),
                "GroupServiceProvider"  => array_keys( get_object_vars ( new GroupServiceProvider()   ) ),
                "AdditionLine"  => array_keys( get_object_vars ( new AdditionLine()   ) ),
                "ChangeNumber"  => array_keys( get_object_vars ( new ChangeNumber()   ) ),
                "DeleteLine"  => array_keys( get_object_vars ( new DeleteLine()   ) ),
                "CreationGroup"  => array_keys( get_object_vars ( new CreationGroup()   ) ),
                "DeleteGroup"  => array_keys( get_object_vars ( new DeleteGroup()   ) ),
                "Preference"=> $preferences
                
            ),
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "XmlTemplateDao",$namespace = "Provisioning")
    {
        //if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->serviceDao;
    }

}
