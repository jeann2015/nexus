<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Qv\Language;

use Provisioning\Model\Order;
use Provisioning\Model\Group;
use Provisioning\Model\Customer;
use Provisioning\Model\Device;
use Provisioning\Model\User;
use Provisioning\Model\Xml;
use Provisioning\Model\Dn;
use Provisioning\Model\Message;
use Provisioning\Model\Queue;
use Provisioning\Model\GroupServiceProvider;
use Provisioning\Model\DomainAdd;
use Provisioning\Model\ProviderAdd;
use Provisioning\Model\Provider;
use Provisioning\Model\Domain;
use Provisioning\Model\ServiceProviderEmpresaAdd;
use Provisioning\Model\UserModifyAdd;





use Provisioning\Form\OrderForm;
use Provisioning\Form\GroupForm;
use Provisioning\Form\CustomerForm;
use Provisioning\Form\DeviceForm;
use Provisioning\Form\DomainAddForm;
use Provisioning\Form\UserForm;
use Provisioning\Form\GroupDnAssignForm;
use Provisioning\Form\UserDnDeviceAssignForm;
use Provisioning\Form\ServiceProviderAddForm;
use Provisioning\Form\ServiceProviderOrderAddForm;
use Provisioning\Form\UserModifyAddForm;



use Preference\Dao\PreferenceDao;

use Generic\Controller\GenericController;

use Provisioning\Service\XmlService;
use Provisioning\Service\OCISoapService;

class AdditionLineController extends GenericController
{ 
    public $serviceDao;
    
    public function indexAction() {
        
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $_SESSION['current']            = "order";
        $_SESSION['activities']         = null;
        $_SESSION['current_priority']   = null; 

        return;
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $filter     = false;
        $paginate   = true;
        
        $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $filter = $request->getPost();
        }

        // fetchAll(true) return paginator
        // fetchAll() return resultset
        $paginator = $this->getDao()->fetchAll($paginate,$filter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        $paginator->setPageRange(7);
         
        return new ViewModel(array(
            'paginator'     => $paginator,
        ));
    }

    public function viewAction()
    {
        $id         = $this->params()->fromRoute('id');
        $order      = $this->getDao()->getOrder($id);
        $xmlFetch   = $this->getDao("XmlDao")->fetchByOrderId($order->id);
        //$customer   = $this->getDao("CustomerDao")->get($order->customerId);
        $group     = $this->getDao("GroupDao")->get($order->groupId);

        //$groupsArray = array();
        //foreach ($groups as $group) {
            $devices    = $this->getDao("DeviceDao")->fetchByGroupId($group->id);
            $devicesArray = array();
            foreach ($devices as $device) {
                $devicesArray[] = $device;
            }

            $group->devices = $devicesArray;
            //$groupsArray[] = $group; 
        //}
        

        return new ViewModel(array(
            'order'     => $order,
            //'customer'  => $customer,
            'group'     => $group,
            'xmls'      => $xmlFetch,
        ));
    }
    

    public function addAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);

        if(!$order){
            $order      = new Order();
        }

        

        $form = new OrderForm();
        $error = "";
        /**
         * Llenamos el select con los servicios
         */
        $caseSelect = $form->get('caseId');       
        $caseFetch = $this->getDao("CaseDao")->fetchAll();
        $serviceArray = array();
        foreach ($caseFetch as $case) {
            $caseArray[$case->id] = $case->name;
        }
        $caseSelect->setAttribute('options',$caseArray);
        
        
        /**
         * Llenamos la plataformas
         */
        $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();
        $order->setPlatformId($currentUser->platform_id);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $form->setInputFilter($order->getInputFilter());

            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                
                $order->setDateCreated(\date("Y-m-d H:i:s"));
                $order->setStatusId(1); // Status nuevo
                $order->setCisid($form->get("cisid")->getValue());
                //$order->setProductId($form->get("productId")->getValue());
                $order->setCaseId($form->get("caseId")->getValue());
                $order->setPlatformId($form->get("platformId")->getValue());
                $order->setDescription($form->get("description")->getValue());
                $order->setIntervention("manual");
                $order->setUserId($currentUser->id);
                $order->setCurrentStep(1);
                $order->setCurrentStep(1);

                $caseXmlTemplateFetch = $this->getDao("CaseXmlTemplateDao")
                                        ->fetchByCaseId($order->getCaseId());

                $activities = array();
                foreach ($caseXmlTemplateFetch as $caseXmlTemplate) {

                    $xmltemplate    = $this->getDao("XmlTemplateDao")
                                       ->get($caseXmlTemplate->xmltemplate_id);

                    $commandExploded = explode("Request",$xmltemplate->command);
                    $action = strtolower($commandExploded[0]);

                    $activities[ $caseXmlTemplate->priority ] = $action;
                }

                $_SESSION['activities']         = $activities; 

                $order->setTotalStep(count($activities));

                $thisStep = 0;
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }

                $order->setCurrentStep( $thisStep + 1 );

                $order->setId($this->getDao()->save($order));

                //$_SESSION['order_id'] = $order->getId();
                //$_SESSION['case_id']  = $order->getCaseId();                                     

                $this->next($order);

            } else {
                $error = $form->getMessages();
            }

        } 

        $form->setData($order->getArrayCopy());

        return new ViewModel(array(
            'form'  =>  $form,
            'error' =>  $error,
            'order' =>  $order,
            //'step'  =>  $step,
        ));
    }


    public function systemdomainaddAction(){

        
        $form = new DomainAddForm();
        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        
        $id_platform = $order->getPlatformId();
        

         //Llena el dropdown de Domain
        $id_domainSelect = $form->get('id_domain');       
        $id_domainFetch = $this->getDao("DomainDao")->fetchByPlatformId($id_platform);
        $id_domainArray = array();
        foreach ($id_domainFetch as $domain) {
            $id_domainArray[$domain->id] = $domain->name;
        }
        $id_domainSelect->setAttribute('options',$id_domainArray);
        //Llena el dropdown de Domain


        $error = "";
        
        $request = $this->getRequest();


        if ($request->isPost()) {
            
            
            $form->setData($request->getPost());
             $domainadd = new DomainAdd();
            

            if ($form->isValid()) {
                
                $id_domain = $form->get("id_domain")->getValue();
                $domain = $this->getDao("DomainDao")->fetchById($id_domain);

                $domainadd->setName($domain->name);
                $domainadd->setid_order($id);
                $domainadd->setid_domain($id_domain);
                $domainadd->setId( $this->getDao("DomainAddDao")->save($domainadd) );

                $this->generateXml('add','SystemDomainAddRequest',$domainadd,$order);
 
                $thisStep = (int)array_search(explode('Action',__FUNCTION__)[0],$_SESSION['activities']);
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }

                $order->setCurrentStep( $thisStep + 1 );
                $order->setId( $this->getDao("OrderDao")->save($order) );

                $this->next($order);
            }


        }
        
        $form->setData($order->getArrayCopy());
        return new ViewModel(array(
            'form'   =>  $form,
            //'error'  =>  $error,
            'order'  =>  $order,
        ));
        

    }

    public function serviceprovideraddAction(){

        
        $form = new ServiceProviderAddForm();
        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $id_platform = $order->getPlatformId();
        
        
        //Llena el dropdaown de Proveedor de Servicios
        $id_service_providerSelect = $form->get('id_service_provider');       
        $providerFetch = $this->getDao("ProviderDao")->fetchByPlatformId($id_platform);
        $providerArray = array();
        foreach ($providerFetch as $provider) {
            $providerArray[$provider->id] = $provider->name;
        }

        $id_service_providerSelect->setAttribute('options',$providerArray);
        //Llena el dropdaown de Proveedor de Servicios

        //Coloca el Id de domain_order
        //$id_order_domain_value = $domainadd_id->id;
        //$form->get('id_order_domain')->setValue($id_order_domain_value);
        //Coloca el Id de domain_order

        $error = "";
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            
            $form->setData($request->getPost());
            $provideradd = new ProviderAdd();
            

            if ($form->isValid()) {

                $id_service_provider = $form->get("id_service_provider")->getValue();
                $provider = $this->getDao("ProviderDao")->fetchById($id_service_provider);
                $id_domainFetch = $this->getDao("DomainAddDao")->fetchByOrderId($id);
                
                $provideradd->setName($provider->name);
                $provideradd->setid_order($id);
                $provideradd->setid_provider($id_service_provider);
                $provideradd->setname_domain($id_domainFetch->name);
                
                $provideradd->setId( $this->getDao("ProviderAddDao")->save($provideradd) );

                $this->generateXml('add','ServiceProviderAddRequest13mp2',$provideradd,$order);
 
                $thisStep = (int)array_search(explode('Action',__FUNCTION__)[0],$_SESSION['activities']);
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }

                $order->setCurrentStep( $thisStep + 1 );
                $order->setId( $this->getDao("OrderDao")->save($order) );

                $this->next($order);
            }


        }
        
        $form->setData($order->getArrayCopy());
        return new ViewModel(array(
            'form'   =>  $form,
            //'error'  =>  $error,
            'order'  =>  $order,
        ));
        

    }

    public function serviceproviderdnaddlistAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $form = new ServiceProviderOrderAddForm();

        $request = $this->getRequest();

        if ($request->isPost()) {
            
            $serviceproviderempresaadd = new ServiceProviderEmpresaAdd();
            

            $form->setData($request->getPost());

              if ($form->isValid()) {

                $number = $form->get("number")->getValue();
                $providerFetch = $this->getDao("ProviderAddDao")->fetchBy_OrderId($id);
                
                foreach ($providerFetch as $provider) {
                        $nameProvider = $provider->name;
                        $nameIdServiceProvider = $provider->id_service_provider;
                }


                

                $serviceproviderempresaadd->setName($nameProvider);
                $serviceproviderempresaadd->setNumber($number);
                $serviceproviderempresaadd->setid_order($id);
                $serviceproviderempresaadd->set_service_provider($nameIdServiceProvider);

                $serviceproviderempresaadd->setId( $this->getDao("ServiceProviderEmpresaAddDao")->save($serviceproviderempresaadd) );

                $this->generateXml('add','ServiceProviderDnAddListRequest',$serviceproviderempresaadd,$order);
 
                $thisStep = (int)array_search(explode('Action',__FUNCTION__)[0],$_SESSION['activities']);
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }

                $order->setCurrentStep( $thisStep + 1 );
                $order->setId( $this->getDao("OrderDao")->save($order) );

                $this->next($order);
            }

        }

        return new ViewModel(array(
            'form'   =>  $form,
            'error'  =>  $error,
            'order'  =>  $order,
        ));
    }

    public function userserviceassignlistAction(){

         $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group  = $this->getDao("GroupDao")->get($order->getGroupId());

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        $groupserviceprovider   = new GroupServiceProvider();

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId(); 
        $id_user = $order->getUserId();       

        $groupserviceprovider->setOrderId($id);
        $groupserviceprovider->setcommand('userserviceassignlist');
        $groupserviceprovider->setUserId($id_user);
        
        $groupserviceprovider->setId($this->getDao("GroupServiceProviderDao")->save($groupserviceprovider));

        $xmlGenerated = $this->generateXml('add','UserServiceAssignListRequest',$groupserviceprovider,$order);

        $stepCurrentOrder = $order->getCurrentStep();
        $order->setCurrentStep($stepCurrentOrder+1);

        $stepCompleteStep = $order->getCompleteStep();
        $order->setCompleteStep($stepCompleteStep+1);

        $order->setId( $this->getDao("OrderDao")->save($order));
        return $this->next($order);

    }


    public function usermodifyAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $form = new UserModifyAddForm();
        $error="";

        //Datos a mostar

        $providerFetch = $this->getDao("ProviderAddDao")->fetchBy_OrderId($id);        
        foreach ($providerFetch as $provider) {
                $nameDomain = $provider->name_domain;
        }

        $numberFetch  = $this->getDao("ServiceProviderEmpresaAddDao")->fetchBy_OrderId($id);        
        foreach ($numberFetch as $num) {
                $numbervalue = $num->number;
        }

        $deviceFetch  = $this->getDao("DeviceDao")->fetchUnassignedByGroupId($order->getGroupId());        
        foreach ($deviceFetch as $dev) {
                $devicevalue = $dev->type;
        }

        //Datos a mostrar
        $device = $numbervalue."@".$nameDomain;
        $form->get('number')->setValue($numbervalue);
        $form->get('name_device')->setValue($devicevalue);
        $form->get('lineport')->setValue($device);


        $request = $this->getRequest();

        if ($request->isPost()) {
            
            $UserModifyAdd = new UserModifyAdd();
            

            $form->setData($request->getPost());

              if ($form->isValid()) {

                $number = $form->get("number")->getValue();
                $name_device = $form->get("name_device")->getValue();
                $lineport = $form->get("lineport")->getValue();
                $id_user = $order->getUserId();
                $deviceLevel = "Group";

                $UserModifyAdd->setid_order($id);
                $UserModifyAdd->setid_user($id_user);
                $UserModifyAdd->setNumber($number);
                $UserModifyAdd->setlineport($lineport);
                $UserModifyAdd->setdevicegroup($deviceLevel);
                $UserModifyAdd->setdevicename($name_device);
                $UserModifyAdd->setId( $this->getDao("UserModifyAddDao")->save($UserModifyAdd) );

                $this->generateXml('modify','UserModifyRequest17sp4',$UserModifyAdd,$order);
                $thisStep = (int)array_search(explode('Action',__FUNCTION__)[0],$_SESSION['activities']);
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }

                $order->setCurrentStep( $thisStep + 1 );
                $order->setId( $this->getDao("OrderDao")->save($order) );

                $this->next($order);
            }

        }

        return new ViewModel(array(
            'form'   =>  $form,
            'error'  =>  $error,
            'order'  =>  $order,
        ));
    }

    public function groupaddAction()
    {
        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);

        $form = new GroupForm();
        $error = "";

        /**
         * Llenamos el select con los productos
         */
        $productSelect = $form->get('productId');       
        $productFetch = $this->getDao("ProductDao")->fetchAll();
        $productArray = array();
        foreach ($productFetch as $product) {
            $productArray[$product->id] = $product->name;
        }
        $productSelect->setAttribute('options',$productArray);

        if(isset($order->groupId)){
            $group = $this->getDao("GroupDao")->get($order->groupId);
            $form->setData($group->getArrayCopy());
            $timezones = \DateTimeZone::listIdentifiers();
            $form->get('timezone')->setValue(array_search($group->timezone,$timezones));
        } else {
            $group = new Group();
            $form->get('timezone')->setValue(159);
        }
        
        $request = $this->getRequest();
        
        if ($request->isPost()) {
            
            $form->setInputFilter($group->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                
                $group->setName($form->get("name")->getValue());
                $group->setContactName($form->get("contactName")->getValue());
                $group->setContactNumber($form->get("contactNumber")->getValue());
                $group->setContactEmail($form->get("contactEmail")->getValue());
                $group->setProductId($form->get("productId")->getValue());
                $group->setUserlimit($form->get("userlimit")->getValue());
                $group->setCisid($form->get("cisid")->getValue());
                $group->setProvisioned(0);
                $group->setSyncStatus('warning');

                $product = $this->getDao("ProductDao")->get($group->productId);
                $group->setProviderId($product->providerId);

                $timezones = \DateTimeZone::listIdentifiers();
                $timezone = $timezones[(int)$form->get("timezone")->getValue()];
                $group->setTimezone($timezone);

                //$letter = $this->getDao("GroupDao")->getLastGroupLetterByCustomerId($_SESSION['customer_id']);
                $nextLetter = "A";
                /*if($letter){
                    $abc = range('A','Z');
                    $index = array_search($letter, $abc);
                    $nextLetter = $abc[$index+1];
                } */

                $group->setLetter($nextLetter);

                $group->setId( $this->getDao("GroupDao")->save($group) );
                $order->setGroupId($group->getId());

                $this->generateXml('add','GroupAdd',$group,$order);
 
                $thisStep = (int)array_search(explode('Action',__FUNCTION__)[0],$_SESSION['activities']);
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }

                $order->setCurrentStep( $thisStep + 1 );
                $order->setId( $this->getDao("OrderDao")->save($order) );

                $this->next($order);

            } else {
                $error = $form->getMessages();
            }

        }

        return new ViewModel(array(
            'form'   =>  $form,
            'error'  =>  $error,
            'order'  =>  $order,
        ));
    }

   

    public function groupaccessdeviceaddAction()
    {
        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);

        $form = new DeviceForm();
        $error = "";
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $device   = new Device();
            $form->setInputFilter($device->getInputFilter());

            $form->setData($request->getPost());
                       
            if ($form->isValid()) {
                
                $device->setName($form->get("name")->getValue());
                $device->setType($form->get("type")->getValue());
                $device->setProtocol($form->get("protocol")->getValue());
                
                $device->setGroupId($order->getGroupId());
                
                $device->setId( $this->getDao("DeviceDao")->save($device) );
                $xmlGenerated = $this->generateXml('add','GroupAccessDeviceAdd',$device,$order);
      
                $thisStep = (int)array_search(explode('Action',__FUNCTION__)[0],$_SESSION['activities']);
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }

                $this->getDao("OrderDao")->save($order);


            } else {
                $error = $form->getMessages();
            }

        }

        $order  = $this->getDao("OrderDao")->get($id);
        $devices = $this->getDao("DeviceDao")->fetchByGroupId($order->getGroupId());
        $form = new DeviceForm();

        return new ViewModel(array(
            'form'  =>  $form,
            'error' =>  $error,
            'order' =>  $order,
            'devices'=> $devices,
        ));
    }

    public function groupaccessdevicedeleteAction()
    {
        $id     = $this->params()->fromRoute('id');
        $this->getDao("DeviceDao")->delete($id);
        $this->deleteXML($id);
        return new JsonModel();
    }

    public function useraddAction()
    {
        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);

        $form = new UserForm();
        $error = "";

        $group = $this->getDao("GroupDao")->get($order->getGroupId());
        $timezones = \DateTimeZone::listIdentifiers();
        $key = array_search($group->timezone, $timezones);
        $form->get('timezone')->setValue($key);

        /**
         * Llenamos el select con los idiomas
         */
        $languageSelect = $form->get('language');       
        $languageArray = array();
        foreach (Language::getListNames() as $language) {
            $languageArray[$language] = $language;
        }
        $languageSelect->setAttribute('options',$languageArray);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $user   = new User();
            $form->setInputFilter($user->getInputFilter());

            $form->setData($request->getPost());
            
            if ($form->isValid()) {
                

                $user->setFirstname($form->get("firstname")->getValue());
                $user->setLastname($form->get("lastname")->getValue());
                $user->setUserid($form->get("userid")->getValue());
                $user->setLanguage($form->get("language")->getValue());
                $user->setPassword('123456789');

                $timezones = \DateTimeZone::listIdentifiers();
                $timezone = $timezones[(int)$form->get("timezone")->getValue()];
                $user->setTimezone($timezone);
                
                $user->setGroupId($order->getGroupId());
                
                $user->setId ( $this->getDao("UserDao")->save($user) );
                $xmlGenerated = $this->generateXml('add','User',$user,$order);

                $thisStep = (int)array_search(explode('Action',__FUNCTION__)[0],$_SESSION['activities']);
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }
                
                $this->getDao("OrderDao")->save($order);

            } else {
                $error = $form->getMessages();
            }

        }

        //$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        //$code = Language::getListCodesNames();
        $form->get('language')->setValue('Spanish');
        $order  = $this->getDao("OrderDao")->get($id);
        $users = $this->getDao("UserDao")->fetchByGroupId($order->getGroupId());

        return new ViewModel(array(
            'form'  =>  $form,
            'error' =>  $error,
            'order' =>  $order,
            'users' =>  $users,
        ));
    }

    public function userdeleteAction()
    {
        $id     = $this->params()->fromRoute('id');
        $this->getDao("UserDao")->delete($id);
        $this->deleteXML($id);
        return new JsonModel();
    }


    public function groupdnassignlistAction()
    {
        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group = $this->getDao("GroupDao")->get($order->getGroupId());
        $request = $this->getRequest();
        $availableDnArray = array();

        if ($request->isPost()) {
            
            //$form->setInputFilter($user->getInputFilter());

            //$form->setData($request->getPost());
            $dns = $this->getDao("DnDao")->fetchByGroupId($group->getId());
            foreach ($dns as $dn) {
                $savedDn = $this->getDao("DnDao")->getByNumber($dn->number);
                $this->deleteXML($savedDn->id);
                $this->getDao("DnDao")->delete($savedDn->id);
                $groupDnArray[$dn->number] = $dn->number;
            }

             $form = new GroupDnAssignForm();

            
            /**
             * Llenamos el select con los numeros disponibles
             */
            $availableDnSelect = $form->get('availableDnRange');       
            $availableDnSelect->setAttribute('options',$availableDnArray);
            $groupDnSelect = $form->get('groupDn'); 
            $groupDnArray = array();
            $groupDnSelect->setAttribute('options',$groupDnArray);

            $numbers = $request->getPost("groupDn");
            foreach ($numbers as $number) {
                $dn = new Dn();
                $dn->setNumber($number);
                $dn->setGroupId($group->getId());
                $dn->setId($this->getDao("DnDao")->save($dn));

                $xmlGenerated = $this->generateXml('add','GroupDnAssignListRequest',$dn,$order);

                $thisStep = (int)array_search(explode('Action',__FUNCTION__)[0],$_SESSION['activities']);
                if($thisStep > $order->getCompleteStep() ){
                    $order->setCompleteStep( $thisStep );
                }
                $order->setCurrentStep( $thisStep + 1 );
                
                $order->setId( $this->getDao("OrderDao")->save($order) );
            }

            $this->next($order);

        } else {
            
            try {
                $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand('list','ServiceProviderDnGetAvailableList');

                if($xmltemplate) { 
                    $xml = $xmltemplate->getXml();

                    $variableDao = $this->getDao("VariableDao");
                    $variables = $variableDao->fetchByXmlTemplateId($xmltemplate->getId());

                    //$array = $request->getPost();
                    $provider = $this->getDao("ProviderDao")->get($group->providerId);
                    $array = array("name" => $provider->name);

                    $xmlService = new XmlService($this->getServiceLocator());
                    $xmlGenerated = $xmlService->mergeArray($xml,$variables,$array);
                    
                    $OCISoapService = new OCISoapService($this->getServiceLocator());
                    //$response = $OCISoapService->send($xmlGenerated);
                    $response = null;
                    $xmlresponse = $xmlService->getCommnadResponseFromXml($response);

                    $availableDnArray = array();
                    if($xmlresponse->result == "success"){ 
                        $availableDnArray = (array)$xmlresponse->command->availableDn;
                    } 
                } 

            } catch (\Exception $ex) {
                
            }

             $form = new GroupDnAssignForm();

            
            /**
             * Llenamos el select con los numeros disponibles
             */
            $availableDnSelect = $form->get('availableDnRange');       
            $availableDnSelect->setAttribute('options',$availableDnArray);

            /**
             * Llenamos el select con los numeros ya del grupo
             */
            $dns = $this->getDao("DnDao")->fetchByGroupId($group->getId());
            $groupDnSelect = $form->get('groupDn');  
            $groupDnArray = array();
            foreach ($dns as $groupDn) {
                $groupDnArray[$groupDn->number] = $groupDn->number;
            }     
            $groupDnSelect->setAttribute('options',$groupDnArray);
        }

        return new ViewModel(array(
            'form'      =>  $form,
            //'error'     =>  $error,
            'order'     =>  $order,
            'available' =>  $availableDnArray,
        ));
    }

    public function userdndeviceassignAction()
    {

        $id     = $this->params()->fromRoute('id');
       
        if($id<>"") {
            $order  = $this->getDao("OrderDao")->get($id);
            $group = $this->getDao("GroupDao")->get($order->getGroupId());
            
            $unassignedUsers    = $this->getDao("UserDao")->fetchUnassignedByGroupId($group->id);
            $unassignedDns      = $this->getDao("DnDao")->fetchUnassignedByGroupId($group->id);
            //echo $group->id."Hola";
            $unassignedDevices  = $this->getDao("DeviceDao")->fetchUnassignedByGroupId($group->id);

            $form = new UserDnDeviceAssignForm();

            /* Id Order*/
            $id_order_input = $form->get('id_order'); 
            $id_order_input->setAttribute('value',$id);
            /*Id Order*/

            /* Asignamos usuarios a su select */
            $unassignedUsersArray = array();
            foreach ($unassignedUsers as $unassignedUser) {
                $unassignedUsersArray[$unassignedUser->id] = 
                    $unassignedUser->userid . " (" . $unassignedUser->firstname . " " . $unassignedUser->lastname . ")";
            }
            $unassignedUsersSelect = $form->get('id');       
            $unassignedUsersSelect->setAttribute('options',$unassignedUsersArray);


            /* Asignamos dn a su select */
            $unassignedDnsArray = array();
            foreach ($unassignedDns as $unassignedDn) {
                $unassignedDnsArray[$unassignedDn->id] = $unassignedDn->number;
            }
            $unassignedDnsSelect = $form->get('dnId');       
            $unassignedDnsSelect->setAttribute('options',$unassignedDnsArray);

            /* Asignamos device a su select */
            $unassignedDevicessArray = array();
            foreach ($unassignedDevices as $unassignedDevice) {
                $unassignedDevicesArray[$unassignedDevice->id] = $unassignedDevice->name;
            }
            $unassignedDevicesSelect = $form->get('deviceId');       
            $unassignedDevicesSelect->setAttribute('options',$unassignedDevicesArray);
        }

        $request = $this->getRequest();
        
        

        if ($request->isPost()) {

           
            $id_order      = $request->getPost("id_order");
            $order  = $this->getDao("OrderDao")->get($id_order);
            $group = $this->getDao("GroupDao")->get($order->getGroupId());
            
            $id      = $request->getPost("id");
            $dnId    = $request->getPost("dnId");
            $deviceId= $request->getPost("deviceId");

            $user = $this->getDao("UserDao")->get($id);
            $user->setDnId($dnId);
            $user->setDeviceId($deviceId);
            $user->setId($this->getDao("UserDao")->save($user));
            

           

            $xmlGenerated = $this->generateXml('modify','UserDnDeviceAssign',$user,$order);

            
            
            $stepCurrentOrder = $order->getCurrentStep();
            $order->setCurrentStep($stepCurrentOrder+1);

            $stepCompleteStep = $order->getCompleteStep();
            $order->setCompleteStep($stepCompleteStep+1);
            
            $order->setId( $this->getDao("OrderDao")->save($order));

        
            return $this->next($order);

        }

        return new ViewModel(array(
            'form'  =>  $form,
            //'error' =>  $error,
            //'step'  =>  $step,
        ));
    }

    public function groupservicemodifyauthorizationlistAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group  = $this->getDao("GroupDao")->get($order->getGroupId());

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        $groupserviceprovider   = new GroupServiceProvider();

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        

        $groupserviceprovider->setGroupId($GroupId);
        $groupserviceprovider->setServiceproviderId($ProviderId);
        $groupserviceprovider->setOrderId($id);
        $groupserviceprovider->setcommand('groupservicemodifyauthorizationlist');

        $groupserviceprovider->setId($this->getDao("GroupServiceProviderDao")->save($groupserviceprovider));

        $xmlGenerated = $this->generateXml('modify','GroupServiceModifyAuthorizationListRequest',$groupserviceprovider,$order);

        $stepCurrentOrder = $order->getCurrentStep();
        $order->setCurrentStep($stepCurrentOrder+1);

        $stepCompleteStep = $order->getCompleteStep();
        $order->setCompleteStep($stepCompleteStep+1);

        $order->setId( $this->getDao("OrderDao")->save($order));
        return $this->next($order);


    }

    public function groupserviceassignlistAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group  = $this->getDao("GroupDao")->get($order->getGroupId());

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        $groupserviceprovider   = new GroupServiceProvider();

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();
        $provider  = $this->getDao("ProviderDao")->get($ProviderId);
        

        $groupserviceprovider->setGroupId($GroupId);
        $groupserviceprovider->setServiceproviderId($ProviderId);
        $groupserviceprovider->setOrderId($id);
        $groupserviceprovider->setname($provider->getName());
        $groupserviceprovider->setcommand('GroupServiceAssignList');

        $groupserviceprovider->setId($this->getDao("GroupServiceProviderDao")->save($groupserviceprovider));

        $xmlGenerated = $this->generateXml('list','GroupServiceAssignListRequest',$groupserviceprovider,$order);

        $stepCurrentOrder = $order->getCurrentStep();
        $order->setCurrentStep($stepCurrentOrder+1);

        $stepCompleteStep = $order->getCompleteStep();
        $order->setCompleteStep($stepCompleteStep+1);

        $order->setId( $this->getDao("OrderDao")->save($order));
        return $this->next($order);


    }

    public function groupcallprocessingmodifypolicyAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group  = $this->getDao("GroupDao")->get($order->getGroupId());

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        $groupserviceprovider   = new GroupServiceProvider();

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();
        $UserId = $order->getUserId();

        $groupserviceprovider->setGroupId($GroupId);
        $groupserviceprovider->setUserId($UserId);
        $groupserviceprovider->setServiceproviderId($ProviderId);
        $groupserviceprovider->setcommand('groupcallprocessingmodifypolicyrequest');
        $groupserviceprovider->setOrderId($id);
    
        $groupserviceprovider->setId($this->getDao("GroupServiceProviderDao")->save($groupserviceprovider));

        $xmlGenerated = $this->generateXml('modify','GroupCallProcessingModifyPolicyRequest15sp2',$groupserviceprovider,$order);

        $stepCurrentOrder = $order->getCurrentStep();
        $order->setCurrentStep($stepCurrentOrder+1);

        $stepCompleteStep = $order->getCompleteStep();
        $order->setCompleteStep($stepCompleteStep+1);

        $order->setId( $this->getDao("OrderDao")->save($order));
        return $this->next($order);


    }



    public function userauthenticationmodifyAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group  = $this->getDao("GroupDao")->get($order->getGroupId());

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        $groupserviceprovider   = new GroupServiceProvider();

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();
        $UserId = $order->getUserId();

        $userFetch  = $this->getDao("UserDao")->fetchByGroupId($GroupId);

        foreach ($userFetch as $user) {
            $password = $user->password;
        }

        $groupserviceprovider->setUserId($UserId);
        $groupserviceprovider->setcommand('UserAuthenticationModifyRequest');
        $groupserviceprovider->setOrderId($id);
        $groupserviceprovider->setclave($password);
        $groupserviceprovider->setId($this->getDao("GroupServiceProviderDao")->save($groupserviceprovider));

        $xmlGenerated = $this->generateXml('modify','UserAuthenticationModifyRequest',$groupserviceprovider,$order);

        $stepCurrentOrder = $order->getCurrentStep();
        $order->setCurrentStep($stepCurrentOrder+1);

        $stepCompleteStep = $order->getCompleteStep();
        $order->setCompleteStep($stepCompleteStep+1);

        $order->setId( $this->getDao("OrderDao")->save($order));
        return $this->next($order);

    }


    public function groupdnactivatelistAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group  = $this->getDao("GroupDao")->get($order->getGroupId());

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        $groupserviceprovider   = new GroupServiceProvider();

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();
        

        

        $groupserviceprovider->setGroupId($GroupId);
        $groupserviceprovider->setServiceproviderId($ProviderId);
        $groupserviceprovider->setOrderId($id);
        $groupserviceprovider->setcommand('GroupDnActivateListRequest');

        $groupserviceprovider->setId($this->getDao("GroupServiceProviderDao")->save($groupserviceprovider));

        $xmlGenerated = $this->generateXml('list','GroupDnActivateListRequest',$groupserviceprovider,$order);

        $stepCurrentOrder = $order->getCurrentStep();
        $order->setCurrentStep($stepCurrentOrder+1);

        $stepCompleteStep = $order->getCompleteStep();
        $order->setCompleteStep($stepCompleteStep+1);

        $order->setId( $this->getDao("OrderDao")->save($order));
        return $this->next($order);


    }


    public function ServiceProviderServiceModifyAuthorizationListAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group  = $this->getDao("GroupDao")->get($order->getGroupId());

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        $groupserviceprovider   = new GroupServiceProvider();

        $ProviderId = $group->getProviderId();
        $provider  = $this->getDao("ProviderDao")->get($ProviderId);
        $GroupId = $order->getGroupId();
        $UserId = $order->getUserId();

        $groupserviceprovider->setGroupId($GroupId);
        $groupserviceprovider->setServiceproviderId($ProviderId);
        $groupserviceprovider->setOrderId($id);
        $groupserviceprovider->setUserId($UserId);
        $groupserviceprovider->setname($provider->getName());
        $groupserviceprovider->setcommand('ServiceProviderServiceModifyAuthorizationListRequest');

        $groupserviceprovider->setId($this->getDao("GroupServiceProviderDao")->save($groupserviceprovider));

        $xmlGenerated = $this->generateXml('modify','ServiceProviderServiceModifyAuthorizationListRequest',$groupserviceprovider,$order);

        $stepCurrentOrder = $order->getCurrentStep();
        $order->setCurrentStep($stepCurrentOrder+1);

        $stepCompleteStep = $order->getCompleteStep();
        $order->setCompleteStep($stepCompleteStep+1);

        $order->setId( $this->getDao("OrderDao")->save($order));
        return $this->next($order);


    }

    public function groupseriescompletionaddinstanceAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group  = $this->getDao("GroupDao")->get($order->getGroupId());

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();

        $groupserviceprovider   = new GroupServiceProvider();

        $ProviderId = $group->getProviderId();
        $GroupId = $order->getGroupId();
        $UserId = $order->getUserId();
        



        $groupserviceprovider->setGroupId($GroupId);
        $groupserviceprovider->setServiceproviderId($ProviderId);
        $groupserviceprovider->setOrderId($id);
        $groupserviceprovider->setUserId($UserId);
        $groupserviceprovider->setname($group->getName());
        $groupserviceprovider->setcommand('GroupSeriesCompletionAddInstanceRequest');

        $groupserviceprovider->setId($this->getDao("GroupServiceProviderDao")->save($groupserviceprovider));

        $xmlGenerated = $this->generateXml('add','GroupSeriesCompletionAddInstanceRequest',$groupserviceprovider,$order);

        $stepCurrentOrder = $order->getCurrentStep();
        $order->setCurrentStep($stepCurrentOrder+1);

        $stepCompleteStep = $order->getCompleteStep();
        $order->setCompleteStep($stepCompleteStep+1);

        $order->setId( $this->getDao("OrderDao")->save($order));
        return $this->next($order);


    }
     
    private function next($order){

            $action = $_SESSION['activities'][$order->getCurrentStep()];
            if($action){
                $toRoute = array(
                    'action'    =>  $action,
                    'id'        =>  $order->getId(),
                );
            } else {
                $toRoute = array(
                    'action'    =>  'view',
                    'id'        =>  $order->getId(),
                );
            }

            return $this->redirect()->toRoute('order',$toRoute);
    }


    public function nextAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $order->setCurrentStep((int)$order->getCurrentStep()+1);
        $this->getDao("OrderDao")->save($order);

        $action = $_SESSION['activities'][$order->getCurrentStep()];
        if($action){
            $toRoute = array(
                'action'    =>  $action,
                'id'        =>  $id,
            );
        } else {
            $toRoute = array(
                'action'    =>  'view',
                'id'        =>  $id,
            );
        }

        return $this->redirect()->toRoute('order',$toRoute);
    }

    public function completeAction()
    {
        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $group = $this->getDao("GroupDao")->get($order->getGroupId());

        $stepCurrentOrder = $order->getCurrentStep();
        $activities = $_SESSION['activities'][$stepCurrentOrder];



    }

    public function previousAction(){

        $id     = $this->params()->fromRoute('id');
        $order  = $this->getDao("OrderDao")->get($id);
        $order->setCurrentStep((int)$order->getCurrentStep()-1);
        $this->getDao("OrderDao")->save($order);

        $action = $_SESSION['activities'][$order->getCurrentStep()];
        if($action){
            $toRoute = array(
                'action'    =>  $action,
                'id'        =>  $id,
            );
        } else {
            $toRoute = array(
                'action'    =>  'add',
                'id'        =>  $id,
            );
        }

        return $this->redirect()->toRoute('order',$toRoute);
    }

    public function informacionClienteNumero($step)
    {
        $form = new CustomerNumberForm();
        $error = "";

        $numbersInStock = $this->getDao("NumberStockDao")->fetchAvailable();
        $numbersAvailableArray = array();
        foreach ($numbersInStock as $numberStock) {
            $numbersAvailableArray[$numberStock->number] = $numberStock->number;
        }
        $form->get('numberStock')->setAttribute('options',$numbersAvailableArray);
        
 

        return new ViewModel(array(
            'form'  =>  $form,
            'error' =>  $error,
            'step'  =>  $step,
        ));
    }
    
    private function provisioning($step) {

        $orderDao       = $this->getDao("OrderDao");
        $order = $orderDao->get($_SESSION['order_id']);

        $platform = $this->getDao("PreferenceDao","Preference")->getByName($order->platform);

        return new ViewModel(array(
            'step'  =>  $step,
            'host'  =>  $platform->value,
            'orderId'=> $_SESSION['order_id'],
        ));
    }
    
    public function merge($body, $vars)
    {
        if (count($vars) == 0) {
            return $body;
        }

        $patterns = array();
        $replacements = array();
        foreach ($vars as $key => $value) {
            $patterns[] = '/' . $key . '/i';
            $replacements[] = $this->cleanUp($value);
        }

        if (null === ($body = preg_replace($patterns, $replacements, $body))) {
            throw new \Exception('The message could not be merged');
        }

        return $body;
    }
    
    public function cleanUp($var)
    {
        $workingCopy = $var;

        //Additional clean ups should be implemented here
        //utf8 encoding
        //replacing HTML entities via html_entity_decode/encode as
        //the message should go with tildes inside it

        $workingCopy = trim($var);

        return $workingCopy;
    }
    
    /*public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }*/
    
    public function getDao($dao = "OrderDao",$namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->serviceDao;
    }





    private function generateOneXml($orderId, $priority, $class = null) {
        
        $relationship = array(
            "Device" => array(
                "relationField" => "groupId",
                "daoFunction"   => "getBy",
                "parentClass"   => "Group", 
            ), 
            "Group" => array(
                "relationField" => "customerId",
                "daoFunction"   => "fetchByCustomerId",
            ),
        );

        $classpath = get_class($class);
        $classpathArray = explode("\\", $classpath);
        $classname = trim($classpathArray[count($classpathArray)-1]);

        $orderDao       = $this->getDao("OrderDao");
        $xmlDao         = $this->getDao("XmlDao");
        $scDao          = $this->getDao("ServiceCommandDao");
        $commandDao     = $this->getDao("CommandDao");
        //$deviceDao     = $this->getDao("DeviceDao");

        $order          = $orderDao->get($orderId);
        $scModel        = $scDao->getByServiceIdAndPriority($order->getServiceId(),$priority);

        //foreach ($scModels as $scModel) {
            
            $command = $commandDao->get($scModel->commandId);
            //print_r($command);
            $xml = $command->getXml();
            
            $variableDao = $this->getDao("VariableDao");
            $variables = $variableDao->fetchByCommandId($command->id);
            $variablesArray = array();
            foreach ($variables as $variable) {
                $key        = $variable->name;
                $object     = $variable->mappingObject      ?   $variable->mappingObject    : $variable->mapping_object;
                $attribute  = $variable->mappingAttribute   ?   $variable->mappingAttribute : $variable->mapping_attribute;
                $treatment  = $variable->treatment;
                $daoName    = $object . "Dao";
                $objectDao  = $this->getDao($daoName,$object=="Preference"?$object:"Provisioning");
                $objectAttr = lcfirst($object) . "Id";

                if($object == "Preference"){
                    $instance   = $objectDao->getByName($attribute);
                    $value      = isset($instance->value)?$instance->value:"";
                } elseif($classname == $object) {
                    $instance   = $class;
                    $value      = isset($instance->$attribute)?$instance->$attribute:"";
                } elseif(property_exists($order, $objectAttr)) {
                    $instance   = $objectDao->get($order->$objectAttr);
                    $value      = isset($instance->$attribute)?$instance->$attribute:"";
                } else {
                    $instance   = $objectDao->get($class->$objectAttr);
                    $value      = isset($instance->$attribute)?$instance->$attribute:"";
                }
                
                switch ($treatment) {
                    case "concat":
                        $array = explode(" ", $value);
                        $value = implode("_", $array);
                        break;
                    case "initials":
                        $array = explode(" ", $value);
                        $initials = "";
                        foreach ($array as $v) {
                            $initials .= strtoupper($v[0]);
                        }
                        $value = $initials;
                        break;
                    default:
                        break;
                }
                
                $variablesArray[$key] = $value;
            }

            $xmlGenerated = $this->merge($xml, $variablesArray);
            /*
             * Nombre del archivo:
             * orderid + commandid + timestamp
             */
            $filename = $order->id . $command->id . time();
            file_put_contents("tmp/" . $filename,
                    $xmlGenerated, LOCK_EX);

            $xml = new Xml();
            $xml->setName($command->name);
            $xml->setOrderId($order->id);
            $xml->setStatus("pendiente");
            $xml->setPriority($scModel->priority);
            $xml->setFile($filename);

            $xml->id = $xmlDao->save($xml);

    }

    /** Método para generar el xml para aprovisionar
    *** a partir de la categoria, nombre de comando (like)
    *** y la data proporcionada.
    *** 
    *** $category puede ser add|get|delete|list|modify  
    *** $likeCommand puede ser un fragmento de la cadena del comando
    *** $instance es mandatoriamente el objeto instanciado del que se creará el xml
    **/
    public function generateXml($category = 'add', $likeCommand = null, $instance = null, $order){

        if(!isset($likeCommand)) { return false; }
        if(!isset($instance)) { return false; }

        try {
            $xmltemplate = $this->getDao("XmlTemplateDao")->getByCategoryLikeCommand($category,$likeCommand);

            if($xmltemplate) { 
                
                $this->deleteXML($instance->id);

                $xmltemplateBody = $xmltemplate->getXml();

                $variableDao = $this->getDao("VariableDao");
                $variables = $variableDao->fetchByXmlTemplateId($xmltemplate->getId());

                $xmlService = new XmlService($this->getServiceLocator());
                $xmlGenerated = $xmlService->mergeOrder($xmltemplateBody,$variables,$instance);

                $xml = new Xml();
                $xml->setOrderId($order->getId());
                $xml->setName($xmltemplate->getCommand());
                $xml->setStatusId(1);
                $xml->setBody($xmlGenerated);
                $xml->setPriority((int)$order->getCurrentStep());
                $xml->setObjectId($instance->id);
                $this->getDao("XmlDao")->save($xml);
                
                return $xmlGenerated;


            } else {
                return false;
            }

        } catch (\Exception $ex) {
            return false;
        }
    }

    /*private function deleteXML($order,$likeCommand,$likeContent=null)
    {
        if($likeContent){
            $this->getDao("XmlDao")->deleteByOrderIdLikeCommandLikeContent($order->getId(),$likeCommand,$likeContent);
        } else {
            $this->getDao("XmlDao")->deleteByOrderIdLikeCommand($order->getId(),$likeCommand);
        }
    }*/

    private function deleteXML($id)
    {
        $this->getDao("XmlDao")->deleteByObjectId($id);
    }

}
