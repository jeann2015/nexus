<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Generic\Controller\GenericController;

use Provisioning\Model\DomainAdd;
use Provisioning\Form\DomainAddForm;

use Provisioning\Service\OCISoapService;
use Provisioning\Service\XmlService;


class DomainAddController extends GenericController
{ 
    public $dao;
  
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $domain = $this->getDao()->getCountSyncStatus('warning');

        return new ViewModel(array(
            'warnings'  => $domain->cantidad,
        ));
    }

    

}
