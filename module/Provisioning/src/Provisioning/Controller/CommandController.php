<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Provisioning\Model\Command;
use Provisioning\Model\Variable;

use Provisioning\Model\Customer;
use Provisioning\Model\Group;
use Provisioning\Model\Order;
use Provisioning\Model\Product;
use Provisioning\Model\Device;
use Provisioning\Model\User;
use Provisioning\Model\Domain;

use Provisioning\Form\CommandForm;

use Preference\Dao\PreferenceDao;

use Generic\Controller\GenericController;

class CommandController extends GenericController
{ 
    public $serviceDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }


        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $commands = $this->getDao()->fetchAll();

        return new ViewModel(array('commands' => $commands));
    }
      
    public function addAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $command = new Command();
            $form = new CommandForm();
            $form->setInputFilter($command->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $command->exchangeArray($form->getData());
                  
                $id = $this->getDao()->save($command);

                $xml = $command->xml;
                $xml = str_replace("\n", '', $xml);
                $xml = str_replace("\t", '', $xml);

                if(preg_match_all('/%[A-Z_-]*%/mi', $xml, $variablesInXml) === 0)
                {
                    $this->getDao("VariableDao")->deleteByCommandId($id);
                    return $this->redirect()->toRoute('command',array('action'=>'list'));
                } else {
                    if(!is_array($variablesInXml)) { $variablesInXml[0] = $variablesInXml; } 
                    foreach ($variablesInXml[0] as $variableInXml){
                        
                        $variable = $this->getDao("VariableDao")->getByCommandIdAndName($id,$variableInXml);
                        if($variable === null){
                            $variable = new Variable();
                            $variable->setCommandId($id);
                            $variable->setName($variableInXml);
                            $this->getDao("VariableDao")->save($variable);
                        }

                    }
                    return $this->redirect()->toRoute('command',array("action"=>"variable","id"=>$id));
                }
            }

        }

        return new ViewModel();
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $command = $this->getDao()->get($id);
        
        $form = new CommandForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $command = new Command();
            $form->setInputFilter($command->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $command->exchangeArray($form->getData());
                  
                $id = $this->getDao()->save($command);
                
                $xml = $command->xml;
                $xml = str_replace("\n", '', $xml);
                $xml = str_replace("\t", '', $xml);

                if(preg_match_all('/%[A-Z_-]*%/mi', $xml, $variablesInXml) === 0)
                {
                    $this->getDao("VariableDao")->deleteByCommandId($id);
                    return $this->redirect()->toRoute('command',array('action'=>'list'));
                } else {
                    if(!is_array($variablesInXml)) { $variablesInXml[0] = $variablesInXml; } 
                    foreach ($variablesInXml[0] as $variableInXml){
                        
                        $variable = $this->getDao("VariableDao")->getByCommandIdAndName($id,$variableInXml);
                        if($variable === null){
                            $variable = new Variable();
                            $variable->setCommandId($id);
                            $variable->setName($variableInXml);
                            $this->getDao("VariableDao")->save($variable);
                        }
                    }

                    return $this->redirect()->toRoute('command',array("action"=>"variable","id"=>$id));
                }
            }

        } else {
            $form->setData($command->getArrayCopy());
        }

        return new ViewModel(array(
            'form'      => $form,
            'command'   => $command,
        ));
    }
    
    public function variableAction()
    {
        
        $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        if ($request->isPost()) {
            foreach($request->getPost() as $key => $value){
                $pos = strpos($key, "id");
                if($pos === 0){
                    $variable = new Variable();
                    $variable->setId($value);
                    $variable->setXmltemplateId($id);
                    $variable->setName($request->getPost("name".$value));
                    $variable->setMappingObject($request->getPost("mappingObject".$value));
                    $variable->setMappingAttribute($request->getPost("mappingAttribute".$value));
                    $variable->setTreatment($request->getPost("treatment".$value));
                    $this->getDao("VariableDao")->save($variable);
                } else {
                    $pos = strpos($key, "delete");
                    if($pos === 0){
                        $this->getDao("VariableDao")->delete($value);
                    }
                }
            }
            return $this->redirect()->toRoute('command',array('action'=>'list'));
        }
        
        
        $command = $this->getDao()->get($id);
        $variables = $this->getDao("VariableDao")->fetchByCommandId($id);
        
        $xml = $command->xml;
        $xml = str_replace("\n", '', $xml);
        $xml = str_replace("\t", '', $xml);

        preg_match_all('/%[A-Z_-]*%/mi', $xml, $variablesInXml);
        if(!is_array($variablesInXml)) { $variablesInXml[0] = $variablesInXml; } 
        
        $preferenceFetch = $this->getDao("PreferenceDao","Preference")->fetchByCategory("provisioning");
        $preferences = array();
        foreach ($preferenceFetch as $p) {
            array_push($preferences, $p->name);
        }
        
        return new ViewModel(array(
            'id'                =>  $id,
            'variables'         =>  $variables,
            'variablesInXml'    =>  $variablesInXml[0],
            'objects'           =>  array(
                                        "Order"     =>"Orden",
                                        "Customer"  =>"Cliente",
                                        "Group"     =>"Grupo",
                                        "Product"   =>"Producto",
                                        "Preference"=>"Preferencias",
                                        "Device"    =>"Dispositivo",
                                        "User"      =>"Usuario",
                                        "Domain"    =>"Dominio",
                                    ),
            'attributes'        =>  array( 
                "Customer"  => array_keys( get_object_vars ( new Customer() ) ),
                "Group"     => array_keys( get_object_vars ( new Group() ) ),
                "Order"     => array_keys( get_object_vars ( new Order()    ) ),
                "Product"   => array_keys( get_object_vars ( new Product()  ) ),
                "Device"    => array_keys( get_object_vars ( new Device()   ) ),
                "User"      => array_keys( get_object_vars ( new User()   ) ),
                "Domain"    => array_keys( get_object_vars ( new Domain()   ) ),
                "Preference"=> $preferences,
            ),
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "CommandDao",$namespace = "Provisioning")
    {
        //if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->serviceDao;
    }

}
