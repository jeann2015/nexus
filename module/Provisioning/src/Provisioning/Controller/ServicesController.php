<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Provisioning\Model\Services;
use Provisioning\Form\ServicesForm;
use Generic\Controller\GenericController;

class ServicesController extends GenericController
{ 
    public $serviceDao;

    public function loadAction()
    {
        
        $xml = file_get_contents("C:\Users\Rodolfo\Desktop\services.xml");
        $obj = simplexml_load_string($xml);

        $groups = 0;
        foreach ($obj->groupServiceAuthorization as $item) {
            $service = new Services();
            $service->setName($item->serviceName);
            $service->setCategory(2);
            $this->getDao()->save($service);
            $groups++;
        }

        $users = 0;
        foreach ($obj->userServiceAuthorization as $item) {
            $service = new Services();
            $service->setName($item->serviceName);
            $service->setCategory(1);
            $this->getDao()->save($service);
            $users++;
        }

        return new JsonModel(array(
            'userServices'  => $users,
            'groupServices' => $groups,
        ));
    }
    
    public function indexAction()
    {
    	
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    public function listAction()
    {
    	
		
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
	
	 public function listDataAction()
    {
        $services = $this->getDao()->fetchAll();
		$ser =array();
		foreach ($services as $domain) {
            $ser[] = $domain;
        }
		
        return new JsonModel($ser);
    }
      
    public function addAction()
    {
    	
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $service = new Services();
            $form = new ServicesForm();
            $form->setInputFilter($service->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $service->exchangeArray($form->getData());
                $this->getDao()->save($service);

                return $this->redirect()->toRoute('services',array('action'=>'list'));
            }

        }

        return new ViewModel();
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $service = $this->getDao()->get($id);
        
        $form = new ServicesForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $service = new Services();
            $form->setInputFilter($service->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $service->exchangeArray($form->getData());
                  
                $this->getDao()->save($service);

                return $this->redirect()->toRoute('services',array('action'=>'list'));
            }

        } else {
            $form->setData($service->getArrayCopy());
        }
        
        //$commands = $this->getDao("CommandDao")->fetchAll();
		$commands = $this->getDao()->fetchAll();
        $commandsArray = array();
        foreach ($commands as $command) {
            $commandsArray[$command->id] = $command->name;
        }

        return new ViewModel(array(
            'form'      => $form,
            'service'   => $service,
        ));
    }
    
    public function commandAction()
    {
        $id = $this->params()->fromRoute('id');
        $request = $this->getRequest();
        if ($request->isPost()) {
            try {
                
                $thePost = json_decode($request->getContent());
                $scdao = $this->getDao("ServiceCommandDao");
                $scdao->delete($id); //Elimina todo
                foreach ($thePost as $item) {
                    //$sc = $scdao->getByServiceIdAndCommandId($item->serviceId,$item->commandId);
                    //if($sc) {
                        
                    //}
                    $sc = new ServiceCommand();
                    $sc->setPriority($item->priority);
                    $sc->setXmltemplateId($item->xmltemplateId);
                    $sc->setCaseId($item->caseId);
                    $scdao->save($sc);
                    unset($sc);
                }
                
                $return = array("result" => "success","payload"=>$thePost);
                
                
            } catch (\Exception $e){
                
                $return = array("result" => "error","payload"=>$e->getMessage());
            }
            
            return new JsonModel($return);

        } else {
            $id = $this->params()->fromRoute('id');
            $service = $this->getDao()->get($id);

            $form = new ServiceForm();
            $form->setData($service->getArrayCopy());
        
        
            $commands = $this->getDao("CommandDao")->fetchAll();
            $commandsArray = array();
            foreach ($commands as $command) {
                $commandsArray[$command->id] = $command->name;
            }
            
            $servicecommands = $this->getDao("ServiceCommandDao")->fetchByServiceId($id);
            $servicecommandsArray = array();
            foreach ($servicecommands as $servicecommand) {
                $servicecommandsArray[$servicecommand->xmltemplate_id] = $servicecommand->priority;
            }
            
            return new ViewModel(array(
                'service'           => $service,
                'commands'          => $commandsArray,
                'servicecommands'   => $servicecommandsArray,
            ));
        }
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "ServicesDao")
    {
        //if (!$this->serviceDao) {
            $sm = $this->getServiceLocator();
            $this->servicesDao = $sm->get('Provisioning\Dao\\'.$dao);
        //}
        
        return $this->servicesDao;
    }

    public function filterAction(){

        $model = new JsonModel();

        $request = $this->getRequest();
        if($request->isPost()){
            $post   = (array)$request->getPost();
            $limit  = isset( $post['limit'] ) ?  $post['limit'] : null;
            $offset = $post['offset'];
            $order  = $post['order'];
            $sort   = $post['sort'];

            $filters = array_key_exists('filters',$post) ? $post['filters'] : null;

            if($filters)
            foreach ($filters as $key => $value) {
                if (strpos($key,'QGDATE') !== false) {
                    if(stripos($value,":") !== false){
                        $filters[$key] = $this->convertToUTC($value);
                    } else {
                        $filters[$key] = $this->convertToUTC($value,array('date'=>true));
                    }
                    //print $filters[$key] . "\n";
                }
            }

            $result = $this->getDao()->fetchFilter($limit,$offset,$order,$sort,$filters);
        } 

        $items = array();
        foreach ($result['resultSet'] as $item) {
            $item->options = "";
            foreach ($item as $key => $value) {

                //Aqui se hace alguna transformación que los valores del grid requieran
                //$value      = $this->translator($value);
                //$item->$key = $value;

                if($this->isDate($value)){
                    if(stripos($value,":") === false){
                        $item->$key = $this->convertFromUTC($value,array('date'=>true));
                    } else {
                        $item->$key = $this->convertFromUTC($value);
                    }  
                }

                if($key == "options"){
                    $url = $this->url()->fromRoute('services',array('action'=>'edit','id'=>$item->id));
                    $edit = "<a class='btn btn-link btn-xs text-warning' href='$url'><i class='fa fa-search'></i> Detalles</a> ";
                    $item->$key = $edit . "<a class='btn btn-link btn-xs text-danger' data-toggle='modal' data-target='#myModal' data-id='".$item->id."'><i class='fa fa-trash-o'></i> Eliminar</a>";
                }

                if($key == "inbroadsoft"){
                    $item->$key = $value == 1 ? "<i class='fa fa-check text-success'></i>" : "<i class='fa fa-close text-danger'></i>";
                }

                if($key == "active" || $key == "default"){
                    $item->$key = $value == 1 ? "<i class='fa fa-check text-success'></i>" : "";
                }

                if($key == "category"){
                    $item->$key = $value == 1 ? "Usuario" : "Grupo";
                }
            }

            array_push($items, $item);
        }

        $model->setVariable("status","success");
        $model->setVariable("payload",array(
            "total"     =>  $result['total'],
            "items"     =>  $items,
        ));

        return $model;
    }

}
