<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Provisioning\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Provisioning\Model\Product;
use Provisioning\Model\ProductServices;
use Provisioning\Form\ProductForm;
use Generic\Controller\GenericController;




class ProductController extends GenericController
{ 
    public $serviceDao;
  
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();

        $products = $this->getDao()->fetchByPlatformId($currentUser->platform_id);

        return new ViewModel(array('products' => $products));
    }
      
    public function addAction()
    {
        $form = new ProductForm();
        $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();
        /**
         * Llenamos el select con los providers
         */
        $providerSelect = $form->get('providerId');       
        $providerFetch = $this->getDao("ProviderDao")->fetchByPlatformId($currentUser->platform_id);
        $providerArray = array("");
        foreach ($providerFetch as $provider) {
            $providerArray[$provider->id] = $provider->name;
        }
        $providerSelect->setAttribute('options',$providerArray);

        //Services
        $servicesMultiSelect = $form->get('services');
        $servicesFetch = $this->getDao("ServicesDao")->fetchAll();
        foreach ($servicesFetch as $services) {
            $servicesArray[$services->id] = $services->name;
        }
        $servicesMultiSelect->setAttribute('options',$servicesArray);
        //Services


        $request = $this->getRequest();
        if ($request->isPost()) {
			
			/**
             * Llenamos el select con los dominios
             */
             
            $domainSelect = $form->get('domainId');       
            $domainFetch = $this->getDao("DomainDao")->fetchByProviderId($request->getPost('providerId'));
            $domainArray = array("");
            foreach ($domainFetch as $domain) {
                $domainArray[$domain->id] = $domain->name;
            }
            $domainSelect->setAttribute('options',$domainArray);
            
            $product = new Product();
            
            $form->setInputFilter($product->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {

                $values = $form->getData();

                $product_services_values = array_slice($values, 4);


                $product_values = array_slice($values,0, 4);

            
                $product_insert = array('id' => '','name'=>$product_values['name'],
                'provider_id'=>$product_values['providerId'],
                'domain_id'=>$product_values['domainId']);

                
                $id = $this->getDao('ProductDao')->saveProduct($product_insert);
                
                $cont=count($product_services_values, COUNT_RECURSIVE )-1;
                
                for($con=0;$con<$cont;$con++){
                    $id_services = $product_services_values['services'][$con];

                    unset($array);

                    $array = array(
                    "id_product" => $id,
                    "id_services" => $id_services,
                    "provider_id" => $product_values['providerId'] );

                    $this->getDao('ProductServicesDao')->saveProductServices($array);
                }
                
                return $this->redirect()->toRoute('product',array('action'=>'list'));
                
            
            }

        }

        return new ViewModel(array(
            'form' => $form,
        ));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $product = $this->getDao()->get($id);
        $form = new ProductForm();
        $currentUser = (object)$this->getServiceLocator()->get('AuthService')->getIdentity();
        /**
         * Llenamos el select con los providers
         */
        $providerSelect = $form->get('providerId');       
        $providerFetch = $this->getDao("ProviderDao")->fetchByPlatformId($currentUser->platform_id);
        $providerArray = array("");
        foreach ($providerFetch as $provider) {
            $providerArray[$provider->id] = $provider->name;
        }

        //print_r($providerArray);
        
        $providerSelect->setAttribute('options',$providerArray);



        /**
         * Llenamos el select con los dominios
         */
        $domainSelect = $form->get('domainId');       
        $domainFetch = $this->getDao("DomainDao")->fetchByProviderId($product->providerId);
        $domainArray = array("");
        foreach ($domainFetch as $domain) {
            $domainArray[$domain->id] = $domain->name;
        }
        $domainSelect->setAttribute('options',$domainArray);

         //Services
        $servicesMultiSelect = $form->get('services');
        $servicesFetch = $this->getDao("ServicesDao")->fetchByServicesId($id);
        foreach ($servicesFetch as $services) {
            $servicesArray[$services->id] = $services->name;
        }        
        $servicesMultiSelect->setAttributes(array('options'=>$servicesArray));
        //Services

        $request = $this->getRequest();
        if ($request->isPost()) {
            
            
            
            $domainSelect = $form->get('domainId');       
            $domainFetch = $this->getDao("DomainDao")->fetchByProviderId($request->getPost('providerId'));
            $domainArray = array("");
            foreach ($domainFetch as $domain) {
                $domainArray[$domain->id] = $domain->name;
            }
            $domainSelect->setAttribute('options',$domainArray);




            $product = new Product();
            $form->setInputFilter($product->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) 
            {


                    $product->exchangeArray($form->getData());

                    $id = $this->params()->fromRoute('id');
                    $values = $form->getData();

                    $product_services_values = array_slice($values, 4);
                    
                    $product_values = array_slice($values,0, 4);
                    
                    $product_set = array('name'=>$product_values['name'],
                    'provider_id'=>$product_values['providerId'],
                    'domain_id'=>$product_values['domainId']);

                    $product_where = array('id' => $id);

                    $this->getDao('ProductDao')->updateProduct($product_set,$product_where); 

                    $this->getDao('ProductServicesDao')->deleteProductServices($id); 
                    $cont=count($product_services_values, COUNT_RECURSIVE )-1;

                    for($con=0;$con<$cont;$con++){
                        $id_services = $product_services_values['services'][$con];

                        unset($array);

                        $array = array(
                        "id_product" => $id,
                        "id_services" => $id_services,
                        "provider_id" => $product_values['providerId'] );

                        $this->getDao('ProductServicesDao')->saveProductServices($array);
                    }
                    
                    return $this->redirect()->toRoute('product',array('action'=>'list'));

           }



        } else {
            $form->setData($product->getArrayCopy());
        }

        return new ViewModel(array(
            'form'      => $form,
            'product'   => $product,
        ));
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            
            $this->getDao('ProductServicesDao')->deleteProductServices($id);

            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }
    
    public function getDao($dao = "ProductDao",$namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->serviceDao;
    }

}
