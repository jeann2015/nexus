<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Authentication\Controller;

use Zend\View\Model\ViewModel;

use Generic\Controller\GenericController;

class AuthController extends GenericController
{ 

    protected $form;
    protected $storage;
    protected $authservice;

    public $dao;
    
    //$this->layout('layout/layout.phtml');     
    public function getAuthService()
    {
        if (! $this->authservice) {
            $this->authservice = $this->getServiceLocator()
                                      ->get('AuthService');
        }
         
        return $this->authservice;
    }
     
    public function getSessionStorage()
    {
        if (! $this->storage) {
            $this->storage = $this->getServiceLocator()
                                  ->get('Authentication\Model\AuthStorage');
        }
         
        return $this->storage;
    }
    
    public function loginAction()
    {
        if(!$_SESSION['platform']){
            if($_SESSION['platform'] === ""){
                return $this->redirect()->toRoute('broadsoft');
            }
        }
        
        //if already login, redirect to success page
        if ($this->getAuthService()->hasIdentity()){
            return $this->redirect()->toRoute('home');
        }

        $platform = $this->getDao()->get($_SESSION['platform_id']);
        
        $this->layout('layout/navbar-less.phtml');
        return new ViewModel(array('platform'=>$platform));
    }
     
    public function authenticateAction()
    {
        $server = $this->getRequest()->getServer();
        $this->log(json_encode((array)$server,JSON_PRETTY_PRINT));
        
        $this->layout('layout/navbar-less.phtml'); 
        $redirect = 'auth';
         
        $request = $this->getRequest();
        if ($request->isPost()){
            
            if($request->getPost('username') == "" || $request->getPost('password') == "" ){
                $this->flashmessenger()->addErrorMessage("Ambos campos son requeridos!");
                return $this->redirect()->toRoute($redirect);
            }
            
            $this->log('Autenticando....' . $request->getPost('username'));

            $this->getAuthService()->getAdapter()
                                   ->setIdentity($request->getPost('username'))
                                   ->setCredential($request->getPost('password'));

            $result = $this->getAuthService()->authenticate();

            foreach($result->getMessages() as $message)
            {
                $this->flashmessenger()->addMessage($message);
            }

            if ($result->isValid()) {
                $redirect = 'home';
                $resultRow = $this->getAuthService()->getAdapter()->getResultRowObject();
                //check if it has rememberMe :
                if ($request->getPost('rememberme') == 1 ) {
                    $this->getSessionStorage()
                         ->setRememberMe(1);
                    //set storage again
                    $this->getAuthService()->setStorage($this->getSessionStorage());
                }

                $this->getAuthService()->getStorage()->write(
                     array( 'id'            => $resultRow->id,
                            'username'      => $resultRow->username,
                            'name'          => $resultRow->name,
                            'ip_address'    => $this->getRequest()->getServer('REMOTE_ADDR'),
                            'user_agent'    => $request->getServer('HTTP_USER_AGENT'),
                            'roleId'        => $resultRow->role_id,
                            'groupId'       => $resultRow->group_id,
                            'platform'      => $_SESSION['platform'],
                            'platform_id'   => $_SESSION['platform_id'],
                         )
                );

                $this->log('Usuario ' . $request->getPost('username') . ' autenticado!');

                if($resultRow->change_password == 1){
                    return $this->redirect()->toRoute('profile',array('action'=>'reset'));
                }

            } else {
                $this->log('Usuario ' . $request->getPost('username') . ': Usuario o contraseña inválida!','info');
                $this->flashmessenger()->addErrorMessage("Usuario o contraseña inválida!");
            }
        }
         
        return $this->redirect()->toRoute($redirect);
    }
     
    public function logoutAction()
    {
        $_SESSION['platform']       = "";
        $_SESSION['platform_id']    = "";
        $this->getSessionStorage()->forgetMe();
        $this->getAuthService()->clearIdentity();
        $this->log('Desconectando....');
        $this->flashmessenger()->addMessage("You've been logged out");
        return $this->redirect()->toRoute('auth');
    }

    public function getDao($dao = "PlatformDao",$namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->serviceDao;
    }
}
