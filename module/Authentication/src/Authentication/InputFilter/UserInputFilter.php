<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Authentication\InputFilter;

/**
 * Description of UserInputFilter
 *
 * @author rodolfo
 */

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

use Generic\InputFilter\GenericInputFilter;

class UserInputFilter extends GenericInputFilter
{
       
    protected $inputFilter;

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value) {
            
            if(empty($data[$key])){
                $this->$key = $value;
            } else {
                $this->$key = null;
            }
            
        }
        //$this->id   = (isset($data['id']))     ? $data['id']    : null;
        //$this->name = (isset($data['name']))   ? $data['name']  : null;
        //$this->code = (isset($data['code']))   ? $data['code']  : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}

