<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Authentication\Model;

use Generic\Model\Generic;
/**
 * Description of User
 *
 * @author rodolfo
 */
class User extends Generic {
    
    public $groupId;
    public $roleId;
    public $name;
    public $phone;
    public $mobile;
    public $username;
    public $password;

    public function getGroupId() {
        return $this->groupId;
    }

    public function getRoleId() {
        return $this->roleId;
    }

    public function getName() {
        return $this->name;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getMobile() {
        return $this->mobile;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setGroupId($groupId) {
        $this->groupId = $groupId;
    }

    public function setRoleId($roleId) {
        $this->roleId = $roleId;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }
    
}
