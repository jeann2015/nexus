<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Order\Model;

use Order\InputFilter\OrderInputFilter;
/**
 * Description of Group
 *
 * @author rodolfo
 */
class Order extends OrderInputFilter {
    
    public $statusId;
    public $groupId;
    public $caseId;
    public $dateCreated;
    public $description;
    public $platformId;
    public $cisid;
    public $userId;
    public $intervention;
    public $currentStep;
    public $totalStep;
    public $completeStep;

    /**
     * Gets the value of statusId.
     *
     * @return mixed
     */
    public function getStatusId()
    {
        return $this->statusId;
    }

    /**
     * Sets the value of statusId.
     *
     * @param mixed $statusId the status id
     *
     * @return self
     */
    public function setStatusId($statusId)
    {
        $this->statusId = $statusId;

        return $this;
    }

    /**
     * Gets the value of groupId.
     *
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * Sets the value of groupId.
     *
     * @param mixed $groupId the group id
     *
     * @return self
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * Gets the value of caseId.
     *
     * @return mixed
     */
    public function getCaseId()
    {
        return $this->caseId;
    }

    /**
     * Sets the value of caseId.
     *
     * @param mixed $caseId the case id
     *
     * @return self
     */
    public function setCaseId($caseId)
    {
        $this->caseId = $caseId;

        return $this;
    }

    /**
     * Gets the value of dateCreated.
     *
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Sets the value of dateCreated.
     *
     * @param mixed $dateCreated the date created
     *
     * @return self
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Gets the value of description.
     *
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the value of description.
     *
     * @param mixed $description the description
     *
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Gets the value of platformId.
     *
     * @return mixed
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * Sets the value of platformId.
     *
     * @param mixed $platformId the platform id
     *
     * @return self
     */
    public function setPlatformId($platformId)
    {
        $this->platformId = $platformId;

        return $this;
    }

    /**
     * Gets the value of cisid.
     *
     * @return mixed
     */
    public function getCisid()
    {
        return $this->cisid;
    }

    /**
     * Sets the value of cisid.
     *
     * @param mixed $cisid the cisid
     *
     * @return self
     */
    public function setCisid($cisid)
    {
        $this->cisid = $cisid;

        return $this;
    }

    /**
     * Gets the value of userId.
     *
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Sets the value of userId.
     *
     * @param mixed $userId the user id
     *
     * @return self
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Gets the value of intervention.
     *
     * @return mixed
     */
    public function getIntervention()
    {
        return $this->intervention;
    }

    /**
     * Sets the value of intervention.
     *
     * @param mixed $intervention the intervention
     *
     * @return self
     */
    public function setIntervention($intervention)
    {
        $this->intervention = $intervention;

        return $this;
    }

    /**
     * Gets the value of currentStep.
     *
     * @return mixed
     */
    public function getCurrentStep()
    {
        return $this->currentStep;
    }

    /**
     * Sets the value of currentStep.
     *
     * @param mixed $currentStep the current step
     *
     * @return self
     */
    public function setCurrentStep($currentStep)
    {
        $this->currentStep = $currentStep;

        return $this;
    }

    /**
     * Gets the value of totalStep.
     *
     * @return mixed
     */
    public function getTotalStep()
    {
        return $this->totalStep;
    }

    /**
     * Sets the value of totalStep.
     *
     * @param mixed $totalStep the total step
     *
     * @return self
     */
    public function setTotalStep($totalStep)
    {
        $this->totalStep = $totalStep;

        return $this;
    }

    /**
     * Gets the value of completeStep.
     *
     * @return mixed
     */
    public function getCompleteStep()
    {
        return $this->completeStep;
    }

    /**
     * Sets the value of completeStep.
     *
     * @param mixed $completeStep the complete step
     *
     * @return self
     */
    public function setCompleteStep($completeStep)
    {
        $this->completeStep = $completeStep;

        return $this;
    }
}
   