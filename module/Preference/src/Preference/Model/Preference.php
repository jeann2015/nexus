<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Preference\Model;

use Preference\InputFilter\PreferenceInputFilter;
/**
 * Description of Preference
 *
 * @author rodolfo
 */
class Preference extends PreferenceInputFilter {
    
    public $name;
    public $description;
    public $value;
    public $category;

    public function getName() {
        return $this->name;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getValue() {
        return $this->value;
    }

    public function getCategory() {
        return $this->category;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function setValue($value) {
        $this->value = $value;
    }

    public function setCategory($category) {
        $this->category = $category;
    }


}
