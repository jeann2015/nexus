<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Preference\Dao;

/**
 * Description of PreferenceDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;

use Generic\Dao\GenericDao;

class PreferenceDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }
    
    public function fetchByCategory($category)
    {
        $resultSet = $this->tableGateway->select(array(
            'category'=>$category
        ));

        return $resultSet;
    }
    
    public function getByName($name)
    {
        $rowset = $this->tableGateway->select(array('name' => $name));
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row);
    }
    
}
