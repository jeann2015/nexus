<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Preference\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Preference\Model\Preference;
//use Preference\Form\PreferenceForm;
use Generic\Controller\GenericController;

class PreferenceController extends GenericController
{ 
    public $preferenceDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        
        return new ViewModel();
    }
    
    public function globalAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $preferences = $this->getDao()->fetchByCategory('global');
        
        return new ViewModel(array('preferences' => $preferences));
    }
    
    public function componentAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $preferences = $this->getDao()->fetchByCategory('component');
        return new ViewModel(array('preferences' => $preferences));
    }
    
    public function provisioningAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $preferences = $this->getDao()->fetchByCategory('provisioning');
        return new ViewModel(array('preferences' => $preferences));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $preference = $this->getDao()->get($id);
        
        $form = new PreferenceForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $preference = new Preference();
            $form->setInputFilter($preference->getInputFilter());

            $form->setData($request->getPost());
            if ($form->isValid()) {
                $preference->exchangeArray($form->getData());
                  
                $this->getDao()->save($preference);

                return $this->redirect()->toRoute('preference');
            }

        } else {
            $form->setData($preference->getArrayCopy());
        }

        return new ViewModel(array(
            'form' => $form,
            'preference'=> $preference,
        ));
    }
    
    public function updateAction()
    {
        $model = new JsonModel();
        
        try {

            $request = $this->getRequest();
            $post = $request->getPost();
            
            $preference = $this->getDao()->get($post['id']);
            $preference->value = $post['value'];
            $this->getDao()->save($preference);
            
            $model->setVariable('status', "success");
            $model->setVariable('payload', $post['id']);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }
        
        return $model;
    }
    
    public function getDao()
    {
        if (!$this->preferenceDao) {
            $sm = $this->getServiceLocator();
            $this->preferenceDao = $sm->get('Preference\Dao\PreferenceDao');
        }
        
        return $this->preferenceDao;
    }

}
