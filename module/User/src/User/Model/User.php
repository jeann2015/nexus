<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Model;

use User\InputFilter\UserInputFilter;
/**
 * Description of User
 *
 * @author rodolfo
 */
class User extends UserInputFilter {
    
    public $groupId;
    public $roleId;
    public $name;
    public $phone;
    public $mobile;
    public $username;
    public $password;
    public $email;
    public $changePassword;
    public $active;
    public $expire;
    public $expirePassword;
    public $createdDate;

    public function getGroupId() {
        return $this->groupId;
    }

    public function getRoleId() {
        return $this->roleId;
    }

    public function getName() {
        return $this->name;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function getMobile() {
        return $this->mobile;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setGroupId($groupId) {
        $this->groupId = $groupId;
    }

    public function setRoleId($roleId) {
        $this->roleId = $roleId;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function setMobile($mobile) {
        $this->mobile = $mobile;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function setPassword($password) {
        $this->password = $password;
    }
    
    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    
    public function getChangePassword() {
        return $this->changePassword;
    }

    public function setChangePassword($changePassword) {
        $this->changePassword = $changePassword;
    }
    
    public function getActive() {
        return $this->active;
    }

    public function getExpire() {
        return $this->expire;
    }

    public function getExpirePassword() {
        return $this->expirePassword;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function setExpire($expire) {
        $this->expire = $expire;
    }

    public function setExpirePassword($expirePassword) {
        $this->expirePassword = $expirePassword;
    }

    public function getCreatedDate() {
        return $this->createdDate;
    }

    public function setCreatedDate($createdDate) {
        $this->createdDate = $createdDate;
    }


}
