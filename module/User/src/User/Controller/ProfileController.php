<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Zend\View\Model\ViewModel;
 
use User\Model\User;
use User\Form\PasswordForm;
use User\InputFilter\PasswordInputFilter;
use User\InputFilter\PasswordResetInputFilter;

use Generic\Controller\GenericController;

class ProfileController extends GenericController
{ 
    public $userDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $logged = $this->getServiceLocator()
                 ->get('AuthService')->getIdentity();
        $id = $logged['id'];
        //$id = $this->params()->fromRoute('id');
        $user = $this->getDao()->get($id);

        return new ViewModel(array('user' => $user));
    }
    
    public function getDao()
    {
        if (!$this->userDao) {
            $sm = $this->getServiceLocator();
            $this->userDao = $sm->get('User\Dao\UserDao');
        }
        
        return $this->userDao;
    }
    
    public function passwordAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $logged = $this->getServiceLocator()
                 ->get('AuthService')->getIdentity();
        
        $id = $logged['id'];
        $errors = array();
        $error_message = "";
        $form = new PasswordForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
            
            $current    = $request->getPost('current');
            $new        = $request->getPost('password');
            $confirm    = $request->getPost('confirm');
            
            //$user = new User();
            $passwordInputFilter = new PasswordInputFilter();
            $form->setInputFilter($passwordInputFilter->getInputFilter());
            //$user->unsetInputFilter();

            if ($form->isValid()) {
                
                if($new !== $confirm){
                    $form->setMessages(array(
                        "password" => array("Contraseña nueva no coincide!"),
                        "confirm"  => array("Contraseña confirmada no coincide!")
                    ));
                    $error_message = "Contraseña nueva y confirmación no coinciden!";
                    $form->setValid(false);
                } else {
                
                    $user = $this->getDao()->get($id);
                    $password = $user->getPassword();

                    if($password !== sha1($current)){
                        $form->setMessages(array("current" => array("Contraseña actual no es correcta!")));
                        $error_message = "Contraseña actual no es correcta!";
                        $form->setValid(false);
                    } else {
                        
                        $userToUpdate = new User();
                        $userToUpdate->exchangeArray($user->getArrayCopy());
                        $userToUpdate->setPassword( sha1($new) );
                        
                        $preferenceDao = $this->getServiceLocator()->get('Preference\Dao\PreferenceDao');
                        $preference = $preferenceDao->getByName('expire_password');
                        $expire_password = $preference->value;
                        $userToUpdate->setExpirePassword(date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + ".$expire_password." day")));
                        
                        $userToUpdate->remove('role');
                        $userToUpdate->remove('group');
                        
                        $this->getDao()->save($userToUpdate);
                        return $this->redirect()->toRoute('profile');
                    }
                }
            } else {
                $error_message = "Estos datos son requeridos!";
            }
            
            $messages = $form->getMessages();
            foreach ($messages as $element => $message){
                array_push($errors, $element);
            }
        }

        return new ViewModel(array(
            'id'            =>  $id,
            'errors'        =>  $errors,
            'error_message' =>  $error_message,
            'form'          => $form,
        ));
    }
    
    public function resetAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $this->layout('layout/navbar-less.phtml');
        
        $logged = $this->getServiceLocator()
                 ->get('AuthService')->getIdentity();
        
        $id = $logged['id'];
        $errors = array();
        $error_message = "";
        $form = new PasswordForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $form->setData($request->getPost());
            
            $new        = $request->getPost('password');
            $confirm    = $request->getPost('confirm');
            
            //$user = new User();
            $passwordInputFilter = new PasswordResetInputFilter();
            $form->setInputFilter($passwordInputFilter->getInputFilter());
            //$user->unsetInputFilter();

            if ($form->isValid()) {
                
                if($new !== $confirm){
                    $form->setMessages(array(
                        "password" => array("Contraseña nueva no coincide!"),
                        "confirm"  => array("Contraseña confirmada no coincide!")
                    ));
                    $error_message = "Contraseña nueva y confirmación no coinciden!";
                    $form->setValid(false);
                } else {
                
                    $user = $this->getDao()->get($id);

                    $userToUpdate = new User();
                    $userToUpdate->exchangeArray($user->getArrayCopy());
                    $userToUpdate->setPassword( sha1($new) );
                    $userToUpdate->setChangePassword(0);
                    
                    $preferenceDao = $this->getServiceLocator()->get('Preference\Dao\PreferenceDao');
                    $preference = $preferenceDao->getByName('expire_password');
                    $expire_password = $preference->value;
                    $userToUpdate->setExpirePassword(date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + ".$expire_password." day")));
                        
                    $userToUpdate->remove('role');
                    $userToUpdate->remove('group');

                    $this->getDao()->save($userToUpdate);
                    return $this->redirect()->toRoute('home');

                }
            } else {
                $error_message = "Estos datos son requeridos!";
            }
            
            $messages = $form->getMessages();
            foreach ($messages as $element => $message){
                array_push($errors, $element);
            }
        }

        return new ViewModel(array(
            'id'            =>  $id,
            'errors'        =>  $errors,
            'error_message' =>  $error_message,
            'form'          => $form,
        ));
    }

}
