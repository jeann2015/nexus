<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
 
use User\Model\User;
use User\Form\UserForm;

use Generic\Controller\GenericController;
use Role\Dao\RoleDao;
use Group\Dao\GroupDao;
use Preference\Dao\PreferenceDao;

class UserController extends GenericController
{ 
    public $userDao;
    
    public function indexAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }

        return new ViewModel();
    }
    
    public function listAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $filter     = false;
        $paginate   = true;
        
        $page = $this->params()->fromRoute('page') ? (int) $this->params()->fromRoute('page') : 1;
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $filter = $request->getPost();
        }

        // fetchAll(true) return paginator
        // fetchAll() return resultset
        $paginator = $this->getDao()->fetchAll($paginate,$filter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        $paginator->setPageRange(7);
         
        return new ViewModel(array(
            'paginator'     => $paginator,
            'currentUser'   => (object)$this->getServiceLocator()
                 ->get('AuthService')->getIdentity(),
        ));
    }
      
    public function addAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $preferenceDao = $this->getServiceLocator()->get('Preference\Dao\PreferenceDao');
        
        $preference = $preferenceDao->getByName('expire_password');
        $expire_password = $preference->value;
        
        $preference     = $preferenceDao->getByName('expire_user');
        $expire_user = $preference->value;
        
        $roleTableGateway = $this->getServiceLocator()->get('RoleTableGateway');
        $roleDao = new RoleDao($roleTableGateway);
        $roleFetch = $roleDao->fetchAll();
        $roleArray = array();
        
        foreach ($roleFetch as $role) {
            $roleArray[$role->id] = $role->name;
        }
        
        $groupTableGateway = $this->getServiceLocator()->get('GroupTableGateway');
        $groupDao = new GroupDao($groupTableGateway);
        $groupFetch = $groupDao->fetchAll();
        $groupArray = array();
        
        foreach ($groupFetch as $group) {
            $groupArray[$group->id] = $group->name;
        }
        
        $form = new UserForm();
        $form->get('roleId')->setValueOptions($roleArray);
        $form->get('groupId')->setValueOptions($groupArray);
        $errors = array();
        $error_message = "";
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $user = new User();
            
            $password = $request->getPost('password');
            $confirm  = $request->getPost('confirm');

            if(!empty($password) || !empty($confirm))
            {
                if($confirm != $password){
                    array_push($errors, "password");
                    array_push($errors, "confirm");
                    $error_message = "Contraseñas no coinciden!";
                }
            }
            
            $form->setInputFilter($user->getInputFilter());

            $post = $request->getPost();
            $changePassword = $request->getPost('changePassword');
            if(!isset($changePassword)){
                $post['changePassword'] = 0;
            }
            $active = $request->getPost('active');
            if(!isset($active)){
                $post['active'] = 0;
            }

            $form->setData($post);
            if ($form->isValid() && count($errors) == 0) {
                $user->exchangeArray($form->getData());
                $user->setPassword( sha1($user->getPassword()) );
                $user->setChangePassword($request->getPost('changePassword')?1:0);
                $user->setActive($request->getPost('active')?1:0);
                $user->setCreatedDate(\date('Y-m-d H:i:s'));
                try {
                    $this->getDao()->save($user);   
                    return $this->redirect()->toRoute('user',array('action'=>'list'));
                } catch (\Exception $e){

                    $exploded = explode("'", $e->getMessage());
                    $exploded2 = explode("_",$exploded[3]);
                    $element = $exploded2[0];
                    $form->setMessages(array($element => array($e->getMessage())));
                    $error_message = $e->getMessage();
                }

            }
            
            $messages = $form->getMessages();
            foreach ($messages as $element => $message){
                array_push($errors, $element);
            }
        }

        return new ViewModel(array(
            'roles'             => $roleArray,
            'groups'            => $groupArray,
            'form'              => $form,
            'errors'            => $errors,
            'error_message'     => $error_message,
            'expire_password'   => $expire_password,
            'expire_user'       => $expire_user,
        ));
    }
    
    public function editAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }        
        
        $id = $this->params()->fromRoute('id');
        $user = $this->getDao()->get($id);
        
        $roleTableGateway = $this->getServiceLocator()->get('RoleTableGateway');
        $roleDao = new RoleDao($roleTableGateway);
        $roleFetch = $roleDao->fetchAll();
        $roleArray = array();
        
        foreach ($roleFetch as $role) {
            $roleArray[$role->id] = $role->name;
        }
        
        $groupTableGateway = $this->getServiceLocator()->get('NxGroupTableGateway');
        $groupDao = new GroupDao($groupTableGateway);
        $groupFetch = $groupDao->fetchAll();
        $groupArray = array();
        
        foreach ($groupFetch as $group) {
            $groupArray[$group->id] = $group->name;
        }
        
        $form = new UserForm();
        $form->get('roleId')->setValueOptions($roleArray);
        $form->get('groupId')->setValueOptions($groupArray);
        $errors = array();
        $error_message = "";
        $request = $this->getRequest();
        if ($request->isPost()) {
                        
            $newuser = new User();
            $password = $request->getPost('password');
            $confirm  = $request->getPost('confirm');
            
            if(!empty($password) || !empty($confirm)){
                if($confirm != $password){
                    array_push($errors, "password");
                    array_push($errors, "confirm");
                }
            }
            
            if(count($errors) == 0){
                $newuser->getInputFilter()->get('password')->setRequired(false);
            }
            
            $form->setInputFilter($newuser->getInputFilter());
            
            $post = $request->getPost();
	        $changePassword = $request->getPost('changePassword');
            if(empty($changePassword)){
                $post['changePassword'] = 0;
            }
            $active = $request->getPost('active');
            if(!isset($active)){
                $post['active'] = 0;
            }

            $form->setData($post);
            if ($form->isValid() && count($errors) == 0) {
                $newuser->exchangeArray($form->getData());
                if($newuser->getPassword() != ""){
                    $newuser->setPassword( sha1($newuser->getPassword()) );
                } else {
                    $newuser->setPassword( $user->password );
                }
                $newuser->setChangePassword($request->getPost('changePassword')?1:0);
                $newuser->setActive($request->getPost('active')?1:0);
                $newuser->setCreatedDate($user->createdDate);
                try {
                    $this->getDao()->save($newuser);   
                    return $this->redirect()->toRoute('user',array('action'=>'list'));
                } catch (\Exception $e){

                    $exploded = explode("'", $e->getMessage());
                    $exploded2 = explode("_",$exploded[3]);
                    $element = $exploded2[0];
                    $form->setMessages(array($element => array($e->getMessage())));
                    $error_message = "Campo duplicado en la base de datos!";
                }
            }
            
            $messages = $form->getMessages();

            foreach ($messages as $element => $message){
                array_push($errors, $element);
            }
        } else {
            $form->setData($user->getArrayCopy());
        }

        return new ViewModel(array(
            'roles'         => $roleArray,
            'groups'        => $groupArray,
            'form'          => $form,
            'errors'        => $errors,
            'error_message' => $error_message,
            'id'            => $id,
            'user'          => $user,
        ));
    }
    
    public function detailAction()
    {
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }        
        
        $id = $this->params()->fromRoute('id');
        $user = $this->getDao()->get($id);
        
        $roleTableGateway = $this->getServiceLocator()->get('RoleTableGateway');
        $roleDao = new RoleDao($roleTableGateway);
        $roleFetch = $roleDao->fetchAll();
        $roleArray = array();
        
        foreach ($roleFetch as $role) {
            $roleArray[$role->id] = $role->name;
        }
        
        $groupTableGateway = $this->getServiceLocator()->get('GroupTableGateway');
        $groupDao = new GroupDao($groupTableGateway);
        $groupFetch = $groupDao->fetchAll();
        $groupArray = array();
        
        foreach ($groupFetch as $group) {
            $groupArray[$group->id] = $group->name;
        }
        
        $form = new UserForm();
        $form->get('roleId')->setValueOptions($roleArray);
        $form->get('groupId')->setValueOptions($groupArray);
        
        $form->setData($user->getArrayCopy());

        return new ViewModel(array(
            'roles'     => $roleArray,
            'groups'    => $groupArray,
            'form'      => $form,
            'id'        => $id,
        ));
    }
    
    public function getDao()
    {
        if (!$this->userDao) {
            $sm = $this->getServiceLocator();
            $this->userDao = $sm->get('User\Dao\UserDao');
        }
        
        return $this->userDao;
    }
    
    public function deleteAction() 
    {
        $model = new JsonModel();
        
        try {
            $id = $this->params()->fromRoute('id');
            $this->getDao()->delete($id);
            $model->setVariable('status', "success");
            $model->setVariable('payload', $id);
        } catch (\Exception $ex) {
            $model->setVariable('status', 'error');
            $model->setVariable('payload', $ex->getMessage());
        }

        return $model;
    }

}
