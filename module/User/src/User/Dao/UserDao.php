<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace User\Dao;

/**
 * Description of UserDao
 *
 * @author rodolfo
 */

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

use Zend\Paginator\Adapter\DbTableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

use Generic\Dao\GenericDao;

class UserDao extends GenericDao
{
    
    public function __construct(TableGateway $tableGateway = null)
    {
        if (!$tableGateway) {
            $sm = $this->getServiceLocator();
            $tableGateway = $sm->get('UserTableGateway');
        }
        parent::__construct($tableGateway);
        //$this->tableGateway = $tableGateway;
    }
    
    public function fetchAll($paginated = false, $filter = false)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_role', 'nx_user.role_id = nx_role.id',array('role'=>'name'));
        $select->join('nx_group','nx_user.group_id = nx_group.id',array('group'=>'name'));
        
        if($filter) {
            $where = new Where();
            foreach ($filter as $left => $right) {
                $where->equalTo("nx_user.".$left, $right);
            }
            $select->where($where);
        }
        
        if ($paginated) {
            $dbTableGatewayAdapter = new DbSelect($select,$sql);
            //$dbTableGatewayAdapter = new DbTableGateway($this->tableGateway);
            $paginator = new Paginator($dbTableGatewayAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->selectWith($select);

        return $resultSet;
    }
    
    public function get($id)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        $select->join('nx_role', 'nx_user.role_id = nx_role.id',array('role'=>'name'));
        $select->join('nx_group','nx_user.group_id = nx_group.id',array('group'=>'name'));
        
        $where = new Where();
        $where->equalTo("nx_user.id", $id );
        
        $select->where($where);

        $rowset = $this->tableGateway->selectWith($select);
        
        //$rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row);
    }
    
}
