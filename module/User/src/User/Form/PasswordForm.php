<?php

namespace User\Form;

use Generic\Form\GenericForm;

class PasswordForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
        $this->add(array(
            'name' => 'current',
            'attributes' => array(
                'type'  => 'password',
                'id'    => 'current',
                'required' => true,
            ),
        ));
        
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'password',
                'id'    => 'password',
                'required' => true,
            ),
        ));

        $this->add(array(
            'name' => 'confirm',
            'attributes' => array(
                'type'  => 'password',
                'id'    => 'confirm',
                'required' => true,
            ),
        ));
    }
    
    public function setValid($valid) {
        $this->isValid = $valid; 
    }
}
