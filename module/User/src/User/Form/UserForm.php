<?php

namespace User\Form;

use Generic\Form\GenericForm;

class UserForm extends GenericForm
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id'    => 'id'
            ),
        ));
                
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'username',
                'required' => true,
            ),
        ));
        
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type'  => 'password',
                'id'    => 'password'
            ),
        ));

        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'email',
                'required' => true,
            ),
        ));
        
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'name',
                'required' => true,
            ),
        ));
        
        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'phone'
            ),
        ));
        
        $this->add(array(
            'name' => 'mobile',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'mobile'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'roleId',
            'attributes' => array(
                'id'    => 'roleId',
                'required' => true,
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name'  => 'groupId',
            'attributes' => array(
                'id'    => 'groupId',
                'required' => true,
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name'  => 'changePassword',
            'attributes' => array(
                'id'    => 'changePassword',
                'required' => false,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name'  => 'active',
            'attributes' => array(
                'id'    => 'active',
                'required' => false,
                'checked_value' => 1,
                'unchecked_value' => 0
            ),
        ));
        
        $this->add(array(
            'name' => 'expire',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'expire'
            ),
        ));
        
        $this->add(array(
            'name' => 'expirePassword',
            'attributes' => array(
                'type'  => 'text',
                'id'    => 'expirePassword'
            ),
        ));
    }
}
