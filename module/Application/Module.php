<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Authentication\Storage;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

use Application\Service\ErrorHandling;
use Zend\Log\Logger;
use Zend\Log\Writer\Stream as LogWriterStream;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        
        $identity = $e->getApplication()->getServiceManager()->get('auth_service')->getIdentity();
        $e->getViewModel()->setVariable('user',(object)$identity);
        
        $eventManager->attach('dispatch.error', function($event){
            $exception = $event->getResult()->exception;
            if ($exception) {
                $sm = $event->getApplication()->getServiceManager();
                $service = $sm->get('ApplicationServiceErrorHandling');
                $service->logException($exception);
            }
        });
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
    
    public function getServiceConfig()
    {
        return array(
            'factories'=>array(
                'Authentication\Model\AuthStorage' => function($sm){
                    return new \Authentication\Model\AuthStorage('nexus'); 
                },

                'AuthService' => function($sm) {
                    //My assumption, you've alredy set dbAdapter
                    //and has users table with columns : username and password
                    //that password hashed with md5
                    $dbAdapter           = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter  = new DbTableAuthAdapter($dbAdapter,
                                    'user',array('username','email'),'password', 'SHA1(?)');

                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('Authentication\Model\AuthStorage'));

                    return $authService;
                },
                'Application\Service\ErrorHandling' =>  function($sm) {
                    $logger = $sm->get('Zend\Log');
                    $service = new ErrorHandling($logger);
                    return $service;
                },
                'Zend\Log' => function ($sm) {
                    $preference = $sm->get('Preference\Dao\PreferenceDao');
                    
                    $log_format = $preference->getByName('log_format')->value;
                    $log_path   = $preference->getByName('log_path')->value;
                    $log_path   = dirname(__DIR__) . "/../" . $log_path;
                    
                    $log_format = str_replace("%Y", \date('Y'), $log_format);
                    $log_format = str_replace("%y", \date('y'), $log_format);
                    
                    $log_format = str_replace("%m", \date('m'), $log_format);
                    $log_format = str_replace("%M", \date('M'), $log_format);
                    
                    $log_format = str_replace("%d", \date('d'), $log_format);
                    $log_format = str_replace("%D", \date('D'), $log_format);
                    
                    $log = new Logger();
                    $writer = new LogWriterStream($log_path . $log_format);
                    $log->addWriter($writer);

                    return $log;
                },
            ),
        );
    }
}
