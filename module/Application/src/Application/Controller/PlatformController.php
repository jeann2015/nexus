<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class PlatformController extends AbstractActionController
{   
    public function indexAction()
    {        
        $this->layout('layout/navbar-less.phtml');
        return new ViewModel();
    }
    
    public function comercialAction()
    {        
        $_SESSION['platform'] = "comercial";
        return $this->redirect()->toRoute('home');
    }
    
    public function rnmsAction()
    {        
        $_SESSION['platform'] = "rnms";
        return $this->redirect()->toRoute('home');
    }
}
