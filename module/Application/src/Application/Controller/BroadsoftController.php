<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Provisioning\Dao\PlatformDao;

class BroadsoftController extends AbstractActionController
{   
    public $dao;

    public function indexAction()
    {        
        $this->layout('layout/navbar-less.phtml');
        $platforms = $this->getDao()->fetchAll();
        return new ViewModel(array(
            'platforms' => $platforms,
        ));
    }
    
    public function comercialAction()
    {        
        $_SESSION['platform'] = "comercial";
        return $this->redirect()->toRoute('home');
    }
    
    public function rnmsAction()
    {        
        $_SESSION['platform'] = "rnms";
        return $this->redirect()->toRoute('home');
    }

    public function chooseAction()
    {        
        $id = $this->params()->fromRoute('id');
        $_SESSION['platform_id'] = $id;

        $platform = $this->getDao()->get($id);
        $_SESSION['platform']   = $platform->name;
        
        return $this->redirect()->toRoute('home');
    }

    public function getDao($dao = "PlatformDao",$namespace = "Provisioning")
    {
        //$serviceDao = lcfirst($dao);
        //if (!$this->$serviceDao) {
            $sm = $this->getServiceLocator();
            $this->serviceDao = $sm->get($namespace.'\Dao\\'.$dao);
        //}
        
        return $this->serviceDao;
    }
}
