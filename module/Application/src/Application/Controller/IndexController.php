<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
                
        if($_SESSION['platform'] === ""){
            return $this->redirect()->toRoute('broadsoft');
        }
        
        if (! $this->getServiceLocator()
                 ->get('AuthService')->hasIdentity()){
            return $this->redirect()->toRoute('auth');
        }
        
        $_SESSION['current'] = "home";
        
        return new ViewModel(array(
            'currentUser'   => (object)$this->getServiceLocator()
                 ->get('AuthService')->getIdentity(),
        ));
    }

    public function availableAction($host, $port=80, $timeout=10) { 
        $fp = fSockOpen($host, $port, $errno, $errstr, $timeout); 
        return $fp!=false;
    }

}
