<?php
namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;

class ConvertToUTC extends AbstractHelper
{
    /**
     * Convert time from current time zone to utc and mysql format
     * @param string $date_time
     * @param array $options
     * @return string
     */
    public function __invoke($date_time,$options = null) {
        
        if(!is_array($options)){

            $date = new \DateTime($date_time, new \DateTimeZone('America/Panama'));
            $date->setTimezone(new \DateTimeZone('UTC'));
            $date_time = $date->format('Y-m-d H:i:s');
            return $date_time;

        } else {

            if($options['date']){
                $date = new \DateTime($date_time);
                $date_time = $date->format('Y-m-d');
                return $date_time;
            } elseif($options['time']){
                $date = null;
                if(isset($options['timezone'])){
                    $date = new \DateTime($date_time, new \DateTimeZone($options['timezone']));
                } else {
                    $date = new \DateTime($date_time, new \DateTimeZone('America/Panama'));
                }

                $date->setTimezone(new \DateTimeZone('UTC'));
                $date_time = $date->format('H:i:s');
                return $date_time;
            } else {
                $date = null;
                if($options['timezone']){
                    $date = new \DateTime($date_time, new \DateTimeZone($options['timezone']));
                } else {
                    $date = new \DateTime($date_time, new \DateTimeZone('America/Panama'));
                }
                $date->setTimezone(new \DateTimeZone('UTC'));
                $date_time = $date->format('Y-m-d H:i:s');
                return $date_time;
            }
        }

        return $date_time;        
    }
  
}