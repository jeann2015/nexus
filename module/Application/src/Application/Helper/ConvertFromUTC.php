<?php
namespace Application\Helper;

use Zend\View\Helper\AbstractHelper;

class ConvertFromUTC extends AbstractHelper
{
    /**
     * Convert time from utc to current time zone and 'd-m-Y h:i:s a' format
     * @param string $date_time
     * @param array $options
     * @return string
     */
    public function __invoke($date_time,$options = null) {
        
        if(!is_array($options)){

            $date = new \DateTime($date_time, new \DateTimeZone('UTC'));
            $date->setTimezone(new \DateTimeZone('America/Panama'));
            $date_time = $date->format('d-m-Y h:i:s a');
            return $date_time;

        } else {

            if($options['date']){
                $date = new \DateTime($date_time);
                $date_time = $date->format('d-m-Y');
                return $date_time;
            } elseif($options['time']){
                $date = new \DateTime($date_time, new \DateTimeZone('UTC'));
                if(isset($options['timezone'])){
                    $date->setTimezone(new \DateTimeZone($options['timezone']));
                } else {
                    $date->setTimezone(new \DateTimeZone('America/Panama'));
                }

                $date_time = $date->format('h:i:s a');
                return $date_time;
            } else {
                $date = new \DateTime($date_time, new \DateTimeZone('UTC'));
                if($options['timezone']){
                    $date->setTimezone(new \DateTimeZone($options['timezone']));
                } else {
                    $date->setTimezone(new \DateTimeZone('America/Panama'));
                }
                $date_time = $date->format('d-m-Y h:i:s a');
                return $date_time;
            }
        }

        return $date_time;
    }
}