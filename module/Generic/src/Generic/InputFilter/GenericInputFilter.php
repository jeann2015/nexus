<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Generic\InputFilter;

/**
 * Description of GenericInputFilter
 *
 * @author rodolfo
 */

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class GenericInputFilter implements InputFilterAwareInterface
{
       
    protected $inputFilter;

    public function exchangeArray($data)
    {
        foreach ($data as $key => $value) {
            //if( property_exists($this,$key) ){
                if(empty($value)){
                    $this->$key = null;
                } else {
                    $this->$key = $value;
                }
            //}
        }
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
    
    public function unsetInputFilter() {
        unset($this->inputFilter);
    }

    public function getArray()
    {
        $vars = get_object_vars($this);
        unset($vars['*inputFilter']);
        unset($vars['inputFilter']);
        return $vars;
    }

    public function getArrayVarsId()
    {
        $vars = get_object_vars($this);
        unset($vars['*inputFilter']);
        unset($vars['inputFilter']);
        $varsId = array();
        foreach ($vars as $key => $value) {
            # code...
        }
        return $vars;
    }
}

