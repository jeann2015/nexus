<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Generic\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
 
use Generic\Model\Generic;
use Generic\Form\GenericForm;

class GenericController extends AbstractActionController
{ 
    public $genericDao;
    
    public function indexAction()
    {
        return new ViewModel();
    }
      
    public function addAction()
    {
        return new ViewModel();
    }
    
    public function editAction()
    {
        return new ViewModel();
    }
    
    public function detailAction()
    {
        return new ViewModel();
    }
    
    public function deleteAction()
    {
        return new ViewModel();
    }
    
    public function log($message, $level = 'info'){
        $this->getServiceLocator()->get('Zend\Log')->$level($message);
    }

    public function isDate($string){
        
        if (strpos($string, '-') !== false){
            $dtInfo = date_parse($string);
            if($dtInfo['warning_count'] == 0 && $dtInfo['error_count'] == 0 ){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function convertFromUTC($date,$options = null){
        $convertFromUTC = $this->getServiceLocator()->get('viewhelpermanager')->get('convertFromUTC');
        return $convertFromUTC($date,$options);
    }

    public function convertToUTC($date,$options = null){
        $convertToUTC = $this->getServiceLocator()->get('viewhelpermanager')->get('convertToUTC');
        return $convertToUTC($date,$options);
    }

}
