<?php

namespace Generic\Form;

use Zend\Form\Form;

class GenericForm extends Form
{
    public function __construct($name = null)
    {
        // we want to ignore the name passed
        parent::__construct($name);

    }
    
    public function setValid($boolean) {
        $this->isValid = $boolean;
    }
}
