<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Generic\Dao;

/**
 * Description of GenericDao
 *
 * @author rodolfo
 */
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\TableGateway\TableGateway;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;

class GenericDao implements ServiceLocatorAwareInterface
{

    protected $tableGateway;
    protected $serviceLocator;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;

    }
    
    public function setServiceLocator(ServiceLocatorInterface $sl)
    {
        $this->serviceLocator = $sl;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function get($id)
    {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return null;
        }
        return $this->mapperToObject($row);
    }

    public function save($object)
    {
        $id = $object->id;
        $data = $this->mapperToArray($object);
        if (empty($id)) {
            $this->tableGateway->insert($data);
            return $this->tableGateway->getLastInsertValue();
        } else {
            $this->tableGateway->update($data, array('id' => $id));
            return $id;
        }
    }

    public function delete($id)
    {
        $this->tableGateway->delete(array('id' => $id));
    }
    
    /**
     * @todo Convierte los atributos de un objeto a atributos de base de  datos 
     * reemplazando las mayusculas por _ y minuscula.
     */
    protected function mapperToArray($object) 
    {
        unset($object->id);
        $object->unsetInputFilter();
        $data = (array)$object;

        foreach ($data as $key => $value) {
            
            $n = preg_match( '/[A-Z]/', $key, $matches, PREG_OFFSET_CAPTURE );
            if($n){
                unset($data[$key]);
                $temporalArray = explode($matches[0][0], $key);
                $key = implode("_" . strtolower($matches[0][0]), $temporalArray);
                $data[$key] = $value;

            } else {
                continue;
            }
        }

        return $data;
        
    }
    
    /**
     * @todo Convierte los atributos de un array de base de  datos a objeto 
     * reemplazando la _ por una mayuscula.
     */
    protected function mapperToObject($array) 
    {
        $data = $array;
        foreach ($data as $key => $value) {
            $n = preg_match( '/_/', $key, $matches, PREG_OFFSET_CAPTURE );
            if($n){
                unset($data->$key);
                $temporalArray = explode("_", $key);
                $key = $temporalArray[0] . ucfirst($temporalArray[1]);
                $data->$key = $value;
            } else {
                continue;
            }
        }
        
        return $data;
        
    }

    public function getByName($name)
    {
        $sql = new Sql( $this->tableGateway->adapter ) ;
        $select = $sql->select() ;
        $select->from( $this->tableGateway->getTable() );
        
        $where = new Where();
        $where->equalTo("name",$name);

        $select->where($where);
        $select->limit(1);

        $resultSet = $this->tableGateway->selectWith($select);
        
        return $resultSet->current();
    }
}
