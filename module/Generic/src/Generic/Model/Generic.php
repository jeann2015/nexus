<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Generic\Model;

use Generic\InputFilter\GenericInputFilter;
/**
 * Description of Generic
 *
 * @author rodolfo
 */
class Generic extends GenericInputFilter {
    
    public $id;
    
    
    public function remove($attribute) {
        unset($this->$attribute);
    }
   
    /**
     * Gets the value of id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Sets the value of id.
     *
     * @param mixed $id the id 
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
