@echo off
setlocal
if not defined JAVA_HOME goto endjavahome
set JAVA_PATH=%JAVA_HOME%\bin

set AXIS_LIB=lib
set AXISCLASSPATH=%AXIS_LIB%\axis.jar;%AXIS_LIB%\commons-discovery.jar-0.2;%AXIS_LIB%\commons-logging-1.0.4.jar;%AXIS_LIB%\jaxrpc.jar;%AXIS_LIB%\saaj.jar;%AXIS_LIB%\log4j-1.2.8.jar;%AXIS_LIB%\xml-apis.jar;%AXIS_LIB%\xercesImpl.jar;%AXIS_LIB%\commons-discovery-0.2.jar;

set CLASSPATH=classes;%AXISCLASSPATH%;

echo ***************************************************
%JAVA_PATH%\java -version
echo ***************************************************

rem ***************************************************************
rem We need place for 10 arguments.  But only 9 available (%1 - %9)
rem SHIFT, so we can use %0 which is being used for the classname
rem ***************************************************************
SHIFT

@echo on
%JAVA_PATH%\java com.broadsoft.clients.oss.OSSClient %0 %1 %2 %3 %4 %5 %6 %7 %8 %9
@echo off
goto end

:endjavahome
echo "JAVA_HOME not defined"
goto end

:end
