#!/bin/bash
LIB=lib
THE_CLASSPATH=classes
#---------------------------------#
# dynamically build the classpath #
#---------------------------------#
for i in `ls ./$LIB/*.jar`
do
  THE_CLASSPATH=${THE_CLASSPATH}:${i}
done
#echo ***************************************************
#java -version
#echo ***************************************************
java -classpath ".:${THE_CLASSPATH}" com.broadsoft.clients.oci.OCIClient $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10}
