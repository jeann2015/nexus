package com.broadsoft.clients.oci;

import com.broadsoft.clients.oam.BWMessageDigest;

import java.io.*;

public class BWXMLGenerator
{
	private static final String currentFileName = "inputclient.xml";
	private static final String logName = "outputclient_";

	private FileOutputStream out;
	private FileOutputStream log;

	private String filename;
	private String outputFileName;	//xml file ... path as well
	private String logFileName;
	private File outputFile;
	private File logFile;
        private long fileId = System.currentTimeMillis();

	BWXMLGenerator()
	{
		filename = "inputclient_" + fileId + ".xml";
	}
	BWXMLGenerator(String filename_in)
	{
		filename = filename_in;
	}
	public void cleanup()
	{
		try
		{
			out.close();
			log.close();
		}
		catch(IOException e)
		{
		}
	}
        public void reinit() throws Exception
        {
          out.close();
        }

	public void init()
	{
		//open an output stream on the xml file
       		outputFileName = System.getProperty("user.home",  File.separatorChar + "home" +
       							  File.separatorChar + "monicap")
       							+ File.separatorChar + filename;

         	outputFile = new File(outputFileName);
		try
		{
         		out = new FileOutputStream(outputFile);
		}
		catch(IOException e)
		{
		}

		//open an output stream on log file
       		logFileName = System.getProperty("user.home",  File.separatorChar + "home" +
						  File.separatorChar + "monicap")
						+ File.separatorChar + logName + fileId + ".xml";
         	logFile = new File(logFileName);
		try
		{
         		log = new FileOutputStream(logFile);
		}
		catch(IOException e)
		{
		}
	}

	public void init(String args[])
	{
		//open an output stream on the xml file
       		outputFileName = args[4] + File.separatorChar + filename;
         	outputFile = new File(outputFileName);
		try
		{
         		out = new FileOutputStream(outputFile);
		}
		catch(IOException e)
		{
		}

		//open an output stream on log file
       		logFileName = args[5] + File.separatorChar + logName + fileId + ".xml";
         	logFile = new File(logFileName);
		try
		{
         		log = new FileOutputStream(logFile);
		}
		catch(IOException e)
		{
		}
	}

	public void generate_xml_header()
	{
                BWMessageDigest messages = BWMessageDigest.getDefaultDigest();

		write_xml_file("<?xml version=\"1.0\" encoding=\"" + messages.getValue("BW_ENCODING") + "\"?>");
		write_xml_file("<BroadsoftDocument protocol = \"OSSP\"  version = \"11.0\" >");
	}

	public void generate_xml_footer()
	{
		write_xml_file("</BroadsoftDocument>");
	}
	String form_start_tag(String str)
	{
		String rv = new String();
		rv += BWDTD.TAGSTART;
		rv += str;
		rv += BWDTD.TAGEND;
		return rv;
	}
	String form_end_tag(String str)
	{
		String rv = new String();
		rv += BWDTD.TAGSTART;
		rv += BWDTD.ANCHORTAG;
		rv += str;
		rv += BWDTD.TAGEND;
		return rv;
	}

	public void write_xml_start_tag(String str)
	{
		write_xml_file(form_start_tag(str));
	}
	public void write_xml_end_tag(String str)
	{
		write_xml_file(form_end_tag(str));
	}
	public void write_xml_tag_n_attribute(String tag, String att_name, String att_value)
	{
		String element = new String();
		element += BWDTD.TAGSTART;
		element += tag;
		element += " ";
		element += att_name;
		element += " = \"";
		element += att_value;
		element += "\"";
		element += BWDTD.TAGEND;
		write_xml_file(element);
	}
	public void write_xml_element(String element_tag, String value)
	{
		String element = new String();
		element += form_start_tag(element_tag);
		element += value;
		element += form_end_tag(element_tag);
		write_xml_file(element);
	}
	public void write_xml_file(String str)
	{
		//str += "\n";
		byte b[] = str.getBytes();
		try
		{
			out.write(b);
		}
		catch(IOException e)
		{
		}
	}
	public void write_to_log(String text)
	{
		text += "\r";
		byte b[] = text.getBytes();
		try
		{
			log.write(b);
		}
		catch(IOException e)
		{
		}
	}
	public String get_xmlfile()
	{
		return outputFileName;
	}
}