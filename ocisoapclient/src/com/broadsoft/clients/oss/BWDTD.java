package com.broadsoft.clients.oss;
import java.io.*;
//DTD

public  class BWDTD
{
  public static final String COMMAND_NESTED = "command";
  public static final String COMMANDATA_NESTED = "commandData";
  public static final String SYSTEMPROVIDER_NESTED = "systemProvider";
  public static final String SERVICEPROVIDER_NESTED = "serviceProvider";
  public static final String GROUP_NESTED = "group";
  public static final String USER_NESTED = "user";
  public static final String LOGININFO_NESTED = "loginInfo";
  public static final String ERROR_NESTED = "error";
  public static final String ADMIN_NESTED = "admin";
  public static final String USERNAME_NESTED = "userName";
  public static final String USERNAMECLID_NESTED = "userNameCLID";
  public static final String USERNAMEHIRAGANA_NESTED = "userNameHiragana";
  public static final String DEPARTMENT_NESTED = "department";
  public static final String ACCESSDEVICE_NESTED = "accessDevice";
  public static final String DOMAIN_NESTED = "domain";
  public static final String LDAPDIRECTORY_NESTED = "ldapDirectory";
  public static final String GROUPLEVELPOLICY_NESTED = "groupLevelPolicy";
  public static final String GROUPADMINPOLICY_NESTED = "groupAdminPolicy";
  public static final String SVCPROVADMINPOLICY_NESTED = "svcProvAdminPolicy";
  public static final String CLIENTINFO_NESTED = "clientInfo";
  public static final String CLIENTDATA_PCDATA = "clientData";
  public static final String BROADSOFTDOCUMENT_NESTED = "BroadsoftDocument";

  //ATTLIST : command
  public static final String ATT_COMMANDTYPE = "commandType";

  public static final String ATT_COMMANDTYPE_REQUESTAUTHENTICATION = "requestAuthentication";
  public static final String ATT_COMMANDTYPE_REQUESTLOGIN = "requestLogin";
  public static final String ATT_COMMANDTYPE_REQUESTLOGOUT = "requestLogout";
  public static final String ATT_COMMANDTYPE_GETHIERARCHY = "getHierarchy";

  public static final String ATT_COMMANDTYPE_ADDSERVICEPROVIDER = "addServiceProvider";
  public static final String ATT_COMMANDTYPE_MODIFYSERVICEPROVIDER = "modifyServiceProvider";
  public static final String ATT_COMMANDTYPE_DELETESERVICEPROVIDER = "deleteServiceProvider";
  public static final String ATT_COMMANDTYPE_GETSERVICEPROVIDER = "getServiceProvider";

  public static final String ATT_COMMANDTYPE_ADDSERVICEPACK = "addServicePack";
  public static final String ATT_COMMANDTYPE_DELETESERVICEPACK = "deleteServicePack";
  public static final String ATT_COMMANDTYPE_GETSERVICEPACK = "getServicePack";
  public static final String ATT_COMMANDTYPE_MODIFYSERVICEPACK = "modifyServicePack";

  public static final String ATT_COMMANDTYPE_ADDGROUP = "addGroup";
  public static final String ATT_COMMANDTYPE_MODIFYGROUP = "modifyGroupProfile";
  public static final String ATT_COMMANDTYPE_MODIFYGROUPSERVICE = "modifyGroupService";
  public static final String ATT_COMMANDTYPE_DELETEGROUP = "deleteGroup";
  public static final String ATT_COMMANDTYPE_GETGROUP = "getGroup";
  public static final String ATT_COMMANDTYPE_GETGROUPPROFILE = "getGroupProfile";
  public static final String ATT_COMMANDTYPE_GETGROUPSERVICE = "getGroupService";
  public static final String ATT_COMMANDTYPE_GETUSER = "getUser";
  public static final String ATT_COMMANDTYPE_GETUSERPROFILE = "getUserProfile";
  public static final String ATT_COMMANDTYPE_GETUSERSERVICE = "getUserService";
  public static final String ATT_COMMANDTYPE_GETUSERCALLLOGS = "getUserCallLogs";
  public static final String ATT_COMMANDTYPE_GETUSERLIST = "getUserList";

  public static final String ATT_COMMANDTYPE_ASSIGNDIRECTORYNUMBER = "assignDirectoryNumber";
  public static final String ATT_COMMANDTYPE_UNASSIGNDIRECTORYNUMBER = "unassignDirectoryNumber";
  public static final String ATT_COMMANDTYPE_GETDIRECTORYNUMBER = "getDirectoryNumber";

  public static final String ATT_COMMANDTYPE_ADDACCESSDEVICE = "addAccessDevice";
  public static final String ATT_COMMANDTYPE_DELETEACCESSDEVICE = "deleteAccessDevice";
  public static final String ATT_COMMANDTYPE_GETACCESSDEVICE = "getAccessDevice";
  public static final String ATT_COMMANDTYPE_MODIFYACCESSDEVICE = "modifyAccessDevice";

  public static final String ATT_COMMANDTYPE_AUTHORIZESERVICE = "authorizeService";
  public static final String ATT_COMMANDTYPE_UNAUTHORIZESERVICE = "unauthorizeService";
  public static final String ATT_COMMANDTYPE_GETSERVICE = "getAuthorizeService";

  public static final String ATT_COMMANDTYPE_ASSIGNGROUPADMINISTRATOR = "assignGroupAdministrator";
  public static final String ATT_COMMANDTYPE_UNASSIGNGROUPADMINISTRATOR = "unassignGroupAdministrator";
  public static final String ATT_COMMANDTYPE_ASSIGNDEPTADMINISTRATOR = "assignDeptAdministrator";
  public static final String ATT_COMMANDTYPE_UNASSIGNDEPTADMINISTRATOR = "unassignDeptAdministrator";
  public static final String ATT_COMMANDTYPE_GETGROUPADMINISTRATOR = "getGroupAdministrator";
  public static final String ATT_COMMANDTYPE_GETDEPTADMINISTRATOR = "getDeptAdministrator";

  public static final String ATT_COMMANDTYPE_ASSIGNSERVICEPROVIDERADMINISTRATOR = "assignServiceProviderAdministrator";
  public static final String ATT_COMMANDTYPE_UNASSIGNSERVICEPROVIDERADMINISTRATOR = "unassignServiceProviderAdministrator";
  public static final String ATT_COMMANDTYPE_GETSERVICEPROVIDERADMINISTRATOR = "getServiceProviderAdministrator";
  public static final String ATT_COMMANDTYPE_ASSIGNADMINISTRATOR = "assignAdministrator";
  public static final String ATT_COMMANDTYPE_UNASSIGNADMINISTRATOR = "unassignAdministrator";
  public static final String ATT_COMMANDTYPE_GETADMINISTRATOR = "getAdministrator";
  public static final String ATT_COMMANDTYPE_DUMPGROUP = "dumpGroup";
  public static final String ATT_COMMANDTYPE_DUMPSERVICEPROVIDER = "dumpServiceProvider";
  public static final String ATT_COMMANDTYPE_ASSIGNUSERSERVICE = "assignUserService";
  public static final String ATT_COMMANDTYPE_UNASSIGNUSERSERVICE = "unassignUserService";
  public static final String ATT_COMMANDTYPE_GETASSIGNUSERSERVICE = "getAssignUserService";
  public static final String ATT_COMMANDTYPE_ADDUSER = "addUser";
  public static final String ATT_COMMANDTYPE_MODIFYUSER = "modifyUserProfile";
  public static final String ATT_COMMANDTYPE_MODIFYUSERSERVICE = "modifyUserService";
  public static final String ATT_COMMANDTYPE_DELETEUSER = "deleteUser";
  public static final String ATT_COMMANDTYPE_ASSIGNGROUPSERVICE = "assignGroupService";
  public static final String ATT_COMMANDTYPE_UNASSIGNGROUPSERVICE = "unassignGroupService";
  public static final String ATT_COMMANDTYPE_GETASSIGNGROUPSERVICE = "getAssignGroupService";

  public static final String ATT_COMMANDTYPE_ADDDEPARTMENT = "addDepartment";
  public static final String ATT_COMMANDTYPE_MODIFYDEPARTMENT = "modifyDepartment";
  public static final String ATT_COMMANDTYPE_DELETEDEPARTMENT = "deleteDepartment";
  public static final String ATT_COMMANDTYPE_GETDEPARTMENT = "getDepartment";

  public static final String ATT_COMMANDTYPE_GETDOMAIN = "getDomain";
  public static final String ATT_COMMANDTYPE_ADDDOMAIN = "addDomain";
  public static final String ATT_COMMANDTYPE_DELETEDOMAIN = "deleteDomain";
  public static final String ATT_COMMANDTYPE_ASSIGNDOMAIN = "assignDomain";
  public static final String ATT_COMMANDTYPE_UNASSIGNDOMAIN = "unassignDomain";

  public static final String ATT_COMMANDTYPE_ADDUSERPHONELIST = "addUserPhoneList";
  public static final String ATT_COMMANDTYPE_GETUSERPHONELIST = "getUserPhoneList";
  public static final String ATT_COMMANDTYPE_DELETEUSERPHONELIST = "deleteUserPhoneList";

  public static final String ATT_COMMANDTYPE_ADDGROUPCOMMONPHONELIST = "addGroupCommonPhoneList";
  public static final String ATT_COMMANDTYPE_GETGROUPCOMMONPHONELIST = "getGroupCommonPhoneList";
  public static final String ATT_COMMANDTYPE_DELETEGROUPCOMMONPHONELIST = "deleteGroupCommonPhoneList";

  public static final String ATT_COMMANDTYPE_AUTHENTICATEUSER = "authenticateUser";
  public static final String ATT_COMMANDTYPE_GETDEVICETYPE = "getDeviceType";

  public static final String ATT_COMMANDTYPE_GETLDAPENTRIES = "getLdapEntries";
  public static final String ATT_COMMANDTYPE_GETLDAPENTRYDETAIL = "getLdapEntryDetail";

  //character data
  public static final String  PROTOCOL_PCDATA				= "protocol";
  public static final String  VERSION_PCDATA				= "version";
  public static final String  TRANSACTIONID_PCDATA			= "transactionId";
  public static final String  LOGINID_PCDATA			 = "loginId";
  public static final String  RESULT_PCDATA				=  "result";
  public static final String  NONCE_PCDATA				=  "nonce";

  public static final String  SERVICEPROVIDERNAME_PCDATA              =  "serviceProviderId";
  public static final String  GROUPNAME_PCDATA			=  "groupId";
  public static final String  USEGROUPNAME_PCDATA			=  "useGroupName";
  public static final String  USEGROUPCLID_PCDATA			=  "useGroupCLID";
  public static final String  GROUPCLID_PCDATA			=  "groupCLID";
  public static final String  CONTACTNAME_PCDATA			=  "contactName";
  public static final String  CONTACTTELEPHONENUMBER_PCDATA		=  "contactTelephoneNumber";
  public static final String  CONTACTEMAIL_PCDATA			=  "contactEmail";
  public static final String  SUPPORTEMAIL_PCDATA			=  "supportEmail";
  public static final String  DESCRIPTION_PCDATA		=  "description";
  public static final String  COMPANYNAME_PCDATA		=  "enterpriseName";
  public static final String  ADDRESSLOCATION_PCDATA		=  "addressLocation";
  public static final String  ADDRESSLINE1_PCDATA		=  "addressLine1";
  public static final String  ADDRESSLINE2_PCDATA		=  "addressLine2";
  public static final String  CITY_PCDATA			=  "city";
  public static final String  STATE_OR_PROVINCE_PCDATA	=  "stateOrProvince";
  public static final String  ZIP_OR_POSTALCODE_PCDATA	=  "zipOrPostalCode";
  public static final String  COUNTRY_PCDATA			=  "country";
  public static final String  DIRECTORYNUMBER_PCDATA			=  "directoryNumber";
  public static final String  ACCESSDEVICE_PCDATA			=  "accessDevice";
  public static final String  SERVICENAME_PCDATA			=  "name";
  public static final String  SERVICEINFO 			        =  "serviceInfo";
  public static final String  SERVICEQUANTITY_PCDATA			=  "quantity";
  public static final String  SERVICEPACKINFO 			        =  "servicePackInfo";
  public static final String  USERID_PCDATA				=  "userId";
  public static final String  PASSWORD_PCDATA				=  "password";
  public static final String  ID_PCDATA				=  "id";
  public static final String  SUMMARY_PCDATA				=  "summary";
  public static final String  DETAIL_PCDATA				=  "detail";
  public static final String  LASTNAME_PCDATA				=  "lastName";
  public static final String  FIRSTNAME_PCDATA			=  "firstName";
  public static final String  TELEPHONE_PCDATA			=  "telephone";
  public static final String  LOCATION_PCDATA				=  "location";
  public static final String  TIMEZONE_PCDATA                         =  "timeZone";
  public static final String  LANGUAGE_PCDATA			      =  "language";
  public static final String  DEVICE_NAME_PCDATA                      =  "deviceName";
  public static final String  DEVICE_TYPE_PCDATA                      =  "deviceType";
  public static final String  SOFTWARE_LOAD_PCDATA                    =  "softwareLoad";
  public static final String  IPADDRESS_PCDATA                        =  "ipAddress";
  public static final String  PORT_PCDATA                             =  "port";
  public static final String  MAC_ADDRESS_PCDATA                      =  "macAddress";
  public static final String  SERIAL_NUMBER_PCDATA                    =  "serialNumber";
  public static final String  MAXPORTS_PCDATA			                    =  "maxPorts";
  public static final String  USER_LIMIT                              = "userLimit";
  public static final String  SEARCH_BASE_PCDATA            = "searchBase";
  public static final String  USE_SYS_LDAP_PCDATA           = "useSysLdapDir";
  public static final String REQ_AUTH_PCDATA                = "requireAuthentication";
  public static final String AUTH_DN_PCDATA                 = "authenticatedDN";
  public static final String AUTH_PWD_PCDATA                = "authenticatedPassword";
  public static final String SORT_CONTROL_PCDATA            = "sortControl";
  public static final String PAGED_RESULT_CONTROL_PCDATA    = "pagedResultControl";
  public static final String PHONELISTENTRY_NESTED          = "phoneListEntry";
  public static final String NAME_PCDATA                    = "name";
  public static final String NEWNAME_PCDATA                 = "newName";
  public static final String CALLID_PCDATA                  = "callId";
  public static final String AVAILABLE_FOR_USE_NESTED       = "availableForUse";
  public static final String REASON_PCDATA     = "reason";

  public static final String GROUP_EXT_ACCESS_PCDATA               = "groupExtensionAccess";
  public static final String GROUP_LDAP_ACCESS_PCDATA              = "groupLDAPAccess";
  public static final String GROUP_DEPT_ADMIN_USER_ACCESS_PCDATA   = "groupDeptAdminUserAccess";
  public static final String GROUP_USER_AUTH_ACCESS_PCDATA         = "groupUserAuthenticationAccess";
  public static final String GROUP_USER_PROFILE_ACCESS_PCDATA      = "groupUserProfileAccess";

  public static final String GROUP_ADMIN_PROFILE_ACCESS_PCDATA     = "groupAdminProfileAccess";
  public static final String GROUP_ADMIN_USER_ACCESS_PCDATA        = "groupAdminUserAccess";
  public static final String GROUP_ADMIN_ADMIN_ACCESS_PCDATA       = "groupAdminAdminAccess";
  public static final String GROUP_ADMIN_DEPT_ACCESS_PCDATA        = "groupAdminDeptAccess";
  public static final String GROUP_ADMIN_DEVICE_ACCESS_PCDATA      = "groupAdminDeviceAccess";
  public static final String GROUP_ADMIN_NUMBER_ACCESS_PCDATA      = "groupAdminNumberAccess";
  public static final String GROUP_ADMIN_SERVICE_ACCESS_PCDATA     = "groupAdminServiceAccess";

  public static final String SVC_PROV_ADMIN_PROFILE_ACCESS_PCDATA     = "svcProvAdminProfileAccess";
  public static final String SVC_PROV_ADMIN_GROUP_ACCESS_PCDATA        = "svcProvAdminGroupAccess";
  public static final String SVC_PROV_ADMIN_ADMIN_ACCESS_PCDATA       = "svcProvAdminAdminAccess";
  public static final String SVC_PROV_ADMIN_DEPT_ACCESS_PCDATA        = "svcProvAdminDeptAccess";
  public static final String SVC_PROV_ADMIN_DEVICE_ACCESS_PCDATA      = "svcProvAdminDeviceAccess";
  public static final String SVC_PROV_ADMIN_NUMBER_ACCESS_PCDATA      = "svcProvAdminNumberAccess";
  public static final String SVC_PROV_ADMIN_SERVICE_ACCESS_PCDATA     = "svcProvAdminServiceAccess";
  public static final String SVC_PROV_ADMIN_SERVICE_PACK_ACCESS_PCDATA     = "svcProvAdminServicePackAccess";
  public static final String SVC_PROV_ADMIN_WEB_BRANDING_ACCESS_PCDATA     = "svcProvAdminWebBrandingAccess";

  public static final String DEPARTMENT_NAME                          = "name";
  public static final String DEPARTMENT_NEWNAME                       = "newName";
  public static final String DEPARTMENT_USERS                         = "numberOfUsers";

  public static final String DOMAIN_NAME_PCDATA                       = "domainName";
  public static final String DOMAIN_NO_USERS                          = "numberOfUsers";

  public static final String LDAPLISTENTRY_NESTED       = "ldapListEntry";
  public static final String LDAPENTRYATTRIBUTE_NESTED  = "ldapEntryAttribute";
  public static final String LDAPPAGENUMBER_PCDATA      = "ldapPageNumber";
  public static final String LDAPSEARCHKEY_PCDATA       = "ldapSearchKey";
  public static final String NUMBER_PCDATA              = "number";
  public static final String ATTRIBUTENAME_PCDATA       = "attributeName";
  public static final String ATTRIBUTEVALUE_PCDATA      = "attributeValue";

  public static final String  SERVICE_AA 		= "Auto Attendant";
  public static final String  SERVICE_CFA 		= "Call Forward Always";
  public static final String  SERVICE_CFB 		= "Call Forward Busy";
  public static final String  SERVICE_CFNA 		= "Call Forward No Answer";
  public static final String  SERVICE_CN 		= "Call Notify";
  public static final String  SERVICE_CP 		= "CommPilot";
  public static final String  SERVICE_DND 		= "Do Not Disturb";
  public static final String  SERVICE_EVMU 		= "Third-Party Voice Mail Support";
  public static final String  SERVICE_ICP 		= "Incoming Calling Plan";
  public static final String  SERVICE_OCP 		= "Outgoing Calling Plan";
  public static final String  SERVICE_VMR 		= "Voice Mail Retrieval";
  public static final String  SERVICE_VM 		= "Voice Messaging";

  public static final String  ANCHORTAG				=  "/";
  public static final String  TAGSTART				=  "<";
  public static final String  TAGEND					=  ">";
  public static final String  NOTAG				  	=  "*";

  /**
   * Services Support.
   */
  public static final String  USER_SERVICE_NESTED = "userService";
  public static final String  GROUP_SERVICE_NESTED = "groupService";

  public static final String  SERVICE_AAC = "Account/Authorization Codes";
  public static final String  SERVICE_ALTERNATE_NUMBERS = "Alternate Numbers";
  public static final String  SERVICE_ANONYMOUS_CALL_REJECTION = "Anonymous Call Rejection";
  public static final String  SERVICE_ATTENDANT_CONSOLE = "Attendant Console";
  public static final String  SERVICE_AUTHENTICATION = "Authentication";
  public static final String  SERVICE_AUTO_ATTENDANT = "Auto Attendant";
  public static final String  SERVICE_BARGE_IN_EXEMPT = "Barge-in Exempt";
  public static final String  SERVICE_CALL_CAPACITY_MANAGEMENT = "Call Capacity Management";
  public static final String  SERVICE_CALL_CENTER = "Call Center";
  public static final String  SERVICE_CF_ALWAYS = "Call Forwarding Always";
  public static final String  SERVICE_CF_BUSY = "Call Forwarding Busy";
  public static final String  SERVICE_CF_NO_ANSWER = "Call Forwarding No Answer";
  public static final String  SERVICE_CF_SELECTIVE = "Call Forwarding Selective";
  public static final String  SERVICE_CLIO = "Calling Line ID Blocking Override";
  public static final String  SERVICE_CALL_NOTIFY = "Call Notify";
  public static final String  SERVICE_CALLING_NAME_RETRIEVAL = "Calling Name Retrieval";
  public static final String  SERVICE_CALL_PARK = "Call Park";
  public static final String  SERVICE_CALL_PICKUP = "Call Pickup";
  public static final String  SERVICE_CALL_RETURN = "Call Return";
  public static final String  SERVICE_CALL_WAITING = "Call Waiting";
  public static final String  SERVICE_CLID_DELIVERY_BLOCKING = "Calling Line ID Delivery Blocking";
  public static final String  SERVICE_CITY_WIDE_CENTREX = "City-Wide Centrex";
  public static final String  SERVICE_CLIENT_CALL_CONTROL = "Client Call Control";
  public static final String  SERVICE_COMMPILOT_CALL_MANAGER = "CommPilot Call Manager";
  public static final String  SERVICE_COMMPILOT_EXPRESS = "CommPilot Express";
  public static final String  SERVICE_DIRECTED_CALL_PICKUP = "Directed Call Pickup";
  public static final String  SERVICE_DPUBI = "Directed Call Pickup with Barge-in";
  public static final String  SERVICE_DO_NOT_DISTURB = "Do Not Disturb";
  public static final String  SERVICE_EMERGENCY_ZONES = "Emergency Zones";
  public static final String  SERVICE_EXTERNAL_VOICE_MAIL_USER  = "Third-Party Voice Mail Support";
  public static final String  SERVICE_EXTERNAL_CALLING_LINE_ID_DELIVERY = "External Calling Line ID Delivery";
  public static final String  SERVICE_FLASH_CALL_HOLD = "Flash Call Hold";
  public static final String  SERVICE_FLASH_CALL_TRANSFER = "Flash Call Transfer";
  public static final String  SERVICE_FLASH_3WC = "Flash Three-Way Call";
  public static final String  SERVICE_GROUP_ACCT_LOGS = "Group Call Detail Reporting";
  public static final String  SERVICE_HUNT_GROUP = "Hunt Group";
  public static final String  SERVICE_INCOMING_CALLING_PLAN = "Incoming Calling Plan";
  public static final String  SERVICE_INSTANT_CONFERENCING = "Instant Conferencing";
  public static final String  SERVICE_INTERCEPT_GROUP = "Intercept Group";
  public static final String  SERVICE_INTERCEPT_USER = "Intercept User";
  public static final String  SERVICE_INTERNAL_CALLING_LINE_ID_DELIVERY = "Internal Calling Line ID Delivery";
  public static final String SERVICE_LDAP_INTEGRATION = "LDAP Integration";
  public static final String  SERVICE_LNR = "Last Number Redial";
  public static final String  SERVICE_MUSIC_ON_HOLD = "Music On Hold";
  public static final String  SERVICE_OUTGOING_CALLING_PLAN = "Outgoing Calling Plan";
  public static final String  SERVICE_EOCP = "Enhanced Outgoing Calling Plan";
  public static final String  SERVICE_OUTLOOK_INTEGRATION = "Outlook Integration";
  public static final String  SERVICE_PRIORITY_ALERT = "Priority Alert";
  public static final String  SERVICE_REMOTE_OFFICE = "Remote Office";
  public static final String  SERVICE_SELECTIVE_CALL_ACCEPTANCE = "Selective Call Acceptance";
  public static final String  SERVICE_SELECTIVE_CALL_REJECTION = "Selective Call Rejection";
  public static final String  SERVICE_SERIES_COMPLETION = "Series Completion";
  public static final String  SERVICE_SERVICE_SCRIPTS = "Service Scripts";
  public static final String  SERVICE_SHARED_CALL_APPEAR = "Shared Call Appearance";
  public static final String  SERVICE_SHARED_CALL_APPEAR5 = "Shared Call Appearance 5";
  public static final String  SERVICE_SHARED_CALL_APPEAR10 = "Shared Call Appearance 10";
  public static final String  SERVICE_SHARED_CALL_APPEAR15 = "Shared Call Appearance 15";
  public static final String  SERVICE_SHARED_CALL_APPEAR20 = "Shared Call Appearance 20";
  public static final String  SERVICE_SHARED_CALL_APPEAR25 = "Shared Call Appearance 25";
  public static final String  SERVICE_SHARED_CALL_APPEAR30 = "Shared Call Appearance 30";
  public static final String  SERVICE_SHARED_CALL_APPEAR35 = "Shared Call Appearance 35";
  public static final String  SERVICE_SIM_RING = "Simultaneous Ring Personal";
  public static final String  SERVICE_SPEED_CALL = "Speed Dial 8";
  public static final String  SERVICE_VOICE_MESSAGING_GROUP = "Voice Messaging Group";
  public static final String  SERVICE_VOICE_MESSAGING_USER = "Voice Messaging User";
  public static final String  SERVICE_WINDOWS_MESSENGER = "Windows Messenger";
  public static final String  SERVICE_DUMMY_FOR_TEST = "Dummy for Test";
  public static final String  SERVICE_SPEED_CALL_100 = "Speed Dial 100";
  public static final String  SERVICE_CALLING_PARTY_CATEGORY = "Calling Party Category";

  // All service names are defined here.
  public static final String[] SERVICE_NAMES_USER =
  {
    SERVICE_ALTERNATE_NUMBERS,
    SERVICE_ANONYMOUS_CALL_REJECTION,
    SERVICE_ATTENDANT_CONSOLE,
    SERVICE_AUTHENTICATION,
    SERVICE_BARGE_IN_EXEMPT,
    SERVICE_CF_ALWAYS,
    SERVICE_CF_BUSY,
    SERVICE_CF_NO_ANSWER,
    SERVICE_CF_SELECTIVE,
    SERVICE_CLIO,
    SERVICE_CALL_NOTIFY,
    SERVICE_CALLING_PARTY_CATEGORY,
    SERVICE_CALLING_NAME_RETRIEVAL,
    SERVICE_CALL_RETURN,
    SERVICE_CALL_WAITING,
    SERVICE_CLID_DELIVERY_BLOCKING,
    SERVICE_COMMPILOT_CALL_MANAGER,
    SERVICE_COMMPILOT_EXPRESS,
    SERVICE_DIRECTED_CALL_PICKUP,
    SERVICE_DPUBI,
    SERVICE_DO_NOT_DISTURB,
    SERVICE_FLASH_CALL_HOLD,
    SERVICE_FLASH_CALL_TRANSFER,
    SERVICE_FLASH_3WC,
    SERVICE_GROUP_ACCT_LOGS,
    SERVICE_INTERCEPT_USER,
    SERVICE_LNR,
    SERVICE_OUTLOOK_INTEGRATION,
    SERVICE_PRIORITY_ALERT,
    SERVICE_REMOTE_OFFICE,
    SERVICE_SELECTIVE_CALL_ACCEPTANCE,
    SERVICE_SELECTIVE_CALL_REJECTION,
    SERVICE_SERVICE_SCRIPTS,
    SERVICE_SHARED_CALL_APPEAR,
    SERVICE_SHARED_CALL_APPEAR5,
    SERVICE_SHARED_CALL_APPEAR10,
    SERVICE_SHARED_CALL_APPEAR15,
    SERVICE_SHARED_CALL_APPEAR20,
    SERVICE_SHARED_CALL_APPEAR25,
    SERVICE_SHARED_CALL_APPEAR30,
    SERVICE_SHARED_CALL_APPEAR35,
    SERVICE_SIM_RING,
    SERVICE_SPEED_CALL,
    SERVICE_EXTERNAL_VOICE_MAIL_USER,
    SERVICE_VOICE_MESSAGING_USER,
    SERVICE_WINDOWS_MESSENGER,
    SERVICE_DUMMY_FOR_TEST,
    SERVICE_VOICE_MESSAGING_GROUP,
    SERVICE_SPEED_CALL_100,
    SERVICE_EXTERNAL_CALLING_LINE_ID_DELIVERY,
    SERVICE_INTERNAL_CALLING_LINE_ID_DELIVERY
  };

  public static final String[] SERVICE_NAMES_GROUP =
  {
    SERVICE_AAC,
    SERVICE_AUTO_ATTENDANT,
    SERVICE_CALL_CAPACITY_MANAGEMENT,
    SERVICE_CALL_CENTER,
    SERVICE_CALL_PARK,
    SERVICE_CALL_PICKUP,
    SERVICE_CITY_WIDE_CENTREX,
    SERVICE_EMERGENCY_ZONES,
    SERVICE_GROUP_ACCT_LOGS,
    SERVICE_HUNT_GROUP,
    SERVICE_INCOMING_CALLING_PLAN,
    SERVICE_INSTANT_CONFERENCING,
    SERVICE_INTERCEPT_GROUP,
    SERVICE_LDAP_INTEGRATION,
    SERVICE_MUSIC_ON_HOLD,
    SERVICE_OUTGOING_CALLING_PLAN,
    SERVICE_EOCP,
    SERVICE_SERIES_COMPLETION,
    SERVICE_SERVICE_SCRIPTS,
    SERVICE_VOICE_MESSAGING_GROUP,
    SERVICE_DUMMY_FOR_TEST,
    SERVICE_VOICE_MESSAGING_USER
  };

  public static final String ATTENDANT_CONSOLE_SERVICE_NESTED = "attendantConsoleService";
  public static final String CALL_NOTIFY_SERVICE_NESTED = "callNotifyService";
  public static final String CNAM_SERVICE_NESTED = "cnamService";
  public static final String CFA_SERVICE_NESTED = "cfaService";
  public static final String CFB_SERVICE_NESTED = "cfbService";
  public static final String CFNA_SERVICE_NESTED = "cfnaService";
  public static final String CF_SELECTIVE_SERVICE_NESTED = "cfSelectiveService";
  public static final String INCOMING_CALL_INFO_NESTED = "incomingCallInfo";
  public static final String TIME_RANGE_NESTED = "timeRange";
  public static final String START_TIME_NESTED = "startTime";
  public static final String STOP_TIME_NESTED = "stopTime";
  public static final String TELEPHONE_LIST_NESTED = "telephoneList";
  public static final String REMOTE_OFFICE_SERVICE_NESTED = "remoteOfficeService";
  public static final String AUTHENTICATION_SERVICE_NESTED = "authenticationService";
  public static final String ALTERNATE_NUMBERS_SERVICE_NESTED = "alternateNumbersService";
  public static final String ALTERNATE_NUMBER1_NESTED = "alternateNumber1";
  public static final String ALTERNATE_NUMBER2_NESTED = "alternateNumber2";
  public static final String ALTERNATE_NUMBER_NESTED = "alternateNumber";
  public static final String USE_DISTINCTIVE_RING_NESTED = "useDistinctiveRing";
  public static final String USE_TELEPHONE_LIST_NESTED = "useTelephoneList";
  public static final String SCA_NESTED = "selectiveCallAcceptanceService";
  public static final String SCR_NESTED = "selectiveCallRejectionService";
  public static final String COMM_PILOT_EXPRESS_NESTED = "commPilotExpressService";
  public static final String CPE_AVAIL_IN_OFFICE_NESTED = "availableInOfficeProfileInfo";
  public static final String RING_EXTRA_PHONE_NESTED = "ringExtraPhone";
  public static final String USE_FWD_ON_BUSY_NESTED = "useForwardOnBusyInsteadOfVM";
  public static final String USE_FWD_ON_NO_ANSWER_NESTED = "useForwardOnNoAnswerInsteadOfVM";
  public static final String CPE_AVAIL_OUT_OFFICE_NESTED = "availableOutOfOfficeProfileInfo";
  public static final String SEND_NOTIFICATION_NESTED = "sendNotification";
  public static final String CPE_BUSY_NESTED = "busyProfileInfo";
  public static final String USE_FWD_INSTEAD_OF_VM_NESTED = "useForwardInsteadOfVM";
  public static final String CPE_UNAVAILABLE_NESTED = "unavailableProfileInfo";
  public static final String INTERCEPT_SERVICE_NESTED = "interceptService";
  public static final String USE_PERSONAL_ANN_NESTED = "usePersonalAnnouncement";
  public static final String PLAY_NEW_NUMBER_NESTED = "playNewNumber";
  public static final String TRANSFER_ON_ZERO_NESTED = "transferOnZero";
  public static final String FILE_LOCATOR_NESTED = "fileLocator";
  public static final String PRIORITY_ALERT_NESTED = "priorityAlertService";
  public static final String SIM_RING_NESTED = "simRingService";
  public static final String ACTIVATE_ON_BUSY_NESTED = "activateOnBusy";
  public static final String VM_USER_NESTED = "voiceMessagingUserService";
  public static final String USE_PHONE_AND_EMAIL_RETRIEVAL_NESTED = "useUnifiedMessaging";
  public static final String DFLT_MAIL_SERVER_NESTED = "defaultVoiceMessagingServer";
  public static final String USE_MWI_NESTED = "useMWI";
  public static final String SEND_VOICE_MSG_NESTED = "sendVoiceMessage";
  public static final String SEND_VOICE_MSG_NOTIF_NESTED = "sendVoiceMessageNotification";
  public static final String VOICE_MESSAGE_CARBON_COPY_NESTED = "voiceMessageCarbonCopy";
  public static final String USE_PERSONAL_BUSY_GREETING_NESTED = "usePersonalBusyGreeting";
  public static final String USE_PERSONAL_NO_ANSWER_GREETING_NESTED = "usePersonalNoAnswerGreeting";
  public static final String USE_ALTERNATE_NO_ANSWER_GREETING_NESTED = "useAlternateNoAnswerGreeting";
  public static final String VM_ALIAS_NESTED = "voiceMessagingAlias";
  public static final String VM_DIST_LIST_NESTED = "voiceMessagingDistributionList";
  public static final String ALT_VM_SERVER_NESTED = "alternateVoiceMessagingServer";
  public static final String PERSONALIZED_NAME_NESTED = "personalizedName";
  public static final String SHARED_CALL_APPEAR_SERVICE_NESTED = "sharedCallAppearanceService";
  public static final String ALTERNATE_LOCATION_LIST_NESTED = "alternateLocationList";
  public static final String ALTERNATE_LOCATION_NESTED = "alternateLocation";
  public static final String SPEED_CALL_SERVICE_NESTED = "speedDialService";
  public static final String SPEED_CALL_INFO_NESTED = "speedDialInfo";
  public static final String USE_RING_SPLASH_NESTED = "useRingSplash";
  public static final String DND_SERVICE_NESTED = "dndService";
  public static final String OUTGOING_SMDI_MWI_CONFIG_NESTED = "outgoingSmdiMwiConfig";
  public static final String SPEED_CALL_100_SERVICE_NESTED = "speedDial100Service";
  public static final String SPEED_CALL_100_INFO_NESTED = "speedDial100Info";
  public static final String OUTLOOK_INTEGRATION_NESTED = "outlookIntService";
  public static final String DPUBI_NESTED = "dpubiService";
  public static final String WARNING_TONE_NESTED = "warningTone";
  public static final String CALLING_PARTY_CATEGORY_SERVICE_NESTED = "callingPartyCategoryService";


  // group
  public static final String AGENT_LOGIN_NESTED = "agentLogin";
  public static final String MESSAGE_AGING_NESTED = "messageAging";
  public static final String MUSIC_ON_CALL_HOLD_NESTED = "musicOnCallHold";
  public static final String PLAY_COMFORT_MESSAGE_NESTED = "playComfortMessage";
  public static final String MUSIC_ON_CALL_PARK_NESTED = "musicOnCallPark";
  public static final String SYSTEM_PROVIDER_MAIL_SERVER_NESTED = "systemProviderMailServer";
  public static final String USE_CUSTOM_MUSIC_ON_HOLD_NESTED = "useCustomMusicOnHold";
  public static final String CALL_TYPE_NESTED = "callType";
  public static final String DIAL_BY_EXTENSION_ACTION_NESTED = "dialByExtensionAction";
  public static final String DIAL_BY_NAME_ACTION_NESTED = "dialByNameAction";
  public static final String REACH_OPERATOR_ACTION_NESTED = "reachOperatorAction";
  public static final String VOICE_PORTAL_NESTED = "voicePortal";
  public static final String VOICE_PORTAL_ACCEPT_ALIAS_LOGIN = "acceptAliasLogin";
  public static final String VOICE_PORTAL_USE_VP_WIZARD = "useVoicePortalWizard";
  public static final String ENABLE_NO_ANSWER_TIMEOUT_NESTED = "enableNoAnswerTimeout";
  public static final String USE_CUSTOM_COMFORT_MESSAGE_NESTED = "useCustomComfortMessage";
  public static final String USE_CUSTOM_ENTRANCE_MESSAGE_NESTED = "useCustomEntranceMessage";
  public static final String USER_CONFIGURABLE_MAIL_SETTINGS = "userConfigurableMailSettings";
  public static final String USER_ID_LIST_NESTED = "userIdList";
  public static final String FAC_BASED_USER_ID_LIST_NESTED = "facBasedUserIdList";
  public static final String MANDATORY_USAGE_USER_ID_LIST_NESTED = "mandatoryUsageUserIdList";
  public static final String TRANSFER_ACTION_NESTED = "transferAction";
  public static final String SERIES_COMPLETION_INFO_NESTED = "seriesCompletionInfo";
  public static final String CALL_PICKUP_INFO_NESTED = "callPickupInfo";
  public static final String DFLT_CALLING_PLAN_ENTRY_NESTED = "defaultCallingPlanEntry";
  public static final String CALLING_PLAN_ENTRY_NESTED = "callingPlanEntry";
  public static final String CODE_ENTRY_NESTED = "codeEntry";
  public static final String MENU_ENTRY_NESTED = "menuEntry";
  public static final String INSTANT_CONF_INFO_NESTED = "instantConferencingInfo";
  public static final String HUNT_GROUP_INFO_NESTED = "huntGroupInfo";
  public static final String AUTO_ATTENDANT_INFO_NESTED = "autoAttendantInfo";
  public static final String CODE_LIST_NESTED = "codeList";
  public static final String MENU_NESTED = "menu";
  public static final String CALL_CAPACITY_INFO_NESTED = "callCapacityInfo";
  public static final String BUSINESS_HOURS_NESTED = "businessHours";
  public static final String AFTER_HOURS_NESTED = "afterHours";
  public static final String CALL_CENTER_INFO_NESTED = "callCenterInfo";
  public static final String DFLT_GROUP_MAIL_SERVER_NESTED = "defaultGroupMailServer";
  public static final String AAC_SERVICE_NESTED = "accountAuthorizationCodeService";
  public static final String AA_SERVICE_NESTED = "autoAttendantService";
  public static final String CALL_CAPACITY_SERVICE_NESTED = "callCapacityService";
  public static final String CALL_CENTER_SERVICE_NESTED = "callCenterService";
  public static final String CALL_PICKUP_SERVICE_NESTED = "callPickupService";
  public static final String GROUP_ACCT_LOGS_SERVICE_NESTED = "groupCallDetailReportingService";
  public static final String HUNT_GROUP_SERVICE_NESTED = "huntGroupService";
  public static final String INCOMING_CALLING_PLAN_SERVICE_NESTED = "incomingCallingPlanService";
  public static final String INSTANT_CONF_SERVICE_NESTED = "instantConferencingService";
  public static final String MUSIC_ON_HOLD_MESSAGE = "musicOnHoldMessage";
  public static final String MUSIC_ON_HOLD_SERVICE_NESTED = "musicOnHoldService";
  public static final String MUSIC_ON_HOLD_SOURCE = "source";
  public static final String OUTGOING_CALLING_PLAN_SERVICE_ORIG_NESTED = "outgoingCallingPlanServiceOrig";
  public static final String OUTGOING_CALLING_PLAN_SERVICE_DO_FWD_NESTED = "outgoingCallingPlanServiceFwd";
  public static final String OUTGOING_CALLING_PLAN_SERVICE_BE_FWD_NESTED = "outgoingCallingPlanServiceBeingFwd";
  public static final String OUTGOING_CALLING_PLAN_SERVICE_NESTED = "outgoingCallingPlanService";
  public static final String SERIES_COMPLETION_SERVICE_NESTED = "seriesCompletionService";
  public static final String VOICE_MESSAGING_GROUP_SERVICE_NESTED = "voiceMessagingGroupService";
  public static final String USE_PERSONAL_GREETING_NESTED = "usePersonalGreeting";
  public static final String USE_PROMPT_NESTED = "usePrompt";
  public static final String REPEAT_MENU_ACTION_NESTED = "repeatMenuAction";
  public static final String EXIT_MENU_ACTION_NESTED = "exitMenuAction";
  public static final String AUTO_ATTENDANT_SERVICE_NESTED = "autoAttendantService";
  public static final String USE_GROUP_DEFAULT_NESTED = "useGroupDefault";
  public static final String FIRST_LAST_NAME_DIALING_NESTED = "firstLastNameDialing";
  public static final String RESTRICT_NAME_DIALING_TO_DEPT_NESTED = "restrictNameDialingToDepartment";
  public static final String FIRST_LEVEL_EXTN_DIALING_NESTED = "firstLevelExtnDialing";
  public static final String GAC_HOLD_LOGS_STATUS  = "holdLogsStatus";
  public static final String GAC_HOLD_LOGS_PERIOD  = "holdLogsPeriod";
  public static final String GAC_EMAIL_LOGS_STATUS = "emailLogsStatus";
  public static final String GAC_EMAIL_LOGS_ADDR1  = "emailLogsAddress1";
  public static final String GAC_EMAIL_LOGS_ADDR2  = "emailLogsAddress2";
  public static final String EXTVMG_SERVICE_ELEMENT  = "thirdPartyVoiceMailServiceInfo";
  public static final String EXTVMG_MAIL_SERVER      = "externalVoiceMailServer";
  public static final String EXTVMU_SERVICE_ELEMENT  = "thirdPartyVoiceMailService";
  public static final String EXTVMU_FWD_ON_BUSY      = "forwardOnBusy";
  public static final String EXTVMU_FWD_ON_NO_ANSWER = "forwardOnNoAnswer";
  public static final String EXTVMU_CUSTOM_MAILBOXID = "customMailboxId";
  public static final String EMERGENCY_ZONES_SERVICE_NESTED = "emergencyZonesService";
  public static final String CM_GROUP_SERVICE_ELEMENT = "callManagerServiceInfo";
  public static final String CM_GROUP_DIRECTORY_CONFIG = "groupDirectoryConfiguration";

    public static final String ALLOW_CALL_DETAILS_T = "allowUserConfigurableCallDetails";
    public static final String ENABLE_CALL_DETAILS_T = "enableCallDetails";

  // status tag
  public static final String STATUS_T = "status";
  public static final String ATT_ENABLED = "enabled";
  public static final String ATT_ENABLED_FALSE = "false";
  public static final String ATT_ENABLED_TRUE = "true";
  public static final String[] STATUS_CHOICES =
  {
    ATT_ENABLED_FALSE,
    ATT_ENABLED_TRUE
  };

  // operation tag
  public static final String OPERATION_T = "operation";
  public static final String ATT_TYPE = "type";
  public static final String ATT_TYPE_ADD = "add";
  public static final String ATT_TYPE_DELETE = "delete";
  public static final String ATT_TYPE_MODIFY = "modify";
  public static final String ATT_TYPE_GET = "get";
  public static final String[] OPERATION_TYPE_CHOICES =
  {
    ATT_TYPE_ADD,
    ATT_TYPE_DELETE,
    ATT_TYPE_MODIFY,
    ATT_TYPE_GET
  };

    // AC column tags
    public static final String AC_COLUMN_NAME_T = "acColumnName";
    public static final String AC_COLUMN_LIST_T = "acColumnList";
    public static final String ATT_AC_COLUMN_VALUE = "colValue";
    public static final String ATT_AC_COLUMN_DEPT = "Department";
    public static final String ATT_AC_COLUMN_TITLE = "Title";
    public static final String ATT_AC_COLUMN_MOBILE = "Mobile";
    public static final String ATT_AC_COLUMN_PAGER = "Pager";
    public static final String ATT_AC_COLUMN_EMAIL = "Email";
    public static final String ATT_AC_COLUMN_STATUS = "Status";
    public static final String ATT_AC_COLUMN_NAME = "Name";
    public static final String ATT_AC_COLUMN_NUMBER = "Number";
    public static final String ATT_AC_COLUMN_EXT = "Extension";
    public static final String ATT_AC_COLUMN_ACTION = "Action";
  public static final String[] AC_COLUMN_CHOICES =
  {
    ATT_AC_COLUMN_DEPT,
    ATT_AC_COLUMN_TITLE,
    ATT_AC_COLUMN_MOBILE,
    ATT_AC_COLUMN_PAGER,
    ATT_AC_COLUMN_EMAIL,
    ATT_AC_COLUMN_STATUS,
    ATT_AC_COLUMN_NAME,
    ATT_AC_COLUMN_NUMBER,
    ATT_AC_COLUMN_EXT,
    ATT_AC_COLUMN_ACTION
  };



  // day tag
  public static final String DAY_T = "day";
  public static final String ATT_DAY_VALUE = "dayValue";
  public static final String ATT_DAY_MON = "Mon";
  public static final String ATT_DAY_TUE = "Tue";
  public static final String ATT_DAY_WED = "Wed";
  public static final String ATT_DAY_THU = "Thu";
  public static final String ATT_DAY_FRI = "Fri";
  public static final String ATT_DAY_SAT = "Sat";
  public static final String ATT_DAY_SUN = "Sun";
  public static final String ATT_DAY_ALL = "all";
  public static final String[] DAY_CHOICES =
  {
    ATT_DAY_MON,
    ATT_DAY_TUE,
    ATT_DAY_WED,
    ATT_DAY_THU,
    ATT_DAY_FRI,
    ATT_DAY_SAT,
    ATT_DAY_SUN,
    ATT_DAY_ALL
  };

  // Comm Pilot Express profile tag
  public static final String COMMPILOT_T = "commPilot";
  public static final String ATT_CPE_PROFILE = "profile";
  public static final String ATT_AVAIL_OFFICE_PROFILE = "availableInOffice";
  public static final String ATT_AVAIL_NOT_OFFICE_PROFILE = "availableOutOfOffice";
  public static final String ATT_BUSY_PROFILE = "busy";
  public static final String ATT_UNAVAILABLE_PROFILE = "unavailable";
  public static final String ATT_NONE_PROFILE = "none";
  public static final String[] PROFILE_CHOICES =
  {
    ATT_AVAIL_OFFICE_PROFILE,
    ATT_AVAIL_NOT_OFFICE_PROFILE,
    ATT_BUSY_PROFILE,
    ATT_UNAVAILABLE_PROFILE,
    ATT_NONE_PROFILE
  };

  // AAC status tag
  public static final String AAC_STATUS_T = "accountAuthorizationStatus";
  public static final String AAC_STATUS_SERVICE = "service";
  public static final String AAC_ACCOUNT_CODE = "accountCode";
  public static final String AAC_AUTHORIZATION_CODE = "authorizationCode";
  public static final String AAC_DISABLED = "disabled";
  public static final String AAC_FREE_CALLS_EXEMPTED = "freeCallsExempted";
  public static final String[] AAC_STATUS_CHOICES =
  {
    AAC_ACCOUNT_CODE,
    AAC_AUTHORIZATION_CODE,
    AAC_DISABLED
  };

  // Auto attendant actions
  public static final String AA_XFER_OPERATOR_ACTION = "transfer to operator";
  public static final String AA_XFER_ACTION = "transfer call";
  public static final String AA_DIAL_BY_EXT_ACTION = "dial by extension";
  public static final String AA_DIAL_BY_NAME_ACTION = "dial by name";
  public static final String AA_REPEAT_MENU_ACTION = "repeat menu";
  public static final String AA_EXIT_MENU_ACTION = "exit menu";
  public static final String[] AA_ACTION_CHOICES =
  {
    AA_XFER_OPERATOR_ACTION,
    AA_XFER_ACTION,
    AA_DIAL_BY_EXT_ACTION,
    AA_DIAL_BY_NAME_ACTION,
    AA_REPEAT_MENU_ACTION,
    AA_EXIT_MENU_ACTION
  };

  // Basic tags
  public static final String CODE_VALUE_T = "codeValue";
  public static final String EXTENSION_T = "extension";
  public static final String CLID_PHONE_T = "clidPhone";
  public static final String FILE_PATH_T = "filePath";
  public static final String FILE_NAME_T = "fileName";
  public static final String FORWARD_TO_NUMBER_T = "forwardToNumber";
  public static final String FULL_MB_LIMIT_T = "fullMailboxLimit";
  public static final String GREETING_T = "greeting";
  public static final String GROUP_POLICY_T = "groupPolicy";
  public static final String HOLD_PERIOD_T = "holdPeriod";
  public static final String KEY_T = "key";
  public static final String MAIL_SERVER_ID_T = "mailServerId";
  public static final String MAIL_SERVER_PROTOCOL_T = "mailServerProtocol";
  public static final String MAX_ACTIVE_CALLS_T = "maxActiveCalls";
  public static final String MAX_ACTIVE_INCOMING_CALLS_T = "maxActiveIncomingCalls";
  public static final String MAX_ACTIVE_OUTGOING_CALLS_T = "maxActiveOutgoingCalls";
  public static final String NUMBER_OF_DIGITS_T = "numberOfDigits";
  public static final String NUMBER_OF_RINGS_T = "numberOfRings";
  public static final String QUEUE_LENGTH_T = "queueLength";
  public static final String SEND_TO_EMAIL_T = "sendToEmail";
  public static final String SILENCE_DURATION_BETWEEN_MESSAGES_T = "silenceDurationBetweenMessages";
  public static final String TELEPHONE_T = "telephone";
  public static final String HOUR_T = "hour";
  public static final String MINUTE_T = "minute";
  public static final String VOICE_PORTAL_PASSCODE_T = "voicePortalPasscode";
  public static final String LINE_PORT_T = "linePort";
  public static final String CONTACT_T = "contact";
  public static final String ALIAS_T = "alias";
  public static final String MOBILE_T = "mobile";
  public static final String PAGER_T = "pager";
  public static final String TITLE_T = "title";
  public static final String NEW_DESCRIPTION_T = "newDescription";
  public static final String NO_ANSWER_NUMBER_OF_RINGS_T = "noAnswerNumberOfRings";

  // Enhanced OCP support.
  public static final String DIGIT_MATCH_STRING_T = "digitMatchString";
  public static final String DIGIT_MATCH_NESTED = "digitMatch";
  public static final String DIGIT_MATCH_LIST_NESTED = "digitMatchList";
  public static final String DIGIT_MATCH_ENTRY_NESTED = "digitMatchEntry";
  public static final String OCP_STATUS_T = "ocpStatus";
  public static final String ATT_OCP_STATUS_ENABLED = "enabled";
  public static final String ATT_OCP_STATUS_ALLOW = "allow";
  public static final String ATT_OCP_STATUS_BLOCK = "block";
  public static final String ATT_OCP_STATUS_AUTHCODE = "authcode";
  public static final String ATT_OCP_STATUS_XFER1 = "xfer1";
  public static final String ATT_OCP_STATUS_XFER2 = "xfer2";
  public static final String ATT_OCP_STATUS_XFER3 = "xfer3";
  public static final String[] OCP_STATUS_CHOICES =
  {
    ATT_OCP_STATUS_ALLOW,
    ATT_OCP_STATUS_BLOCK,
    ATT_OCP_STATUS_AUTHCODE,
    ATT_OCP_STATUS_XFER1,
    ATT_OCP_STATUS_XFER2,
    ATT_OCP_STATUS_XFER3
  };
  public static final String OCP_TRANSFER_NUMBER_NESTED = "ocpTransferNumber";
  public static final String DEFAULT_CALLING_PLAN_ENTRY_ORIG_NESTED = "defaultCallingPlanEntryOrig";
  public static final String CALLING_PLAN_ENTRY_ORIG_NESTED = "callingPlanEntryOrig";
  public static final String CALL_TYPE_OCP_NESTED = "callTypeOcp";

  // Call Center Stats Reporting Support.
  protected static final String DAILY_REPORT_NESTED = "dailyReport";
  protected static final String REPORTING_PERIOD_T = "reportingPeriod";
  protected static final String REPORT_EMAIL_1 = "reportEmail1";
  protected static final String REPORT_EMAIL_2 = "reportEmail2";

  // Emergency Zones support
  public static final String REJECT_ALL_CALLS_T = "rejectAllCalls";
  public static final String HOME_ZONES_LIST_T = "homeZoneList";
  public static final String HOME_ZONE_ENTRY_T = "homeZoneEntry";
  public static final String IP_ADDRESS_RANGE_T = "ipAddressRange";
  public static final String IP_ADDRESS_START_T = "startIPAddress";
  public static final String IP_ADDRESS_END_T = "endIPAddress";
  public static final String SEND_EMERGENCY_EMAIL_T = "sendEmergencyEmail";

  // zone entry type
  public static final String ATT_TYPE_IPADDRESS = "IP Address";
  public static final String ATT_TYPE_IPADDRESS_RANGE  = "IP Address Range";
  public static final String[] ZONE_ENTRY_TYPE_CHOICES =
  {
    ATT_TYPE_IPADDRESS,
    ATT_TYPE_IPADDRESS_RANGE,
  };

  // Music on Hold source information
  public static final String MUSIC_ON_HOLD_SYSTEM = "system";
  public static final String MUSIC_ON_HOLD_CUSTOM = "custom";
  public static final String MUSIC_ON_HOLD_EXTERNAL = "external";

  public static final String[] MUSIC_ON_HOLD_SOURCE_CHOICES =
  {
    MUSIC_ON_HOLD_SYSTEM,
    MUSIC_ON_HOLD_EXTERNAL,
    MUSIC_ON_HOLD_CUSTOM,
  };


  // User's category
   public static final String CPC_CATEGORY = "callingPartyCategory";
   public static final String ATT_CPC_CATEGORY = "category";
   public static final String ATT_CPC_ORDINARY = "Ordinary";
   public static final String ATT_CPC_HOTEL = "Hotel";
   public static final String ATT_CPC_HOSPITAL = "Hospital";
   public static final String ATT_CPC_PRISON = "Prison";
   public static final String ATT_CPC_PAYPHONE = "Payphone";
   public static final String ATT_CPC_SPECIAL = "Special";
   public static final String[] CPC_CATEGORY_CHOICES =
   {
     ATT_CPC_ORDINARY,
     ATT_CPC_PAYPHONE,
     ATT_CPC_PRISON,
     ATT_CPC_HOTEL,
     ATT_CPC_HOSPITAL,
     ATT_CPC_SPECIAL
  };

  //outlook integration
  public static final String OUTLOOK_INT_ENABLE_EXCHANGE = "enableExchange";

}
