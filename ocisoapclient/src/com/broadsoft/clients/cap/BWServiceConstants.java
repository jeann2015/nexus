package com.broadsoft.clients.cap;

public class BWServiceConstants {
  /**
   * constants
   */
  public static String NO_OCS_CONNECTION = "NO_OCS_CONNECTION";
  public static String ACCESS_DENIED = "ACCESS_DENIED";
  public static String LOGIN_FAILED = "LOGIN_FAILED";
  public static String REQUEST_TIMEOUT = "REQUEST_TIMEOUT";
  public static String INVALID_MESSAGE = "INVALID_MESSAGE";
  public static String INVALID_REQUEST = "INVALID_REQUEST";
  public static String INVALID_USER = "INVALID_USER";
  public static String UNKNOWN_ERROR = "UNKNOWN_ERROR";


  public static String CONFIG_FILE = "configFile";
  public static String CONFIG_FILE_NAME = "serviceconfig.xml";
  public static String EXECUTION_SERVICE_NAME = "ExecutionService";
  public static String PROVISIONING_SERVICE_NAME = "ProvisioningService";


  //OSS message constants
  /***************************Message Types**********************************************/
  static String OSS_MSG_REQUESTAUTHENTICATION		= "requestAuthentication";
  static String OSS_MSG_REQUESTLOGIN			= "requestLogin";
  static String OSS_MSG_REQUESTLOGOUT			= "requestLogout";

  static String OSS_MSG_TAG_CLIENTINFO			= "clientInfo";
  static String OSS_MSG_TAG_CLIENTDATA			= "clientData";

  /*********************** Command ***********************/
  static String OSS_MSG_TAG_COMMAND              = "command";
  static String OSS_MSG_ATTR_COMMAND_TYPE        = "commandType";
  static String OSS_MSG_TAG_COMMAND_DATA         = "commandData";

  /*********************** User ***********************/
  static String OSS_MSG_TAG_USER                 = "user";
  static String OSS_MSG_ATTR_USER_UID            = "userUid";
  static String OSS_MSG_ATTR_USER_TYPE           = "userType";
  static String OSS_MSG_ATTR_APPLICATION_ID      = "applicationId";
  static String OSS_MSG_ATTR_USER_LOGOUT_REASON  = "userLogoutReason";
  static String OSS_VAL_USER_LOGOUT_REASON_CL    = "ClientLogout";
  static String OSS_MSG_TAG_LOGIN_INFO               = "loginInfo";
  static String OSS_MSG_TAG_LOGIN_ID                   = "loginId";
  static String OSS_MSG_TAOSS_MSG_TAG_LDAP_SEARCH_KEY	= "ldapSearchKey";

  static String RESPONSE_ID	= "responseId";
  static String ERROR	= "error";

  //Executions server (CAP) messages
  static String MSG_TYPE_REGISTER_AUTH       = "registerAuthentication";
  static String MSG_TYPE_RESPONSE_AUTH       = "responseAuthentication";
  static String MSG_TYPE_REGISTER_REQUEST    = "registerRequest";
  static String MSG_TYPE_REGISTER_RESPONSE   = "registerResponse";
  static String MSG_TYPE_UNREGISTER          = "unRegister";
  static String MSG_TYPE_SESSION_UPDATE      = "sessionUpdate";
  static String MSG_TYPE_PROFILE_UPDATE      = "profileUpdate";
  static String MSG_TYPE_CALL_UPDATE         = "callUpdate";
  static String MSG_TYPE_CALL_ACTION         = "callAction";
  static String MSG_TYPE_CALL_CONTROL_INFO   = "callControlInfo";
  static String MSG_TYPE_ACKNOWLEDGEMENT     = "acknowledgement";
  static String MSG_TYPE_INFO_REQUEST        = "infoRequest";
  static String MSG_TYPE_INFO_RESPONSE       = "infoResponse";
  static String MSG_TYPE_SERVER_STATUS_REQUEST = "serverStatusRequest";
  static String MSG_TAG_MSG_TAG_COMMAND_DATA         = "commandData";
  static String MSG_TAG_USER                 = "user";
  static String MSG_ATTR_USER_UID            = "userUid";
  static String MSG_ATTR_USER_TYPE           = "userType";
  static String MSG_TAG_ID                   = "id";
  static String MSG_TAG_PASSWORD             = "password";
  static String MSG_TAG_SECURE_PASSWORD      = "securePassword";
  static String MSG_TAG_MESSAGE              = "message";
  static String MSG_ATTR_MESSAGE_NAME        = "messageName";
  static String MSG_TAG_FAILURE              = "failure";
  static String MSG_ATTR_FAILURE_CAUSE       = "failureCause";

  public BWServiceConstants() {
  }
}
