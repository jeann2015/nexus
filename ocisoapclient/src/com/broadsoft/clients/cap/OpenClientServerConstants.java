/**
 * Title:        Broadworks
 * Description:
 * Copyright:    Copyright (c) 1999-2002, Broadsoft Inc.
 * Company:      BroadSoft
 * @author       BroadSoft Engineering
 * @version
 */
package com.broadsoft.clients.cap;

public interface OpenClientServerConstants
{

  public static final String XML_ENCODING                   = "UTF-8";

  //*************************
  // CAP constants
  //*************************
  public static final int PRIMARY_SERVER                    = 0;
  public static final int REDUNDANT_SERVER                  = 1;
  public static final int CLIENT_CONNECTION                 = 0;
  public static final int SERVER_CONNECTION                 = 1;
  public static final String CLIENT_CONN_ID_PREFIX          = "ClientTCPConnection";
  public static final String PRIM_SERVER_CONN_ID_PREFIX     = "PrimaryServerCAPConnection";
  public static final String REDUN_SERVER_CONN_ID_PREFIX    = "RedundantServerCAPConnection";
  public static final String AUTH_FAIL_UNKNOWN_ID           = "UnknownID";
  public static final String AUTH_FAIL_NS_CONNECTIVITY      = "NetworkServerConnectivityFailure";
  public static final String AUTH_FAIL_EXT_AUTH_ERROR       = "ExternalAuthenticationError";

  //*************************
  // OSS constants
  //*************************
  public static final String OSS_HOST_ENV_NAME              = "org.omg.CORBA.ORBInitialHost";
  public static final String OSS_PORT_ENV_NAME              = "org.omg.CORBA.ORBInitialPort";

  //*************************
  // NSOSS
  //*************************
  public static final String NSOSS_PROT_NAME = "NSOSS";
  public static final String NSOSS_VERSION_NAME = "9.0";

  public static final String XML_TAG_BROADSOFT_DOCUMENT_NS =
      "com.broadsoft.protocols.nsoss.BroadsoftDocument";
  public static final String XML_TAG_CMD_ARRAY_NS = "commandArray";
  public static final String XML_TAG_COMMAND_NS =
      "com.broadsoft.protocols.nsoss.Command";
  public static final String TAG_BROADSOFT_DOCUMENT_END_NS =
      "</com.broadsoft.protocols.nsoss.BroadsoftDocument>";

  // OSS types that we support.
  public static final int ASOSS = 0;
  public static final int NSOSS = 1;
  public static final int NUM_OSS_TYPES = 2;
}